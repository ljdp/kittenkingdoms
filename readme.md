# Kitten Kingdoms
Kitten Kingdoms was an browser based strategy game about a feudal feline society.
Initially released in September 2017. The game is now offline, but this repository contains all
the code that was used for both server-side logic and client-side logic.

An archive of the Kitten Kingdoms landing and trailer page can be found at [www.locogame.co.uk/archive/kittenkingdoms](http://locogame.co.uk/archive/kittenkingdoms)  
as well as various update pages:
[Treasure Update](http://locogame.co.uk/archive/kittenkingdoms/treasureupdate.html)
[Potions and Books Update](http://locogame.co.uk/archive/kittenkingdoms/potionsandbooksupdate.html)

The server-side code exists in the `./lib` directory and the client-side code exists inside the `./client_src` directory. The bulk of the game logic exists in `./lib/games/systems/v1.2.x/game_logic.js`. The overall architecture is mostly functional. Each match's game state is contained within a single object and functions mutate the state. The state is then diffed against each client's state
and a patch is sent to the clients.

You can see how the payment subscription logic was implement in `./lib/subscriptions/`. I used **Stripe** to handle credit card payments.

## Gameplay
Kitten Kingdoms is a turn based browser game, played over the course of 2 or 3 weeks. Although turns happen each hour, you decide when and how frequently you log in to play.

Every player starts with a kitten which has two desires. You score points by satisfying those desires and expanding your empire. Such desires might be noble like marrying a soulmate, having a family and creating harmony. Some may desire to be wealthy, or to possess certain items. Other desires are more nefarious like warfare, sabotage and murder. When a cat dies its heir inherits the land, money, items and blueprints from its successor. Beware, if your cat dies with no heir you lose the game!

Turns happen on an hourly basis, during which the counties you control will generate income, troops and science. By default, every 24 turns you collect income from your counties and vassals.

[See here to read the full tutorial](http://locogame.co.uk/archive/kittenkingdoms/tutorial.html)