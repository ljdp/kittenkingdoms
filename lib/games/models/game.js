const assert = require("assert");
const moment = require('moment');

module.exports = class Game {
    constructor({
            _id,
            version,
            name, 
            owner, 
            nextTurnTime,
            nextIncomeTurnTime,
            gameOverTime,
            status = 'active',
            config, 
            state}) 
    {
        this.id = _id;
        this.version = version;
        this.name = name;
        this.owner = owner;
        this.nextTurnTime = nextTurnTime;
        this.nextIncomeTurnTime = nextIncomeTurnTime;
        this.gameOverTime = gameOverTime;
        this.status = status;
        this.config = config;
        this.state = state;
    }

    save( db ) {
        if (this.id) {
            return db.updateOne({_id: this.id}, this).then(result => {
                return this;
            });
        } else {
            return db.insert(this).then(doc => {
                this.id = doc._id;
                return this;
            });
        }
    }

    updateState( db, state ) {
        return db.updateOne({_id: this.id}, {$set: {"state": state}});
    }

    updateNextTurnTime( db, nextTurnTime, nextIncomeTurnTime ) {
        assert(this.id, "Cannot update a game that hasn't been created yet!");
        this.nextTurnTime = nextTurnTime;
        this.nextIncomeTurnTime = nextIncomeTurnTime;
        return db.updateOne({_id: this.id}, {$set: {"nextTurnTime": nextTurnTime, "nextIncomeTurnTime": nextIncomeTurnTime}});
    }

    turn() {
        return this.state.map.turn;
    }

    isIncomeTurn() {
        return (this.turn() % this.config.incomeTurnInterval) == 0;
    }

    hasPlayer( username ) {
        return this.state.players.findIndex(player => player.username == username) != -1;
    }

    basicData() {
        return {
            id: this.id,
            version: this.version,
            name: this.name,
            owner: this.owner,
            config: this.config,
            turn: this.turn(),
            players: this.state.players.map(player => player.username),
            nextTurnTime: this.nextTurnTime,
            nextIncomeTurnTime: this.nextIncomeTurnTime,
            status: this.status
        }
    }
}