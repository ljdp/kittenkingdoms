const DB = require('../../db');
const Games = require('../systems/games');
const assert = require('assert');
const moment = require('moment');
const sinon = require('sinon');

const gamesConfig = {
    TURN_INTERVAL: {
        value: 60,
        unit: 'seconds'
    },
    GRACE_PERIOD: {
        value: 60,
        unit: 'seconds'
    },
    NEW_GAME_VERSION: 'v1.2.x',
    GAME_VERSIONS: ['v1.1.x', 'v1.2.x'],
    MAX_USER_CREATED_GAMES: {
        basic: 0,
        ninja: 10,
        wizard: 15,
        admin: 100
    },
    MAX_USER_JOINED_GAMES: {
        basic: 2,
        ninja: 6,
        wizard: 12,
        admin: 100
    }
};

const validGameOptions = {
    name: 'Test Game',
    owner: 'xxxx',
    role: 'admin',
    config: {
        maxPlayers: 1,
        maxTurns: 2
    }
};

const validGameOptionsMultiPlayers = {
    name: 'Test Game 2',
    owner: 'xxxy',
    config: {
        maxPlayers: 10,
        maxTurns: 2
    }
};

describe('Games:', () => {

    describe('Creating a game as a basic user', () => {
        var gameDoc, createError;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            let opts = Object.assign({}, validGameOptions);
            opts.role = 'basic';
            games.create(opts).then(doc => {
                gameDoc = doc;
                done();
            }).catch(err => {
                createError = err;
                done();
            });
        })
        it('returns an error', () => {
            assert(createError);
            assert(createError.includes("game creation limit"));
        });
    })

    describe('Creating a game as an admin', () => {
        var gameDoc, games, db;
        before((done) => {
            db = new DB({inMemory: true});
            games = new Games({db, config: gamesConfig});
            games.create(validGameOptions).then(doc => {
                gameDoc = doc;
                done();
            }).catch(err => {
                createError = err;
                done();
            });
        });
        it('exists in the database', () => {
            return db.findOne({name: 'Test Game'}).then(doc => {
                assert(doc);
            });
        })
        it('is on turn 0', () => {
            assert.equal(gameDoc.turn(), 0);
        })
        it('has nextTurnTime in 12 hours', () => {
            assert(moment().add(12, "hours").isSameOrAfter(gameDoc.nextTurnTime));
        })
    });

    describe('Joining a game as a basic user', () => {
        var gameDoc;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            games.create(validGameOptions, 'admin').then(_gameDoc => {
                gameDoc = _gameDoc;
                return games.joinGame({gameId: gameDoc.id, userid: 'xxxx', username: 'foobar', role: 'basic'});
            }).then(_ => {
                done();
            }).catch(assert.fail);
        });
        it('adds the user to the game', () => {
            assert.equal(gameDoc.state.players.length, 1);
            assert.equal(gameDoc.state.players[0].username, 'foobar');
            assert.equal(gameDoc.state.players[0].userid, 'xxxx');
        });
    });

    describe('Joining 3 games as a basic user', () => {
        var gameDocs, joinError, games;
        before((done) => {
            let db = new DB({inMemory: true});
            games = new Games({db, config: gamesConfig});
            let gameCreatePromises = []
            for (let i = 0; i < 3; i++) {
                gameCreatePromises.push(games.create(validGameOptions, 'admin'));
            }
            Promise.all(gameCreatePromises).then(_gameDocs => {
                gameDocs = _gameDocs;
                let gameJoinPromises = []
                for (let i = 0; i < 3; i++) {
                    gameJoinPromises.push(games.joinGame({
                        gameId: gameDocs[i].id, 
                        userid: 'xxxx', 
                        username: 'foobar', 
                        role: 'basic'}));
                }
                return Promise.all(gameJoinPromises);
            }).then(result => {
                done();
            }).catch(err => {
                joinError = err;
                done();
            })
        });
        it('returns an error message', () => {
            assert(joinError);
            assert(joinError.includes("cannot join"), joinError);
        });
        it('adds the user to the 1st game', () => {
            assert.equal(gameDocs[0].state.players.length, 1);
        })
        it('adds the user to the 2nd game', () => {
            assert.equal(gameDocs[1].state.players.length, 1);
        })
        it('does not add the user to the 3rd game', () => {
            assert.equal(gameDocs[2].state.players.length, 0);
        });

        describe('leaving game 2 then joining game 3', () => {
            var joinError;
            before((done) => {
                games.leaveGame({gameId: gameDocs[1].id, userid: 'xxxx'}).then(_ => {
                    return games.joinGame({gameId: gameDocs[2].id, userid: 'xxxx', username: 'foobar', role: 'basic'});
                }).then(_ => {
                    done();
                }).catch(err => {
                    joinError = err;
                    done();
                });
            });
            it('removes the user from game 2', () => {
                assert.equal(gameDocs[1].state.players.length, 0);
            });
            it('adds the user to game 3', () => {
                assert.equal(gameDocs[2].state.players.length, 1);
            });
            it('does not return an error message', () => {
                assert(!joinError);
            })
        });
    });

    describe('Joining 7 games as a ninja user', () => {
        var gameDocs, joinError;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            let gameCreatePromises = []
            for (let i = 0; i < 7; i++) {
                gameCreatePromises.push(games.create(validGameOptions, 'admin'));
            }
            Promise.all(gameCreatePromises).then(_gameDocs => {
                gameDocs = _gameDocs;
                let gameJoinPromises = []
                for (let i = 0; i < 7; i++) {
                    gameJoinPromises.push(games.joinGame({
                        gameId: gameDocs[i].id, 
                        userid: 'xxxx', 
                        username: 'foobar', 
                        role: 'ninja'}));
                }
                return Promise.all(gameJoinPromises);
            }).then(result => {
                done();
            }).catch(err => {
                joinError = err;
                done();
            })
        });
        it('returns an error message', () => {
            assert(joinError);
            assert(joinError.includes("cannot join"), joinError);
        });
        it('adds the user to the first 6 games', () => {
            for (let i = 0; i < 6; i++) {
                assert.equal(gameDocs[i].state.players.length, 1);
            }
        })
        it('does not add the user to the 7th game', () => {
            assert.equal(gameDocs[6].state.players.length, 0);
        });
    });

    describe('Joining 13 games as a wizard user', () => {
        var gameDocs, joinError;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            let gameCreatePromises = []
            for (let i = 0; i < 13; i++) {
                gameCreatePromises.push(games.create(validGameOptions, 'admin'));
            }
            Promise.all(gameCreatePromises).then(_gameDocs => {
                gameDocs = _gameDocs;
                let gameJoinPromises = []
                for (let i = 0; i < 13; i++) {
                    gameJoinPromises.push(games.joinGame({
                        gameId: gameDocs[i].id, 
                        userid: 'xxxx', 
                        username: 'foobar', 
                        role: 'wizard'}));
                }
                return Promise.all(gameJoinPromises);
            }).then(result => {
                done();
            }).catch(err => {
                joinError = err;
                done();
            })
        });
        it('returns an error message', () => {
            assert(joinError);
            assert(joinError.includes("cannot join"), joinError);
        });
        it('adds the user to the first 12 games', () => {
            for (let i = 0; i < 12; i++) {
                assert.equal(gameDocs[i].state.players.length, 1);
            }
        })
        it('does not add the user to the 13th game', () => {
            assert.equal(gameDocs[12].state.players.length, 0);
        });
    });

    describe('Joining a full game', () => {
        var joinError, gameDoc;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            let opts = Object.assign({}, validGameOptions);
            opts.config.maxPlayers = 0;
            games.create(opts).then(_gameDoc => {
                gameDoc = _gameDoc;
                return games.joinGame({gameId: gameDoc.id, userid: 'xxxy', username: 'fizzbuzz'});
            }).then(_ => {
                done();
            }).catch(err => {
                joinError = err;
                done();
            });
        })
        it('returns an error message', () => {
            assert(joinError.includes("full"));
        })
        it('does not add the user to the game', () => {
            assert.equal(gameDoc.state.players.findIndex(player => player.username == 'fizzbuzz'), -1);
        })
    })

    describe('Joining a finished game', () => {
        var joinError;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            let opt = Object.assign({role: 'admin'}, validGameOptionsMultiPlayers);
            games.create(opt).then(_gameDoc => {
                gameDoc = _gameDoc;
                games.setGameOver(gameDoc);
                return games.joinGame({gameId: gameDoc.id, userid: 'xxxy', username: 'fizzbuzz'});
            }).then(_ => {
                done();
            }).catch(err => {
                joinError = err;
                done();
            });
        })
        it('returns an error message', () => {
            assert(joinError.includes("has finished"), joinError);
        })
        it('does not add the user to the game', () => {
            assert.equal(gameDoc.state.players.findIndex(player => player.username == 'fizzbuzz'), -1);
        })
    })

    describe('leaving a game', () => {
        var gameDoc;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            let opts = Object.assign({}, validGameOptions);
            opts.config.maxPlayers = 2;
            games.create(opts).then(_gameDoc => {
                gameDoc = _gameDoc;
                return games.joinGame({gameId: gameDoc.id, userid: 'xxx1', username: 'foobar'});
            }).then(doc => {
                return games.leaveGame({gameId: gameDoc.id, userid: 'xxx1'});
            }).then(_ => {
                done();
            }).catch(assert.fail);
        })
        it('removes the player from the game', () => {
            assert.equal(gameDoc.state.players.length, 0);
        })
    })

    describe('Leaving the wrong game', () => {
        var leaveError, beforePlayers, gameDocs;
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            let opts = Object.assign({}, validGameOptions);
            opts.config.maxPlayers = 2;

            Promise.all([games.create(opts), games.create(opts)]).then(result => {
                gameDocs = result;
                return games.joinGame({gameId: gameDocs[0].id, userid: 'xxxx', username: 'foobar'});
            }).then(_ => {
                beforePlayers = Object.assign({}, gameDocs[1].state.players);
                return games.leaveGame({gameId: gameDocs[1].id, userid: 'xxxx'});
            }).catch(err => {
                leaveError = err;
                done();
            })
        })
        it('returns an error message', () => {
            assert(leaveError);
            assert(leaveError.includes('not in game'));
        })
        it('does not remove any player', () => {
            assert.deepEqual(gameDoc.state.players, beforePlayers);
        });
    });

    describe('Updating the game soon after creation', () => {
        before((done) => {
            let db = new DB({inMemory: true});
            let games = new Games({db, config: gamesConfig});
            games.create(validGameOptions).then(gameDoc => {
                games.updateAll();
                done();
            })
        });
        it('does not increment the turn number', () => {
            assert.equal(gameDoc.turn(), 0);
        });
    });

});