const assert = require('assert');
const Game = require('../systems/v1.2.x/game_logic');
const Logger = require('winston');

const validGameConfig = {
    maxPlayers: 3,
    maxTurns: 100
}