const assert = require('assert');
const Game = require('../systems/v1.2.x/game_logic');
const winston = require('winston');
const sinon = require('sinon');

let game, player1, player2, bob, alice, chanceStub, 
    getItemChanceEffect, applyItemActionSpy, activateItemSpy, killCharacterSpy;

const SUCCESS_PUBLIC = 1;
const SUCCESS_SILENT = 2;
const FAIL_PUBLIC = 3;
const FAIL_SILENT = 4;

function gameSuite( tests ) {
    return {
        '...': tests,
        before: function() {
            game = new Game({}, null, winston);
            player1 = game.addPlayer('a', 'player1');
            player2 = game.addPlayer('b', 'player2');
            bob = game.getPlayerCharacters('player1')[0];
            alice = game.getPlayerCharacters('player2')[0];
            bob.name = 'bob';
            bob.age = 50;
            alice.name = 'alice';
            alice.age = 50;
            bob.location = alice.location;
            chanceStub = sinon.stub(game.chance, 'weighted');
            chanceStub.callsFake((values, _) => {
                return values[0]; // return success.
            });
            getItemChanceEffect = sinon.stub(game, 'getItemChanceEffect');
            applyItemActionSpy = sinon.spy(game, 'applyItemAction');
            activateItemSpy = sinon.spy(game, 'activateItem');
            killCharacterSpy = sinon.spy(game, 'killCharacter');
        }
    }
}

module.exports = {
'[Poison Plot]': {
    'When bob succesfully plots to poison alice with cyanide lotion.': {
        'Case 1: the potion has a succesful outcome.': gameSuite({
            before: function() {
                this.poison = game.giveCharacterItemOfId(bob.guid, 'poison_1');
                getItemChanceEffect.callsFake((item) => {
                    return item.chanceEffect[0];
                });
                this.plotResult = game.performPlot(player1, bob.guid, alice.guid, 'poison', this.poison.guid);
            },
            'Plot was succesful.': function() {
                assert.equal(this.plotResult, SUCCESS_PUBLIC);
            },
            'activateItem was called.': function() {
                assert.equal(activateItemSpy.callCount, 1, 'activeItem was not called once');
                assert.equal(activateItemSpy.getCall(0).args[1], this.poison.guid, 'activated item was not poison');
            },
            'applyItemAction was called.': function() {
                assert.equal(applyItemActionSpy.callCount, 1);
                let call = applyItemActionSpy.getCall(0);
                assert.equal(call.args[0], alice.guid, 'applyItemAction was not applied to alice');
                assert.equal(call.args[2], 'kill', 'applyItemAction did not receive kill action');
            },
            'killCharacter was called': function() {
                assert.equal(killCharacterSpy.callCount, 1, 'killCharacter was not called once');
                assert.equal(killCharacterSpy.getCall(0).args[0], alice.guid, 'killCharacter was not called with alice guid');
            },
            'Alice is killed.': function() {
                assert.equal(alice.alive, false);
            },
            'Alice is 50 turns old.': function() {
                assert.equal(alice.age, 50);
            },
            'Bob is alive.': function() {
                assert.equal(bob.alive, true);
            },
            'Bob is 50 turns old.': function() {
                assert.equal(alice.age, 50);
            }
        }),
        'Case 2: the potion has an unsuccesful outcome.': gameSuite({
            before: function() {
                this.poison = game.giveCharacterItemOfId(bob.guid, 'poison_1');
                getItemChanceEffect.callsFake((item) => {
                    return item.chanceEffect[1];
                });
                this.plotResult = game.performPlot(player1, bob.guid, alice.guid, 'poison', this.poison.guid);
            },
            'Plot was succesful.': function() {
                assert.equal(this.plotResult, SUCCESS_PUBLIC);
            },
            'Poison belongs to victim.': function() {
                assert.equal(this.poison.owningCharacter, alice.guid);
            },
            'killCharacter is not called.': function() {
                assert.equal(killCharacterSpy.callCount, 0);
            },
            'Alice is still alive.': function() {
                assert.equal(alice.alive, true);
            },
            'applyItemAction was called.': function() {
                assert.equal(applyItemActionSpy.callCount, 1, 'applyItemAction was not called once');
                let call = applyItemActionSpy.getCall(0);
                assert.equal(call.args[0], alice.guid);
                assert.equal(call.args[2], 'changeAge');
                assert.equal(call.args[3], -40);
            },
            'Alice is 40 turns younger.': function() {
                assert.equal(alice.age, 10);
            },
            'Bob is alive.': function() {
                assert.equal(bob.alive, true);
            },
            'Bob is 50 turns old.': function() {
                assert.equal(bob.age, 50);
            }
        })
    },
    'When bob unsuccesfully plots to poison alice': gameSuite({
        before: function() {
            chanceStub.callsFake((values, _) => {
                return values[2]; // return failure.
            });
            this.poison = game.giveCharacterItemOfId(bob.guid, 'poison_1');
            this.plotResult = game.performPlot(player1, bob.guid, alice.guid, 'poison', this.poison.guid);
        },
        'The plot failed.': function() {
            assert.equal(this.plotResult, FAIL_PUBLIC);
        },
        'activeItem was not called.': function() {
            assert.equal(activateItemSpy.callCount, 0);
        },
        'The potion is destroyed.': function() {
            assert.equal(game.getItem(this.poison.guid), null);
        },
        'Alice is still alive.': function() {
            assert.equal(alice.alive, true);
        },
        'Alive is still 50 turns old.': function() {
            assert.equal(alice.age, 50);
        }
    })
}
};