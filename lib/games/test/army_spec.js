const assert = require('assert');
const Game = require('../systems/v1.2.x/game_logic');
const winston = require('winston');
const sinon = require('sinon');

let game, player, otherPlayer, character, otherCharacter, county, army;

module.exports = {
    '[Army]': {
    before: function() {
        game = new Game({}, null, winston);
        player = game.addPlayer('xxxx', 'foobar');
        character = game.getCharacter(player.characters[0]);
        character.owningPlayer = player.username;
        otherPlayer = game.addPlayer('xxxy', 'baz');
        otherCharacter = game.getCharacter(otherPlayer.characters[0]);
        otherCharacter.owningPlayer = otherPlayer.username;
        sinon.spy(game, 'performBattle');
    },
    'A county with 100 troops.': {
        before: function() {
            county = game.state.counties.find(c => !c.impassable);
            county.troops = 100;
            county.owningCharacter = player.characters[0];
        },
        after: function() {
            game.state.armies = [];
            county.troops = 0;
        },
        'When you raise 100 troops.': {
            before: function() {
                army = game.makeArmy(player, county.guid, 100);
            },
            'An army with 100 troops is created': function() {
                assert.equal(game.state.armies.length, 1);
                assert.equal(army.troops, 100);
            },
            'The army is stored in the games state object': function() {
                assert.equal(army.guid, game.state.armies[0].guid);
            },
            'The county has 0 unraised troops': function() {
                assert.equal(county.troops, 0);
            },
            'The army belongs to the character that raised it': function() {
                assert.equal(army.owningCharacter, player.characters[0]);
            },
            'The army has a 1 turn cooldown': function() {
                assert.equal(army.cooldown, 1);
            }
        }
    },
    'A county with 100 troops and an army with 50 troops.': {
        before: function() {
            county = game.state.counties.find(c => !c.impassable);
            county.troops = 50;
            county.owningCharacter = player.characters[0];
            game.makeArmy(player, county.guid, 50);
            county.troops = 100;
        },
        after: function() {
            game.state.armies = [];
            county.troops = 0;
        },
        'When you raise 50 troops.': {
            before: function() {
                game.makeArmy(player, county.guid, 50);
            },
            'The county now has an army with 100 troops.': function() {
                let armies = game.state.armies.filter(a => a.location === county.guid);
                assert.equal(armies.length, 1);
                assert.equal(armies[0].troops, 100);
            },
            'The county now has 50 unraised troops.': function() {
                assert.equal(county.troops, 50);
            }
        }
    },
    'Two adjacent armies with 50 troops each.': {
        before: function() {
            this.otherCounty = county.adjacent.map(id => game.getCounty(id)).find(c => !c.impassable && c.guid !== county.guid);
            this.otherCounty.owningCharacter = character.guid;
            county.troops = 50;
            this.otherCounty.troops = 50;
            this.army1 = game.makeArmy(player, county.guid, 50);
            this.army2 = game.makeArmy(player, this.otherCounty.guid, 50);
        },
        after: function() {
            game.state.armies = [];
        },
        'When one army moves to the location of the other.': {
            before: function() {
                game.performGameTick(); // wait for army cooldown.
                game.moveArmy(this.army2.guid, county.guid);
            },
            'They combine to one army with 100 troops.': function() {
                assert.equal(game.state.armies.length, 1);
                assert.equal(game.state.armies[0].troops, 100);
            }
        }
    },
    'Two adjacent enemy armies with 50 troops each, in counties with no characters.': {
        before: function() {
            this.otherCounty = county.adjacent.map(id => game.getCounty(id)).find(c => c.guid !== county.guid);
            this.otherCounty.owningCharacter = otherCharacter.guid;
            this.otherCounty.troops = 50;
            county.troops = 50;
            county.owningCharacter = character.guid;
            this.army1 = game.makeArmy(null, county.guid, 50);
            this.army2 = game.makeArmy(null, this.otherCounty.guid, 50);
        },
        after: function() {
            game.state.armies = [];
        },
        'When one attacks.': {
            before: function() {
                game.performGameTick();
                game.moveArmy(this.army2.guid, this.army1.location);
            },
            'Both armies will be destroyed, the owner of the county will remain the same.': function() {
                assert.equal(game.state.armies.length, 0);
                assert.equal(county.owningCharacter, character.guid);
            }
        }
    },
    'Two adjacent enemy armies, one with 51 troops, the other with 50, in counties with no characters.': {
        before: function() {
            this.otherCounty = county.adjacent.map(id => game.getCounty(id)).find(c => c.guid !== county.guid);
            this.otherCounty.owningCharacter = otherCharacter.guid;
            this.otherCounty.troops = 100;
            county.troops = 100;
            this.army50 = game.makeArmy(null, county.guid, 50);
            this.army51 = game.makeArmy(null, this.otherCounty.guid, 51);
            game.performBattle.reset();
            sinon.spy(game, 'removeArmy');
        },
        after: function() {
            game.state.armies = [];
            game.performBattle.reset();
        },
        'When the 51 troop army attacks.': {
            before: function() {
                game.performGameTick();
                county.troops = 0;
                this.otherCounty.troops = 0;
                game.moveArmy(this.army51.guid, this.army50.location);
                this.battle = game.performBattle.returnValues[0][0];
            },
            'performBattle method is called.': function() {
                assert(game.performBattle.calledOnce);
            },
            'Only one battle was had.': function() {
                assert(game.performBattle.returnValues[0].length, 1);
            },
            'The battle was army to army.': function() {
                assert.equal(this.battle.type, 'army');
            },
            'The battle attacking troops is 51.': function() {
                assert.equal(this.battle.attackingTroops, 51);
            },
            'The battle defending troops is 50.': function() {
                assert.equal(this.battle.defendingTroops, 50);
            },
            'The battle attack bonus is 1.0x.': function() {
                assert.equal(this.battle.attackingBonusMultiplier, 1);
            },
            'The battle defenses bonus is 1.0x.': function() {
                assert.equal(this.battle.defendingBonusMultiplier, 1);
            },
            'The 51 troop will survive with 1 troop.': function() {
                assert.equal(game.removeArmy.callCount, 1, 'removeArmy called more than once');
                assert.equal(game.state.armies.length, 1, 'incorrect number of armies');
                assert.equal(game.state.armies[0].guid, this.army51.guid, 'incorrect surviving army');
                assert.equal(game.state.armies[0].troops, 1, 'incorrect number of troops');
            },
            'The 50 troop army will be destroyed.': function() {
                assert.equal(game.state.armies.length, 1);
                assert.notEqual(game.state.armies[0].guid, this.army50.guid);
            },
            'The county will belong to the attacking character.': function() {
                assert.equal(county.owningCharacter, otherCharacter.guid);
            }
        }
    },
    'An army with 51 troops attacking a neutral county with 100 troops.': {
        before: function() {
            county.troops = 100;
            county.owningCharacter = character.guid;
            this.army = game.makeArmy(player, county.guid, 51);
            this.neutralAdjacent = county.adjacent.find(adjId => !game.getCounty(adjId).owningCharacter);
            this.neutralAdjacent = game.getCounty(this.neutralAdjacent);
            game.performBattle.reset();
            game.performGameTick();
            this.neutralAdjacent.troops = 100;
            game.moveArmy(this.army.guid, this.neutralAdjacent.guid);
            this.battle = game.performBattle.returnValues[0][0];
        },
        after: function() {
            this.neutralAdjacent.owningCharacter = null;
            game.state.armies = [];
            game.state.map.turn = 1;
            game.performBattle.reset();
        },
        'performBattle method is called.': function() {
            assert(game.performBattle.calledOnce);
        },
        'The battle was army to county.': function() {
            assert.equal(this.battle.type, 'county');
        },
        'The battle attack bonus is 1.0x.': function() {
            assert.equal(this.battle.attackingBonusMultiplier, 1);
        },
        'The battle defenses bonus is 0.5x.': function() {
            assert.equal(this.battle.defendingBonusMultiplier, 0.5);
        },
        'The battle attacking troops is 51.': function() {
            assert.equal(this.battle.attackingTroops, 51);
        },
        'The battle defending troops is 100.': function() {
            assert.equal(this.battle.defendingTroops, 100);
        },
        'The battle attacking power is 51.': function() {
            assert.equal(this.battle.attackPower, 51);
        },
        'The battle defending power is 50.': function() {
            assert.equal(this.battle.defendPower, 50);
        },
        'The neutral county now belongs to our character.': function() {
            assert.equal(this.neutralAdjacent.owningCharacter, character.guid);
        },
        'The neutral county has 49 troops (51 - 100).': function() {
            assert.equal(this.neutralAdjacent.troops, 49);
        },
        'Our army has 1 troop.': function() {
            assert.equal(this.army.troops, 1);
        }
    },
    'An army with 49 troops attacking a neutral county with 100 troops.': {
        before: function() {
            county.troops = 100;
            county.owningCharacter = character.guid;
            this.army = game.makeArmy(player, county.guid, 49);
            this.neutralAdjacent = county.adjacent.find(adjId => !game.getCounty(adjId).owningCharacter);
            this.neutralAdjacent = game.getCounty(this.neutralAdjacent);
            game.performGameTick();
            this.neutralAdjacent.troops = 100;
            game.moveArmy(this.army.guid, this.neutralAdjacent.guid);
            winston.debug(game.performBattle.callCount);
            this.battle = game.performBattle.returnValues[0][0];
        },
        'performBattle method is called.': function() {
            assert(game.performBattle.calledOnce);
        },
        'The battle was army to county.': function() {
            assert.equal(this.battle.type, 'county');
        },
        'The battle attack bonus is 1.0x.': function() {
            assert.equal(this.battle.attackingBonusMultiplier, 1);
        },
        'The battle defenses bonus is 0.5x.': function() {
            assert.equal(this.battle.defendingBonusMultiplier, 0.5);
        },
        'The battle attacking troops is 49.': function() {
            assert.equal(this.battle.attackingTroops, 49);
        },
        'The battle defending troops is 100.': function() {
            assert.equal(this.battle.defendingTroops, 100);
        },
        'The battle attacking power is 49.': function() {
            assert.equal(this.battle.attackPower, 49);
        },
        'The battle defending power is 50.': function() {
            assert.equal(this.battle.defendPower, 50);
        },
        'The neutral county stays neutral.': function() {
            assert.equal(this.neutralAdjacent.owningCharacter, null);
        },
        'The neutral county has 51 troops (100 - 49).': function() {
            assert.equal(this.neutralAdjacent.troops, 51);
        },
        'Our army is destroyed.': function() {
            assert.equal(game.state.armies.length, 0);
        }
    },
}
};