const assert = require('assert');
const Game = require('../systems/v1.2.x/game_logic');
const winston = require('winston');
const sinon = require('sinon');

let game;

module.exports = {
    '[GAME_LOGIC]: Test 200 turns with 10 bot characters': {
        before: function( done ) {
            this.succesful = null;
            game = new Game({}, null, winston);
            sinon.spy(game, "performGameTick");
            try {
                for (let i = 0; i < 200; i++) {
                    game.performGameTick();
                }
                this.succesful = true;
            } catch( err ) {
                this.succesful = false;
                winston.error(err);
            }
            done();
        },
        'Completed all 200 turns without an error': function() {
            assert.equal(this.succesful, true);
            assert.equal(game.performGameTick.callCount, 200);
            assert.equal(game.state.map.turn, 200);
        }
    },
    '[GAME_LOGIC]: Test 200 turns with 10 characters and 5 players': {
        before: function( done ) {
            let usernames = ['p1', 'p2', 'p3', 'p4', 'p5'];
            let players = [];
            game = new Game({}, null, winston);
            sinon.spy(game, "performGameTick");
            usernames.forEach(username => {
                players.push(
                    game.addPlayer(username, username)
                );
            });
            try {
                for (let i = 0; i < 200; i++) {
                    game.performGameTick();
                    players.forEach(player => game.getStateForPlayer(player));
                }
                this.succesful = true;
            } catch( err ) {
                this.succesful = false;
                winston.error(err);
            }
            done();
        },
        'Completed all 200 turns without an error': function() {
            assert.equal(this.succesful, true);
            assert.equal(game.performGameTick.callCount, 200);
            assert.equal(game.state.map.turn, 200);
        }
    }
}