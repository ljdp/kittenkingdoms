const assert = require('assert');
const Game = require('../systems/v1.2.x/game_logic');
const Logger = require('winston');
const sinon = require('sinon');
Logger.level = 'debug';

describe('[Marriage]', () => {
    var game, kitten1, kitten2;
    describe('when two kittens marry', () => {
        before(() => {
            game = new Game({}, null, Logger);
            kitten1 = game.state.characters[0];
            kitten2 = game.state.characters[1];
            game.marry(kitten1.guid, kitten2.guid);
        })
        it('Should set the spouse field on both', () => {
            assert.equal(kitten1.spouse, kitten2.guid);
            assert.equal(kitten2.spouse, kitten1.guid);
        })
        it('Gives both kittens an economy bonus', () => {
            let bonus1 = game.getGameSettingValue(kitten1.guid, 'spouseMoneyBonus');
            let bonus2 = game.getGameSettingValue(kitten1.guid, 'spouseMoneyBonus');
            assert.equal(bonus1, game.constants.spouseMoneyBonus);
            assert.equal(bonus2, game.constants.spouseMoneyBonus);
        })
        describe('When two kittens are married in the same location', () => {
            var kittenCount;
            before(() => {
                kitten2.location = kitten1.location;
                kittenCount = game.state.characters.length;
                game.chance.d4 = sinon.stub().returns(1);
                game.performGameTick();
            })
            it('Creates two new kittens', () => {
                assert.equal(game.state.characters.length, kittenCount + 2);
            })
            it('getHeir for kitten1 and kitten2 returns eldest child', () => {
                let child1 = game.state.characters[game.state.characters.length - 2];
                let child2 = game.state.characters[game.state.characters.length - 1];
                let kitten1Heir = game.getCharacterHeir(kitten1.guid);
                let kitten2Heir = game.getCharacterHeir(kitten2.guid);
                assert(kitten1Heir.guid == child1.guid && kitten2Heir.guid == child1.guid);
            })
            it('getHeir for kitten1 and 2 does not return themselves', () => {
                let kitten1Heir = game.getCharacterHeir(kitten1.guid);
                let kitten2Heir = game.getCharacterHeir(kitten2.guid);
                assert.notEqual(kitten1Heir, kitten1.guid);
                assert.notEqual(kitten1Heir, kitten2.guid);
                assert.notEqual(kitten2Heir, kitten2.guid);
                assert.notEqual(kitten2Heir, kitten1.guid);
            })
        })
    })
})