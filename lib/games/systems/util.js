exports.createMapFromGuids = function( arr ) {
    return arr.reduce((map, val) => {
        map[val.guid] = val;
        return map;
    }, {});
};

exports.mergeOptions = require('merge-options');

exports.defaultConfig = function() {
    return {
        maxTurns: 336,
        maxCounties: 200,
        maxPlayers: 16,
        maxCharactersPerPlayer: 1,
        botsEnabled: true,
        fogOfWarEnabled: true,
        incomeTurnInterval: 24,
        mapWidth: 500,
        mapHeight: 400
    };
};

exports.defaultState = function() {
    return {
        players: [], counties: [], characters: [], 
        armies: [], items: [], messages: [], news: [],
        map: {
            turn: 0,
            mapWidth: 500,
            mapHeight: 400
        }
    };
};