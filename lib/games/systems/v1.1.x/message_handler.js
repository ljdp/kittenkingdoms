module.exports = function({player, game, event, client, userInfo, auth}) {
    let pushAllPlayersResponse = false;
    let pushPlayerResponse = false;
    let clientResponses = [];
    switch(event.type) {
        case 'REFRESH':
            pushPlayerResponse = true;
            break;
        case 'RAISE_TROOPS':
            game.logic.makeArmy(player, event.countyId, event.troops);
            pushAllPlayersResponse = true;
            break;
        case 'MOVE_ARMY':
            game.logic.moveArmyWithOrders(player, event.armyId, event.orders);
            pushAllPlayersResponse = true;
            break;
        case 'STAND_DOWN_ARMY':
            game.logic.standDownArmy(player, event.armyId);
            pushAllPlayersResponse = true;
            break;
        case 'SET_FOCUS':
            game.logic.setCharacterFocus(player, event.characterId, event.focus);
            break;
        case 'SEND_MESSAGE':
            game.logic.sendMessage(
                event.fromCharacterId, 
                event.toCharacterId,
                escape(event.message),
                event.proposals,
                event.proposalAcceptType);
            pushAllPlayersResponse = true;
            break;
        case 'MARK_MESSAGE_READ':
            game.logic.markMessageRead(event.messageId);
            break;
        case 'SEND_PROPOSAL_RESPONSE':
            var message = game.logic.respondToProposal(
                player, 
                event.messageId, 
                event.proposalIndex,
                event.response);
            if (message) {
                pushAllPlayersResponse = true;
            }
            break;
        case 'CHANGE_RESEARCH':
            game.logic.changeCharacterResearch(
                player, 
                event.characterId, 
                event.researchIndex);
            pushPlayerResponse = true;
            break;
        case 'ACTIVATE_ITEM':
            game.logic.activateItem(player, event.itemId);
            pushPlayerResponse = true;
            break;
        case 'MOVE_CHARACTER':
            game.logic.moveCharacterToLocation(player, event.characterId, event.countyId);
            pushAllPlayersResponse = true;
            break;
        case 'BREAK_FEALTY':
            game.logic.breakFealty(player, event.characterId);
            pushAllPlayersResponse = true;
            break;
        case 'ACCEPT_DESIRE':
            game.logic.acceptDesire(player, event.messageId, event.desireIndex);
            pushAllPlayersResponse = true;
            break;
        case 'RENAME_CHARACTER':
            if (auth.isUserPaidSubscriber(userInfo)) {
                game.logic.renameCharacter(player, event.characterId, escape(event.name));
                pushAllPlayersResponse = true;
            } else {
                clientResponses.push({type: "ERROR", message: 'You do not have permission to perform this action'});
            }
            break;
        case 'RENAME_COUNTY':
            if (auth.isUserPaidSubscriber(userInfo)) {
                game.logic.renameCounty(player, event.countyId, escape(event.name));
                pushAllPlayersResponse = true;
            } else {
                clientResponses.push({type: "ERROR", message: 'You do not have permission to perform this action'});
            }
            break;
        case 'START_PLOT':
            game.logic.performPlot(
                player,
                event.characterId,
                event.targetId,
                event.plotType
            );
            pushAllPlayersResponse = true;
            break;
        case 'POST_NEWS':
            game.logic.addNews(event.characterId, escape(event.message));
            if (event.denounceTarget) {
                game.logic.denounce(player, event.characterId, event.denounceTarget);
            }
            pushAllPlayersResponse = true;
            break;
        case 'READ_NEWS':
            player.lastReadNewsIndex = event.newsIndex;
            break;
        case 'RAZE_WITH_ARMY':
            game.logic.razeCountyWithArmy(player, event.armyId);
            pushAllPlayersResponse = true;
            break;
        case 'REPAIR_COUNTY':
            game.logic.repairCounty(player, event.countyId);
            pushAllPlayersResponse = true;
            break;
    }
    return {
        pushAllPlayersResponse,
        pushPlayerResponse,
        clientResponses
    };
}