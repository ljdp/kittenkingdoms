const yaml = require('js-yaml');
const fs = require('fs');
const MESSAGES = yaml.safeLoad(fs.readFileSync(__dirname + '/data/ai_messages.yml'));

class AICharacter {
    constructor( game, character ) {
        this.game = game;
        this.character = character;
    }

    sendMessage( toId, data ) {
        let message = this.game.safePick(data.texts);

        let pattern = /{(.+?)}/g;
        let matches = [];
        let match = pattern.exec(message);
        while (match != null) {
            matches.push(match);
            match = pattern.exec(message);
        }
        matches.forEach(match => {
            let opts = match[1].split('|');
            let optIndex = Math.floor(Math.random() * opts.length);
            message = message.replace(match[0], opts[optIndex]);
        });

        let proposals = data.proposals ? data.proposals.slice() : null;
        this.game.sendMessage(this.character.guid, toId, message, proposals);
    }

    ageDependentChance( ageSplit, roll1, roll2 ) {
        let age = this.character.age;
        return (age < ageSplit && roll1 == 1) || (age >= ageSplit && roll2 == 1);
    }

    rollToAcceptProposal( proposal ) {
        let chance = this.game.chance;
        switch(proposal.type) {
            case 'SWEAR_FEALTY':
            case 'GIVE_RELEASE':
                return 1;
            case 'REQUEST_FEALTY':
                return chance.d20();
            case 'BUILD_CHILD':
                if (this.game.getCharacterChildren(this.charater).length < 2) {
                    return chance.d6();
                } else {
                    return chance.d20();
                }
            case 'MARRY':
            case 'GIVE_MONEY':
            case 'GIVE_LAND':
                return chance.d4();
            default:
                return chance.d10();
        }
    }

    tick() {
        let chance = this.game.chance;

        // AI is idle until 16 turns old.
        if (this.character.age < 16) return;

        // Only 1/6 chance the ai will act.
        if (chance.d6() != 1) return;

        let character = this.character;
        let mainDesire = character.desires[0];
        let secondDesire = character.desires[1];
        var target;

        // Respond to messages.
        let unreadMessages = this.game.state.messages.filter(m => !m.read && m.to == character.guid);
        unreadMessages.forEach(message => {
            for (let i = 0; i < message.proposals.length; i++) {
                // 1/6 chance of accepting proposal.
                if (this.rollToAcceptProposal(message.proposals[i]) == 1) {
                    this.game.respondToProposal(null, message.guid, i, true);
                } else {
                    this.game.respondToProposal(null, message.guid, i, false);
                }
            }
            this.game.markMessageRead(message.guid);
        });

        // Move Armies (1/2 chance).
        let armies = this.game.getCharacterArmies(character.guid).filter(army => army.cooldown == 0);
        for (let i = 0; i < armies.length; i++) {
            if (chance.d4() <= 2) {
                let army = armies[i];
                let adjacent = this.game.getAdjacentCounties(army.location).filter(county => {
                    return county.owningCharacter !== character.guid && !county.impassable;
                });
                if (adjacent.length == 0) {
                    adjacent = this.game.getAdjacentCounties(army.location).filter(county => {
                        return !county.impassable;
                    });
                }
                if (adjacent.length > 0) {
                    this.game.moveArmy(null, army.guid, this.game.safePick(adjacent).guid);
                }
            }
        }

        // Stand-down armies (1/4 chance).
        if (character.moneyRate && character.moneyRate < 0 && character.money <= 100 && chance.d4() == 1) {
            armies = this.game.getCharacterArmies(character.guid).filter(army => army.cooldown == 0);
            armies.forEach(army => {
                this.game.standDownArmy(null, army.guid);
            });
        }

        // Raise Armies (1/6 chance).
        let counties = this.game.getCharacterCounties(character.guid).filter(county => county.troops >= 100);
        if (counties.length > 0 && character.money >= 200 && chance.d6() == 1) {
            let county = this.game.safePick(counties);
            this.game.makeArmy(null, county.guid, Math.floor(county.troops/2));
        }

        // Request to build child (1/20 chance then 1/6 chance).
        if (character.spouse 
                && this.game.getCharacterChildren(character.guid).length < 3
                && this.ageDependentChance(38, chance.d20(), chance.d6()))
        {
            this.sendMessage(character.spouse, MESSAGES.buildChild);
        }

        // Request to marry (1/20 chance then 1/10 chance).
        if (!character.spouse && this.ageDependentChance(30, chance.d20(), chance.d10()))
        {
            var target = this.game.state.characters.filter(c => {
                return !c.spouse && c.age < 80;
            });
            if (target.length > 0) {
                target = chance.pickone(target);
                this.sendMessage(target.guid, MESSAGES.marry);
            }
        }

        // Release prisoners (1/4 chance).
        let prisoners = this.game.state.characters.filter(c => c.imprisoner == character.guid);
        prisoners.forEach(prisoner => {
            if (chance.d4() == 1) {
                this.game.releasePrisoner(prisoner.guid);
            }
        });
        
        if (mainDesire && chance.d4() == 1) {
            switch(mainDesire.key) {
                case 'family':
                    if (character.spouse) {
                        this.sendMessage(character.spouse, MESSAGES.buildChild);
                    }
                    break;
                case 'marryTarget':
                    if (!character.spouse) {
                        this.sendMessage(mainDesire.target, MESSAGES.marry);
                    }
                    break;
                case 'marryAnyone':
                    if (!character.spouse) {
                        target = this.game.safePick(
                            this.game.state.characters.filter(c => c.guid !== character.guid && this.game.canCharactersMarry(character, c))
                        );
                        if (target) {
                            this.sendMessage(target.guid, MESSAGES.marry);
                        }
                    }
                    break;
                case 'overlord':
                    target = this.game.safePick(
                        this.game.state.characters.filter(c => c.guid !== character.guid && c.overlord !== character.guid)
                    );
                    if (target) {
                        this.sendMessage(target.guid, MESSAGES.requestFealty);
                    }
                    break;
                case 'swear_fealty':
                    if (!character.overlord) {
                        this.sendMessage(mainDesire.target, MESSAGES.swearFealty);
                    }
                    break;
                case 'army':
                case 'armyReserve':
                case 'warmonger':
                    character.focus = 'military';
                    break;
                case 'wealth':
                    character.focus = 'economy';
                    break;
                case 'science':
                    character.focus = 'science';
                    break;
            }
        }
    }
}

module.exports = AICharacter;