const Game = require('../models/game');
const EventEmitter = require('events');
const moment = require('moment');
const gameNameGenerator = require('project-name-generator');

module.exports = class Games extends EventEmitter {
    constructor({db, mailer, config, users, auth, redis, logger}) {
        super();

        this.gameLogicClasses = config.GAME_VERSIONS.reduce((arr, version) => {
            arr.push({
                version,
                gameLogic: require(`./${version}/game_logic`)
            });
            return arr;
        }, []);
        this.newGameLogicClass = require(`./${config.NEW_GAME_VERSION}/game_logic`);

        this.db = db;
        this.mailer = mailer;
        this.config = config;
        this.users = users;
        this.auth = auth;
        this.redis = redis;
        this.logger = logger;
        this.games = [];
        db.deleteMany({
            status: 'ended',
            $where: function() {
                return moment().isSameOrAfter(moment(this.gameOverTime).add(24, "hours"));
            }
        });
        db.findMany({status: 'active'}).then(games => {
            games.forEach(gameDoc => {
                let doc = new Game(gameDoc);
                let logicObj = this.gameLogicClasses.find(obj => {
                    let versionSplit = obj.version.split('.');
                    let docVersionSplit = doc.version.split('.');
                    return versionSplit[0] === 'v'+docVersionSplit[0] &&
                        versionSplit[1] === docVersionSplit[1] &&
                        (versionSplit[2] === 'x' || versionSplit[2] === docVersionSplit[2]);
                });
                let logic;
                if (logicObj) {
                    logic = new logicObj.gameLogic(doc.config, doc.state, logger);
                } else {
                    logic = new this.newGameLogicClass(doc.config, doc.state, logger);
                }
                this.games.push({doc, logic});
                this.listenToGameEvents({doc, logic});
            });
        });
    }

    forEach( cb ) {
        this.games.forEach(cb);
    }

    activeGames() {
        return this.games.filter(game => game.doc.turn() < 24).map(game => game.doc.basicData());
    }

    updateAll() {
        let promises = [];
        for (let i = this.games.length - 1; i >= 0; i--) {
            let game = this.games[i];
            let doc = game.doc;
            let logic = game.logic;
            if (moment().isSameOrAfter(doc.nextTurnTime || 0)) {
                try {
                    logic.performGameTick();
                } catch (err) {
                    this.logger.error(err, err);
                    this.removeGameFromMemory(doc);
                    continue;
                }
                this.emit('gameTick', game);
                let p1 = doc.updateState(this.db, logic.state);
                let nextTurnTime = moment().add(this.config.TURN_INTERVAL.value, this.config.TURN_INTERVAL.unit).format();
                let nextIncomeTurnTime = doc.nextIncomeTurnTime;
                if (doc.isIncomeTurn()) {
                    nextIncomeTurnTime = moment().add(doc.config.incomeTurnInterval * this.config.TURN_INTERVAL.value, this.config.TURN_INTERVAL.unit).format();
                }
                let p2 = doc.updateNextTurnTime(this.db, nextTurnTime, nextIncomeTurnTime);
                promises.push(p1, p2);

                if (process.env.NODE_ENV === 'production' && logic.isGameOver() && doc.status !== 'ended') {
                    this.setGameOver(doc);
                }
            }

            // Remove games that have been finished for a while.
            if (doc.status == 'ended' 
                && doc.gameOverTime 
                && (moment().isSameOrAfter(moment(doc.gameOverTime).add(24, "hours")) || doc.state.players.length < 1) )
            {
                this.removeGameFromMemory(doc);
                this.db.deleteOne({_id: doc.id});
            }
        }

        // Automatically create new public games.
        // let joinableGames = this.games.filter(game => game.doc.turn() < 24 && game.doc.status !== 'ended');
        // if (joinableGames.length < 2) {
        //     let autoGames = getAutoGamesConfig();
        //     autoGames.forEach(gameConfig => {
        //         let gameName = gameNameGenerator({words: 2}).spaced;
        //         gameName = gameName.replace(/\w\S*/g, (txt) => { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        //         this.create({
        //             name: gameName,
        //             owner: null,
        //             config: gameConfig,
        //             role: 'admin'
        //         });
        //     });
        // }

        return Promise.all(promises);
    }

    saveAll() {
        let promises = [];
        this.games.forEach(game => {
            promises.push(
                game.doc.updateState(this.db, game.logic.state)
            );
        });
        return Promise.all(promises);
    }

    create({name, owner, config, state, role = 'basic'}) {
        if (this.countUserCreatedGames(owner) < this.config.MAX_USER_CREATED_GAMES[role]) { 
            let nextTurnTime = moment().add(this.config.GRACE_PERIOD.value, this.config.GRACE_PERIOD.unit);
            let nextIncomeTurnTime = moment(nextTurnTime).add(config.incomeTurnInterval, 'hours');
            let logic = new this.newGameLogicClass(config, state, this.logger);
            let doc = new Game({
                version: this.newGameLogicClass.version(),
                name,
                owner, 
                config: logic.config, 
                state: logic.state, 
                nextTurnTime: nextTurnTime.format(),
                nextIncomeTurnTime: nextIncomeTurnTime.format()
            });
            this.games.push({doc, logic});
            this.listenToGameEvents({doc, logic});
            return doc.save(this.db).then(doc => {
                this.emit('gameCreated', doc);
                return doc;
            });
        } else {
            return Promise.reject("User has reached their game creation limit");
        }
    }

    listenToGameEvents( game ) {
        game.logic.on('messageSent', event => this.onGameMessageSent(game, event));
        game.logic.on('characterDied', event => this.onGameCharacterDied(game, event));
        game.logic.on('characterBorn', event => this.onGameCharacterBorn(game, event));
        game.logic.on('playerAFK', event => this.onPlayerAFK(game, event));
    }

    setGameOver( gameDoc ) {
        let scoreboard = this.getGameScoreBoard(gameDoc);
        gameDoc.status = 'ended';
        gameDoc.gameOverTime = moment().format();
        if (scoreboard.length > 0) {
            let winnerName = scoreboard[0].username;
            let winnerScore = scoreboard[0].score;
            let kittensAlive = gameDoc.state.characters.filter(character => character.alive).length;
            let formattedScoreboard = '';
            scoreboard.forEach((score, i) => {
                formattedScoreboard += `${i}. ${score.username} - ${score.score} points - ${score.counties} counties<br>`;
            });
            gameDoc.state.players.forEach(player => {
                this.users.retrieveByUsername(player.username).then(user => {
                    if (user) {
                        this.mailer.sendMail({
                            to: user.email,
                            template: 'game_over',
                            params: {
                                username: user.username,
                                turn: gameDoc.turn(),
                                gameName: gameDoc.name,
                                winnerName: winnerName,
                                winnerScore: winnerScore,
                                kittensAlive: kittensAlive,
                                scoreboard: formattedScoreboard
                            }
                        })
                    }
                });
                
            });
        }
        return gameDoc.save(this.db);
    }

    getGameScoreBoard( gameDoc ) {
        let players = gameDoc.state.players.slice();
        players.sort((a, b) => {
            return b.score - a.score;
        });
        let result = [];
        players.forEach(player => {
            let characters = gameDoc.state.characters.filter(c => c.owningPlayer === player.username);
            let countySum = 0;
            characters.forEach(character => {
                let counties = gameDoc.state.counties.filter(c => c.owningCharacter === character.guid);
                countySum += counties.length;
            });
            result.push({
                username: player.username,
                score: player.score,
                counties: countySum
            });
        });
        return result;
    }

    removeGameFromMemory( gameDoc ) {
        let index = this.games.findIndex(game => game.doc.id == gameDoc.id);
        if (index != -1) {
            this.games.splice(index, 1);
        }
    }

    countUserCreatedGames( userid ) {
        let games = this.games.filter(game => game.doc.owner == userid);
        return games.length;
    }

    countUserJoinedGames( userid ) {
        let games = this.games.filter(game => {
            let index = game.doc.state.players.findIndex(player => player.userid == userid);
            return index != -1 && game.doc.status !== 'ended';
        });
        return games.length;
    }

    retrieve( id ) {
        return this.db.findOne({_id: id}).then(doc => {
            if (doc) {
                return new Game(doc);
            } else {
                return null;
            }
        });
    }

    findActiveGame( gameId ) {
        return this.games.find(game => game.doc.id == gameId);
    }

    findActiveGamesWithPlayer( playerName ) {
        return this.games.filter(game => {
            return game.doc.hasPlayer(playerName);
        });
    }

    joinGame( {gameId, userid, username, role = 'basic'} ) {
        if (this.countUserJoinedGames(userid) < this.config.MAX_USER_JOINED_GAMES[role]) {
            let game = this.findActiveGame(gameId);
            if (game) {
                if (game.doc.status !== 'active') {
                    return Promise.reject("Game has finished");
                }
                if (!game.doc.hasPlayer(username)) {
                    let result = game.logic.addPlayer(userid, username);
                    if (result) {
                        return game.doc.updateState(this.db, game.logic.state);
                    } else {
                        return Promise.reject("Game is full");
                    }
                } else {
                    return Promise.reject("Already in game");
                }
            } else {
                return Promise.reject("Game not found");
            }
        } else {
            return Promise.reject("You cannot join more than "+this.config.MAX_USER_JOINED_GAMES[role]+" games");
        }
    }

    leaveGame( {gameId, userid} ) {
        let game = this.findActiveGame(gameId);
        if (game) {
            let result = game.logic.removePlayer(userid);
            if (result) {
                return Promise.resolve(result);
            } else {
                return Promise.reject("User not in game");
            }
        } else {
            return Promise.reject("Game not found");
        }
    }

    handleEvent( event, player ) {
        let game = this.findActiveGame(event.gameId);
        let responses = [];
        switch(event.type) {
            case 'GET_FULL_STATE':
                responses.push({
                    username: player,
                    message: game.logic.state
                });
                break;
        }
        return responses;
    }

    onGameIncomeTurn( game, event ) {
        game.doc.state.players.forEach(player => {
            this.users.retrieve(player.userid).then(user => {
                if (user && user.emailNotify) {
                    let gameUrl = this.auth.generateOneTimeGameLogin(this.redis, {username: user.username, gameId: game.doc.id});
                    this.mailer.sendMail({
                        to: user.email,
                        template: 'income_turn',
                        params: {
                            username: user.username,
                            turn: game.doc.turn(),
                            gameName: game.doc.name,
                            gameUrl
                        }
                    })
                }
            })
        });
    } 

    onGameMessageSent( game, event ) {
        if (!event.receiverPlayer) return;
        this.users.retrieve(event.receiverPlayer.userid).then(user => {
            if (user && user.emailNotify) {
                let gameUrl = this.auth.generateOneTimeGameLogin(this.redis, {username: user.username, gameId: game.doc.id});
                this.mailer.sendMail({
                    to: user.email,
                    template: 'kitten_message',
                    params: {
                        username: user.username,
                        senderName: event.senderCharacter.name,
                        senderHouse: event.senderCharacter.house,
                        kittenName: event.receiverCharacter.name,
                        kittenHouse: event.receiverCharacter.house,
                        fromKittenImg: `https://robohash.org/${event.senderCharacter.guid}.jpg?set=set4&size=128x128`,
                        toKittenImg: `https://robohash.org/${event.receiverCharacter.guid}.jpg?set=set4&size=128x128`,
                        message: event.message,
                        gameUrl
                    }
                })
            }
        })
    }

    onGameCharacterBorn( game, event ) {
        if (!event.players || event.players.length == 0) return;
        event.players.forEach((player, i) => {
            let spouseIndex = (i == 0) ? 1 : 0;
            if (player && player.userid) {
                this.users.retrieve(player.userid).then(user => {
                    if (user && user.emailNotify) {
                        let gameUrl = this.auth.generateOneTimeGameLogin(this.redis, {username: user.username, gameId: game.doc.id});
                        this.mailer.sendMail({
                            to: user.email,
                            template: 'kitten_born',
                            params: {
                                username: user.username,
                                parentFullName: `${event.parents[i].name} ${event.parents[i].house}`,
                                spouseFullName: `${event.parents[spouseIndex].name} ${event.parents[spouseIndex].house}`,
                                childFullName: `${event.child.name} ${event.child.house}`,
                                gameUrl
                            }
                        });
                    }
                });
            }
        });
    }

    onGameCharacterDied( game, event ) {
        if (!event.player) return;
        this.users.retrieve(event.player.userid).then(user => {
            if (user && user.emailNotify) {
                let gameUrl = this.auth.generateOneTimeGameLogin(this.redis, {username: user.username, gameId: game.doc.id});
                this.mailer.sendMail({
                    to: user.email,
                    template: 'kitten_died',
                    params: {
                        username: user.username,
                        kittenName: event.character.name,
                        kittenHouse: event.character.house,
                        age: event.character.age,
                        heirFullName: event.heir ? `${event.heir.name} ${event.heir.house}` : 'No-one',
                        gameUrl
                    }
                });
            }
        });
    }

    onPlayerAFK( game, event ) {
        if (!event.player) return;
        this.users.retrieve(event.player.userid).then(user => {
            if (user) {
                let gameUrl = this.auth.generateOneTimeGameLogin(this.redis, {username: user.username, gameId: game.doc.id});
                this.mailer.sendMail({
                    to: user.email,
                    template: 'afk',
                    params: {
                        username: user.username,
                        gameName: game.doc.name,
                        gameUrl
                    }
                });
            }
        })
    }

    emailAllPlayersInGame( gameId, subject, message ) {
        this.retrieve(gameId).then(gameDoc => {
            if (gameDoc) {
                gameDoc.state.players.forEach(player => {
                    this.emailPlayer(gameDoc, player, 'admin_message', subject, message);
                });
            }
        });
    }

    emailPlayer( gameDoc, player, template, subject, message ) {
        this.users.retrieve(player.userid).then(user => {
            if (user) {
                let gameUrl = this.auth.generateOneTimeGameLogin(this.redis, {username: user.username, gameId: gameDoc.id});
                this.mailer.sendMail({
                    to: user.email,
                    template: template,
                    params: {
                        username: user.username,
                        gameName: gameDoc.name,
                        subject, message, gameUrl
                    }
                });
            }
        });
    }
}