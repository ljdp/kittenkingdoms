const moment = require('moment');

module.exports = function( {auth, games} ) {
    let router = require('express').Router();
    this.auth = auth;
    let ensureValidToken = auth.ensureValidToken;

    router.post('/game', ensureValidToken, (req, res) => {
        let config = req.body.config;
        let name = req.body.name;
        games.create({
            name, config, 
            owner: req.decodedToken.id, 
            role: auth.getUserHighestRole(req.decodedToken)
        }).then(gameDoc => {
            res.json({
                gameId: gameDoc.id
            });
        }, err => {
            res.status(400).send(err);
        });
    });
    router.get('/game/:id/version', (req, res) => {
        games.retrieve(req.params.id).then(gameDoc => {
            if (gameDoc) {
                res.send(gameDoc.version);
            } else {
                res.sendStatus(404);
            }
        }, err => {
            res.sendStatus(500);
        });
    })
    router.delete('/game/:id', ensureValidToken, (req, res) => {
        games.retrieve(req.params.id).then(gameDoc => {
            if (gameDoc) {
                if (gameDoc.owner == req.decodedToken.id || req.decodedToken.roles.includes('admin')) {
                    gameDoc.status = 'ended';
                    games.removeGameFromMemory(gameDoc);
                    games.db.deleteOne({_id: gameDoc.id});
                    res.send('OK');
                } else {
                    res.status(403).send('You do not have permission to delete this game.');
                }
            } else {
                res.status(404).send('Game not found.');
            }
        }, err => {
            res.status(500).send('Internal server error.');
        });
    });
    router.post('/game/:id/join', ensureValidToken, (req, res) => {
        games.joinGame({
            gameId: req.params.id, 
            userid: req.decodedToken.id,
            username: req.decodedToken.username, 
            role: auth.getUserHighestRole(req.decodedToken)
        }).then(result => {
            res.send('OK');
        }, err => {
            res.status(400).send(err);
        });
    });
    router.post('/game/:id/leave', ensureValidToken, (req, res) => {
        games.leaveGame({gameId: req.params.id, userid: req.decodedToken.id}).then(result => {
            res.send('OK');
        }, err => {
            res.status(400).send(err);
        });
    });
    router.post('/game/:id/newsletter', ensureValidToken, (req, res) => {
        let role = auth.getUserHighestRole(req.decodedToken)
        if (role === 'admin') {
            games.emailAllPlayersInGame(req.params.id, req.body.message);
            res.send('OK');
        } else {
            res.sendStatus(403);
        }
    });
    router.get('/games', (req, res) => {
        res.json(games.activeGames());
    });
    router.get('/player/:name/games', ensureValidToken, (req, res) => {
        let playerGames = games.findActiveGamesWithPlayer(req.params.name).map(game => game.doc.basicData());
        if (!playerGames) {
            playerGames = [];
        }
        res.json(playerGames);
    });
    if (process.env.NODE_ENV == 'development') {
        router.get('/games/tick', (req, res) => {
            games.forEach(game => {
                game.doc.nextTurnTime = moment();
            });
            games.updateAll();
            res.send('OK');
        });
    }

    this.router = router;
}