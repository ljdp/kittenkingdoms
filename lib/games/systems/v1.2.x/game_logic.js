const AICharacter = require('./ai_character');
const d3 = require('d3');
const util = require('../util');
const Chance = require('chance');
const moment = require('moment');
const yaml = require('js-yaml');
const fs = require('fs');
const DESIRES = yaml.safeLoad(fs.readFileSync(__dirname+'/data/desires.yml'));
const TECHNOLOGY = yaml.safeLoad(fs.readFileSync(__dirname+'/data/technology.yml'));
const K = yaml.safeLoad(fs.readFileSync(__dirname+'/data/constants.yml'));
const FIRST_NAMES = fs.readFileSync(__dirname+'/data/first_names.txt', 'utf8').split('\n');
const EventEmitter = require('events');
function coLink( county ) { return county.name; }
function chLink( character ) { return `${character.name} ${character.house}`; }

class GameLogic extends EventEmitter {
    static version() {
        return '1.2.0';
    }

    constructor( config, loadedState, logger ) {
        super();
        let chance = new Chance();
        this.constants = K;
        this.chance = chance;
        this.logger = logger;
        this.config = util.mergeOptions(util.defaultConfig(), config);
        this.config.maxPlayers = Math.min(this.config.maxPlayers, 32);

        if (loadedState) {
            this.state = loadedState;

            // Migrate changes for v1.1
            this.state.counties.forEach(county => {
                if (!county.specialists) county.specialists = [];
                if (!county.negatives) county.negatives = [];
            });
        } else {
            let generatedData = this.generateCountiesAndCharacters(chance);
            this.state = util.mergeOptions(util.defaultState(), {
                counties: generatedData.counties,
                characters: generatedData.characters,
                map: {
                    width: this.config.mapWidth,
                    height: this.config.mapHeight
                }
            });
        }

        // Create hashmaps.
        this.countyMap = util.createMapFromGuids(this.state.counties);
        this.characterMap = util.createMapFromGuids(this.state.characters);
        this.armyMap = util.createMapFromGuids(this.state.armies);

        // Calculate county sizes.
        let voronoi = d3.voronoi();
        voronoi.extent([[0, 0], [this.config.mapWidth, this.config.mapHeight]]);
        let sites = this.state.counties.map(county => {
            let site = [county.x, county.y];
            site.county_guid = county.guid;
            return site;
        });
        let preDiagram = voronoi(sites);
        let squiggleAmp = 2;
        preDiagram.polygons().forEach((poly, i) => {
            let center = d3.polygonCentroid(poly);
            let area = d3.polygonArea(poly);
            let size = Math.ceil(area * 0.002);
            let cell = preDiagram.cells[i];
            let x = center[0] + Math.sin(chance.floating({min: -Math.PI, max: Math.PI})) * size * squiggleAmp;
            let y = center[1] + Math.cos(chance.floating({min: -Math.PI, max: Math.PI})) * size * squiggleAmp;
            let site = [x, y];
            site.county_guid = cell.site.data.county_guid;
            if (!loadedState) {
                let county = this.countyMap[cell.site.data.county_guid];
                county.size = size;
                county.impassable = (size <= 1);
                county.sites = [
                    [cell.site[0], cell.site[1]],
                    [x, y]
                ];
                county.center = center;
                county.maxTroops = K.caps.countyTroopsPerCountySize * size;
            }
            sites.push(site);
        });
        this.diagram = voronoi(sites);
        
        if (!loadedState) {
            // Mark cells along the edge of the map as impassable.
            this.diagram.cells.forEach(cell => {
                cell.halfedges.forEach(halfedge => {
                    let edge = this.diagram.edges[halfedge];
                    if (!edge.left || !edge.right) {
                        let county = this.getCounty(cell.site.data.county_guid);
                        county.impassable = true;
                    }
                });
            });

            let counties = this.state.counties.filter(county => !county.impassable);
            counties.forEach(county => {
                let adjacent = this.getAdjacentCounties(county.guid);
                county.adjacent = adjacent.map(adj => adj.guid);
            });
            counties = counties.filter(county => county.adjacent.length >= 3);
            this.state.characters.forEach(character => {
                // Assign counties to characters.
                let county = counties.pop();
                county.owningCharacter = character.guid;
                character.location = county.guid;
                // Remove county bonuses, negatives and treasure from starting counties
                // for a fairer game.
                county.specialists = [];
                county.negatives = [];
                county.hasTreasure = false;
                // Generate desires for characters.
                character.desires = [];
                let desires = this.generateDesires(character, 4);
                character.desireChoices = [
                    [desires[0], desires[1]],
                    [desires[2], desires[3]]
                ];
            });
        }
    }

    generateCounty( name, xCoord, yCoord ) {
        let chance = this.chance;

        let _specialists = chance.shuffle(K.countySpecialists.slice());
        let specialists = [];
        if (chance.d6() === 1) {
            specialists.push(_specialists.pop());
            if (chance.d6() === 1) {
                specialists.push(_specialists.pop());
            }
        }

        let _negatives = chance.shuffle(K.countyNegatives.slice());
        let negatives = [];
        if (chance.d8() === 1) {
            negatives.push(_negatives.pop());
            if (chance.d8() === 1) {
                negatives.push(_negatives.pop());
            }
        }

        let county = {
            guid: chance.guid(),
            name: name,
            x: xCoord,
            y: yCoord,
            troops: chance.integer({min: K.generation.troops.min, max: K.generation.troops.max}),
            money: 10,
            science: 1,
            armies: [],
            owningCharacter: null,
            specialists: specialists,
            negatives: negatives,
            buildings: [],
            hasTreasure: (chance.d6() === 1)
        }
        return county;
    }

    generateCountiesAndCharacters( chance ) {
        let countyNames = chance.unique(chance.city, this.config.maxCounties);
        let countyXCoords = chance.unique(chance.integer, this.config.maxCounties, {min: 0, max: this.config.mapWidth});
        let countyYCoords = chance.unique(chance.integer, this.config.maxCounties, {min: 0, max: this.config.mapHeight});
        let counties = [];
        for (let i = 0; i < this.config.maxCounties; i++) {
            counties.push(
                this.generateCounty(countyNames.pop(), countyXCoords.pop(), countyYCoords.pop())
            );
        }
        let houseNames = chance.unique(chance.last, this.config.maxPlayers);
        let characters = [];
        for (let i = 0; i < this.config.maxPlayers; i++) {
            let character = this.makeCharacter(
                chance.integer({min: K.generation.age.min, max: K.generation.age.max}), 
                houseNames.pop());
            characters.push(character);
        };
        return {counties, characters};
    }

    isGameOver() {
        let playersWithCharacters = this.state.characters.filter(character => {
            return character.owningPlayer && character.alive;
        });
        let result = this.state.map.turn >= this.config.maxTurns || playersWithCharacters.length <= 1;
        return result;
    }

    getAdjacentCells( cell ) {
        let diagram = this.diagram;
        let cellIndex = cell.site.index;
        var leftEdges = diagram.edges.filter(function(edge) {
            return edge.left && edge.left.index == cellIndex;
        });
        var rightEdges = diagram.edges.filter(function(edge) {
            return edge.right && edge.right.index == cellIndex;
        });
        var adjacentCells = [];
        for (var i = 0; i < leftEdges.length; i++) {
            if (leftEdges[i].right) {
                adjacentCells.push(diagram.cells[leftEdges[i].right.index]);
            }
        }
        for (var i = 0; i < rightEdges.length; i++) {
            if (rightEdges[i].left) {
                adjacentCells.push(diagram.cells[rightEdges[i].left.index]);
            }
        }
        return adjacentCells.filter((cell) => {
            if (cell) {
                let county = this.getCounty(cell.site.data.county_guid);
                return !county.impassable;
            }
            return false;
        });
    }

    getAdjacentCounties( countyId ) {
        let adjacentIds = [];
        let cells = this.diagram.cells.filter(cell => cell.site.data.county_guid == countyId);
        cells.forEach(cell => {
            this.getAdjacentCells(cell).forEach(adjacent => {
                let adjacentId = adjacent.site.data.county_guid;
                if (adjacentIds.indexOf(adjacentId) == -1 && adjacentId !== countyId) {
                    adjacentIds.push(adjacentId);
                }
            });
        });
        return this.getCounties(adjacentIds);
    }

    getAdjacentCountiesSteps( countyId, maxSteps = 1, step = 1 ) {
        let result = [];
        if (step <= maxSteps) {
            this.getAdjacentCounties(countyId).forEach(adjacent => {
                result.push(adjacent);
                result = result.concat(this.getAdjacentCountiesSteps(adjacent.guid, maxSteps, step + 1));
            });
        }
        return result;
    }

    getCounty( guid ) {
        if (typeof guid === 'string') {
            return this.countyMap[guid];
        } else {
            return guid;
        }
    }

    getCounties( guids ) {
        let result = [];
        guids.forEach(guid => {
            result.push(this.countyMap[guid]);
        });
        return result;
    }

    getCharacter( guid ) {
        if (typeof guid === 'string') {
            return this.characterMap[guid];
        } else {
            return guid;
        }
    }

    getCharacters( guids ) {
        let result = [];
        guids.forEach(guid => {
            result.push(this.getCharacter(guid));
        });
        return result;
    }

    fullname( characterOrGuid ) {
        let character = characterOrGuid;
        if (typeof(characterOrGuid) == 'string') {
            character = this.getCharacter(characterOrGuid);
        }
        return `${character.name} ${character.house}`;
    }

    getCharacterCounties( guid ) {
        return this.state.counties
            .filter(county => county.owningCharacter == guid);
    }

    getPlayerCharacters( username ) {
        return this.state.characters.filter(c => c.owningPlayer && c.owningPlayer == username);
    }

    getCharacterArmies( guid ) {
        return this.state.armies
            .filter(army => army.owningCharacter == guid);
    }

    getCharacterChildren( guid, onlyAlive = true ) {
        let character = this.getCharacter(guid);
        let children = this.state.characters.filter(child => {
            if (child.parents && (!onlyAlive || (onlyAlive && child.alive))) {
                return child.parents.indexOf(guid) != -1;
            }
            return false;
        });
        children.sort((a, b) => b.age - a.age);
        return children;
    }

    getCharacterSiblings( guid, onlyAlive = true) {
        let character = this.getCharacter(guid);
        if (!character.parents || character.parents.length == 0) {
            return [];
        }
        let siblings = this.state.characters.filter(sibling => {
            return sibling.guid != guid && (!onlyAlive || (onlyAlive && sibling.alive))
                && sibling.parents && character.parents && sibling.parents.findIndex(parent => character.parents.indexOf(parent)) != -1
        });
        siblings.sort((a, b) => b.age - a.age);
        return siblings;
    }

    getCharacterHeir( guid ) {
        let character = this.getCharacter(guid);

        let isChild = (other) => {
            return other.parents.indexOf(guid) !== -1;
        }

        let isSibling = (other) => {
            return other.parents.findIndex(parent => {
                return character.parents.indexOf(parent) !== -1;
            }) !== -1;
        }

        // Return eldest, non-controlled child as heir.
        for (let i = 0; i < this.state.characters.length; i++) {
            let other = this.state.characters[i];
            if (other.alive && !other.owningPlayer && isChild(other)) {
                return other;
            }
        }

        // Otherwise return eldest, non-controlled sibling as heir.
        for (let i = 0; i < this.state.characters.length; i++) {
            let other = this.state.characters[i];
            if (other.alive && !other.owningPlayer && isSibling(other)) {
                return other;
            }
        }

        // No heir.
        return null;
    }

    getCharacterUnderlords( characterId ) {
        return this.state.characters.filter( character => {
            return character.overlord == characterId;
        });
    }

    getCharacterUnderlordHeirarchy( characterId ) {
        let character = this.getCharacter(characterId);
        let tree = {
            character: character,
            underlords: []
        };
        let underlords = this.getCharacterUnderlords(characterId);
        if (underlords && underlords.length > 0) {
            for (let underlord of underlords) {
                tree.underlords.push(this.getCharacterUnderlordHeirarchy(underlord.guid));
            }
        }
        return tree;
    }

    getCharacterPrisoners( characterId ) {
        return this.state.characters.filter(c => c.imprisoner == characterId);
    }

    getCharacterParents( characterId ) {
        let character = this.getCharacter(characterId);
        let result = [];
        character.parents.forEach(parentId => result.push(this.getCharacter(parentId)));
        return result;
    }

    isNameValid( name ) {
        return name && name.length > 0 && name.length < 16;
    }

    renameCharacter( player, characterId, newName ) {
        let character = this.getCharacter(characterId);
        let parents = this.getCharacterParents(characterId);
        if ((character.owningPlayer == player.username || parents.filter(p => p.owningPlayer == player.username).length > 0)
            && character.age < K.renameCharacterMaxAge 
            && this.isNameValid(newName))
        {
            character.name = newName;
        }
    }

    renameHouse( player, characterId, newName ) {
        let character = this.getCharacter(characterId);
        if (character.owningPlayer === player.username && this.isNameValid(newName)) {
            let existingHouseName = this.state.characters.find(char => char.house === newName);
            if (!existingHouseName) {
                character.house = newName;
            }
        }
    }

    renameCounty( player, countyId, newName ) {
        let county = this.getCounty(countyId);
        let countyNames = this.state.counties.map(c => c.name);
        if (this.doesItemBelongToPlayer(county, player) 
            && (county.lastRenameTurn == null || (this.state.map.turn - county.lastRenameTurn) >= K.renameCountyCooldown)
            && this.isNameValid(newName)
            && countyNames.indexOf(newName) == -1)
        {
            county.name = newName;
            county.lastRenameTurn = this.state.map.turn;
        }
    }

    getArmy( armyId ) {
        if (typeof armyId === 'string') {
            return this.armyMap[armyId];
        } else {
            return armyId;
        }
    }

    getPlayer( username ) {
        if (typeof username === 'string') {
            return this.state.players.find(player => player.username == username);
        } else {
            return username;
        }
    }

    getItem( guid ) {
        if (typeof guid === 'string') {
            return this.state.items.find(item => item.guid == guid);
        } else {
            return guid;
        }
    }

    getGameSettingFromPath( path ) {
        let splitPath = path.split("/");
        let value = null;
        for (let i = 0, k = K; i < splitPath.length; i++) {
            k = k[splitPath[i]];
            if (i == splitPath.length - 1) {
                value = k;
            }
        }
        return value;
    }

    getGameSettingObject( characterId, path ) {
        let obj = this.getGameSettingFromPath(path);
        let result = {};
        if (obj !== null) {
            for (let key in obj) {
                result[key] = this.getGameSettingValue(characterId, path + '/' + key);
            }
        }
        return result;
    }

    getGameSettingValue( characterId, path ) {
        let value = this.getGameSettingFromPath(path);
        if (value !== null) {
            let character = this.getCharacter(characterId);
            let itemEffects = this.state.items
                .filter(item => item.active && item.effect)
                .map(item => {
                    return item.effect.filter(effect => effect.path === path);
                })
                .reduce((pre, cur) => pre.concat(cur), []);
            let otherEffects = this.getCharacter(characterId).effects.filter(effect => {
                return effect.path === path;
            });
            let focusEffects = (K.focus[character.focus] || {effect:[]}).effect.filter(effect => {
                return effect.path === path;
            });
            [...focusEffects, ...itemEffects, ...otherEffects].forEach(effect => {
                if (effect) {
                    value = this.modifyValueWithEffect(value, effect);
                }
            });
            return value;
        } else {
            this.logger.warn('getGameSettingValue, path was retreived null.');
        }
    }

    getObjectModifiedValue( obj, path, effects ) {
        let value = path.split('/').reduce((pre, cur) => pre[cur], obj);
        effects.filter(effect => effect.path === path).forEach(effect => {
            value = this.modifyValueWithEffect(value, effect);
        });
        return value;
    }

    modifyValueWithEffect( value, effect ) {
        if (effect.add) {
            value += effect.add;
        } else if (effect.multiply) {
            value *= effect.multiply;
        } else if (effect.set) {
            value = effect.set;
        }
        if (effect.min) {
            value = Math.min(value, effect.min);
        }
        if (effect.max) {
            value = Math.max(value, effect.max);
        }
        if (effect.floor) {
            value = Math.floor(value);
        }
        return value;
    }

    getCountyBuildingsEffects( county, path, checkAdjacent = true ) {
        let value = 0;
        let effects = [];
        county.buildings.forEach(building => {
            effects.push(...building.countyEffect.filter(effect => {
                return effect.path === path && !effect.applyToAdjacentOnly;
            }));
        });
        if (checkAdjacent && county.adjacent) {
            county.adjacent.forEach(id => {
                let other = this.getCounty(id);
                other.buildings.forEach(building => {
                    effects.push(...building.countyEffect.filter(effect => {
                        return effect.path === path && effect.applyToAdjacentOnly;
                    }));
                });
            });
        }
        effects.forEach(effect => {
            let canAffect = false;
            if ((effect.requireSpecialist && county.specialists.find(sp => sp.type === effect.requireSpecialist)) || 
                !effect.requireSpecialist) 
            {
                if (effect.addPerAdjacent && county.adjacent) {
                    county.adjacent.forEach(adjCountyId => {
                        let adjCounty = this.getCounty(adjCountyId);
                        let targetBuildings = adjCounty.buildings.filter(adjBuilding => adjBuilding.id === effect.addPerAdjacent.id);
                        let borderState = this.getCountyBorderState(adjCountyId, county.guid);
                        if (effect.addPerAdjacent.friendlyOnly && (borderState === 'OPEN' || borderState === 'INNER')) {
                            value += effect.addPerAdjacent.add * targetBuildings.length;
                        } else if(!effect.addPerAdjacent.friendlyOnly) {
                            value += effect.addPerAdjacent.add * targetBuildings.length;
                        }
                    });
                } else if (!effect.addPerAdjacent) {
                    value = this.modifyValueWithEffect(value, effect);
                }
            }
        });
        return value;
    }

    setCharacterFocus( player, characterId, focus ) {
        let character = this.getCharacter(characterId);
        if (character.owningPlayer == player.username && character.cooldowns.focus == 0) {
            character.cooldowns.focus = (character.focus == focus) ? 0 : K.focusCooldown;
            character.focus = focus;
        }
    }

    setCharacterOverlord( characterId, overlordId ) {
        if (characterId !== overlordId) {
            let character = this.getCharacter(characterId);
            if (character.cooldowns.breakFealty == 0 && character.alive) {
                if (character.overlord) {
                    this.sendMessage(character.overlord, character.overlord, 
                        `${chLink(character)} has broken fealty.`, null);
                }
                if (overlordId) {
                    let overlord = this.getCharacter(overlordId);
                    if (overlord && overlord.alive && overlord.overlord !== character.guid) {
                        character.overlord = overlordId;
                        character.cooldowns.breakFealty = this.getGameSettingValue(characterId, "breakFealtyCooldown");
                    }
                    this.addNews(null, `${chLink(character)} has sworn fealty to ${chLink(overlord)}.`);
                } else {
                    character.overlord = null;
                }
            }
        }
    }

    breakFealty( player, characterId ) {
        let character = this.getCharacter(characterId);
        if ((player == null || character.owningPlayer == player.username) && character.overlord != null) {
            this.setCharacterOverlord(characterId, null);
        }
    }

    addPlayer( userid, username ) {
        if (this.state.players.length >= this.config.maxPlayers) {
            return null;
        }

        let availableCharacters = this.state.characters
            .filter(character => character.owningPlayer == null && character.alive)
            .map(character => character.guid);
        let player = {
            userid: userid,
            username: username,
            score: 0,
            characters: [],
            lastActive: moment().format()
        };
        let charactersToPick = 1;
        if (availableCharacters.length >= charactersToPick) {
            let ids = this.chance.pickset(availableCharacters, charactersToPick);
            ids.forEach(id => {
                let character = this.getCharacter(id);
                if (character) {
                    character.owningPlayer = username;
                    let id = availableCharacters.pop();
                    player.characters.push(id);
                }
            });
        }
        this.state.players.push(player);
        return player;
    }

    removePlayer( userid ) {
        let playerIndex = this.state.players.findIndex(p => p.userid == userid);
        if (playerIndex != -1) {
            let player = this.state.players[playerIndex];
            this.state.characters.filter(ch => ch.owningPlayer == player.username).forEach(ch => {
                ch.owningPlayer = null;
            });
            this.state.players.splice(playerIndex, 1);
            return true;
        }
        return false;
    }

    makeChild( parentId ) {
        let parent = this.getCharacter(parentId);
        let spouse = this.getCharacter(parent.spouse);
        if (parent && spouse && parent.cooldowns.buildChild == 0 && spouse.cooldowns.buildChild == 0) {
            if (parent.stats.children) {
                parent.stats.childrenMade++;
            }
            if (spouse.stats.children) {
                spouse.stats.childrenMade++;
            }
            parent.cooldowns.buildChild += K.buildChildCooldown;
            spouse.cooldowns.buildChild += K.buildChildCooldown;

            let parents = [parent, spouse];
            let parentIds = [parent.guid, parent.spouse];
            let playerCharacterCount = [
                parent.owningPlayer ? this.getPlayerCharacters(parent.owningPlayer).length : 0,
                spouse.owningPlayer ? this.getPlayerCharacters(spouse.owningPlayer).length : 0
            ];
            let children = [];
            for(let i = 0; i < 2; i++) {
                let child = this.makeCharacter(0, parent.house);
                if (playerCharacterCount[i] < this.config.maxCharactersPerPlayer) {
                    child.owningPlayer = parents[i].owningPlayer;
                }
                child.parents = parentIds.slice(0);
                child.location = parent.location;
                child.lifespan = this.chance.integer({
                    min: Math.max(
                        this.getGameSettingValue(parent.guid, "generation/lifespan/min"), 
                        this.getGameSettingValue(parent.spouse, "generation/lifespan/min")),
                    max: Math.max(
                        this.getGameSettingValue(parent.guid, "generation/lifespan/max"), 
                        this.getGameSettingValue(parent.spouse, "generation/lifespan/max"))
                    });
                this.state.characters.push(child);
                this.characterMap[child.guid] = child;
                children.push(child);
                
                let message = `I'm a parent! Welcome to the world ${chLink(child)}.`;
                this.sendMessage(parentIds[0], parentIds[0], message);
                this.sendMessage(parentIds[1], parentIds[1], message);

                // Emit event
                // this.emit('characterBorn', {
                //     players: child.parents.map(parent => this.getPlayer(this.getCharacter(parent).owningPlayer)),
                //     parents: child.parents.map(parent => this.getCharacter(parent)),
                //     child: child
                // });
            }
            return children;
        }
        return [];
    }

    makeCharacter( age, house, autoAdd = false ) {
        let chance = this.chance;
        let character = {
            guid: chance.guid(),
            name: chance.pickone(FIRST_NAMES),
            house: house,
            age: age,
            lifespan: chance.integer(K.generation.lifespan),
            alive: true,
            spouse: null,
            overlord: null,
            parents: [],
            research: [K.research.potion, K.research.building, K.research.book],
            activeResearchIndex: 0,
            effects: [],
            cooldowns: {
                focus: 0,
                buildChild: 0,
                breakFealty: 0,
                movement: 0,
                plotting: 0,
                newsfeed: 0
            },
            focus: null,
            science: 0,
            maxCounties: K.caps.county,
            maxArmies: K.caps.army,
            money: K.startingMoney,
            location: null,
            owningPlayer: null,
            desires: [],
            pacts: [],
            stats: {
                dayMoneyBreakdown: {},
                dayScienceBreakdown: {},
                unrazedCounties: 0,
                razedCounties: 0,
                treasureLooted: 0,
                income: 0,
                unraisedTroops: 0,
                vassals: 0,
                moneyGifted: 0,
                itemsMade: 0,
                messagesReceived: 0,
                children: 0,
                troopsKilled: 0,
                turnsAtPeace: 0,
                repairedCounties: 0,
                charactersCaptured: [],
                charactersKilled: [],
                charactersStolenFrom: [],
                charactersPoisoned: [],
                lastTurnAttackOrDefended: 0,
                technologiesDiscovered: 0,
                experimentsTried: 0
            }
        };
        if (autoAdd) {
            this.state.characters.push(character);
            this.characterMap[character.guid] = character;
        }
        return character;
    }

    setCountyOwner( countyId, characterId ) {
        let county = this.countyMap[countyId];
        county.owningCharacter = characterId;
    }

    moveCharacterToLocation( player, characterId, countyId ) {
        let character = this.getCharacter(characterId);
        if (!character.cooldowns.movement) {
            character.cooldowns.movement = 0;
        }
        if ((player == null || character.owningPlayer == player.username) 
            && character.cooldowns.movement == 0
            && this.isCountyFriendly(countyId, characterId))
        {
            character.location = countyId;
            character.cooldowns.movement = this.getGameSettingValue(characterId, "movementCooldown/character");
        }
    }

    imprisonCharacter( imprisonerId, prisonerId ) {
        let imprisoner = this.getCharacter(imprisonerId);
        let prisoner = this.getCharacter(prisonerId);
        prisoner.imprisoner = imprisonerId;
        this.sendMessage(prisonerId, prisonerId, 
            `I have been imprisoned by ${chLink(imprisoner)}.`);
        this.addNews(imprisonerId, 
            `${chLink(prisoner)} has been imprisoned by ${chLink(imprisoner)}.`);
    }

    releasePrisoner( characterId ) {
        let character = this.getCharacter(characterId);
        if (character.imprisoner) {
            let imprisoner = this.getCharacter(character.imprisoner);
            character.imprisoner = null;
            let counties = this.getCharacterCounties(characterId);
            if (counties.length > 0) {
                character.location = this.chance.pickone(counties).guid;
            }
            this.addNews(imprisoner.guid, 
                `${chLink(character)} has been released from prison by ${chLink(imprisoner)}.`);
        }
    }

    respondToProposal( player, messageId, proposalIndex, response ) {
        let message = this.state.messages.find(msg => msg.guid == messageId);
        let respondingCharacter = this.getCharacter(message.to);
        let proposal = message.proposals[proposalIndex];
        if ((player == null || respondingCharacter.owningPlayer == player.username) && !proposal.receivedResponse) {
            if (message.proposalAcceptType == 'any') {
                if (response && this.canAcceptProposal(proposal, message)) {
                    this.setProposalResponse(message, proposal, true);
                } else if (!response) {
                    this.setProposalResponse(message, proposal, false);
                }
                return message;
            } else if (message.proposalAcceptType == 'all') {
                if (response) {
                    let canAcceptCount = 0;
                    let proposalNames = [];
                    message.proposals.forEach(proposal => {
                        if (this.canAcceptProposal(proposal, message)) {
                            canAcceptCount++;
                        }
                        proposalNames.push(proposal.type);
                    });
                    if (canAcceptCount == message.proposals.length) {
                        message.proposals.forEach(proposal => this.setProposalResponse(message, proposal, true));
                    }
                } else {
                    message.proposals.forEach(proposal => this.setProposalResponse(message, proposal, false));
                }
            }
        }
    }

    setProposalResponse( message, proposal, response ) {
        proposal.response = response;
        proposal.receivedResponse = true;
        if (response && this.canAcceptProposal(proposal, message)) {
            this.acceptProposal(proposal, message);
        } else {
            this.declineProposal(proposal, message);
        }
    }

    canAcceptProposal( proposal, message ) {
        let sender = this.getCharacter(message.from);
        let receiver = this.getCharacter(message.to);
        if ((this.state.map.turn - message.turn) >= K.boltonExpiration) {
            return false;
        }
        switch(proposal.type) {
            case 'MARRY':
                return this.canCharactersMarry(sender, receiver);
            case 'GIVE_MONEY':
                return true;
            case 'REQUEST_MONEY':
                return receiver.money >= proposal.intValue;
            case 'GIVE_LAND':
                var county = this.getCounty(proposal.stringValue);
                return sender.location == receiver.location 
                    && !sender.hidden && !receiver.hidden 
                    && county && county.owningCharacter == sender.guid;
            case 'REQUEST_LAND':
                var county = this.getCounty(proposal.stringValue);
                return sender.location == receiver.location
                    && !sender.hidden && !receiver.hidden
                    && county && county.owningCharacter == receiver.guid;
            case 'GIVE_ITEM':
                return sender.location === receiver.location && !sender.hidden && !receiver.hidden;
            case 'REQUEST_ITEM':
                return sender.location === receiver.location && !sender.hidden && !receiver.hidden;
            case 'SWEAR_FEALTY':
                return !sender.overlord && receiver.overlord !== sender.guid;
            case 'REQUEST_FEALTY':
                return sender.overlord !== receiver.guid;
            case 'GIVE_RELEASE':
                return receiver.imprisoner == sender.guid;
            case 'REQUEST_RELEASE':
                var prisoner = this.getCharacter(proposal.stringValue);
                return prisoner && prisoner.imprisoner == receiver.guid;
            case 'OFFER_VISIT':
            case 'REQUEST_VISIT':
                return sender.location != receiver.location;
            case 'GIVE_PRISONER':
                let givenPrisoner = this.getCharacter(proposal.stringValue);
                return givenPrisoner.imprisoner == sender.guid;
            case 'REQUEST_PRISONER':
                let requestedPrisoner = this.getCharacter(proposal.stringValue);
                return requestedPrisoner.imprisoner == receiver.guid;
            case 'NO_PLOTTING_PACT':
                return !this.doCharactersHavePact(sender.guid, receiver.guid, 'NO_PLOTTING');
            case 'OPEN_BORDERS_PACT':
                return !this.doCharactersHavePact(sender.guid, receiver.guid, 'OPEN_BORDERS');
            case 'CLOSED_BORDERS_PACT':
                return !this.doCharactersHavePact(sender.guid, receiver.guid, 'CLOSED_BORDERS');
            case 'SCIENCE_PACT':
                return !this.doCharactersHavePact(sender.guid, receiver.guid, 'SCIENCE');
            case 'MILITARY_PACT':
                return !this.doCharactersHavePact(sender.guid, receiver.guid, 'MILITARY');
            default:
                return true;
        }
    }

    acceptProposal( proposal, message ) {
        let toCharacter = this.getCharacter(message.to);
        let fromCharacter = this.getCharacter(message.from);
        let county = this.getCounty(proposal.stringValue);
        let intValue = proposal.intValue;
        switch(proposal.type) {
            case 'MARRY':
                this.marry(message.from, message.to);
                break;
            case 'GIVE_MONEY':
                this.changeCharacterMoney(toCharacter, 'Trade', intValue);
                break;
            case 'REQUEST_MONEY':
                this.changeCharacterMoney(toCharacter, 'Trade', -intValue);
                this.changeCharacterMoney(fromCharacter, 'Trade', intValue);
                break;
            case 'GIVE_LAND':
                this.setCountyOwner(county.guid, message.to);
                break;
            case 'REQUEST_LAND':
                this.setCountyOwner(county.guid, message.from);
                break;
            case 'GIVE_TROOPS':
                if (!this.addTroopsToCharacter(toCharacter.guid, intValue)) {
                    this.addTroopsToCharacter(fromCharacter.guid, intValue);
                }
                break;
            case 'REQUEST_TROOPS':
                this.transferTroopsBetweenCharacters(toCharacter.guid, fromCharacter.guid, intValue);
                break;
            case 'SWEAR_FEALTY':
                this.setCharacterOverlord(fromCharacter.guid, toCharacter.guid);
                break;
            case 'REQUEST_FEALTY':
                this.setCharacterOverlord(toCharacter.guid, fromCharacter.guid);
                break;
            case 'GIVE_ITEM':
                this.transferItemToCharacter(fromCharacter.guid, toCharacter.guid, proposal.stringValue);
                break;
            case 'REQUEST_ITEM':
                this.transferItemToCharacter(toCharacter.guid, fromCharacter.guid, proposal.stringValue);
                break;
            case 'GIVE_RELEASE':
                this.releasePrisoner(toCharacter.guid);
                break;
            case 'REQUEST_RELEASE':
                this.releasePrisoner(fromCharacter.guid);
                break;
            case 'OFFER_VISIT':
                toCharacter.location = fromCharacter.location;
                break;
            case 'REQUEST_VISIT':
                fromCharacter.location = toCharacter.location;
                break;
            case 'GIVE_PRISONER':
                let givenPrisoner = this.getCharacter(proposal.stringValue);
                givenPrisoner.imprisoner = toCharacter.guid;
                if (!toCharacter.hidden) {
                    givenPrisoner.location = toCharacter.location;
                }
                break;
            case 'REQUEST_PRISONER':
                let requestedPrisoner = this.getCharacter(proposal.stringValue);
                requestedPrisoner.imprisoner = fromCharacter.guid;
                if (!fromCharacter.hidden) {
                    requestedPrisoner.location = fromCharacter.location;
                }
                break;
            case 'NO_PLOTTING_PACT':
                this.giveCharacterPact(message.from, 'NO_PLOTTING', message.to);
                this.giveCharacterPact(message.to, 'NO_PLOTTING', message.from);
                break;
            case 'OPEN_BORDERS_PACT':
                this.giveCharacterPact(message.from, 'OPEN_BORDERS', message.to);
                this.giveCharacterPact(message.to, 'OPEN_BORDERS', message.from);
                break;
            case 'CLOSED_BORDERS_PACT':
                this.giveCharacterPact(message.from, 'CLOSED_BORDERS', message.to);
                this.giveCharacterPact(message.to, 'CLOSED_BORDERS', message.from);
                break;
            case 'SCIENCE_PACT':
                this.giveCharacterPact(message.from, 'SCIENCE', message.to);
                this.giveCharacterPact(message.to, 'SCIENCE', message.from);
                break;
            case 'MILITARY_PACT':
                this.giveCharacterPact(message.from, 'MILITARY', message.to);
                this.giveCharacterPact(message.to, 'MILITARY', message.from);
                break;
        }
    }

    declineProposal( proposal, message ) {
        let fromCharacter = this.getCharacter(message.from);
        switch(proposal.type) {
            case 'GIVE_MONEY':
                this.changeCharacterMoney(fromCharacter, 'Trade', proposal.intValue);
                break;
            case 'GIVE_TROOPS':
                this.addTroopsToCharacter(fromCharacter.guid, proposal.intValue);
                break;
            case 'GIVE_ITEM':
                var item = this.getItem(proposal.stringValue);
                item.owningCharacter = fromCharacter.guid;
                break;
        }
    }

    sendMessage( fromCharacterId, toCharacterId, message, proposals, proposalAcceptType = 'any', miscData = null ) {
        proposals = proposals || [];
        let messageObj = {
            guid: this.chance.guid(),
            from: fromCharacterId,
            to: toCharacterId,
            message: message,
            proposals: [],
            proposalAcceptType: proposalAcceptType,
            miscData: miscData,
            read: false,
            turn: this.state.map.turn
        };
        for (let i = 0; i < proposals.length; i++) {
            let proposal = proposals[i];
            proposal.receivedResponse = false;
            proposal.response = null;
            proposal.intValue = parseInt(proposal.intValue);
            let fromCharacter = this.getCharacter(fromCharacterId);
            let toCharacter = this.getCharacter(toCharacterId);
            if (this.canAcceptProposal(proposal, messageObj)) {
                switch(proposal.type) {
                    case 'GIVE_MONEY':
                        if (fromCharacter.money >= proposal.intValue) {
                            this.changeCharacterMoney(fromCharacter, 'Trade', -proposal.intValue);
                            if (fromCharacter.guid !== toCharacter.guid) {
                                fromCharacter.stats.moneyGifted += proposal.intValue;
                            }
                            messageObj.proposals.push(proposal);
                        }
                        break;
                    case 'GIVE_ITEM':
                        var item = this.getItem(proposal.stringValue);
                        if (item && item.owningCharacter == fromCharacterId && !item.active) {
                            item.owningCharacter = null;
                            messageObj.proposals.push(proposal);
                        }
                        break;
                    default:
                        messageObj.proposals.push(proposal);
                        break;
                }
            }
        }
        // Add message to state.
        this.state.messages.push(messageObj);

        let toCharacter = this.getCharacter(toCharacterId);
        let fromCharacter = this.getCharacter(fromCharacterId);
        if (toCharacter && fromCharacter) {
            // Record message stats.
            if (fromCharacterId != toCharacterId && fromCharacter.owningPlayer != toCharacter.owningPlayer) {
                toCharacter.stats.messagesReceived++;
            }

            // If sending message to a character controlled by the same player
            // automatically accept all proposals.
            if (fromCharacter.owningPlayer === toCharacter.owningPlayer 
                && fromCharacter.owningPlayer && toCharacter.owningPlayer)
            {
                messageObj.proposals.forEach(proposal => {
                    this.setProposalResponse(messageObj, proposal, true);
                });
            }

            if (process.env.NODE_ENV === 'development') {
                messageObj.proposals.forEach(proposal => {
                    this.setProposalResponse(messageObj, proposal, true);
                });
            }
            
            if (toCharacter.owningPlayer) {
                this.emit('messageSent', {
                    receiverPlayer: this.getPlayer(toCharacter.owningPlayer),
                    senderCharacter: fromCharacter,
                    receiverCharacter: toCharacter,
                    message: message
                });
            }
        }

        return messageObj;
    }

    markMessageRead( messageId ) {
        let msg = this.state.messages.find(msg => msg.guid == messageId);
        if (msg) {
            msg.read = true;
        }
    }

    addNews( characterId, message ) {
        let character = this.getCharacter(characterId);
        if (character == null || character.cooldowns.newsfeed === 0) {
            if (character) {
                character.cooldowns.newsfeed = K.newsfeedCooldown;
            }
            if (this.state.newsIndex == null) {
                this.state.newsIndex = 1;
            }
            this.state.news.push({
                index: this.state.newsIndex++,
                turn: this.state.map.turn,
                author: characterId,
                message: message
            });
        }
    }

    transferTroopsBetweenCharacters( fromCharacterId, toCharacterId, amount ) {
        let fromCounties = this.getCharacterCounties(fromCharacterId);
        let toCounties = this.getCharacterCounties(toCharacterId);
        if (fromCounties.length > 0 && toCounties.length > 0) {
            let troopsSum = this.getCharacterReservedTroopsCount(fromCharacterId);
            if (troopsSum >= amount) {
                let troopAcc = 0;
                let index = 0;
                while(troopAcc != amount) {
                    fromCounties[index++].troops--;
                    troopAcc++;
                    if (index >= fromCounties.length) index = 0;
                }
                index = 0;
                while(troopAcc != 0) {
                    toCounties[index++].troops++;
                    troopAcc--;
                    if (index >= toCounties.length) index = 0;
                }
            }
            return true;
        }
        return false;
    }

    getCharacterReservedTroopsCount( characterId ) {
        let counties = this.getCharacterCounties(characterId);
        return counties.reduce((a, county) => { return a + county.troops; }, 0);
    }

    getCharacterIncomeTurnMoneyRate( characterId ) {
        let character = this.getCharacter(characterId);
        let moneyRate = 0;
        let breakdown = {
            'Counties': 0,
            'Taxes Paid': 0,
            'Taxes Levied': 0,
            'Marriage': 0,
        };
        this.getCharacterCounties(character.guid).forEach(county => {
            let income = county.money;
            moneyRate += income;
            breakdown['Counties'] += income;
        });
        if (character.overlord) {
            let overlord = this.getCharacter(character.overlord);
            let tax = (breakdown['Counties'] * overlord.taxRate);
            moneyRate -= tax;
            breakdown['Taxes Paid'] -= tax;
        }
        let vassals = this.getCharacterUnderlords(character.guid);
        vassals.forEach(vassal => {
            let income = 0;
            this.getCharacterCounties(vassal.guid).forEach(county => {
                income += county.moneyRate * this.config.incomeTurnInterval;
            });
            let tax = income * character.taxRate;
            moneyRate += tax;
            breakdown['Taxes Levied'] += tax;
        });
        if (character.spouse) {
            let bonus = this.getGameSettingValue(character.guid, "spouseMoneyBonus");
            moneyRate += bonus;
            breakdown['Marriage'] += bonus;
        }
        for (let key in breakdown) {
            breakdown[key] = Math.floor(breakdown[key]);
        }
        return {
            sum: moneyRate,
            breakdown
        };
    }

    getCharacterTurnMoneyRate( characterId ) {
        let character = this.getCharacter(characterId);
        let breakdown = {
            'Base': 0,
            'Items': 0,
            'Focus': 0,
            'Marriage': 0,
            'Army Upkeep': 0,
            'Building Upkeep': 0,
            'Fines': 0
        };
        let moneyRate = 0;

        this.getCharacterCounties(character.guid).forEach(county => {
            county.buildings.forEach(building => {
                let upkeep = building.upkeep;
                moneyRate -= upkeep;
                breakdown['Building Upkeep'] -= upkeep;
            });
        });

        this.getCharacterArmies(character.guid).forEach(army => {
            let upkeep = (army.troops * this.getGameSettingValue(character.guid, "armyCostPerTroop")) 
                + this.getGameSettingValue(character.guid, "baseArmyCost");
            moneyRate -= upkeep;
            breakdown['Army Upkeep'] -= upkeep;
        });

        moneyRate += K.characterResourceRates.money;
        breakdown['Base'] = K.characterResourceRates.money;

        if (character.spouse) {
            let spouseMoneyBonus = this.getGameSettingValue(character.guid, "spouseMoneyBonus");
            moneyRate += spouseMoneyBonus;
            breakdown['Marriage'] += spouseMoneyBonus;
        }

        let itemMoney = this.getGameSettingValue(character.guid, "characterResourceRates/money") - K.characterResourceRates.money;
        
        if (character.focus === 'money') {
            let focusMoney = K.focus.money.effect.find(effect => effect.path === 'characterResourceRates/money').add;
            moneyRate += focusMoney;
            breakdown['Focus'] += focusMoney;
            itemMoney -= focusMoney;    
        }

        moneyRate += itemMoney;
        breakdown['Items'] += itemMoney;

        if (character.fines) {
            moneyRate -= character.fines;
            breakdown['Fines'] -= character.fines;
        }

        for (let key in breakdown) {
            breakdown[key] = Math.floor(breakdown[key]);
        }
        return {
            sum: moneyRate,
            breakdown
        };
    }

    getCharacterTurnTroopRate( characterId ) {
        let troopRate = 0;
        this.getCharacterCounties(characterId).forEach(county => {
            troopRate += county.troopRate;
        });
        return {
            sum: troopRate,
            breakdown: {
                ['Counties']: troopRate
            }
        };
    }

    getCharacterTurnScienceRate( characterId ) {
        let character = this.getCharacter(characterId);
        let scienceRate = 0;
        let breakdown = {
            'Base': 0,
            'Focus': 0,
            'Items': 0,
            'Counties': 0
        };

        scienceRate += K.characterResourceRates.science;
        breakdown['Base'] += K.characterResourceRates.science;

        let fromItems = this.getGameSettingValue(characterId, 'characterResourceRates/science') - K.characterResourceRates.science;
        if (character.focus === 'science') {
            let fromFocus = K.focus.science.effect.find(effect => effect.path === 'characterResourceRates/science').add;
            scienceRate += fromFocus;
            breakdown['Focus'] += fromFocus;
            fromItems -= fromFocus;    
        }
        scienceRate += fromItems;
        breakdown['Items'] += fromItems;

        let fromCounties = 0;
        this.getCharacterCounties(characterId).forEach(county => {
            fromCounties += county.scienceRate;
        });
        scienceRate += fromCounties;
        breakdown['Counties'] += fromCounties;

        return {
            sum: scienceRate,
            breakdown
        };
    }

    removeTroopsFromCharacter( characterId, amount ) {
        let counties = this.getCharacterCounties(characterId);
        if (counties.length > 0) {
            let troopSum = this.getCharacterReservedTroopsCount(characterId);
            if (troopSum >= amount) {
                let troopAcc = 0;
                let index = 0;
                while(troopAcc != amount) {
                    counties[index++].troops--;
                    troopAcc++;
                    if (index >= counties.length) index = 0;
                }
                return true;
            }
        }
        return false;
    }

    addTroopsToCharacter( characterId, amount ) {
        let counties = this.getCharacterCounties(characterId);
        if (counties.length > 0) {
            let troopAcc = amount;
            let index = 0;
            while(troopAcc != 0) {
                counties[index++].troops++;
                troopAcc--;
                if (index >= counties.length) index = 0;
            }
            return true;
        }
        return false;
    }

    canCharactersMarry( character1, character2 ) {
        return character1.spouse == null && character2.spouse == null
            && character1.guid != character2.guid
            && character1.alive && character2.alive
            && !character1.imprisoner && !character2.imprisoner;
    }

    doCharactersHavePact( character1Id, character2Id, type ) {
        let character = this.getCharacter(character1Id);
        if (character) {
            return character.effects.findIndex(effect => {
                return effect.type === 'pact' && effect.pactType === type && effect.target === character2Id;
            }) !== -1;
        }
        return false;
    }

    marry( character1_id, character2_id ) {
        let character1 = this.getCharacter(character1_id);
        let character2 = this.getCharacter(character2_id);
        if (this.canCharactersMarry(character1, character2)) {
            character1.spouse = character2_id;
            character2.spouse = character1_id;
            let maidenName = character2.house;
            character2.house = character1.house;
            if (character1.overlord == character2.overlord || character1.overlord == character2_id) {
                // already have same overlord, do nothing.
            } else if (character1.overlord) {
                this.setCharacterOverlord(character2_id, character1.overlord);
            } else if (character2.overlord) {
                this.setCharacterOverlord(character1_id, character2.overlord);
            }
            this.addNews(null, 
                `${chLink(character1)} has married ${chLink(character2)} (formally ${character2.name} ${maidenName}).`);
        }
    }

    changeCharacterResearch( player, characterId, researchIndex ) {
        let character = this.getCharacter(characterId);
        if (character.owningPlayer == player.username && character.research.length > researchIndex && researchIndex >= 0) {
            character.activeResearchIndex = researchIndex;
        }
    }

    getItemChanceEffect( item ) {
        let weights = item.chanceEffect.map(e => e.chance);
        let chosen = this.chance.weighted(item.chanceEffect, weights);
        return chosen;
    }

    activateItem( player, itemId ) {
        let item = this.getItem(itemId);
        if ((player == null || this.doesItemBelongToPlayer(item, player)) && !item.active) {
            if (item.type === 'building') {
                let character = this.getCharacter(item.owningCharacter);
                if (character && !character.hidden) {
                    let county = this.getCounty(character.location);
                    if (county && county.buildings.findIndex(building => building.id === item.id) === -1) {
                        let building = Object.assign({}, item);
                        county.buildings.push(building);
                        let index = this.state.items.findIndex(item => item.guid === itemId);
                        this.state.items.splice(index, 1);
                    }
                }
            } else {
                item.active = true;
                if (item.chanceEffect) {
                    let chanceEffect = this.getItemChanceEffect(item);
                    this.applyItemAction(item.owningCharacter, item, chanceEffect.action, chanceEffect.value);
                }
                if (item.action) {
                    this.applyItemAction(item.owningCharacter, item, item.action, item.value);
                }
                if (item.duration) {
                    item.expires = this.state.map.turn + item.duration;
                } else if (item.type === 'potion') {
                    let itemIndex = this.state.items.findIndex(stateItem => stateItem.guid === item.guid);
                    this.state.items.splice(itemIndex, 1);   
                }
            }
        }
    }

    applyItemAction( characterId, item, action, value = 0 ) {
        let character = this.getCharacter(characterId);
        switch(action) {
            case 'hide':
                character.hidden = true;
                character.cooldowns.hidden = item.duration;
                break;
            case 'kill':
                this.killCharacter(characterId);
                break;
            case 'changeAge':
                character.age += value;
                if (character.age < 1) {
                    character.age = 1;
                }
                break;
            case 'setAge':
                character.age = value;
                break;
            case 'effect':
                item.effect = value;
                break;
            case 'setMovementCooldown':
                character.cooldowns.movement = value;
                break;
            case 'bodyswap':
                // Unswap ourself if we are currently bodyswapped.
                if (character.bodyswap) {
                    this.swapCharacterOwners(character.guid, character.bodyswap, true);
                }
                // Find another kitten to swap bodies with.
                let candidates = this.state.characters.filter(c => c.alive && c.guid !== characterId && c.owningPlayer);
                if (candidates.length > 0) {
                    let otherCharacter = this.chance.pickone(candidates);
                    // Unswap to other kitten if they are currently bodyswapped.
                    if (otherCharacter.bodyswap) {
                        this.swapCharacterOwners(otherCharacter.guid, otherCharacter.bodyswap, true);
                    }
                    // Swap bodies with other kitten.
                    this.swapCharacterOwners(characterId, otherCharacter.guid);
                    character.cooldowns.bodyswap = item.duration;
                    otherCharacter.cooldowns.bodyswap = item.duration;
                }
                break;
        }
    }

    swapCharacterOwners( character1_id, character2_id, isUnswap = false ) {
        let character1 = this.getCharacter(character1_id);
        let character2 = this.getCharacter(character2_id);
        if (character1 && character2) {
            if (isUnswap) {
                character1.bodyswap = null;
                character2.bodyswap = null;
            } else {
                character1.bodyswap = character2.guid;
                character2.bodyswap = character1.guid;
            }
            let owner1 = character1.owningPlayer;
            let owner2 = character2.owningPlayer;
            character1.owningPlayer = owner2;
            character2.owningPlayer = owner1;
        }
    }

    deactivateItem( player, itemId ) {
        let item = this.getItem(itemId);
        if (this.doesItemBelongToPlayer(item, player) && item.active) {
            item.active = false;
        }
    }

    mixPotion( player, itemId ) {
        let item = this.getItem(itemId);
        if (this.doesItemBelongToPlayer(item, player) && item.type === 'potion' && !item.active && item.combinesWith) {
            let otherItem = this.state.items.find(otherItem => {
                return otherItem.owningCharacter === item.owningCharacter && otherItem.type === 'potion' &&
                !otherItem.active && otherItem.id === item.combinesWith.potion;
            });
            if (otherItem) {
                let itemIndex = this.state.items.findIndex(i => i.guid === item.guid);
                let otherItemIndex = this.state.items.findIndex(i => i.guid === otherItem.guid);
                let result = TECHNOLOGY.find(tech => tech.id === item.combinesWith.result);
                let mixedPotion = Object.assign({
                    guid: this.chance.guid(),
                    owningCharacter: item.owningCharacter
                }, result);
                if (mixedPotion.expires) {
                    mixedPotion.expires += this.state.map.turn;
                }
                this.state.items.push(mixedPotion);
                this.state.items.splice(itemIndex, 1);
                this.state.items.splice(otherItemIndex, 1);
            }
        }
    }

    doesItemBelongToPlayer( item, player ) {
        let character = this.characterMap[item.owningCharacter];
        if (character) {
            return character.owningPlayer == player.username;
        } else { 
            return false;
        }
    }

    removeArmy( armyId ) {
        let army = this.armyMap[armyId];
        this.armyMap[armyId] = null;
        let mainIndex = this.state.armies.findIndex(a => a.guid == armyId);
        this.state.armies.splice(mainIndex, 1);
    }

    mergeArmies( county ) {
        let armies = this.state.armies.filter(army => army.location === county.guid);
        let keyArmies = {};
        for (let i = armies.length - 1; i >= 0; i--) {
            let army = armies[i];
            if (keyArmies[army.owningCharacter]) {
                let keyArmy = keyArmies[army.owningCharacter];
                keyArmy.troops += army.troops;
                keyArmy.cooldown = Math.max(keyArmy.cooldown, army.cooldown);
                let keyArmyOrderCount = keyArmy.orders ? keyArmy.orders.length : 0;
                let otherArmyOrderCounty = army.orders ? army.orders.length : 0;
                keyArmy.orders = (otherArmyOrderCounty > keyArmyOrderCount) ? army.orders : keyArmy.orders;
                this.removeArmy(army.guid);
            } else {
                keyArmies[army.owningCharacter] = army;
            }
        }
    }

    getArmyCost( armyId ) {
        let army = this.getArmy(armyId);
        let armyCostPerTroop = this.getGameSettingValue(army.owningCharacter, "armyCostPerTroop");
        let baseArmyCost = this.getGameSettingValue(army.owningCharacter, "baseArmyCost");
        let cost = Math.floor(baseArmyCost + (armyCostPerTroop * army.troops));
        return cost;
    }

    makeArmy( player, countyId, amount ) {
        let county = this.getCounty(countyId);
        let character = this.getCharacter(county.owningCharacter);
        if ((player == null || this.doesItemBelongToPlayer(county, player)) && county && county.troops >= amount && character && !character.imprisoner) {
            county.troops -= amount;
            let existingArmy = this.state.armies.find(army => army.location === county.guid && army.owningCharacter === county.owningCharacter);
            if (existingArmy) {
                existingArmy.troops += amount;
                return existingArmy;
            } else {
                let character = this.getCharacter(county.owningCharacter);
                let army = {
                    guid: this.chance.guid(),
                    location: countyId,
                    cooldown: this.getGameSettingValue(character.guid, "movementCooldown/afterRaise"),
                    troops: amount,
                    owningCharacter: county.owningCharacter
                }
                this.state.armies.push(army);
                this.armyMap[army.guid] = army;
                return army;
            }
        } else if (player) {
            this.logger.warn(`Player ${player.username} cannot make this army!`);
        }
        return null;
    }

    areCharactersFriendly( character1_id, character2_id ) {
        let character1 = this.getCharacter(character1_id);
        let character2 = this.getCharacter(character2_id);
        if (character1 && character2) {
            let vassals1 = this.getCharacterUnderlords(character1_id);
            let vassals2 = this.getCharacterUnderlords(character2_id);
            return character1.house === character2.house ||
                character1.overlord === character2_id ||
                character2.overlord === character1_id ||
                vassals1.find(v => v.guid === character2_id) ||
                vassals2.find(v => v.guid === character1_id);
        } else {    
            return false;
        }
    }

    isCountyFriendly( countyId, characterId ) {
        let county = this.getCounty(countyId);
        if (county && county.owningCharacter) {
            return this.areCharactersFriendly(characterId, county.owningCharacter);
        }
        return false;
    }

    getCountyBorderState( county1_id, county2_id ) {
        let county1 = this.getCounty(county1_id);
        let county2 = this.getCounty(county2_id);
        if (county1 && county2) {
            if (county1.impassable || county2.impassable) {
                return 'IMPASSABLE';
            } else if (county1.adjacent.indexOf(county2_id) === -1) {
                return 'NOT_ADJACENT';
            } else {
                let owner1 = this.getCharacter(county1.owningCharacter);
                let owner2 = this.getCharacter(county2.owningCharacter);
                if (owner1 && owner2) {
                    if (owner1.guid === owner2.guid) {
                        return 'INNER';
                    } else if (this.doCharactersHavePact(owner1.guid, owner2.guid, 'CLOSED_BORDERS')) {
                        return 'CLOSED';
                    } else if (this.doCharactersHavePact(owner1.guid, owner2.guid, 'OPEN_BORDERS') || 
                               this.areCharactersFriendly(owner1.guid, owner2.guid)) {
                        return 'OPEN';
                    } else {
                        return 'ENEMY';
                    }
                } else {
                    return 'NEUTRAL';
                }
            }
        }
    }

    getCountyDefendMultiplier( countyId ) {
        let county = this.getCounty(countyId);
        let defenseMultiplier = 0;
        [...county.buildings, ...county.specialists, ...county.negatives].forEach(obj => {
            if (obj.countyEffect) {
                obj.countyEffect.filter(effect => effect.path === 'defenseMultiplier').forEach(effect => {
                    defenseMultiplier = this.modifyValueWithEffect(defenseMultiplier, effect);
                });
            }
        });
        if (county.owningCharacter) {
            defenseMultiplier += this.getDefendMultiplier(county.owningCharacter);
            let owner = this.getCharacter(county.owningCharacter);
            if (!owner.hidden && owner.location === county.guid) {
                defenseMultiplier += this.getGameSettingValue(owner.guid, 'battleMultipliers/characterLocationDefenseBonus');
            }
        } else {
            defenseMultiplier += K.battleMultipliers.neutralCountyDefending;
        }
        return defenseMultiplier;
    }

    moveArmyWithOrders( player, armyId, orders ) {
        let army = this.getArmy(armyId);
        if (army && orders.length > 0 && this.doesItemBelongToPlayer(army, player)) {
            army.standDownQueued = false;
            let location = orders[0];
            if (this.moveArmy(armyId, location)) {
                orders.shift();
            }
            army.orders = orders;
        }
    }

    moveArmy( armyId, countyId ) {
        let army = this.getArmy(armyId);
        if (!army) return;
        let character = this.getCharacter(army.owningCharacter);
        if (!character.imprisoner && army.cooldown == 0) {
            let borderState = this.getCountyBorderState(army.location, countyId);
            if (borderState !== 'CLOSED' && borderState !== 'NOT_ADJACENT') {
                army.location = countyId;
                if (borderState === 'OPEN' || borderState === 'INNER') {
                    army.cooldown += this.getGameSettingValue(character.guid, "movementCooldown/friendlyTerritory");
                } else {
                    army.cooldown += this.getGameSettingValue(character.guid, "movementCooldown/enemyTerritory");
                }
                let toCounty = this.getCounty(army.location);
                this.mergeArmies(toCounty);
                if ((borderState === 'ENEMY' || borderState === 'NEUTRAL') && toCounty.owningCharacter !== army.owningCharacter) {
                    this.performBattle(toCounty, army);
                }
                return true;
            }
        }
        return false;
    }

    standDownArmy( player, armyId ) {
        let army = this.getArmy(armyId);
        if (!army) return;

        let character = this.getCharacter(army.owningCharacter);
        if ((player == null || this.doesItemBelongToPlayer(army, player)) && !character.imprisoner) {
            if (army.cooldown == 0) {
                let county = this.getCounty(army.location);
                county.troops += army.troops;
                if (county.owningCharacter != army.owningCharacter) {
                    this.sendMessage(army.owningCharacter, county.owningCharacter, 
                        `I have gifted you ${army.troops} troops in ${coLink(county)}}.`);
                }
                this.removeArmy(armyId);
            } else {
                army.orders = null;
                army.standDownQueued = true;
            }
        }
    }

    getAttackMultiplier( characterId ) {
        let character = this.getCharacter(characterId);
        let multiplier = this.getGameSettingValue(character.guid, "battleMultipliers/attacking");
        return multiplier;
    }

    getDefendMultiplier( characterId ) {
        let character = this.getCharacter(characterId);
        let multiplier = this.getGameSettingValue(character.guid, "battleMultipliers/defending");
        return multiplier;
    }

    performBattle( county, attackingArmy ) {
        let attackingCharacter = this.getCharacter(attackingArmy.owningCharacter);
        let attackingBonusMultiplier = this.getAttackMultiplier(attackingArmy.owningCharacter);
        // First the attacking army attacks all the defending standing armies.
        let defendingArmies = this.state.armies.filter(army => 
            army.location === county.guid &&
            army.guid !== attackingArmy.guid &&
            !this.areCharactersFriendly(army.owningCharacter, attackingArmy.owningCharacter));
        defendingArmies.sort((a, b) => b.troops - a.troops);
        let battles = [];
        for (let i = 0; i < defendingArmies.length; i++) {
            let defendingArmy = defendingArmies[i];
            let defendingCharacter = this.getCharacter(defendingArmy.owningCharacter);
            let defendingBonusMultiplier = this.getDefendMultiplier(defendingArmy.owningCharacter);
            if (!defendingCharacter.hidden && defendingCharacter.location === defendingArmy.location) {
                defendingBonusMultiplier += this.getGameSettingValue(defendingCharacter.guid, 'battleMultipliers/characterLocationDefenseBonus');
            }
            let defendPower = Math.floor(defendingArmy.troops * defendingBonusMultiplier);
            let attackPower = Math.floor(attackingArmy.troops * attackingBonusMultiplier);

            battles.push({
                type: 'army',
                attackingBonusMultiplier,
                defendingBonusMultiplier,
                attackingTroops: attackingArmy.troops,
                defendingTroops: defendingArmy.troops,
                attackPower,
                defendPower,
                attackingCharacter,
                defendingCharacter: defendingCharacter
            });

            attackingCharacter.stats.troopsKilled += Math.min(attackPower, defendingArmy.troops);
            attackingArmy.troops -= defendPower;
            defendingArmy.troops -= attackPower;

            defendingCharacter.stats.lastTurnAttackOrDefended = this.state.map.turn;
            attackingCharacter.stats.lastTurnAttackOrDefended = this.state.map.turn;
            
            if (defendingArmy.troops <= 0) {
                defendingArmy.troops = 0;
            }
            if (attackingArmy.troops <= 0) {
                attackingArmy.troops = 0;
                break;
            };
        }

        let captureCounty = false;

        // Then, if the attacking army is still alive, it attacks the county's troops.
        if (attackingArmy.troops > 0) {
            attackingCharacter.stats.lastTurnAttackOrDefended = this.state.map.turn;
            let defendingCharacter = this.getCharacter(county.owningCharacter);
            let defendingBonusMultiplier = this.getCountyDefendMultiplier(county.guid);
            let defendPower = Math.floor(county.troops * defendingBonusMultiplier);
            let attackPower = Math.floor(attackingArmy.troops * attackingBonusMultiplier);

            battles.push({
                type: 'county',
                attackingTroops: attackingArmy.troops,
                defendingTroops: county.troops,
                attackPower: attackPower,
                defendPower: defendPower,
                attackingCharacter,
                defendingCharacter: defendingCharacter,
                attackingBonusMultiplier,
                defendingBonusMultiplier,
                county: county
            });

            attackingCharacter.stats.troopsKilled += Math.min(county.troops, attackPower);
            county.troops -= attackPower;
            attackingArmy.troops -= defendPower;

            if (attackPower > defendPower) {
                captureCounty = true;
            }
            if (county.troops < 0) {
                county.troops = 0;
            }
        }

        if (captureCounty) {
            // Release any prisoners at location.
            this.state.characters.filter(c => {
                return c.location == county.guid && c.imprisoner != null && c.imprisoner != attackingArmy.owningCharacter && c.alive;
            }).forEach(prisoner => {
                this.releasePrisoner(prisoner.guid);
            });

            // Imprison character at location, that controls that location.
            let charactersAtLocation = this.state.characters.filter(char => 
                char.location == county.guid && !char.hidden && char.alive && !char.imprisoner && county.owningCharacter === char.guid);
            if (charactersAtLocation.length > 0) {
                let characterToImprison = this.chance.pickone(charactersAtLocation);
                this.imprisonCharacter(attackingArmy.owningCharacter, characterToImprison.guid);
            }

            // Scatter other characters at location.
            this.state.characters.filter(char => char.location === county.guid && char.guid !== attackingCharacter.guid)
                .forEach(otherCharacter => {
                    if (county.adjacent && county.adjacent.length > 0) {
                        otherCharacter.location = this.chance.pickone(county.adjacent);
                    }
                });

            this.setCountyOwner(county.guid, attackingArmy.owningCharacter);

            if (county.hasTreasure) {
                county.hasTreasure = false;
                let loot = this.chance.weighted(K.countyTreasure, K.countyTreasure.map(treasure => treasure.weight));
                switch(loot.type) {
                    case 'money':
                        this.changeCharacterMoney(attackingCharacter, "Treasure", loot.amount);
                        this.sendMessage(attackingCharacter.guid, attackingCharacter.guid, 
                            `I have looted ${loot.amount}btc from a treasure chest!`);
                        break;
                    case 'potion':
                        this.giveCharacterRandomItemOfType(attackingCharacter.guid, 'potion');
                        break;
                    case 'book':
                        this.giveCharacterRandomItemOfType(attackingCharacter.guid, 'book');
                        break;
                }
                attackingCharacter.stats.treasureLooted++;
            }
        }

        // Send battle report message.
        if (battles.length > 0) {
            let lines = battles.map(battle => {
                if (battle.type === 'army') {
                    return `${battle.attackingTroops} troops (${battle.attackPower} damage from ${battle.attackingBonusMultiplier}x multiplier) 
                        vs ${battle.defendingCharacter.house}'s ${battle.defendingTroops} troops (${battle.defendPower} damage from ${battle.defendingBonusMultiplier}x multiplier)`;
                } else if (battle.type === 'county') {
                    return `${battle.attackingTroops} troops (${battle.attackPower} damage from ${battle.attackingBonusMultiplier}x multiplier) 
                        vs ${battle.county.name}'s ${battle.defendingTroops} troops (${battle.defendPower} damage from ${battle.defendingBonusMultiplier}x multiplier)`;
                }
            });
            let message = `${county.name} Battle Report: ${chLink(attackingCharacter)} ${lines.join(', ')}`;
            this.sendMessage(attackingCharacter.guid, attackingCharacter.guid, message);
            let otherCharactersInvolved = battles.reduce((obj, battle) => {
                if (battle.defendingCharacter && !obj[battle.defendingCharacter.guid]) {
                    obj[battle.defendingCharacter.guid] = battle.defendingCharacter.guid;
                }
                return obj;
            }, {});
            for (let guid in otherCharactersInvolved) {
                this.sendMessage(attackingCharacter.guid, guid, message);
            }
        }

        // Cleanup.
        let armiesAtCounty = this.state.armies.filter(army => army.location === county.guid);
        for (let i = 0; i < armiesAtCounty.length; i++) {
            let army = armiesAtCounty[i];
            if (army.troops <= 0) {
                this.removeArmy(army.guid);
            }
        }
        return battles;
    }

    razeCountyWithArmy( player, armyId ) {
        let army = this.getArmy(armyId);
        if (army && this.doesItemBelongToPlayer(army, player)) {
            let county = this.getCounty(army.location);
            let troopCost = county.size * K.countyRazeTroops;
            if (army.troops >= troopCost) {
                county.razed = true;
                county.specialists = [];
                county.negatives = [];
                army.troops -= troopCost;
                if (army.troops <= 0) {
                    this.removeArmy(armyId);
                }
                let character = this.getCharacter(army.owningCharacter);
                if (character) {
                    character.stats.razedCounties++;
                }
                return true;
            }
        }
        return false;
    }

    repairCounty( player, countyId ) {
        let county = this.getCounty(countyId);
        if (county && this.doesItemBelongToPlayer(county, player) && county.razed) {
            let owner = this.getCharacter(county.owningCharacter);
            let repairCost = county.size * K.countyRepairCost;
            if (owner.money >= repairCost) {
                this.changeCharacterMoney(owner, "Repair", -repairCost);
                county.razed = false;
                owner.stats.repairedCounties++;
                return true;
            }
        }
        return false;
    }

    performPlot( player, characterId, targetId, plotType, potionGuid = null ) {
        let character = this.getCharacter(characterId);
        let target = this.getCharacter(targetId);
        let plotCost = this.getGameSettingValue(characterId, `plotting/${plotType}/cost`);
        if ((player == null || character.owningPlayer == player.username) 
            && !this.doCharactersHavePact(characterId, targetId, 'NO_PLOTTING')
            && character.cooldowns.plotting == 0
            && character.money >= plotCost
            && character.location === target.location
            && character.alive && target.alive
            && !character.imprisoner)
        {
            this.changeCharacterMoney(character, 'Plotting', -plotCost);
            character.cooldowns.plotting = this.getGameSettingValue(characterId, 'plotting/cooldown');
            let succeedPublic, succeedSilent, failPublic, failSilent, defense, result;
            defense = this.getGameSettingValue(targetId, `plotting/${plotType}/defense`);
            succeedPublic = this.getGameSettingValue(characterId, `plotting/${plotType}/succeedPublic`);
            succeedSilent = this.getGameSettingValue(characterId, `plotting/${plotType}/succeedSilent`);
            failPublic = this.getGameSettingValue(characterId, `plotting/${plotType}/failPublic`);
            failSilent = this.getGameSettingValue(characterId, `plotting/${plotType}/failSilent`);
            failPublic += defense;
            const SUCCESS_PUBLIC = 1;
            const SUCCESS_SILENT = 2;
            const FAIL_PUBLIC = 3;
            const FAIL_SILENT = 4;
            
            result = this.chance.weighted(
                    [SUCCESS_PUBLIC, SUCCESS_SILENT, FAIL_PUBLIC, FAIL_SILENT],
                    [succeedPublic, succeedSilent, failPublic, failSilent]);

            switch(plotType) {
                case 'murder':
                    if (result == SUCCESS_PUBLIC || result == SUCCESS_SILENT) {
                        this.killCharacter(target.guid);
                        character.stats.charactersKilled.push(target.guid);
                    }
                    if (result == SUCCESS_PUBLIC) {
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} has been murdered in a rather obvious fashion.`);
                        this.addNews(null, `${chLink(target)} has been murdered by ${chLink(character)}.`);
                    } else if (result == SUCCESS_SILENT) {
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} has been murdered. No-one knows who did it.`);
                    } else if (result == FAIL_PUBLIC) {
                        this.sendMessage(targetId, targetId, 
                            `${chLink(character)} was plotting to murder me!`);
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} caught wind of my plot to murder.`);
                        this.addNews(null, 
                            `${chLink(character)} was caught plotting to murder ${chLink(target)}.`);
                            this.imprisonCharacter(targetId, characterId);
                    } else {
                        this.sendMessage(characterId, characterId, 
                            `I have failed to murder ${chLink(target)}. No-one knows about this.`);
                    }
                    return result;
                case 'steal':
                    let percentToSteal = this.getGameSettingValue(character.guid, 'plotting/steal/percentToSteal');
                    let moneyStolen = 0;
                    if (result == SUCCESS_PUBLIC || result == SUCCESS_SILENT) {
                        let moneyToSteal = Math.floor(target.money * 0.25);
                        moneyStolen = moneyToSteal;
                        if (moneyToSteal > 0) {
                            this.changeCharacterMoney(character, 'Steal Plot', moneyToSteal);
                            this.changeCharacterMoney(character, 'Steal Plot', -moneyToSteal);
                        }
                        let possibleItemsToSteal = this.state.items.filter(item => item.owningCharacter == target.guid && !item.active);
                        if (possibleItemsToSteal.length > 0) {
                            let itemToSteal = this.chance.pickone(possibleItemsToSteal);
                            itemToSteal.owningCharacter = character.guid;
                        }
                        character.stats.charactersStolenFrom.push(target.guid);
                    }
                    if (result == SUCCESS_PUBLIC) {
                        this.addNews(null, `${chLink(character)} has stolen ${moneyStolen}btc from ${chLink(target)}.`);
                        this.sendMessage(targetId, targetId, `${chLink(character)} has stolen ${moneyStolen}btc from me!`);
                    } else if (result == SUCCESS_SILENT) {
                        this.sendMessage(characterId, characterId, 
                            `I have stolen ${moneyStolen}btc from ${chLink(target)}.`);
                    } else if (result == FAIL_PUBLIC) {
                        this.addNews(null, `${chLink(character)} was caught plotting to steal money from ${chLink(target)}.`);
                        this.sendMessage(targetId, targetId, `${chLink(character)} was plotting to steal from me!`);
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} caught wind of my plot to steal.`);
                    } else if (result == FAIL_SILENT) {
                        this.sendMessage(characterId, characterId, 
                            `My plot to steal from ${chLink(target)} has failed. No-one knows about this.`);
                    }
                    return result;
                case 'poison':
                    let potion = this.state.items.find(item => item.guid === potionGuid);
                    if (potion && potion.owningCharacter === characterId && potion.type === 'potion') {
                        if (result == SUCCESS_PUBLIC || result == SUCCESS_SILENT) {
                            potion.owningCharacter = targetId;
                            this.activateItem(null, potionGuid);
                        } else {
                            let itemIndex = this.state.items.findIndex(item => item.guid === potionGuid);
                            this.state.items.splice(itemIndex, 1);
                        }
                        if (result == SUCCESS_PUBLIC) {
                            this.addNews(null, `${chLink(character)} has poisoned ${chLink(target)} with ${potion.name}.`);
                            this.sendMessage(targetId, targetId, `${chLink(character)} has poisoned me with ${potion.name}!`);
                        } else if (result == SUCCESS_SILENT) {
                            this.sendMessage(characterId, characterId, 
                                `I have poisoned ${chLink(target)} with ${potion.name}.`);
                        } else if (result == FAIL_PUBLIC) {
                            this.addNews(null, `${chLink(character)} was caught plotting to poison ${chLink(target)} with ${potion.name}.`);
                            this.sendMessage(targetId, targetId, `${chLink(character)} was plotting to poison me with ${potion.name}!`);
                            this.sendMessage(characterId, characterId, 
                                `${chLink(target)} caught wind of my plot to poison.`);
                        } else if (result == FAIL_SILENT) {
                            this.sendMessage(characterId, characterId, 
                                `My plot to poison ${chLink(target)} has failed. No-one knows about this.`);
                        }
                    }
                    return result;
            }
        } else {
            return -1;
        }
    }

    changeCharacterMoney( characterId, type, value ) {
        let character = this.getCharacter(characterId);
        if (character) {
            if (isNaN(value)) {
                throw new Error('Number is NaN');
            }
            if (!character.stats.dayMoneyBreakdown[type]) {
                character.stats.dayMoneyBreakdown[type] = 0;
            }
            character.money += value;
            character.stats.dayMoneyBreakdown[type] += value;
            if (character.money < 0) {
                character.money = 0;
            }
        } else {
            this.logger.warn('character is null');
        }
    }

    changeCharacterScience( characterId, type, value ) {
        let character = this.getCharacter(characterId);
        if (character) {
            if (!character.stats.dayScienceBreakdown) {
                character.stats.dayScienceBreakdown = {};
            }
            if (!character.stats.dayScienceBreakdown[type]) {
                character.stats.dayScienceBreakdown[type] = 0;
            }
            character.science += value;
            character.stats.dayScienceBreakdown[type] += value;
            if (character.science < 0) {
                character.science = 0;
            }
        } else {
            this.logger.warn('character is null');
        }
    }

    giveCharacterItemOfId( characterId, itemId ) {
        let itemTemplate = TECHNOLOGY.find(tech => tech.id === itemId);
        let item = Object.assign({
            guid: this.chance.guid(),
            owningCharacter: characterId
        }, itemTemplate);
        if (item.type === 'book') {
            item.active = true;
        }
        if (item.expires) {
            item.expires += this.state.map.turn;
        }
        this.state.items.push(item);
        this.sendMessage(characterId, characterId,
            `I have a new item: ${item.name}`);
        return item;
    }

    giveCharacterRandomItemOfType( characterId, type ) {
        let rareMap = {
            [1]: 5, [2]: 3, [3]: 2, [4]: 1
        };
        let technologies = TECHNOLOGY.filter(tech => {
            if (tech.type === 'book' && type === 'book') {
                return this.state.items.findIndex(item => item.owningPlayer === characterId && item.id === tech.id) === -1;
            } else {
                return tech.type === type && tech.rarity
            }
        });
        let weights = technologies.map(tech => {
            return rareMap[tech.rarity];
        });
        let chosen = this.chance.weighted(technologies, weights);
        this.giveCharacterItemOfId(characterId, chosen.id);
    }

    transferItemToCharacter( fromCharacterId, toCharacterId, itemId ) {
        let item = this.getItem(itemId);
        if (item && !item.active && item.owningCharacter === fromCharacterId) {
            if (item.type === 'book') {
                let itemCopy = Object.assign({}, item);
                itemCopy.guid = this.chance.guid();
                itemCopy.owningCharacter = toCharacterId;
                this.state.items.push(itemCopy);
                return true;
            } else {
                item.owningCharacter = toCharacterId;
                return true;
            }
        }
        return false;
    }

    giveCharacterDesire( characterId, desire ) {
        let character = this.getCharacter(characterId);
        character.desires.push(desire);
        if (desire.statKey) {
            character.stats[desire.statKey] = 0;
        }
    }

    giveCharacterPact( characterId, type, target ) {
        let character = this.getCharacter(characterId);
        if (character) {
            if (!character.pacts) {
                character.pacts = [];
            }
            let pact = {
                type: 'pact',
                pactType: type,
                target: target,
                expires: this.state.map.turn + K.pacts.duration
            };

            switch(type) {
                case 'OPEN_BORDERS':
                    for (let i = character.pacts.length - 1; i >= 0; i--) {
                        let otherPact = character.pacts[i];
                        if (otherPact.type === 'CLOSED_BORDERS' && otherPact.target === target) {
                            character.pacts.splice(i, 1);
                        }
                    }
                    break;
                case 'CLOSED_BORDERS':
                    for (let i = character.pacts.length - 1; i >= 0; i--) {
                        let otherPact = character.pacts[i];
                        if (otherPact.type === 'OPEN_BORDERS' && otherPact.target === target) {
                            character.pacts.splice(i, 1);
                        }
                    }
                    break;
                case 'SCIENCE':
                    pact.path = 'countyResourceRates/science';
                    pact.add = 3;
                    break;
                case 'MILITARY':
                    pact.path = 'countyResourceRates/military';
                    pact.add = 3;
                    break;
            }
            
            character.effects.push(pact);
        }
    }

    killCharacter( characterId ) {
        let character = this.getCharacter(characterId);
        character.alive = false;
        // Check Desires.
        character.desires.forEach(desire => {
            this.checkDesire( character, desire );
        });
        
        // When character dies with an heir, the heir will inherit its titles and resources.
        let heir = this.getCharacterHeir(character.guid);

        let didPassControlToPlayer = false;
        let player = null;

        // Emit event.
        if (character.owningPlayer) {
            this.emit('characterDied', {
                player: this.getPlayer(character.owningPlayer),
                character: character,
                heir: heir
            });
        }

        // If character is bodyswapped, swap bodies back.
        if (character.bodyswap) {
            this.swapCharacterOwners(character.guid, character.bodyswap, true);
        }

        if (heir) {
            // Add news message.
            this.addNews(null, 
                `${chLink(character)} has died. ${chLink(heir)} has inherited their titles.`);
            // Player loses control of dead character.
            if (character.owningPlayer) {
                let playerId = character.owningPlayer;
                player = this.state.players.find(p => p.username == playerId);
                let playerCharacterIndex = player.characters.indexOf(character.guid);
                player.characters.splice(playerCharacterIndex, 1);
                // Player gains control of heir.
                if (!heir.owningPlayer) {
                    heir.owningPlayer = character.owningPlayer;
                    player.characters.push(heir.guid);
                    didPassControlToPlayer = true;
                    let desirePick1 = this.generateDesires(heir, 2);
                    let desirePick2 = this.generateDesires(heir, 2);
                    heir.desireChoices = [desirePick1, desirePick2];
                }
                character.owningPlayer = null;
            }
            character.heir = heir.guid;
            // Heir inherits money.
            this.changeCharacterMoney(heir, 'Inheritance', character.money);
            // Heir inherits items.
            this.state.items
                .filter(item => item.owningCharacter == character.guid && item.type !== 'potion')
                .forEach(item => {
                    this.transferItemToCharacter(character.guid, heir.guid, item.guid);
                });
            // Heir inherits parent's titles.
            let counties = this.getCharacterCounties(character.guid);
            counties.forEach(county => {
                county.owningCharacter = heir.guid;
            });
            // Hier inherits armies.
            let armies = this.getCharacterArmies(character.guid);
            armies.forEach(army => {
                army.owningCharacter = heir.guid;
            });
            // Generate desires for heir.
            let pick1 = this.generateDesires(heir, 2);
            let pick2 = this.generateDesires(heir, 2);
            heir.desireChoices = [pick1, pick2];
        } else {
            if (character.owningPlayer) {
                player = this.getPlayer(character.owningPlayer);
                didPassControlToPlayer = false;
            }
            // Add news message.
            this.addNews(null, 
                `${chLink(character)} has died with no heir.`);
            // set counties to no mans land.
            let counties = this.getCharacterCounties(character.guid);
            counties.forEach(county => {
                county.owningCharacter = null;
            });
            // remove items
            if (this.state.items.length > 0) {
                for (let itemIndex = this.state.items.length - 1; itemIndex >= 0; itemIndex--) {
                    if (this.state.items[itemIndex] && this.state.items[itemIndex].owningCharacter == character.guid) {
                        this.state.items.splice(itemIndex, 1);
                    }
                }
            }
        }

        if (player && !didPassControlToPlayer) {
            player.score = 0;
            player.dead = true;
        }

        // Clear references to dead character.
        let underlings = this.getCharacterUnderlords(character.guid);
        underlings.forEach(underling => underling.overlord = null);
        if (character.spouse) {
            this.getCharacter(character.spouse).spouse = null;
        }
        let prisoners = this.getCharacterPrisoners(character.guid);
        prisoners.forEach(prisoner => {
            this.sendMessage(prisoner.guid, prisoner.guid, `My imprisoner, ${chLink(character)}, has died. I have been released.`);
            prisoner.imprisoner = null;
        });
        character.overlord = null;
        character.spouse = null;
        character.imprisoner = null;
        character.money = 0;
    }

    performGameTick() {
        this.state.map.turn++;
        let turn = this.state.map.turn;
        let isIncomeTurn = ((turn % this.config.incomeTurnInterval) == 0);

        if (isIncomeTurn) {
            this.state.players.forEach(player => {
                player.showIncomeTurnInfo = true;
            });
        }

        // Clear characters science points.
        this.state.characters.forEach(character => {
            character.science = 0;
        });

        // Update counties and score points.
        for (let i = 0; i < this.state.counties.length; i++) {
            let county = this.state.counties[i];
            if (county.owningCharacter) {
                county.maxTroops = this.getGameSettingValue(county.owningCharacter, "caps/countyTroopsPerCountySize") * county.size;
                let character = this.getCharacter(county.owningCharacter);
                let resourceRates = this.getGameSettingObject(character.guid, "countyResourceRates");
                
                county.science = 0;

                // Prepare resource rates.
                if (county.razed) {
                    county.troopRate = 0;
                    county.moneyRate = 0;
                    county.scienceRate = 0;
                } else {
                    county.moneyRate = county.size + resourceRates.money;
                    county.troopRate = county.size + resourceRates.military;
                    county.scienceRate = 1 + resourceRates.science;
                    [...county.specialists, ...county.negatives].forEach(obj => {
                        if (obj.countyEffect) {
                            county.moneyRate = this.getObjectModifiedValue(county, 'moneyRate', obj.countyEffect);
                            county.troopRate = this.getObjectModifiedValue(county, 'troopRate', obj.countyEffect);
                            county.scienceRate = this.getObjectModifiedValue(county, 'scienceRate', obj.countyEffect);
                            county.defenseMultiplier = this.getObjectModifiedValue(county, 'defenseMultiplier', obj.countyEffect);
                        }
                    });
                    county.moneyRate += this.getCountyBuildingsEffects(county, 'moneyRate');
                    county.troopRate += this.getCountyBuildingsEffects(county, 'troopRate');
                    county.scienceRate += this.getCountyBuildingsEffects(county, 'scienceRate');
                    county.defenseMultiplier += this.getCountyBuildingsEffects(county, 'defenseMultiplier');
                    county.troopRate = Math.max(county.troopRate, 0);
                    county.moneyRate = Math.max(county.moneyRate, 0);
                    county.scienceRate = Math.max(county.scienceRate, 0);
                }

                // Building upkeep.
                for (let buildingIndex = county.buildings.length - 1; buildingIndex >= 0; buildingIndex--) {
                    let building = county.buildings[buildingIndex];
                    if (building) {
                        if (character.money < building.upkeep) {
                            county.buildings.splice(buildingIndex, 1);
                            this.sendMessage(character.guid, character.guid, 
                                `The ${building.name} in ${county.name} was demolished due to lack of funds.`);
                        }
                        this.changeCharacterMoney(character, 'Buildings', -building.upkeep);
                    }
                }

                // Apply resource rates.
                county.troops += Math.max(county.troopRate, 0);
                if (county.troops >= county.maxTroops) {
                    county.troops = county.maxTroops;
                }
                county.money += county.moneyRate;
                if (county.money < 0) {
                    county.money = 0;
                }
                county.science += county.scienceRate;

                // If this is an income turn, transfer the money to the owning character.
                if (isIncomeTurn && !character.imprisoner) {
                    this.changeCharacterMoney(character, "Counties", county.money);
                    county.money = 0;
                }

                this.changeCharacterScience(character, "Counties", county.scienceRate);

                // Give player points.
                if (character.owningPlayer) {
                    let pointsPerCountySize = this.getGameSettingValue(county.owningCharacter, "pointsPerCountySize");
                    let points = county.size * pointsPerCountySize;
                    let player = this.getPlayer(character.owningPlayer);
                    player.score += points;
                }
            } else {
                county.troopRate = county.size;
                county.moneyRate = county.size;
                county.scienceRate = 0;
                county.maxTroops = K.caps.countyTroopsPerCountySize * county.size;
                county.troops += county.troopRate;
                county.money += county.moneyRate;
                county.science += county.scienceRate;
                if (county.troops >= county.maxTroops) {
                    county.troops = county.maxTroops;
                }
            }
        }

        // get alive characters.
        let aliveCharacters = this.state.characters.filter(c => c.alive);

        // Give points for family size.
        let houseFamily = aliveCharacters.reduce((pre, character) => {
            if (!pre[character.house]) {
                pre[character.house] = [character];
            } else {
                pre[character.house].push(character);
            }
            return pre;
        }, {});
        for (let house in houseFamily) {
            houseFamily[house].forEach(character => {
                if (character.owningPlayer) {
                    let player = this.getPlayer(character.owningPlayer);
                    player.score += houseFamily[house].length * K.pointsPerFamilyMember;
                }
            });
        }

        // Calculate character's tax rate.
        for (let characterIndex = 0; characterIndex < aliveCharacters.length; characterIndex++) {
            let character = aliveCharacters[characterIndex];
            let vassals = this.getCharacterUnderlords(character.guid);
            let baseTaxRate = this.getGameSettingValue(character.guid, "taxes/incomeTax");
            character.taxRate = baseTaxRate;
        }

        // Update alive characters.
        for (let characterIndex = 0; characterIndex < aliveCharacters.length; characterIndex++) {
            let character = aliveCharacters[characterIndex];

            // Sum the character's total number of troops.
            character.totalTroops = this.state.counties.reduce((sum, county) => {
                if (county.owningCharacter == character.guid) {
                    return sum + county.troops;
                }
                return sum;
            }, 0);

            if (isIncomeTurn) {
                let lastTotalTroops = character.totalTroopsLastIncomeTurn || 0;
                character.totalTroopsLastIncomeTurn = character.totalTroops;
            }

            // Update caps.
            character.maxCounties = this.getGameSettingValue(character.guid, "caps/county");
            character.maxArmies = this.getGameSettingValue(character.guid, "caps/army");
            character.maxCounties += this.getCharacterUnderlords(character.guid).length;
            character.maxActiveItems = this.getGameSettingValue(character.guid, "caps/activeItems");
            if (character.overlord) {
                character.maxCounties += this.getCharacterUnderlords(character.overlord).length;
            }
           
            // Mark player as AKF if inactive for 42 hours.
            let owningPlayer = this.getPlayer(character.owningPlayer);
            if (owningPlayer && !owningPlayer.afk && moment().diff(owningPlayer.lastActive, 'hours', false) >= 48) {
                owningPlayer.afk = true;
                this.emit('playerAFK', {player: owningPlayer});
            }
            if(owningPlayer && owningPlayer.afk && moment().diff(owningPlayer.lastActive, 'hours', false) < 48) {
                owningPlayer.afk = false;
            }

            // AI
            if ((!owningPlayer && this.config.botsEnabled) ||
                (owningPlayer && owningPlayer.afk))
            {
                let ai = new AICharacter(this, character);
                ai.tick();
            }

            // Decrease cooldowns.
            for (let key in character.cooldowns) {
                if (character.cooldowns[key] != null && character.cooldowns[key] > 0) {
                    character.cooldowns[key]--;
                }
                if (key === 'hidden' && character.cooldowns[key] === 0) {
                    character.hidden = false;
                    character.cooldowns.hidden = null;
                }
                if (key === 'bodyswap' && character.cooldowns[key] === 0) {
                    this.swapCharacterOwners(character.guid, character.bodyswap, true);
                }
            }

            // Expire effects.
            for (let i = character.effects.length - 1; i >= 0; i--) {
                let effect = character.effects[i];
                if (effect.expires && turn >= effect.expires) {
                    character.effects.splice(i, 1);
                }
            }

            // Pay tax to overlord.
            if (isIncomeTurn && character.overlord && !character.imprisoner) {
                let overlord = this.getCharacter(character.overlord);
                let countyIncome = character.stats.dayMoneyBreakdown['Counties'] || 0;
                let overlordTaxRate = overlord.taxRate;
                if (overlordTaxRate === undefined || overlordTaxRate === null) {
                    overlordTaxRate = K.taxes.incomeTax;
                }
                let tax = Math.max(Math.floor(countyIncome * overlordTaxRate), 0);
                this.changeCharacterMoney(character, "Taxes Paid", -tax);
                this.changeCharacterMoney(overlord, "Taxes Levied", tax);
            }

            // Pay fines for going over cap limits.
            let counties = this.getCharacterCounties(character.guid).filter(c => !c.razed);
            character.fines = 0;
            if (counties.length > character.maxCounties) {
                let finePerCountyOverCap = this.getGameSettingValue(character.guid, "capFines/perCounty");
                let over = counties.length - character.maxCounties;
                let fine = finePerCountyOverCap * over;
                this.changeCharacterMoney(character, "Fines", -fine);
                character.fines += fine;
            }
            let armies = this.getCharacterArmies(character.guid);
            if (armies.length > character.maxArmies) {
                let finePerArmyOverCap = this.getGameSettingValue(character.guid, "capFines/perArmy");
                let over = armies.length - character.maxArmies;
                let fine = finePerArmyOverCap * over;
                this.changeCharacterMoney(character, "Fines", -fine);
                character.fines += fine;
            }

            // Other income effects.
            let itemMoney = this.getGameSettingValue(character.guid, "characterResourceRates/money");
            if (character.focus === 'money') {
                let focusMoney = K.focus.money.effect.find(effect => effect.path === 'characterResourceRates/money').add;
                itemMoney -= focusMoney;
                this.changeCharacterMoney(character.guid, 'Focus', focusMoney);
            }
            this.changeCharacterMoney(character, 'Items', itemMoney);
            let itemScience = this.getGameSettingValue(character.guid, 'characterResourceRates/science');
            if (character.focus === 'science') {
                let focusScience = K.focus.science.effect.find(effect => effect.path === 'characterResourceRates/science').add;
                itemScience -= focusScience;
                this.changeCharacterScience(character, 'Focus', focusScience);
            }
            this.changeCharacterScience(character, 'Items', itemScience);
            
            if (character.spouse) {
                let spouse = this.getCharacter(character.spouse);

                // Couples gain marriage bonus.
                if (!character.imprisoner && !spouse.imprisoner) {
                    let spouseMoneyBonus = this.getGameSettingValue(character.guid, "spouseMoneyBonus");
                    this.changeCharacterMoney(character, "Marriage", spouseMoneyBonus);
                }

                // Couples have children if in same location
                if (spouse.location == character.location && this.chance.d4() == 1) {
                    this.makeChild(character.guid);
                }
            }

            // Children get pocketmoney, from each parent.
            if (isIncomeTurn && character.parents && !character.imprisoner) {
                for (let parentId of character.parents) {
                    let parent = this.getCharacter(parentId);
                    if (parent.alive) {
                        let pocketmoney= this.getGameSettingValue(character.guid, "pocketmoney/base");
                        this.changeCharacterMoney(parent, "Children", -pocketmoney);
                        this.changeCharacterMoney(character, "Parents", pocketmoney);
                    }
                }
            }

            // Research.
            {
                let research = character.research[character.activeResearchIndex];
                research.progress += character.science;
                if (research.progress >= research.cost) {
                    let completeCount = Math.floor(research.progress / research.cost);
                    research.progress = research.progress % research.cost;
                    for (let rs = 0; rs < completeCount; rs++) {
                        this.giveCharacterRandomItemOfType(character.guid, research.type);
                        character.stats.itemsMade++;
                    }
                }
            }

            // Stats.
            character.stats.unrazedCounties = this.getCharacterCounties(character.guid).filter(county => !county.razed).length;
            character.stats.unraisedTroops = this.getCharacterReservedTroopsCount(character.guid);
            character.stats.turnsAtPeace = (this.state.map.turn - character.stats.lastTurnAttackOrDefended);

            // Desires.
            character.desires.forEach(desire => {
                this.checkDesire( character, desire );
            });

            // Increase age.
            character.age++;
            if (character.age >= character.lifespan) {
                this.killCharacter(character.guid);
            }
        }

        // Update army cooldowns and costs.
        for (let i = 0; i < this.state.armies.length; i++) {
            let army = this.state.armies[i]
            if (army.cooldown > 0) {
                army.cooldown--;
            }
            let owner = this.getCharacter(army.owningCharacter);
            let cost = this.getArmyCost(army.guid);
            if (owner.money >= cost) {
                this.changeCharacterMoney(owner, "Army Upkeep", -cost);
                army.attrition = null;
            } else {
                this.changeCharacterMoney(owner, "Army Upkeep", -cost);
                army.attrition = this.getGameSettingValue(army.owningCharacter, "armyUnpaidAttrition");
                army.troops -= army.attrition;
                if (army.troops <= 0) {
                    this.removeArmy(army.guid);
                }
            }
        }

        // Move Armies.
        for (let i = this.state.armies.length - 1; i >= 0; i--) {
            let army = this.state.armies[i];
            if (army) {
                if (army.cooldown == 0 && army.standDownQueued) {
                    army.standDownQueued = null;
                    this.standDownArmy(null, army.guid);
                } else if (army.cooldown == 0 && army.orders && army.orders.length > 0) {
                    let location = army.orders[0];
                    if (this.moveArmy(army.guid, location)) {
                        army.orders.shift();
                        if (army.orders.length == 0) {
                            this.standDownArmy(null, army.guid);
                        }
                    }
                }
            }
        }

        // Update items.
        for (let i = this.state.items.length - 1; i >= 0; i--) {
            let item = this.state.items[i];
            if (item.expires && turn > item.expires) {
                this.state.items.splice(i, 1);
            }
        }

        // Expire proposals.
        for (let i = this.state.messages.length - 1; i >= 0; i--) {
            let message = this.state.messages[i];
            if (message.proposals && message.proposals.length > 0 && (this.state.map.turn - message.turn) >= K.boltonExpiration) {
                message.proposals.forEach(proposal => {
                    if (!proposal.receivedResponse) {
                        this.declineProposal(proposal, message);
                    }
                });
            }
        }

        // Reset daily money stats.
        if (isIncomeTurn) {
            this.state.characters.forEach(character => {
                character.stats.lastDayMoneyBreakdown = Object.assign({}, character.stats.dayMoneyBreakdown);
                character.stats.dayMoneyBreakdown = {};
                character.stats.lastDayScienceBreakdown = Object.assign({}, character.stats.dayScienceBreakdown);
                character.stats.dayScienceBreakdown = {};
            });
        }
    }

    fuzzyfyValue( character, value ) {
        let roundTo = this.getGameSettingValue(character.guid, "fuzzyfy/roundTo");
        return Math.ceil((value + 1) / roundTo) * roundTo;
    }

    getStateForPlayer( player ) {
        let state = {};
        
        // Get relationships.
        let playerCharacters = this.state.characters.filter(c => c.owningPlayer == player.username);
        let playerCharacter = playerCharacters[0];
        let playerCharacterIds = playerCharacters.map(c => c.guid);
        let playerHouses = playerCharacters.map(c => c.house);
        let playerOverlordIds = playerCharacters.filter(c => c.overlord).map(c => c.overlord);
        let playerUnderlingIds = this.state.characters.filter(c => {
            return playerCharacterIds.indexOf(c.overlord) !== -1;
        }).map(c => c.guid);
        let friendlyCharacterIds = [...playerOverlordIds, ...playerUnderlingIds];
        let ourCharacterLocations = playerCharacters.map(c => c.location);

        // Sort players by score.
        let players = this.state.players.map(p => {
            return {
                username: p.username,
                score: p.score,
                lastActive: p.lastActive || 0,
                dead: p.dead,
                afk: p.afk
            }
        });
        players.sort((a, b) => {
            return b.score - a.score;
        });
        state.players = players.map(p => {
            delete p.score;
            return p;
        });

        state.player = player;

        // Set the visible state for all counties to null.
        let countyVisibility = {};
        if (this.config.fogOfWarEnabled) {
            // Set visibility on all our counties.
            this.state.counties.forEach(county => {
                let owningCharacter = this.getCharacter(county.owningCharacter);
                if (owningCharacter 
                    && (playerHouses.indexOf(owningCharacter.house) != -1 || friendlyCharacterIds.indexOf(county.owningCharacter) != -1))
                {
                    countyVisibility[county.guid] = true;
                    // Set visibility on counties adjacent to visible counties.
                    let range = this.getGameSettingValue(county.owningCharacter, "fogOfWar/countyRange");
                    this.getAdjacentCountiesSteps(county.guid, range).forEach(adjacentCounty => {
                        countyVisibility[adjacentCounty.guid] = true;
                    });
                }
            });
            // Set visibilty on all counties in range around player characters.
            let visibleCharacters = [...playerCharacters, ...playerCharacters.map(c => this.getCharacter(c.spouse))];
            visibleCharacters.forEach(character => {
                if (character) {
                    let location = this.getCounty(character.location);
                    if (location) {
                        let range = this.getGameSettingValue(character.guid, "fogOfWar/characterRange");
                        this.getAdjacentCountiesSteps(location.guid, range).forEach(adjacentCounty => {
                            countyVisibility[adjacentCounty.guid] = true;
                        });
                    }
                }
            });
            // Set visibility on all coutnies in range around armies.
            this.state.armies.forEach(army => {
                if (playerCharacterIds.indexOf(army.owningCharacter) !== -1) {
                    let location = this.getCounty(army.location);
                    let range = this.getGameSettingValue(army.owningCharacter, "fogOfWar/armyRange");
                    this.getAdjacentCountiesSteps(location.guid, range).forEach(adjacentCounty => {
                        countyVisibility[adjacentCounty.guid] = true;
                    });
                }
            });
        } else {
            this.state.counties.forEach(county => {
                countyVisibility[county.guid] = true;
            });
        }

        // Collate county information.
        state.counties = this.state.counties.map(county => {
            let result = Object.assign({}, county);
            let owningCharacter = this.getCharacter(county.owningCharacter);

            let isOurCharactersAtLocation = (ourCharacterLocations.indexOf(county.guid) != -1);
            let isVisible = countyVisibility[county.guid];
            let isOurHouse = (owningCharacter && playerHouses.indexOf(owningCharacter.house) != -1);
            let isFriendly = (owningCharacter && friendlyCharacterIds.indexOf(county.owningCharacter) != -1);
            let isOwner = (playerCharacterIds.indexOf(county.owningCharacter) != -1);
            result.isVisible = countyVisibility[county.guid];
            result.isOwner = isOwner;
            if (county.adjacent) {
                result.adjacent = county.adjacent.map(id => {
                    return {
                        state: this.getCountyBorderState(county.guid, id),
                        countyId: id
                    };
                });
            }

            if (isOwner || isOurHouse || isFriendly) {
                result.infoLevel = 1;
                result.isFriendly = true;
            } else if (isVisible) {
                result.infoLevel = 2;
                result.isFriendly = false;
                result.troops = this.fuzzyfyValue(playerCharacter, county.troops);
                result.money = this.fuzzyfyValue(playerCharacter, county.money);
                delete result.troopRate;
                delete result.moneyRate;
            } else {
                result.infoLevel = 3;
                result.isFriendly = false;
                delete result.owningCharacter;
                delete result.troops;
                delete result.troopRate;
                delete result.money;
                delete result.moneyRate;
                delete result.science;
                delete result.scienceRate;
                delete result.hasTreasure;
                delete result.razed;
                result.specialists = [];
                result.negatives = [];
            }

            if (result.infoLevel <= 2) {
                result.defenseBonus = this.getCountyDefendMultiplier(county.guid);
            }

            return result;
        });

        state.armies = this.state.armies
            .filter(army => {
                return countyVisibility[army.location];
            })
            .map(army =>
        {
            let result = Object.assign({}, army);
            let owningCharacter = this.getCharacter(army.owningCharacter);
            let isOwner = (playerCharacterIds.indexOf(army.owningCharacter) != -1);
            let isOurHouse = (playerHouses.indexOf(owningCharacter.house) != -1);
            let isFriendly = (friendlyCharacterIds.indexOf(army.owningCharacter) != -1);
            result.isOwner = isOwner;
            result.cost = this.getArmyCost(army.guid);
            if (isOwner || isOurHouse || isFriendly) {
                result.infoLevel = 1;
                result.isFriendly = true;
            } else {
                result.infoLevel = 2;
                result.troops = this.fuzzyfyValue(playerCharacter, army.troops);
                result.isFriendly = false;
                delete result.orders;
            }
            return result;
        });

        state.items = this.state.items.filter(item => item.owningCharacter == null);
        state.characters = this.state.characters.map(character => {
            let result = Object.assign({}, character);
            delete result.lifespan;

            let isOwner = (character.owningPlayer == player.username);
            let isOurHouse = (playerHouses.indexOf(character.house) != -1);
            let isVisible = countyVisibility[character.location];
            let isFriendly = (friendlyCharacterIds.indexOf(character.guid) != -1);
            let isAtOurLocation = (ourCharacterLocations.indexOf(character.location) != -1);
            let heir = this.getCharacterHeir(character.guid);
            result.isOwner = isOwner;
            result.isVisible = isVisible;
            result.hasOwningPlayer = (result.owningPlayer != null);
            delete result.owningPlayer;

            let characterItems = this.state.items.filter(item => {
                return item.owningCharacter == character.guid;
            });

            if (isAtOurLocation) {
                result.murderPlotting = this.getGameSettingObject(character.guid, 'plotting/murder');
                result.stealPlotting = this.getGameSettingObject(character.guid, 'plotting/steal');
                result.poisonPlotting = this.getGameSettingObject(character.guid, 'plotting/poison');
                result.focusCooldownDuration = this.getGameSettingValue(character.guid, "focusCooldown");
            }

            if (isOurHouse || isOwner) {
                // High priority information.
                result.infoLevel = 1;
                result.isFriendly = true;
                result.heir = heir ? heir.guid : null;
            } else if(isVisible) {
                // Medium priority information.
                result.infoLevel = 2;
                result.isFriendly = false;
                result.money = this.fuzzyfyValue(playerCharacter, character.money);
                result.totalTroops = this.fuzzyfyValue(playerCharacter, character.totalTroops);
                result.heir = heir ? heir.guid : null;
                delete result.stats;
                delete result.desires;
                delete result.research;
                delete result.activeResearchIndex;
            } else {
                // Low priority information.
                result.infoLevel = 3;
                result.isFriendly = false;
                delete result.focus;
                delete result.money;
                delete result.totalTroops;
                delete result.science;
                delete result.stats;
                delete result.desires;
                delete result.research;
                delete result.activeResearchIndex;
            }

            if (result.infoLevel <= 2) {
                result.attackMultiplier = Math.floor(this.getAttackMultiplier(character.guid) * 100) / 100;
                result.defendMultiplier = Math.floor(this.getDefendMultiplier(character.guid) * 100) / 100;
                let countyCount = this.getCharacterCounties(character.guid).filter(county => !county.razed).length;

                let economyBonus = this.getGameSettingValue(character.guid, 'characterResourceRates/money');
                let militaryBonus = this.getGameSettingValue(character.guid, 'characterResourceRates/military');
                let scienceBonus = this.getGameSettingValue(character.guid, 'characterResourceRates/science');
                
                result.economyBonus = economyBonus;
                result.militaryBonus = militaryBonus;
                result.scienceBonus = scienceBonus;
                state.items.push(...characterItems);

                let moneyRate = this.getCharacterTurnMoneyRate(character.guid);
                result.moneyRate = moneyRate.sum;
                result.moneyRateBreakdown = moneyRate.breakdown;
                
                let troopRate = this.getCharacterTurnTroopRate(character.guid);
                result.troopRate = troopRate.sum;
                result.troopRateBreakdown = troopRate.breakdown;

                let scienceRate = this.getCharacterTurnScienceRate(character.guid);
                result.scienceRate = scienceRate.sum;
                result.scienceRateBreakdown = scienceRate.breakdown;
            }

            if (character.hidden) {
                delete result.location;
            }

            return result;
        });

        state.messages = this.state.messages.filter(m => {
            let toCharacter = this.getCharacter(m.to);
            let fromCharacter = this.getCharacter(m.from);
            if (toCharacter && fromCharacter) {
                return toCharacter.owningPlayer == player.username || fromCharacter.owningPlayer == player.username;
            } else {
                return false;
            }
        });

        state.news = this.state.news.filter(news => {
            let newsAge = (this.state.map.turn - news.turn);
            return newsAge <= 42;
        });
        state.map = this.state.map;
        state.config = this.config;
        state.constants = K;

        return state;
    }

    generateDesires( character, count ) {
        let possibleDesires = this.chance.shuffle(DESIRES.slice());
        let desires = [];
        for (let i = 0; i < count; i++) {
            let desire = possibleDesires.pop();
            desire = Object.assign({}, desire);
            if (!desire.generateTarget || this.generateDesireTarget(character, desire)) {
                desires.push(desire);
            }
        }
        return desires;
    }

    generateDesireTarget( character, desire ) {
        var candidates = this.state.characters.filter(c => c.alive && c.guid != character.guid);
        desire.target = null;
        switch(desire.generateTarget) {
            case 'character':
                if (desire.key == 'swear_fealty') {
                    candidates = candidates.filter(c => character.overlord != c.guid);    
                } else if (desire.key == 'marryTarget') {
                    candidates = candidates.filter(c => this.canCharactersMarry(character, c));
                } else if (desire.key == 'captureTarget') {
                    candidates = candidates.filter(c => c.imprisoner != character.guid);
                }
                if (candidates.length > 0) {
                    desire.target = this.chance.pickone(candidates).guid;
                }
                break;
            case 'county':
                candidates = this.state.counties.filter(c => c.owningCharacter != character.guid && !c.impassable);
                if (candidates.length > 0) {
                    desire.target = this.chance.pickone(candidates).guid;
                }
                break;
            case 'item':
                let itemTarget = this.chance.pickone(TECHNOLOGY);
                desire.target = itemTarget;
                break;
        }
        return (desire.target != null);
    }

    acceptDesire( player, choiceIndex, desireIndex ) {
        let character = this.getPlayerCharacters(player.username)[0];
        if (character && character.alive && character.desireChoices[choiceIndex] && character.desireChoices[choiceIndex][desireIndex]) {
            let desire = character.desireChoices[choiceIndex][desireIndex];
            this.giveCharacterDesire(character.guid, desire);
            character.desireChoices.splice(choiceIndex, 1);
        }
    }

    checkDesire( character, desire ) {
        if (!desire) return false;

        let satisfiedDesire = false;

        // check if we have a character target and that target is now dead, invalidate the desire.
        let targetCharacter = this.getCharacter(desire.target);
        if (targetCharacter && !targetCharacter.alive) {
            character.desireChoices.push([...this.generateDesires(character, 2)]);
            let desireIndex = character.desires.findIndex(d => d.key == desire.key);
            character.desires.splice(desireIndex, 1);
            return false;
        }

        switch (desire.key) {
            case 'land':
                satisfiedDesire = (character.stats.unrazedCounties >= desire.target);
                break;
            case 'promiseland':
                let characterCounties = this.getCharacterCounties(character);
                satisfiedDesire = (characterCounties.findIndex(c => c.guid == desire.target) !== -1);
                break;
            case 'wealth':
                var sum = 0;
                for (let key in character.stats.dayMoneyBreakdown) {
                    sum += character.stats.dayMoneyBreakdown[key];
                }
                satisfiedDesire = (sum >= desire.target);
                break;
            case 'armyReserve':
                satisfiedDesire = (character.stats.unraisedTroops >= desire.target);
                break;
            case 'overlord':
            case 'overlordTarget':
                let underlords = this.getCharacterUnderlords(character.guid);
                if (desire.key === 'overlord') {
                    satisfiedDesire = (underlords.length >= desire.target);
                } else {
                    satisfiedDesire = underlords.findIndex(other => other.guid == desire.target) != -1;
                }
                break;
            case 'marryTarget':
                satisfiedDesire = (character.spouse == desire.target);
                break;
            case 'generosity':
                satisfiedDesire = (character.stats.moneyGifted >= desire.target);
                break;
            case 'science':
                satisfiedDesire = (character.stats.itemsMade >= desire.target);
                break;
            case 'social':
                satisfiedDesire = (character.stats.messagesReceived >= desire.target);
                break;
            case 'family':
                satisfiedDesire = (character.stats.children >= desire.target);
                break;
            case 'warmonger':
                satisfiedDesire = (character.stats.troopsKilled >= desire.target);
                break;
            case 'pacifist':
                satisfiedDesire = (character.stats.turnsAtPeace >= desire.target);
                break;
            case 'murderTarget':
                if (character.stats.charactersKilled) {
                    satisfiedDesire = (character.stats.charactersKilled.indexOf(desire.target) != -1);
                } else {
                    satisfiedDesire = false;
                }
                break;
            case 'sabotage':
                if (character.stats.charactersSabotaged) {
                    let sabotageIndex = character.stats.charactersSabotaged.indexOf(desire.target);
                    satisfiedDesire = (sabotageIndex != -1);
                    if (sabotageIndex != -1) {
                        character.stats.charactersSabotaged.splice(sabotageIndex, 1);
                    }
                } else {
                    satisfiedDesire = false;
                }
                break;
            case 'steal':
                if (character.stats.charactersStolenFrom) {
                    let stealIndex = character.stats.charactersStolenFrom.indexOf(desire.target);
                    satisfiedDesire = (stealIndex != -1);
                    if (stealIndex != -1) {
                        character.stats.charactersStolenFrom.splice(stealIndex, 1);
                    }
                } else {
                    satisfiedDesire = false;
                }
                break;
            case 'captureTarget':
                satisfiedDesire = (this.getCharacter(desire.target).imprisoner == character.guid);
                break;
            case 'possess':
                satisfiedDesire = desire.target && this.state.items.filter(item => 
                    item.owningCharacter == character.guid && item.key == desire.target.key).length != 0;
                break;
            case 'swear_fealty':
                satisfiedDesire = (character.overlord == desire.target);
                break;
            case 'raze':
                satisfiedDesire = (character.stats.razedCounties >= desire.target);
                break;
            case 'rebuild':
                satisfiedDesire = (character.stats.rebuiltCounties >= desire.target);
                break;
            case 'treasure':
                satisfiedDesire = (character.stats.treasureLooted >= desire.target);
                break;
            case 'visit_county':
                satisfiedDesire = (character.location === desire.target);
                break;
        }
        if (satisfiedDesire) {
            let player = this.getPlayer(character.owningPlayer);
            if (player) {
                player.score += desire.score;
            }
            if (!character.satisfiedDesires) {
                character.satisfiedDesires = [];
            }
            character.satisfiedDesires.push(
                Object.assign({}, desire)
            );
            let desireIndex = character.desires.findIndex(d => d.key == desire.key);
            character.desires.splice(desireIndex, 1);

            if (desire.upgrades && desire.upgrades.length > 0) {
                let upgrade = desire.upgrades.shift();
                Object.keys(upgrade).forEach(key => {
                    desire[key] = upgrade[key];
                });
                let nextDesires = this.generateDesires(character, 1);
                character.desireChoices.push([desire, ...nextDesires]);
            } else {
                let nextDesires = this.generateDesires(character, 2);
                character.desireChoices.push([...nextDesires]);
            }
        }
    }

    safePick( arr ) {
        if (arr.length > 0) {
            return this.chance.pickone(arr);
        } else {
            return null;
        }
    }
}


module.exports = GameLogic;