const assert = require('assert');
const WebSocketServer = require('uws').Server;
const jsonpatch = require('fast-json-patch');
const escape = require('escape-html');
const moment = require('moment');
const config = require('../../config').config;

const MESSAGE_HANDLERS = config.GAME_VERSIONS.reduce((arr, version) => {
    arr.push({
        version,
        messageHandler: require(`./${version}/message_handler`)
    });
    return arr;
}, []);

module.exports = class GameServer {
    constructor( {httpsServer, games, auth} ) {
        this.wss = new WebSocketServer({server: httpsServer, clientTracking: true, verifyClient: this.verifyClient.bind(this)});
        this.wss.on('connection', this.handleConnection.bind(this));
        this.games = games;
        this.auth = auth;
        this.clients = [];

        games.on('gameTick', game => {
            game.logic.state.players.forEach(player => {
                let client = this.findClient(player.username, game.doc.id);
                if (client) {
                    let meta = this.gameAndPlayerInfo(client.userInfo, game);
                    this.sendPatchedJson(
                        client, 
                        game.logic.getStateForPlayer(player),
                        meta.gameInfo,
                        meta.playerInfo);
                }
            });
        });
    }

    verifyClient( info, next ) {
        let headers = info.req.headers;
        if (info.secure && headers.cookie) {
            // Parse cookies.
            let cookies = headers.cookie.split('; ').reduce((dict, cookie) => {
                let parts = cookie.split('=');
                dict[parts[0]] = parts[1];
                return dict;
            }, {});
            // Validate auth token.
            if (cookies.authToken) {
                this.auth.validateAuthToken(cookies.authToken).then(decodedToken => {
                    info.req.userInfo = decodedToken;
                    next(true);
                }, err => {
                    next(false, 'INVALID_AUTH', 'Invalid Authorization');
                });
            } else {
                next(false, 'INVALID_AUTH', 'Invalid Authorization');
            }
        } else {
            next(false, 'INVALID_AUTH', 'Invalid Authorization');
        }
    }

    close() {
        this.wss.close();
    }

    handleConnection( client ) {
        client.userInfo = client.upgradeReq.userInfo;
        client.cachedResponse = null;
        this.clients.push(client);
        client.on('message', (message) => {
            this.handleMessage(client, message);
        });
        client.on('disconnect', () => {
            let index = this.clients.indexOf(client);
            if (index !== -1) {
                this.clients.splice(index, 1);
            }
        });
    }

    sendJson( client, message ) {
        client.send(JSON.stringify(message));
    }

    sendPatchedJson( client, message, gameInfo, playerInfo ) {
        let response;
        if (client.cachedResponse) {
            let patch = jsonpatch.compare(client.cachedResponse, message);
            response = {
                type: 'STATE_PATCH',
                patch: patch,
                gameInfo,
                playerInfo
            };
        } else {
            response = {
                type: 'STATE_FULL',
                state: message,
                gameInfo,
                playerInfo
            };
        }
        client.cachedResponse = jsonpatch.deepClone(message);
        this.sendJson(client, response);
    }

    findClient( username, gameId ) {
        for (let i = this.clients.length - 1; i >= 0; i--) {
            let client = this.clients[i];
            if (client.userInfo && client.userInfo.username == username && client.gameId == gameId) {
                return client;
            }
        }
        return null;
    }

    gameAndPlayerInfo( userInfo, game ) {
        let gameInfo = {
            name: game.doc.name,
            owner: game.doc.owner,
            nextTurnTime: game.doc.nextTurnTime,
            nextIncomeTurnTime: game.doc.nextIncomeTurnTime,
            turn: game.doc.turn()
        };
        let playerInfo = {
            isPremium: this.auth.isUserPaidSubscriber(userInfo)
        };
        return {gameInfo, playerInfo};
    }

    handleMessage( client, message ) {
        let json = JSON.parse(message);

        // Validate we have the correct structure message.
        try {
            assert(json.gameId, "Missing game id");
            assert(json.type, "Missing message type");
            assert.equal(typeof json.gameId, 'string', "gameId must be of type string");
            assert.equal(typeof json.type, 'string', "type field must be of type string");
        } catch(err) {
            this.games.logger.error(err);
            this.sendJson(client, {type: 'ERROR', message: err});
            return;
        }
        
        client.gameId = json.gameId;
        let userInfo = client.userInfo;
        let game = this.games.findActiveGame(json.gameId);
        
        if (game && game.doc.hasPlayer(userInfo.username)) {
            let responses = this.handleEventForGame(json, game, client, userInfo);
            responses.forEach(response => {
                var targetClient = this.findClient(response.username, json.gameId);
                if (targetClient) {
                    this.sendPatchedJson(targetClient, response.message, response.gameInfo, response.playerInfo);
                }
            });
        } else {
            this.sendJson(client, {type: 'ERROR', message: 'You have not joined this game!'});
            return;
        }
    }

    handleEventForGame( event, game, client, userInfo ) {
        let player = game.logic.getPlayer(userInfo.username);
        player.lastActive = moment().format();
        let responses = [];
        let messageHandlerResponse = {};

        let handlerObj = MESSAGE_HANDLERS.find(obj => {
            let versionSplit = obj.version.split('.');
            let gameVersionSplit = game.doc.version.split('.');
            return versionSplit[0] === 'v'+gameVersionSplit[0] &&
                versionSplit[1] === gameVersionSplit[1] &&
                (versionSplit[2] === 'x' || versionSplit[2] === gameVersionSplit[2]);
        });
        try {
            messageHandlerResponse = handlerObj.messageHandler({
                player, game, client, event, userInfo, auth: this.auth
            });
        } catch(err) {
            console.error(err);
            this.sendJson(client, {type: 'ERROR', message: err});
        }

        let pushPlayerResponse = messageHandlerResponse.pushPlayerResponse;
        let pushAllPlayersResponse = messageHandlerResponse.pushAllPlayersResponse;
        if (messageHandlerResponse.clientResponses) {
            messageHandlerResponse.clientResponses.forEach(res => {
                this.sendJson(client, res);
            });
        }

        let meta = this.gameAndPlayerInfo(userInfo, game);

        if (pushPlayerResponse) {
            responses.push({
                username: player.username,
                message: game.logic.getStateForPlayer(player),
                gameInfo: meta.gameInfo,
                playerInfo: meta.playerInfo
            });
        } else if (pushAllPlayersResponse) {
            game.logic.state.players.forEach(gamePlayer => {
                responses.push({
                    username: gamePlayer.username,
                    message: game.logic.getStateForPlayer(gamePlayer),
                    gameInfo: meta.gameInfo,
                    playerInfo: meta.playerInfo
                });
            });
        }
        return responses;
    }
}