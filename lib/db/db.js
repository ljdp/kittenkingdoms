const EventEmitter = require('events');
const assert = require('assert');
const Datastore = require('nedb');

module.exports = class DB extends EventEmitter {
    constructor( {filename, inMemory} ) {
        super();
        this.db = new Datastore({filename, inMemoryOnly: inMemory});
        this.db.loadDatabase((err) => {
            if (err) {
                this.emit('error', err);
            } else {
                this.emit('load', this);
                if (process.env.NODE_ENV === 'production') {
                    this.db.persistence.setAutocompactionInterval(1000 * 60 * 25) // Compact DB every 25 minutes.
                }
            }
        });
    }

    assertDbLoaded() {
        assert(this.db, "Database not loaded.");
    }

    ensureIndex( fieldName, unique, sparse, expireAfterSeconds ) {
        this.assertDbLoaded();
        this.db.ensureIndex({
            fieldName,
            unique,
            sparse,
            expireAfterSeconds
        });
    }

    insert( doc ) {
        this.assertDbLoaded();
        return new Promise((resolve, reject) => {
            this.db.insert(doc, (err, doc) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(doc);
                }
            })
        });
    }

    findMany( query, projection ) {
        this.assertDbLoaded();
        return new Promise((resolve, reject) => {
            this.db.find(query, projection, (err, docs) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(docs);
                }
            })
        });
    }

    findOne( query, projection ) {
        this.assertDbLoaded();
        return new Promise((resolve, reject) => {
            this.db.findOne(query, projection, (err, doc) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(doc);
                }
            })
        });
    }

    deleteMany( query, options = {} ) {
        this.assertDbLoaded();
        options.multi = true;
        return new Promise((resolve, reject) => {
            this.db.remove(query, options, (err, removedCount) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(removedCount);
                }
            })
        });
    }

    deleteOne( query, options = {} ) {
        this.assertDbLoaded();
        options.multi = false;
        return new Promise((resolve, reject) => {
            this.db.remove(query, options, (err, removedCount) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(removedCount);
                }
            })
        });
    }

    updateMany( query, update, options = {} ) {
        this.assertDbLoaded();
        options.multi = true;
        return new Promise((resolve, reject) => {
            this.db.update(query, update, options, (err, updatedCount, updatedDocs) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({updatedCount, updatedDocs});
                }
            })
        });
    }  

    updateOne( query, update, options = {} ) {
        this.assertDbLoaded();
        options.multi = false;
        return new Promise((resolve, reject) => {
            this.db.update(query, update, options, (err, updatedCount, updatedDocs) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({updatedCount, updatedDocs});
                }
            })
        });
    }
}