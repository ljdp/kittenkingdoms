const Discord = require('eris');
const moment = require('moment');
const EventEmitter = require('events');
const crypto = require("crypto");
const fs = require('fs');
const templater = require('lodash.template');
const templateFiles = fs.readdirSync(__dirname + '/templates');
const templates = templateFiles.reduce((dict, filename) => {
    dict[filename] = templater(fs.readFileSync(__dirname + '/templates/' + filename));
    return dict;
}, {});

module.exports = class DiscordBot extends EventEmitter {
    constructor( {botToken, ids, redis, logger} ) {
        super();
        this.ids = ids;
        this.redis = redis;
        this.logger = logger;
        this.guildId = ids.GUILD_ID;
        let bot = this.bot = new Discord.CommandClient(botToken, {
            restMode: false
        }, {
            description: "Kitten Kingdoms trusty bot.",
            owner: "ljperky",
            prefix: "!",
        });
        this.guild = null;
        bot.on('ready', () => {
            logger.info('Discord bot ready!');
            try {
                this.guild = bot.guilds.find(guild => guild.id == ids.GUILD_ID);
                this.generalChannel = this.guild.channels.find(channel => channel.name == 'general');
                this.commitsChannel = this.guild.channels.find(channel => channel.name == 'commits');
            } catch(err) {
                logger.error('discord error', err);
            }
        });
        bot.on('guildMemberAdd', this.onNewGuildMember.bind(this));
        bot.registerCommand('games', this.gamesCommand.bind(this), {
            description: 'List all the public games',
            usage: '!games',
            cooldown: 1000 * 60,
            cooldownMessage: 'Must wait a minute before using the /games command again.',
            deleteCommand: true
        });
        bot.registerCommand('uptime', this.uptimeCommand.bind(this), {
            description: 'Show how long the bot has been online.',
            usage: '!uptime',
            deleteCommand: true
        });
        bot.registerCommand('link', this.linkAccountCommand.bind(this), {
            description: 'Link your Kitten Kingdoms account with this Discord server, see your user settings for the token',
            usage: '!link [token]',
            deleteCommand: true
        });
        bot.registerCommand('notify', this.notifySettingsCommand.bind(this), {
            description: 'Change your notification settings',
            usage: '!notify [email|discord] [on|off]',
            deleteCommand: true
        });
        bot.connect();
        this.startTime = new Date();
    }

    disconnect() {
        this.bot.disconnect();
    }

    onNewGuildMember( guild, member ) {
        let response = `Welcome our newest kitten: ${member.mention}!`;
        this.bot.createMessage(this.generalChannel.id, response);
    }

    uptimeCommand( message, args ) {
        let time = moment(this.startTime).fromNow(true);
        return time;
    }

    gamesCommand( message, args ) {
        this.emit('gamesCommand');
    }

    linkAccountCommand( message, args ) {
        let token = args[0];
        if (token && token.length >= 4) {
            let key = 'discord:token:'+token;
            this.redis.get(key).then(userId => {
                if (userId) {
                    this.emit('linkAccount', {memberId: message.author.id, userId: userId});
                    this.redis.del(key);
                }
            });
        }
    }

    notifySettingsCommand( message, args ) {
        let settingType = args[0];
        let isOn = (args[1] == 'on') ? true : false;
        if (settingType == 'email') {
            this.emit('changeNotificationSetting', {type:'email', memberId: message.author.id, isOn});
        } else if (settingType == 'discord') {
            this.emit('changeNotificationSetting', {type:'discord', memberId: message.author.id, isOn});
        }
    }

    sendLinkSuccessMessage( memberId, username ) {
        let message = templates['link_success.txt']({username});
        this.messageMember(memberId, message);
    }

    getRoleId( role ) {
        switch(role) {
            case 'ninja':
                return this.ids.NINJA_ROLE_ID;
            case 'wizard':
                return this.ids.WIZARD_ROLE_ID;
            case 'basic':
                return this.ids.KITTEN_ROLE_ID;
            default:
                return null;
        }
    }

    addRoleToMember( memberId, role ) {
        let id = this.getRoleId(role);
        if (id) {
            this.guild.addMemberRole(memberId, id);
        } else {
            this.logger.warn(`ID for role ${role} does not exist.`);
        }
    }

    removeRoleFromMember( memberId, role ) {
        let id = this.getRoleId(role);
        if (id) {
            this.guild.removeMemberRole(memberId, id);
        } else {
            this.logger.warn(`ID for role ${role} does not exist.`);
        }
    }

    messageMember( memberId, message ) {
        this.bot.getDMChannel(memberId).then(privateChannel => {
            privateChannel.createMessage(message);
        });
    }

    sendNewGameMessage( basicGameInfo ) {
        let gameInfoStr = this.parseBasicGameInfo(basicGameInfo);
        this.bot.createMessage(this.generalChannel.id, `**A new game has started!**\n${gameInfoStr}`);
    }

    sendOpenGamesMessage( basicGameInfos ) {
        let lines = basicGameInfos.map(basicGameInfo => {
            return this.parseBasicGameInfo(basicGameInfo);
        })
        if (lines.length > 0) {
            lines = lines.join('\n--------------------\n');
            this.bot.createMessage(this.generalChannel.id, `**Here are the available public games:**\n${lines}`)
        } else {
            this.bot.createMessage(this.generalChannel.id, `There are no available public games :(`);
        }
    }

    parseBasicGameInfo( basicGameInfo ) {
        let gameInfo = {
            gameName: basicGameInfo.name,
            playerCount: basicGameInfo.players.length,
            maxPlayers: basicGameInfo.config.maxPlayers,
            currentTurn: basicGameInfo.turn,
            maxTurns: basicGameInfo.config.maxTurns,
            fogOfWarEnabled: basicGameInfo.config.fogOfWarEnabled,
            joinUrl: (basicGameInfo.players.length < basicGameInfo.config.maxPlayers) ? 
                `https://kittenkingdoms.com/lobby?join=${basicGameInfo.id}` : 'Game full'
        };
        return this._parseBasicGameInfo(gameInfo);
    }

    _parseBasicGameInfo( {gameName, playerCount, maxPlayers, currentTurn, maxTurns, fogOfWarEnabled, joinUrl} ) {
        return `**${gameName}**: ${playerCount}/${maxPlayers} players - Turn ${currentTurn}/${maxTurns} - Fog of war: ${fogOfWarEnabled}\n${joinUrl}`;
    }

    sendGameFinishedMessage( {gameName, turn, players} ) {
        let winnerName = players[0].username;
        let winningScore = players[0].score;
        let msg = `**${gameName}** has finished on turn ${turn}!\nPlayer ${winnerName} won with a score of ${winningScore} :D\nThere were ${players.length} players in total.`;
        this.bot.createMessage(this.generalChannel.id, msg);
    }

    sendBitbucketPushMessage( event ) {
        if (!this.commitsChannel) return;
        if (event.push && event.push.changes) {
            let lines = [];
            event.push.changes.forEach(change => {
                if (change.new && change.new.name === 'master') {
                    change.commits.forEach(commit => {
                        lines.push(`${commit.message}`);
                    });
                }
            });
            let reversedLines = [];
            for (let i = lines.length - 1; i >= 0; i--) {
                reversedLines.push(lines[i]);
            }
            this.bot.createMessage(this.commitsChannel.id, reversedLines.join('\n'));
        }
    }

    generateAccountLinkToken( userid ) {
        let expireTime = 1000 * 60 * 60;
        let token = crypto.randomBytes(4).toString("hex");
        this.redis.set('discord:token:'+token, userid, 'EX', expireTime);
        return token;
    }
}