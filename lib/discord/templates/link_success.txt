We have linked your account (${username})!
You can control notification settings from within discord.
For example:
**!notify discord on** will enable discord notifications for your games.
**!notify email off** will disable email notifications.