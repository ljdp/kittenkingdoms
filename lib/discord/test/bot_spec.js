const DiscordBot = require('../discord_bot');
const secrets = require('../../config').secrets;
const discordBot = new DiscordBot({
    botToken: secrets.DISCORD_TOKEN,
    ids: secrets.DISCORD_IDS
});
const bitbucketEventPayload = require('./bitbucket_push_event.json');

process.on('unhandledRejection', r => console.log(r));

discordBot.on('linkAccountCommand', (event) => {
    if (event.token == 'xxxx') {
        discordBot.addRoleToMember(event.memberId, 'basic');
        discordBot.sendLinkSuccessMessage(event.memberId, 'perky');
    }
})

setTimeout(() => {
    // discordBot.sendBitbucketPushMessage(bitbucketEventPayload);
}, 1000 * 5);

setTimeout(() => {
    console.log('done');
}, 1000 * 15);