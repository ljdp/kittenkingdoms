module.exports = class DiscordRoutes {
    constructor( {auth, discordBot} ) {
        let router = this.router = require('express').Router();
        router.get('/token', auth.ensureValidToken, (req, res) => {
            let token = discordBot.generateAccountLinkToken(req.decodedToken.id);
            res.send(token);
        });
        router.post('/bitbucket', (req, res) => {
            discordBot.sendBitbucketPushMessage(req.body);
            res.send('OK');
        });
    }
}