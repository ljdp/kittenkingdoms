const fs = require('fs');
const yaml = require('js-yaml');


exports.config = yaml.safeLoad(fs.readFileSync('/run/secrets/config', 'utf8').trim());
exports.secrets = yaml.safeLoad(fs.readFileSync('/run/secrets/secrets', 'utf8').trim());
exports.ssl = {
    cert: fs.readFileSync(process.env.SSL_CERT, 'utf8').trim(),
    key: fs.readFileSync(process.env.SSL_KEY, 'utf8').trim()
};
exports.getAutoGamesConfig = function() {
    return yaml.safeLoad(fs.readFileSync(__dirname + '/autogames.yml', 'utf8').trim());
}