global._lib = __dirname;
const fs = require('fs');
const util = require('util');
const logger = require('winston');
const LogMailer = require('./mailer/log_mailer');
const https = require('https');
const bodyParser = require("body-parser");
const app = require('express')();
const secrets = require('./config').secrets;
const config = require('./config').config;
const sslServerKeys = require('./config').ssl;
const Subscriptions = require('./subscriptions');
const Games = require('./games').Games;
const GameServer = require('./games').GameServer;
const GameRoutes = require('./games').Routes;
const DB = require('./db/db');
const DiscordBot = require('./discord').DiscordBot;
const DiscordRouter = require('./discord').DiscordRouter;
const Mailer = require('./mailer');
const Redis = require('ioredis');
const cookierparser = require('cookie-parser');

/*
    ---- INIT LOGGER ----
*/
logger.remove(logger.transports.Console);
if (process.env.NODE_ENV === 'development') {
    logger.add(logger.transports.Console, {level: 'debug', handleExceptions: true});
} else {
    logger.add(logger.transports.Console, {level: 'info'});
}
logger.add(logger.transports.File, {
    name: 'standard-log',
    filename: config.LOG_DIR + "/server.log", 
    json: false
});
logger.add(logger.transports.File, {
    name: 'exceptions-log',
    filename: config.LOG_DIR + '/exceptions.log',
    json: false,
    handleExceptions: true,
    level: 'crit',
    humanReadableUnhandledException: true
});
process.on('unhandledRejection', r => logger.error(r));

/*
    ---- INIT REDIS ----
*/
let redis = new Redis(6379, 'redis');

/*
    ---- INIT SYSTEMS ----
*/
let usersDb = new DB({filename: "/kittenkingdoms_datastore/users.json", inMemory: config.IN_MEMORY_DB});
let gamesDb = new DB({filename: "/kittenkingdoms_datastore/games.json", inMemory: config.IN_MEMORY_DB});
let mailer = new Mailer({amazonSesKeyId: secrets.MAILER_KEY_ID, amazonSesKeySecret: secrets.MAILER_KEY_SECRET});
let payments = new Subscriptions.Payments({stripeKey: secrets.STRIPE_SECRET, logger});
let users = new Subscriptions.Users({db: usersDb, payments, mailer, logger});
let subscriptionRoutes = new Subscriptions.Routes({users, redis, logger});
let games = new Games({db: gamesDb, auth: Subscriptions.Auth, config, mailer, users, redis, logger});
let gameRoutes = new GameRoutes({auth: Subscriptions.Auth, games: games});

if (process.env.NODE_ENV === 'production') {
    logger.add(LogMailer, {
        level: 'error',
        to: 'luke@locogame.co.uk',
        mailer: mailer
});
}

/*
    ---- DISCORD BOT ----
*/
let discordBot;
if (!config.DISABLE_DISCORD_BOT) {
    discordBot = new DiscordBot({botToken: secrets.DISCORD_TOKEN, ids: secrets.DISCORD_IDS, redis, logger});
    discordBot.on('gamesCommand', commandArgs => {
        let activeGames = games.activeGames();
        discordBot.sendOpenGamesMessage(activeGames);
    });
    discordBot.on('linkAccount', ({memberId, userId}) => {
        users.retrieve(userId).then(user => {
            if (user) {
                logger.info(`Found user ${user.username} to link to discord`);
                user.discordMemberId = memberId;
                user.save(users.db);
                discordBot.addRoleToMember(memberId, 'basic');
                if (user.hasRole('wizard')) {
                    discordBot.addRoleToMember(memberId, 'wizard');
                } else if (user.hasRole('ninja')) {
                    discordBot.addRoleToMember(memberId, 'ninja');
                }
                discordBot.sendLinkSuccessMessage(memberId, user.username);
            }
        });
    });
    discordBot.on('changeNotificationSetting', ({type, isOn, memberId}) => {
        users.findOne({"discordMemberId": memberId}).then(user => {
            if (user) {
                if (type == 'email') {
                    user.emailNotify = isOn;
                } else if (type == 'discord') {
                    user.discordNotify = isOn;
                }
                user.save(users.db);
                discordBot.messageMember(memberId, `${type} notifications are ${isOn ? 'on' : 'off'}.`);
            }
        })
    });
    users.on('userRoleChanged', (user) => {
        if (user.discordMemberId) {
            try {
                discordBot.removeRoleFromMember(user.discordMemberId, 'wizard');
                discordBot.removeRoleFromMember(user.discordMemberId, 'ninja');
            } catch(err) {}
            if (user.hasRole('wizard')) {
                discordBot.addRoleToMember(user.discordMemberId, 'wizard');
            } else if (user.hasRole('ninja')) {
                discordBot.addRoleToMember(user.discordMemberId, 'ninja');
            }
        }
    });
    games.on('gameCreated', (gameDoc) => {
        discordBot.sendNewGameMessage(gameDoc.basicData());
    });
}

/*
    ---- HTTPS SERVER ----
*/
let server = https.createServer(sslServerKeys, app);

/*
    ---- WEBSOCKET GAME SERVER ----
*/
let gameServer = new GameServer({httpsServer: server, games: games, auth: Subscriptions.Auth});

/*
    ---- EXPRESS ROUTES ----
*/
app.use(function(req, res, next) {
    let allowedOrigins = ['https://kittenkingdoms.com', 'https://stripe.com', 'http://localhost:8080'];
    let origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS, PATCH');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    return next();
});
app.use(cookierparser());
app.use('/stripe_7sdjn32', bodyParser.text({type: '*/*'}));
app.use(bodyParser.json());
app.use('/', subscriptionRoutes.router);
app.use('/', gameRoutes.router);
if (discordBot) {
    app.use('/discord', new DiscordRouter({auth: Subscriptions.Auth, discordBot}).router);
}
app.get('/', (req, res) => {
    res.send('OK');
});

/*
    ---- HTTPS SERVER LISTEN ----
*/
server.listen(config.PORT);
logger.info(`Server started on port ${config.PORT}`);

/*
    ---- MAIN GAME LOOP ----
*/
setInterval(() => {
    games.updateAll();
    games.saveAll();
}, config.UPDATE_INTERVAL);


/*
    ---- SHUTDOWN ----
*/
function gracefulShutdown() {
    logger.info('Saving games then exiting server...');
    if (discordBot) {
        discordBot.disconnect();
    }
    gameServer.close();
    server.close();
    games.saveAll().then(_ => {
        process.exit(2);
    }).catch(_ => {
        process.exit(2);
    });
}
process.on('SIGTERM', gracefulShutdown);
process.on('SIGINT', gracefulShutdown);