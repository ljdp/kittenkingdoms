const AmazonSES = require('node-ses');
const templater = require('lodash.template');
const fs = require('fs');
const config = require('../config').config;

const emailTemplateFiles = fs.readdirSync(__dirname + '/templates/');
const emailTemplates = emailTemplateFiles.reduce((dict, filename) => {
    let fileExt = filename.match(/\.([0-9a-z]+)$/)[1];
    if (dict[fileExt]) {
        let filenameNoExt = filename.replace("."+fileExt, "");
        let data = fs.readFileSync(`${__dirname}/templates/${filename}`, 'utf8');
        let lines = data.split('\n');
        dict[fileExt][filenameNoExt] = {
            subject: templater(lines[0]),
            body: templater(lines.slice(1).join('\n'))
        };
    }
    return dict;
}, {
    html: {},
    txt: {}
});
const htmlWrapper = emailTemplates.html.wrapper;

module.exports = class Mailer {
    constructor({amazonSesKeyId, amazonSesKeySecret, amazonUrl = 'https://email.eu-west-1.amazonaws.com'}) {
        this.client = AmazonSES.createClient({
            key: amazonSesKeyId,
            secret: amazonSesKeySecret,
            amazon: amazonUrl
        });
        this.queue = [];
        setInterval(() => {
            if (this.queue.length > 0) {
                this.sendQueuedEmails(13);
            }
        }, 1000);
    }

    sendMail({to, template, params = {}, wrapperTemplate = true}) {
        let htmlTemplate = emailTemplates.html[template];
        let textTemplate = emailTemplates.txt[template];
        if (htmlTemplate || textTemplate) {
            let _bcc = [];
            this.queue.push({
                to: to,
                htmlTemplate: htmlTemplate,
                textTemplate: textTemplate,
                params: params,
                wrapperTemplate
            });
        }
    }

    _sendMail({to, htmlTemplate, textTemplate, params, wrapperTemplate}) {
        var message;
        if (htmlTemplate) {
            if (wrapperTemplate) {
                message = htmlWrapper.body({
                    subject: htmlTemplate.subject(params),
                    body: htmlTemplate.body(params)
                });
            } else {
                message = htmlTemplate.body(params);
            }
        } else {
            message = textTemplate.body(params);
        }
        this.client.sendEmail({
            to: to,
            from: 'dontreply@kittenkingdoms.com',
            subject: htmlTemplate ? htmlTemplate.subject(params) : textTemplate.subject(params),
            message: message,
            altText: textTemplate ? textTemplate.body(params) : htmlTemplate.body(params)
        }, (err, data, res) => {
            if (err) {
                console.error(err);
            }
        });
    }

    sendQueuedEmails( max ) {
        let lastIndex = Math.min(this.queue.length, max);
        for (let i = 0; i < lastIndex; i++) {
            this._sendMail(this.queue[i]);
        }
        this.queue = this.queue.slice(lastIndex);
    }
}