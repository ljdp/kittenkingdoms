const winston = require('winston');
const util = require('util');

module.exports = class LogMailer extends winston.Transport {
    constructor( {mailer, level = 'error', to} ) {
        super();
        this.to = to;
        this.mailer = mailer;
        this.level = level;
        this.handleExceptions = true;
    }

    log( level, msg, meta, next ) {
        let stack = '';
        if (meta.stack) {
            stack = (typeof meta.stack == 'object') ? util.inspect(meta.stack, true, null) : meta.stack;
            stack = stack.replace(/\n/g, '<br/>').replace(/\s/g, '&nbsp;');
        }
        let params = {
            level: level,
            meta: util.inspect(meta, true, null),
            msg: msg,
            stack: stack
        };
        this.mailer.sendMail({
            to: this.to,
            template: 'logger',
            params,
            wrapperTemplate: false
        });
        next(null, true);
    }
}