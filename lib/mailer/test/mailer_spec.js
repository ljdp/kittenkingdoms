const Mailer = require('../mailer');

describe('mailer', () => {
    var mailer;
    before((done) => {
        mailer = new Mailer();
        mailer.sendMail({
            to: 'lukeperkin@gmail.com',
            template: 'test',
            params: {
                username: 'Luke',
                number: 42
            }
        });
        setTimeout(() => {
            done();
        }, 5000);
    });
    it("is done", () => {
        return true;
    });
});