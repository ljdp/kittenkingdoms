const moment = require('moment');

module.exports = class User {
    constructor( {
            email, 
            username, 
            passwordHash,
            _id,
            createdAt,
            emailNotify = true,
            discordNotify = false,
            roles = ['basic'], 
            games = [],
            discordMemberId = null,
            stripeCustomerId = null,
            temporaryNinjaEndDate,
            billingReceipts = [],
            subscription} )
    {
        this.id = _id;
        this.createdAt = createdAt || moment().format();
        this.email = email;
        this.username = username;
        this.passwordHash = passwordHash;
        this.emailNotify = emailNotify;
        this.discordNotify = discordNotify;
        this.roles = roles;
        this.games = games;
        this.discordMemberId = discordMemberId;
        this.stripeCustomerId = stripeCustomerId;
        this.temporaryNinjaEndDate = temporaryNinjaEndDate;
        this.subscription = subscription;
        this.billingReceipts = billingReceipts;
        this.validateSubsciption();
    }

    save( db ) {
        if (this.id) {
            return db.updateOne({_id: this.id}, this).then(result => {
                return this;
            });
        } else {
            return db.insert(this).then(doc => {
                this.id = doc._id;
                return this;
            });
        }
    }

    update( db, update ) {
        if (this.id) {
            return db.updateOne({_id: this.id}, update);
        } else {
            return this.save(db);
        }
    }

    hasRole( role ) {
        return this.roles.indexOf(role) != -1;
    }

    addRole( role ) {
        if (!this.hasRole(role)) {
            this.roles.push(role);
        }
    }

    removeRole( role ) {
        let index = this.roles.indexOf(role);
        if (index != -1) {
            this.roles.splice(index, 1);
        }
    }

    emailIsValid() {
        return this.email && this.email.length > 3 && this.email.indexOf('@') != -1;
    }

    usernameIsValid() {
        return this.username && this.username.length > 3;
    }

    isValid() {
        return this.emailIsValid() && this.usernameIsValid();
    }

    validateSubsciption() {
        if (this.subscription && this.subscription.activeUntil > 0) {
            let expiry = this.subscription.activeUntil;
            if (moment().isAfter(moment.unix(expiry))) {
                this.cancelSubscription();
            }
        } else if (this.temporaryNinjaEndDate && moment().isSameOrAfter(this.temporaryNinjaEndDate)) {
            this.removeRole('ninja');
        }
    }

    cancelSubscription() {
        this.subscription.status = 'canceled';
        this.removeRole('ninja');
    }

    reactivateSubscription( periodEnd ) {
        this.subscription.activeUntil = periodEnd;
        this.subscription.status = 'active';
        this.addRole('ninja');
    }

    deleteSubscription() {
        this.removeRole(this.subscription.role);
        delete this.subscription;
    }

    setSubscription( subscription ) {
        this.subscription = subscription;
        this.addRole(subscription.role);
    }
}