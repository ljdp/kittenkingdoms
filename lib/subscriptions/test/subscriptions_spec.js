const assert = require('assert');
const User = require('../models/user');
const Users = require('../systems/users');
const Payments = require('../systems/payments');
const Database = require('../../db/db.js');
const Helpers = require('./helpers');
const nock = require('nock');
const StripeMockWebhooks = require('stripe-mock-webhooks')

// process.on('unhandledRejection', r => console.log(r));

describe("Subscriptions:", function () {
    var db, payments, users, stripeNock;
    before(function(){
        nock.disableNetConnect();
        nock.enableNetConnect('127.0.0.1');
        db = Helpers.testDb();
        payments = new Payments({stripeKey: 'xxxx'});
        users = new Users({db, payments});
        stripeNock = nock("https://api.stripe.com:443/v1");
    })

    describe("A user succesfully starts the 'Ninja' subscription", function () {
        var subscribeResponse, user;
        before(function ( done ) {
            stripeNock.post('/customers').reply(200, Helpers.stripeCustomerResponse);
            stripeNock.post('/subscriptions').reply(200, Helpers.stripeSubscriptionResponse);
            user = Helpers.validUser();
            users.create(user).then(result => {
                users.subscribeToNinjaPlan({user, paymentEmail: user.email, paymentToken: 'xxxx'}).then(response => {
                    subscribeResponse = response;
                    done();
                }, err => {
                    subscribeResponse = err;
                    done();
                })
            }, done);
        })

        it("returns a subscription object", function () {
            assert.equal(subscribeResponse.stripeSubscriptionId, Helpers.stripeSubscriptionResponse.id);
        })
        it("adds the 'ninja' role to the user", function() {
            assert(user.hasRole('ninja'));
        })
        it("saved the user to the database", function() {
            return db.findOne({username: user.username}).then(dbUser => {
                assert(dbUser.subscription, "user in database has no subscription object");
                assert(dbUser.subscription.customerId == user.subscription.customerId);
            });
        })
    })

    describe("A user starts the 'Ninja' subscription with an invalid card", function(){
        var subscribeResponse, user;
        before(function ( done ) {
            db.deleteMany({});
            stripeNock.post('/customers').reply(401, Helpers.badStripeCustomerResponse);
            user = Helpers.validUser();
            users.create(user).then(result => {
                users.subscribeToNinjaPlan({user, paymentEmail: user.email, paymentToken: 'xxxx'}).then(response => {
                    subscribeResponse = response;
                    done();
                }, err => {
                    subscribeResponse = err;
                    done();
                })
            }, done);
        })
        it("returns an error", function() {
            assert(subscribeResponse.statusCode >= 400);
        })
        it("shows a card declined message to the client", function () {
            assert(subscribeResponse.message.includes("declined"));
        })
    })

    describe('A user cancels their subscription...', () => {
        var user, cancelError;
        before((done) => {
            db.deleteMany({});
            stripeNock.post('/customers').reply(200, Helpers.stripeCustomerResponse);
            stripeNock.post('/subscriptions').reply(200, Helpers.stripeSubscriptionResponse);
            stripeNock.delete('/subscriptions/sub_BHcGJGQ5GAp2A0').reply(200, Helpers.stripeSubscriptionResponse);
            user = Helpers.validUser();
            users.create(user).then(result => {
                return users.subscribeToNinjaPlan({user, paymentEmail: user.email, paymentToken: 'xxxx'});
            })
            .then(result => {
                return users.cancelNinjaPlan(user);
            })
            .then(response => {
                done();
            })
            .catch(err => {
                cancelError = err;
                done();
            });
        });
        it("does not error", () => {
            assert(!cancelError, cancelError);
        })
        it('but they keep their "ninja" role', () => {
            assert(user.hasRole('ninja'));
        });
        it('changes their subsciption status to "canceled"', () => {
            assert(user.subscription, "user does not have a subscription object");
            assert.equal(user.subscription.status, 'canceled', "expected user subscription status to be 'canceled'");
        })
    });

    describe('When a user tries to cancel when already canceled', () => {
        var user, cancelResponse;
        before((done) => {
            db.deleteMany({});
            user = Helpers.validUser();
            user.subscription = {
                id: 'tier2',
                status: 'canceled'
            };
            users.cancelNinjaPlan(user).then(result => {
                cancelResponse = "";
                done();
            }, err => {
                cancelResponse = err;
                done();
            });
        });
        it('show the user a helpful error message', () => {
            assert(cancelResponse.includes("already canceled"));
        });
        it('leaves the subscription status to "canceled"', () => {
            assert.equal(user.subscription.status, "canceled");
        })
    });

    describe('Webhooks:', () => {
        var webhooks, user;
        before((done) => {            
            webhooks = new StripeMockWebhooks({
                version: '2015-10-01',
                url: 'https://127.0.0.1:8443/stripe_7sdjn32'
            });
            for(let key in webhooks.stripeData.webhooks) {
                let newkey = key.replace(/_/g, ".");
                webhooks.stripeData.webhooks[newkey] = webhooks.stripeData.webhooks[key];
            }
            stripeNock.post('/customers').reply(200, Helpers.stripeCustomerResponse);
            stripeNock.post('/subscriptions').reply(200, Helpers.stripeSubscriptionResponse);
            nock('https://127.0.0.1:8443').post('/stripe_7sdjn32').reply(function(uri, body, res) {
                try {
                    let result = payments.handleStripeWebhook(null, JSON.parse(body), false);
                } catch(err) {
                    console.error(err);
                }
                res(null, [200, 'OK']);
            });
            user = Helpers.validUser();
            users.create(user).then(_user => {
                return users.subscribeToNinjaPlan({user, paymentEmail: 'test@test.com', paymentToken: 'xxxx'});
            })
            .then(success => {
                done();
            })
            .catch(err => {
                assert.fail(err);
                done();
            })
        });
        describe('customer.subscription.deleted:', () => {
            before((done) => {
                webhooks.trigger('customer.subscription.deleted', {
                    data: {
                        object: {
                            id: user.subscription.stripeSubscriptionId,
                            customer: user.subscription.stripeCustomerId
                        }
                    }
                });
                setTimeout(() => {
                    users.retrieveByUsername(user.username).then(_user => {
                        user = _user;
                        done();
                    });
                }, 50);
            });
            it('removes the subscription object from the user', () => {
                assert.equal(user.subscription, undefined);
            });
            it('removes the "ninja" role', () => {
                assert(!user.hasRole('ninja'));
            });
        })
    });
})