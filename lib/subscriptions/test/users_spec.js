const assert = require('assert');
const sinon = require('sinon');
const User = require('../models/user');
const Users = require('../systems/users');
const Payments = require('../systems/payments');
const Database = require('../../db/db.js');
const Helpers = require('./helpers');

// process.on('unhandledRejection', r => console.log(r));

describe("Users:", function () {
    describe("Creating a valid user", function () {
        var validUser, users, createResponse, db;
        before(function (done) {
            db = Helpers.testDb();
            db.on('load', () => {
                users = Helpers.mockUsers(db);
                sinon.spy(users, "ensureUserValid");
                sinon.spy(users, "ensureUserUnique");
                validUser = Helpers.validUser();
                users.create(validUser).then(result => {
                    createResponse = result;
                    done();
                }, error => {
                    createResponse = error;
                    done();
                });
            });
        })

        it("is successful", function(){
            assert(createResponse.success, "user.create was not successful");
        })
        it("validates user", function () {
            assert(users.ensureUserValid.called, createResponse.message);
        })
        it("ensures the user is unique", function() {
            assert(users.ensureUserUnique.called, createResponse.message);
        })
        it("adds the user to the database", function() {
            return db.findOne({username: validUser.username}).then(user => {
                assert(user != null, "user not found");
                delete user._id;
                assert.deepEqual(user, validUser.doc(), "user in database is not equal to user we created");
            });
        })
    })

    describe("Creating an already existing user", function () {
        var db, user, createResponse;
        before(function (done) {
            db = Helpers.testDb();
            db.on('load', function() {
                users = Helpers.mockUsers(db);
                user = Helpers.validUser();
                users.create(user).then(success => {
                    return users.create(user);
                }).then(success => {
                    createResponse = success;
                    done();
                }, error => {
                    createResponse = error;
                    done();
                });;
            });
        })
        it("is unsuccesful", function () {
            assert(createResponse.success == false, "creating a duplicate user was successful");
        })
        it("does not place the user in the database", function () {
            return db.findMany({username: user.username}).then(users => {
                assert(users.length == 1, "incorrect number of users in the databse");
            });
        })
    })

    describe("Creating a unique but invalid user", function() {
        var db, users, user, createResponse;
        before(function (done) {
            db = Helpers.testDb();
            db.on('load', function() {
                users = Helpers.mockUsers(db);
                sinon.spy(users, "ensureUserValid");
                user = Helpers.invalidUser();
                users.create(user).then(success => {
                    createResponse = success;
                    done();
                }, error => {
                    createResponse = error;
                    done();
                });;
            });
        })
        it("calls the validation method", function () {
            assert(users.ensureUserValid.called, createResponse.message);
        })
        it("is unsuccessful", function(){
            assert(createResponse.success == false, "user.create was successful");
        })
    })
})