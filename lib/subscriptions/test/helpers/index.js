const User = require('../../models/user');
const Users = require('../../systems/users');
const Payments = require('../../systems/payments');
const Database = require('../../../db/db.js');

exports.validUser = function() { 
    return new User({email: 'test@test.com', username: 'foobar', passwordHash: 'xxxx'});
};
exports.invalidUser = function() {
    return new User({email: '1', username: 'fizzbuzz', passwordHash: 'xxxx'});
};
exports.testDb = function() {
    let db = new Database({inMemory: true});
    db.ensureIndex("email", true);
    db.ensureIndex("username", true);
    return db;
};
exports.mockUsers = function(db) {
    let payments = new Payments({stripeKey: 'xxxx'});
    let users = new Users({db, payments});
    return users;
}
exports.stripeCustomerResponse = {
    "id": "cus_BHNcJViJHHi2c5",
    "object": "customer",
    "account_balance": 0,
    "created": 1503780749,
    "currency": "gbp",
    "default_source": "card_1AvBGbJzn0fiuFdybVZdCh0W",
    "delinquent": false,
    "description": null,
    "discount": null,
    "email": "foobar@test.com",
    "livemode": false,
    "metadata": {
      "username": "foobar"
    },
    "shipping": null,
    "sources": {
      "object": "list",
      "data": [
        {
          "id": "card_1AvBGbJzn0fiuFdybVZdCh0W",
          "object": "card",
          "address_city": null,
          "address_country": null,
          "address_line1": null,
          "address_line1_check": null,
          "address_line2": null,
          "address_state": null,
          "address_zip": null,
          "address_zip_check": null,
          "brand": "Visa",
          "country": "US",
          "customer": "cus_BHkmwOp0ljYQgQ",
          "cvc_check": "pass",
          "dynamic_last4": null,
          "exp_month": 1,
          "exp_year": 2020,
          "fingerprint": "1Adc6Lpp8PvN9bEq",
          "funding": "credit",
          "last4": "4242",
          "metadata": {
          },
          "name": null,
          "tokenization_method": null
        }
      ],
      "has_more": false,
      "total_count": 1,
      "url": "/v1/customers/cus_BHkmwOp0ljYQgQ/sources"
    }
};
exports.stripeSubscriptionResponse = {
    "id": "sub_BHcGJGQ5GAp2A0",
    "object": "subscription",
    "application_fee_percent": null,
    "cancel_at_period_end": false,
    "canceled_at": null,
    "created": 1503749063,
    "current_period_end": 1506427463,
    "current_period_start": 1503749063,
    "customer": "cus_BHNcJViJHHi2c5",
    "discount": null,
    "ended_at": null,
    "items": {
      "object": "list",
      "data": [
        {
          "id": "si_1Av31XJzn0fiuFdyDG54sfU6",
          "object": "subscription_item",
          "created": 1503749063,
          "metadata": {
          },
          "plan": {
            "id": "tier2",
            "object": "plan",
            "amount": 199,
            "created": 1503683083,
            "currency": "gbp",
            "interval": "month",
            "interval_count": 1,
            "livemode": false,
            "metadata": {
            },
            "name": "Kitten Ninja",
            "statement_descriptor": "KITTENKINGDOMS.COM",
            "trial_period_days": null
          },
          "quantity": 1
        }
      ],
      "has_more": false,
      "total_count": 1,
      "url": "/v1/subscription_items?subscription=sub_BHcGJGQ5GAp2A0"
    },
    "livemode": false,
    "metadata": {
    },
    "plan": {
      "id": "tier2",
      "object": "plan",
      "amount": 199,
      "created": 1503683083,
      "currency": "gbp",
      "interval": "month",
      "interval_count": 1,
      "livemode": false,
      "metadata": {
      },
      "name": "Kitten Ninja",
      "statement_descriptor": "KITTENKINGDOMS.COM",
      "trial_period_days": null
    },
    "quantity": 1,
    "start": 1503749063,
    "status": "active",
    "tax_percent": null,
    "trial_end": null,
    "trial_start": null
  }
  exports.badStripeCustomerResponse = {
      raw_type: 'card_error',
      code: 'card_declined',
      raw: {
          message: 'Your card was declined.',
          type: 'card_error',
          code: 'card_declined',
          decline_code: 'generic_decline'
      },
      error: {message:'Your card was declined'},
      type: 'card_error',
  };