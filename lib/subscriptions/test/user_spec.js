const assert = require('assert');
const User = require('../models/user');
const moment = require('moment');

describe("User model:", function() {
    var validUser;
    var invalidUser;
    before(function(){
        validUser = new User({
            email: 'test@test.com',
            username: 'foobar'
        })
        invalidUser = new User({
            email: 'nop',
            username: '1'
        })
    })

    describe("User valid if...", function() {
        it("all validators succesful", function() {
            assert.ok(validUser.isValid(), "User not valid");
        })
    })

    describe("User invalid if...", function() {
        it("any validators fail", function(){
            assert.ok(!invalidUser.isValid(), "User is valid");
        })
        it("username is not unique or less than 3 chars", function(){
            assert.ok(!invalidUser.usernameIsValid(), "Username is valid");
        })
        it("email is not unique or has incorrect format", function(){
            assert.ok(!invalidUser.emailIsValid(), "Email is valid");
        })
    })

    describe("Upon creation...", function(){
        it("'createdAt' field set to now", function(){
            assert.ok(validUser.createdAt, "createdAt field does not exist");
            assert.ok(validUser.createdAt == moment().format(), "Date invalid");
        })
        it("has an empty games array", function() {
            assert(validUser.games, "'games' field missing");
            assert(Array.isArray(validUser.games), "'games' is not an array");
            assert(validUser.games.length == 0, "'games' array is not empty");
        })
        it("user has 'basic' role", function() {
            assert.ok(validUser.roles, "'roles' field does not exist");
            assert.ok(Array.isArray(validUser.roles), "'roles' field is not an array");
            assert.ok(validUser.roles.indexOf('basic') != -1, "'basic' role not found");
        })
        it("user does not have 'ninja' role", function(){
            assert.ok(validUser.roles.indexOf('ninja') == -1, "user has 'ninja' role");
        })
        it("user does not have 'wizard' role", function(){
            assert.ok(validUser.roles.indexOf('wizard') == -1, "user has 'wizard' role");
        })
        it("user does not have 'admin' role", function(){
            assert.ok(validUser.roles.indexOf('admin') == -1, "user has 'admin' role");
        })
    })
})