const bcrypt = require('bcrypt');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const AUTH_TOKEN_SECRET = require(`${_lib}/config`).secrets.AUTH_TOKEN_SECRET;

function createTokenForUser( user ) {
    return new Promise((resolve, reject) => {
        let userData = {
            id: user.id,
            roles: user.roles,
            email: user.email,
            username: user.username
        };
        jwt.sign(userData, AUTH_TOKEN_SECRET, {expiresIn: '15d'}, (err, token) => {
            if (err) {
                reject(err);
            } else {
                resolve(token);
            }
        });
    });
}

exports.createTokenForUser = createTokenForUser;

exports.login = function(users, {username, password}) {
    return users.retrieveByUsername(username).then(user => {
        if (user) {
            return bcrypt.compare(password, user.passwordHash).then(isPasswordCorrect => {
                if (isPasswordCorrect) {
                    return createTokenForUser(user).then(token => {
                        return {token, user};
                    });
                } else {
                    return false;
                }
            });
        } else {
            throw {message: 'User not found', status: 404};
        }
    });
};

exports.loginViaEmail = function(users, redis, {username}) {
    return users.retrieveByUsername(username).then(user => {
        if (user) {
            let token = crypto.randomBytes(4).toString('hex');
            let expireTime = 1000 * 60 * 60 * 2; // 2 hours.
            redis.set('go:type:'+token, 'login', 'EX', expireTime);
            redis.set('go:userid:'+token, user.id, 'EX', expireTime);
            users.sendLoginTokenByMail({user: user, loginUrl: `${process.env.GAMESERVER_URL}/go/${token}`});
            return true;
        } else {
            throw {message: 'User not found', status: 404};
        }
    })
}

exports.generateOneTimeLogin = function(redis, {username}) {
    let token = crypto.randomBytes(4).toString('hex');
    let expireTime = 1000 * 60 * 60 * 2; // 2 hours.
    redis.set('go:type:'+token, 'login', 'EX', expireTime);
    redis.set('go:username:'+token, username, 'EX', expireTime);
    return `${process.env.GAMESERVER_URL}/go/${token}`;
}

exports.generateOneTimeGameLogin = function(redis, {username, gameId}) {
    let token = crypto.randomBytes(4).toString('hex');
    let expireTime = 1000 * 60 * 60 * 2; // 2 hours.
    redis.set('go:type:'+token, 'game', 'EX', expireTime);
    redis.set('go:username:'+token, username, 'EX', expireTime);
    redis.set('go:gameid:'+token, gameId, 'EX', expireTime);
    return `${process.env.GAMESERVER_URL}/go/${token}`;
};

exports.activateToken = function(users, redis, {token}) {
    let goType, username, gameId, user;
    let keys = ['go:type:'+token, 'go:username:'+token, 'go:gameid:'+token];
    return redis.mget(keys).then(result => {
        goType = result[0];
        username = result[1];
        gameId = result[2];
        return users.retrieveByUsername(username);
    }).then(_user => {
        if (_user) {
            user = _user;
            redis.del(keys);
            return createTokenForUser(user);
        } else {
            throw {status: 404, message: 'User not found'};
        }
    }).then(authToken => {
        if (authToken) {
            switch(goType) {
                case 'login':
                    return {user, authToken, redirect: `${process.env.WEBSITE_URL}/lobby`};
                case 'game':
                    return {user, authToken, redirect: `${process.env.WEBSITE_URL}/game?id=${gameId}`};
                default:
                    return {user, authToken, redirect: process.env.WEBSITE_URL};
            }
        } else {
            throw {status: 500, message: 'Internal server error'};
        }
    });
} 

exports.register = function(users, {email, username, password}) {
    return bcrypt.hash(password, 10).then(passwordHash => {
        return users.create({username, email, passwordHash});
    });
};

function validateAuthToken(authToken) {
    return new Promise((resolve, reject) => {
        jwt.verify(authToken, AUTH_TOKEN_SECRET, (err, decoded) => {
            if (err) {
                reject(err);
            } else {
                resolve(decoded);
            }
        });
    });
};
exports.validateAuthToken = validateAuthToken;

function ensureValidToken( req, res, next ) {
    if (req.cookies && req.cookies.authToken) {
        validateAuthToken(req.cookies.authToken).then(decoded => {
            req.decodedToken = decoded;
            next();
        }, err => {
            res.status(403).send("Forbidden (invalid auth token)");
        });
    } else {
        res.status(403).send("Forbidden (missing auth token)");
    }
}
exports.ensureValidToken = ensureValidToken;

exports.isUserPaidSubscriber = function( decodedToken ) {
    return decodedToken.roles.includes('ninja') 
        || decodedToken.roles.includes('wizard') 
        || decodedToken.roles.includes('admin');
}

exports.getUserHighestRole = function( decodedToken ) {
    if (decodedToken.roles.includes('admin')) {
        return 'admin';
    } else if (decodedToken.roles.includes('wizard')) {
        return 'wizard';
    } else if (decodedToken.roles.includes('ninja')) {
        return 'ninja';
    } else {
        return 'basic';
    }
}