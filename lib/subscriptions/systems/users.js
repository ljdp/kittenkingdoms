const assert = require('assert');
const User = require('../models/user');
const Payments = require('./payments');
const moment = require('moment');
const EventEmitter = require('events');

module.exports = class Users extends EventEmitter {
    constructor( {db, payments, mailer, logger} ) {
        super();
        assert(db, "Needs a database");
        assert(payments, "Needs a payments handler");
        this.db = db;
        this.payments = payments;
        this.mailer = mailer;
        this.logger = logger;
        this.payments.on('subscription_payment_succeeded', this.onSubscriptionPaymentSucceeded.bind(this));
        this.payments.on('subscription_payment_failed', this.onSubscriptionPaymentFailed.bind(this));
        this.payments.on('subscription_canceled', this.onSubscriptionCanceled.bind(this));
        this.payments.on('subscription_deleted', this.onSubscriptionDeleted.bind(this));
        this.payments.on('charge_succeeded', this.onBillingSucceeded.bind(this));
        this.payments.on('charge_failed', this.onBillingFailed.bind(this));
        this.payments.on('charge_refunded', this.onBillingRefunded.bind(this));
    }

    create( {username, email, passwordHash} ) {
        let user = new User({username, email, passwordHash});
        return new Promise((resolve, reject) => {
            Promise.all([this.ensureUserValid(user), this.ensureUserUnique(user)])
                .then(success => {
                    return user.save(this.db);
                })
                .then(doc => {
                    this.mailer.sendMail({
                        to: user.email,
                        template: 'welcome',
                        params: {
                            username: user.username,
                            loginUrl: 'https://kittenkingdoms.com'
                        }
                    });
                    this.mailer.sendMail({
                        to: 'luke@locogame.co.uk',
                        template: 'basic',
                        params: {
                            subject: 'New User Registered',
                            message: `A new user: ${user.username}, has registered.`
                        }
                    });
                    resolve({success: true, message: "Successfully created user", user: doc});
                })
                .catch(err => {
                    reject({success: false, message: err});
                });
        });
    }

    ensureUserValid( user ) {
        return new Promise((resolve, reject) => {
            if (user.isValid()) {
                resolve();
            } else {
                reject("User is not valid");
            }
        });
    }

    ensureUserUnique( user ) {
        return new Promise((resolve, reject) => {
            let findEmail = this.db.findOne({email: user.email});
            let findUser = this.db.findOne({username: user.username});
            Promise.all([findEmail, findUser]).then(result => {
                if (result[0] == null && result[1] == null) {
                    resolve();
                } else {
                    reject("User is not unique");
                }
            }, err => {
                reject(err || 'Unknown error');
            });
        })
    }

    sendLoginTokenByMail( {username, loginUrl} ) {
        this.retrieveByUsername(username).then(user => {
            if (user) {
                this.mailer.sendMail({
                    to: user.email,
                    template: 'login',
                    params: {
                        username: user.username,
                        loginUrl: loginUrl
                    }
                });
            }
        });
    }

    retrieve( id ) {
        return this.findOne({_id: id});
    }

    retrieveByUsername( username ) {
        return this.findOne({username: username});
    }

    findOne( query ) {
        return new Promise((resolve, reject) => {
            this.db.findOne(query).then(doc => {
                if (doc) {
                    resolve(new User(doc));
                } else {
                    resolve(null);
                }
            }, err => {
                reject(err);
            });
        });
    }

    findMany( query ) {
        return new Promise((resolve, reject) => {
            this.db.findMany(query).then(docs => {
                let users = docs.map(doc => new User(doc));
                resolve(users);
            }, err => {
                reject(err);
            });
        });
    }

    setUserAdmin( {userid, makeAdmin = true} ) {
        return this.retrieve(userid).then(user => {
            if (user) {
                if (!user.hasRole('admin') && makeAdmin) {
                    user.roles.push('admin');
                } else if(user.hasRole('admin') && !makeAdmin) {
                    let index = user.roles.indexOf('admin');
                    user.roles.splice(index, 1);
                }
                return user.save(this.db);
            } else {
                throw {message: 'User not found', status: 404};
            }
        });
    }

    updatePaymentDetails( {userid, paymentEmail, paymentToken} ) {
        var user;
        return this.retrieve(userid).then(_user => {
            user = _user;
            if (!user) {
                throw {message: "User not found", status: 404};
            } else if (!user.stripeCustomerId) {
                throw {message: "User does not have a stripe customer id"};
            }
            return this.payments.updateCustomerPaymentDetails({
                customerId: user.stripeCustomerId,
                email: paymentEmail,
                paymentToken: paymentToken,
                userid: user.id,
                username: user.username
            });
        });
    }

    subscribeToNinjaPlan( {userid, paymentEmail, paymentToken} ) {
        var user;
        return this.retrieve(userid).then(_user => {
            user = _user;
            if (!user) {
                throw {message: "User not found", status: 404};
            }
            if (user.stripeCustomerId) {
                return this.payments.retrieveCustomer(user.stripeCustomerId);
            } else {
                return this.payments.createCustomer({email: paymentEmail, userid: userid, username: user.username, paymentToken: paymentToken});
            }
        })
        .then(customer => {
            return this.payments.createSubscription({customerId: customer.id, planId: 'tier2'});
        })
        .then(subscription => {
            return this.acceptNinjaSubscription({user, subscription});
        });
    }
    
    acceptNinjaSubscription( {user, subscription} ) {
        user.stripeCustomerId = subscription.customer;
        user.setSubscription({
            stripeCustomerId: subscription.customer,
            stripeSubscriptionId: subscription.id,
            id: subscription.plan.id,
            name: subscription.plan.name,
            chargeAmount: subscription.plan.amount,
            activeUntil: subscription.current_period_end,
            status: 'active',
            role: 'ninja'
        });
        this.mailer.sendMail({
            to: user.email,
            template: 'ninja_plan_start',
            params: {
                username: user.username,
                loginUrl: 'https://kittenkingdoms.com',
                charge: subscription.plan.amount / 100,
                nextPaymentDate: moment.unix(subscription.current_period_end).format('dddd, MMMM Do YYYY')
            }
        });
        this.mailer.sendMail({
            to: 'luke@locogame.co.uk',
            template: 'basic',
            params: {
                subject: 'User subscribed to ninja plan',
                message: `${user.username} has subscribed to the ninja plan`
            }
        });
        this.emit('userRoleChanged', user);
        return user.save(this.db);
    }

    cancelNinjaPlan( {userid} ) {
        var user;
        return this.retrieve(userid).then(_user => {
            user = _user;
            if (!user) {
                throw {message: "User not found", status: 404};
            } else if (!user.subscription) {
                throw {message: "User is not subscribed to any plan"};
            } else if (user.subscription.id != "tier2") {
                throw {message: "User not subscribed to ninja plan"};
            } else if (user.subscription.status == 'canceled') {
                throw {message: "User has already canceled their ninja plan"};
            }
            return this.payments.cancelSubscription({
                subscriptionId: user.subscription.stripeSubscriptionId
            });
        }).then(result => {
            user.cancelSubscription();
            this.emit('userRoleChanged', user);
            return user.save(this.db);
        });
    }

    reactivateNinjaPlan( {userid} ) {
        var user;
        return this.retrieve(userid).then(_user => {
            user = _user;
            if (!user) {
                throw {message: "User not found", status: 404};
            } else if (!user.subscription) {
                throw {message: "User does not have any subscription"};
            } else if (user.subscription.status == 'active') {
                throw {message: "User subscription is already active"};
            }
            return this.payments.reativateSubscription({subscriptionId: user.subscription.stripeSubscriptionId, planId: user.subscription.id});
        }).then(_subscription => {
            user.reactivateSubscription(user.subscription.activeUntil);
            this.emit('userRoleChanged', user);
            return user.save(this.db);
        });
    }

    subscribeToWizardPlan( {userid, paymentEmail, paymentToken} ) {
        var user;
        return this.retrieve(userid).then(_user => {
            user = _user;
            if (user.stripeCustomerId) {
                return this.payments.retrieveCustomer(user.stripeCustomerId);
            } else {
                return this.payments.createCustomer({email: paymentEmail, userid: userid, username: user.username, paymentToken: paymentToken});
            }
        }).then(customer => {
            return this.payments.createCharge({customerId: customer.id, chargeAmount: 2999, chargeType: 'wizard'});
        }).then(charge => {
            if (user.subscription) {
                this.payments.cancelSubscription({subscriptionId: user.subscription.stripeSubscriptionId, atPeriodEnd: false});
            }
            return this.acceptWizardSubscription({user, charge});
        });
    }

    acceptWizardSubscription( {user, charge} ) {
        user.stripeCustomerId = charge.customer;
        user.setSubscription({
            stripeCustomerId: charge.customer,
            stripeChargeId: charge.id,
            id: 'tier3',
            name: 'Kitten Wizard',
            chargeAmount: charge.amount,
            activeUntil: 0,
            status: 'active',
            role: 'wizard'
        });
        this.mailer.sendMail({
            to: user.email,
            template: 'wizard_plan_start',
            params: {
                username: user.username,
                loginUrl: 'https://kittenkingdoms.com',
                charge: charge.amount / 100,
            }
        });
        this.mailer.sendMail({
            to: 'luke@locogame.co.uk',
            template: 'basic',
            params: {
                subject: 'User subscribed to wizard plan',
                message: `${user.username} has subscribed to the wizard plan`
            }
        });
        this.emit('userRoleChanged', user);
        return user.save(this.db);
    }

    onSubscriptionPaymentSucceeded( event ) {
        this.logger.info('onSubscriptionPaymentSucceeded', event);
        this.findOne({"stripeCustomerId": event.customerId}).then(user => {
            if (user) {
                user.reactivateSubscription(event.subscriptionPeriodEnd);
                user.save(this.db);
                this.mailer.sendMail({
                    to: user.email,
                    template: 'ninja_plan_payment_succeeded',
                    params: {
                        username: user.username,
                        loginUrl: 'https://kittenkingdoms.com',
                        charge: event.paymentTotal / 100,
                        currency: event.currency,
                        periodStart: moment.unix(event.subscriptionPeriodStart).format('dddd, MMMM Do YYYY'),
                        periodEnd: moment.unix(event.subscriptionPeriodEnd).format('dddd, MMMM Do YYYY')
                    }
                });
            } else {
                this.logger.warn('could not find user with customer id', event.customerId);
            }
        });
    }

    onSubscriptionPaymentFailed( event ) {
        this.logger.info('onSubscriptionPaymentFailed', event);
        this.findOne({"stripeCustomerId": event.customerId}).then(user => {
            if (user) {
                // Send mail to customer.
                this.mailer.sendMail({
                    to: user.email,
                    template: 'ninja_plan_payment_failed',
                    params: {
                        username: user.username,
                        loginUrl: 'https://kittenkingdoms.com',
                        charge: event.amountDue / 100,
                        currency: event.currency,
                        periodStart: moment.unix(event.subscriptionPeriodStart).format('dddd, MMMM Do YYYY'),
                        periodEnd: moment.unix(event.subscriptionPeriodEnd).format('dddd, MMMM Do YYYY')
                    }
                });
            } else {
                this.logger.warn('could not find user with customer id', event.customerId);
            }
        });
    }

    onSubscriptionCanceled( event ) {
        // At this point the subscription has already been canceled and the users
        // subscription data has been updated.
        this.logger.info('onSubscriptionCanceled', event);
        this.findOne({"stripeCustomerId": event.customer}).then(user => {
            if (user) {
                this.mailer.sendMail({
                    to: user.email,
                    template: 'ninja_plan_cancel',
                    params: {
                        username: user.username,
                        loginUrl: 'https://kittenkingdoms.com',
                        endDate: moment.unix(event.current_period_end).format('dddd, MMMM Do YYYY')
                    }
                });
                this.mailer.sendMail({
                    to: 'luke@locogame.co.uk',
                    template: 'basic',
                    params: {
                        subject: 'User canceled ninja subscription',
                        message: `${user.username} has canceled their ninja subscription`
                    }
                });
            }
        });
    }

    onSubscriptionDeleted( event ) {
        this.logger.info('onSubscriptionDeleted', event);
        this.findOne({"stripeCustomerId": event.customerId}).then(user => {
            if (user) {
                // Send mail to customer.
                if (user.subscription.stripeSubscriptionId == event.subscriptionId) {
                    user.deleteSubscription();
                    user.save(this.db);
                    this.emit('userRoleChanged', user);
                    this.mailer.sendMail({
                        to: user.email,
                        template: 'ninja_plan_deleted',
                        params: {
                            username: user.username,
                            loginUrl: 'https://kittenkingdoms.com'
                        }
                    });
                    this.mailer.sendMail({
                        to: 'luke@locogame.co.uk',
                        template: 'basic',
                        params: {
                            subject: 'User ninja subscription has ended',
                            message: `${user.username} has ended their ninja subscription`
                        }
                    });
                } else {
                    this.logger.warn('mismatch with subscription id');
                }
            } else {
                this.logger.warn('could not find user with customer id', event.customerId);
            }
        });
    }

    onBillingSucceeded( event ) {
        this.logger.info('onBillingSucceeded', event);
        this.findOne({"stripeCustomerId": event.customerId}).then(user => {
            if (user) {
                if (!user.billingReceipts) {
                    user.billingReceipts = [];
                }
                user.billingReceipts.push({
                    cardDigitsLast4: event.cardDigitsLast4,
                    billingAmount: event.billingAmount / 100,
                    currency: event.currency,
                    timestamp: event.timestamp
                });
                user.save(this.db);
            } else {
                console.warn('could not find user with customer id', event.customerId);
            }
        });
    }

    onBillingFailed( event ) {
        this.logger.info('onBillingFailed', event);
        this.findOne({"stripeCustomerId": event.customerId}).then(user => {
            if (user) {
                this.mailer.sendMail({
                    to: user.email,
                    template: 'charge_failed',
                    params: {
                        username: user.username,
                        loginUrl: 'https://kittenkingdoms.com',
                        billingAmount: event.billingAmount / 100,
                        currency: event.currency,
                        description: event.description,
                        cardDigitsLast4: event.cardDigitsLast4
                    }
                });
            } else {
                console.warn('could not find user with customer id', event.customerId);
            }
        });
    }

    onBillingRefunded( event ) {
        this.logger.info('onBillingRefunded', event);
        this.findOne({"stripeCustomerId": event.customerId}).then(user => {
            if (user) {
                this.mailer.sendMail({
                    to: user.email,
                    template: 'charge_refunded',
                    params: {
                        username: user.username,
                        loginUrl: 'https://kittenkingdoms.com',
                        amountRefunded: event.amountRefunded / 100,
                        currency: event.currency,
                        description: event.description,
                        cardDigitsLast4: event.cardDigitsLast4
                    }
                });
            } else {
                console.warn('could not find user with customer id', event.customerId);
            }
        });
    }

    retrieveAndUpdateUserSubscription( customerId, userId ) {
        var user, customer;
        return this.findOne({"_id": userId}).then(_user => {
            user = _user;
            if (user) {
                return this.payments.retrieveCustomer(customerId);
            } else {
                return Promise.reject('user not found');
            }
        }).then(_customer => {
            customer = _customer;
            if (customer && customer.subscriptions.data.length > 0) {
                return customer.subscriptions.data[0];
            } else {
                return Promise.reject('customer not found');
            }
        }).then(subscription => {
            user.stripeCustomerId = customerId;
            user.setSubscription({
                stripeCustomerId: subscription.customer,
                stripeSubscriptionId: subscription.id,
                id: subscription.plan.id,
                name: subscription.plan.name,
                chargeAmount: subscription.plan.amount,
                activeUntil: subscription.current_period_end,
                status: subscription.status,
                role: subscription.plan.metadata.type
            });
            return user.save(this.db);
        });
    }

}