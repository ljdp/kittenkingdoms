const assert = require('assert');
const EventEmitter = require('events');
const StripeAPI = require('stripe');
const STRIPE_WEBHOOK_SECRET = require('../../config').secrets.STRIPE_WEBHOOK_SECRET;

module.exports = class Payments extends EventEmitter {
    constructor( {stripeKey, logger} ) {
        super();
        assert(stripeKey, "Needs the a stripe api key");
        this.stripe = StripeAPI(stripeKey);
        this.logger = logger;
    }

    createCustomer({email, userid, username, paymentToken}) {
        assert(email && username && paymentToken, "Invalid parameters");
        return this.stripe.customers.create({email, metadata: {userid, username}, source: paymentToken});
    }

    retrieveCustomer( customerId ) {
        return this.stripe.customers.retrieve(customerId);
    }

    updateCustomerPaymentDetails( {customerId, email, userid, username, paymentToken} ) {
        return this.stripe.customers.update(customerId, {
            source: paymentToken,
            metadata: {
                userid: userid,
                username: username
            }
        });
    }

    createSubscription({customerId, planId}) {
        assert(customerId && planId, "Invalid parameters");
        return this.stripe.subscriptions.create({
            customer: customerId,
            items: [{plan: planId}]
        }).then(subscription => {
            this.emit("subscription_created", {subscription});
            return subscription;
        }, err => {
            this.logger.error('Stripe createSubscription', err);
            if (err.type == 'StripeCardError') {
                throw err;
            } else {
                throw {
                    internalError: true,
                    message: "Internal Server Error. You have not been charged. Please contact luke@locogame.co.uk"
                };
            }
        });
    }

    cancelSubscription({subscriptionId, atPeriodEnd = true}) {
        return this.stripe.subscriptions.del(subscriptionId, {at_period_end: atPeriodEnd}).then(subscription => {
            this.emit("subscription_canceled", {
                subscription
            });
            return subscription;
        });
    }

    reativateSubscription({subscriptionId, planId}) {
        return this.stripe.subscriptions.retrieve(subscriptionId).then(subscription => {
            return this.stripe.subscriptions.update(subscriptionId, {
                items: [{
                    id: subscription.items.data[0].id,
                    plan: planId
                }]
            });
        });
    }

    createCharge({customerId, paymentToken, chargeAmount, chargeType}) {
        return this.stripe.charges.create({
            amount: chargeAmount,
            currency: 'gbp',
            customer: customerId,
            description: 'Kittenkingdoms.com Kitten Wizard Plan',
            metadata: {
                type: chargeType
            }
        }).then(charge => {
            if (charge.paid) {
                return charge;
            } else {
                throw {message: 'Your card was declined'};
            }
        }, err => {
            if (err.type == 'StripeCardError') {
                throw {message: 'Your card was declined'};
            } else {
                throw {
                    internalError: true,
                    message: "Internal Server Error. You have not been charged. Please contact luke@locogame.co.uk"
                };
            }
        });
    }

    handleStripeWebhook( signature, body, checkSignature = true ) {
        let event;

        if (checkSignature) {
            try {
                event = this.stripe.webhooks.constructEvent(body, signature, STRIPE_WEBHOOK_SECRET);
            } catch(err) {
                this.logger.warn(err);
            }
        } else {
            event = body;
        }

        if(!event) {
            return false;
        }

        let stripeObject = event.data.object;
        let subscription;

        switch(event.type) {
            case 'customer.subscription.deleted':
                this.emit("subscription_deleted", {
                    customerId: stripeObject.customer,
                    subscriptionId: stripeObject.id
                });
                break;
            case 'invoice.payment_succeeded':
                subscription = stripeObject.lines.data[0];
                this.emit("subscription_payment_succeeded", {
                    customerId: stripeObject.customer,
                    subscriptionId: subscription.id,
                    planId: subscription.plan.id,
                    planName: subscription.plan.name,
                    paymentTotal: stripeObject.total,
                    currency: stripeObject.currency,
                    subscriptionPeriodStart: subscription.period.start,
                    subscriptionPeriodEnd: subscription.period.end
                });
                break;
            case 'invoice.payment_failed':
                subscription = stripeObject.lines.data[0];
                this.emit("subscription_payment_failed", {
                    customerId: stripeObject.customer,
                    subscriptionId: subscription.id,
                    planId: subscription.plan.id,
                    planName: subscription.plan.name,
                    amountDue: stripeObject.amount_due,
                    currency: stripeObject.currency,
                    subscriptionPeriodStart: subscription.period.start,
                    subscriptionPeriodEnd: subscription.period.end
                });
                break;
            case 'charge.succeeded':
                this.emit("charge_succeeded", {
                    customerId: stripeObject.customer,
                    billingAmount: stripeObject.amount,
                    currency: stripeObject.currency,
                    cardDigitsLast4: stripeObject.source.last4,
                    timestamp: stripeObject.created
                });
                break;
            case 'charge.failed':
                this.emit("charge_failed", {
                    customerId: stripeObject.customer,
                    billingAmount: stripeObject.amount,
                    currency: stripeObject.currency,
                    description: stripeObject.description,
                    cardDigitsLast4: stripeObject.source.last4,
                    timestamp: stripeObject.created
                });
                break;
            case 'charge.failed':
                this.emit("charge_refunded", {
                    customerId: stripeObject.customer,
                    amountRefunded: stripeObject.amountRefunded,
                    currency: stripeObject.currency,
                    description: stripeObject.description,
                    cardDigitsLast4: stripeObject.source.last4,
                    timestamp: stripeObject.created
                });
                break;
        }
        return true;
    }
}