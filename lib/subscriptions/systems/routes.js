const User = require('../models/user');
const Auth = require('./auth');
const bodyParser = require('body-parser');
const crypto = require('crypto');
const moment = require('moment');

function Routes({users, redis, logger}) {
    var router = require('express').Router();
    var ensureValidToken = Auth.ensureValidToken;
    var urlencodedParser = bodyParser.urlencoded({ extended: false })

    function retrieveUser( req, res, next ) {
        users.retrieve(req.decodedToken.id).then(user => {
            if (user) {
                req.user = user;
                next();
            } else {
                res.status(404).send('User ' + req.decodedToken.username + ' not found');
            }
        });
    }

    function sendCookies( res, {authToken, username, userid} ) {
        let maxAge = 1000 * 60 * 60 * 24 * 15 // 15 days.
        if (process.env.NODE_ENV == 'development') {
            res.cookie('authToken', authToken, {maxAge: maxAge, path: '/', secure: false, httpOnly: true});
            res.cookie('username', username, {maxAge: maxAge, path: '/', secure: false});
            res.cookie('userid', userid, {maxAge: maxAge, path: '/', secure: false});
        } else {
            res.cookie('authToken', authToken, {maxAge: maxAge, domain: '.kittenkingdoms.com', path: '/', httpOnly: true, secure: true});
            res.cookie('username', username, {maxAge: maxAge, domain: '.kittenkingdoms.com', path: '/', secure: true});
            res.cookie('userid', userid, {maxAge: maxAge, domain: '.kittenkingdoms.com', path: '/', secure: true});
        }
    }

    function errorHandler( res ) {
        return function(err) {
            if (err.internalError) {
                logger.error('User routes internal error', err);
                res.status(500).json(err);
            } else if(err.message) {
                res.status(err.status || 400).json(err);
            } else {
                logger.error('User routes internal error', err);
                res.status(500).json({message: 'Internal Server Error'});
            }
        }
    }

    router.post('/auth/register', urlencodedParser, (req, res) => {
        Auth.register(users, {
            username:   req.body.username.trim(),
            email:      req.body.email.trim(),
            password:   req.body.password
        }).then(user => {
            res.send('OK');
        }).catch(errorHandler(res));
    });

    router.post('/auth/login', urlencodedParser, (req, res) => {
        Auth.login(users, {
            username:   req.body.username.trim(),
            password:   req.body.password
        }).then(result => {
            if (result) {
                sendCookies(res, {authToken: result.token, username: result.user.username, userid: result.user.id});
                res.send('OK');
            } else {
                res.status(403).send("Incorrect login credentials");
            }
        }).catch(errorHandler(res));
    });

    router.post('/auth/login_via_email', urlencodedParser, (req, res) => {
        let oneTimeLoginUrl = Auth.generateOneTimeLogin(redis, {username: req.body.username});
        users.sendLoginTokenByMail({username: req.body.username, loginUrl: oneTimeLoginUrl});
        res.send('OK');
    });

    router.get('/go/:token', (req, res) => {
        let token = req.params.token || 'x';
        Auth.activateToken(users, redis, {token}).then(result => {
            sendCookies(res, {authToken: result.authToken, username: result.user.username, userid: result.user.id});
            res.redirect(result.redirect);
        }).catch(errorHandler(res));
    });

    router.post('/auth/logout', ensureValidToken, (req, res) => {
        res.clearCookie('authToken');
        res.clearCookie('username');
        res.clearCookie('userid');
        res.send('OK');
    });

    router.get('/auth/check', ensureValidToken, (req, res) => {
        res.send('OK');
    });

    router.get('/auth/cookies', (req, res) => {
        res.json(req.cookies);
    });

    router.post('/subscription/ninja', ensureValidToken, (req, res) => {
        users.subscribeToNinjaPlan({
            userid: req.decodedToken.id,
            paymentEmail: req.body.stripeEmail,
            paymentToken: req.body.stripeToken
        }).then(result => {
            res.send('OK');
        }).catch(errorHandler(res));
    });

    router.delete('/subscription/ninja', ensureValidToken, (req, res) => {
        users.cancelNinjaPlan({
            userid: req.decodedToken.id
        }).then(result => {
            res.send('OK');
        }).catch(errorHandler(res));
    });

    router.post('/subscription/ninja/activate', ensureValidToken, (req, res) => {
        users.reactivateNinjaPlan({
            userid: req.decodedToken.id
        }).then(result => {
            res.send('OK');
        }).catch(errorHandler(res));
    });

    router.post('/subscription/wizard', ensureValidToken, (req, res) => {
        users.subscribeToWizardPlan({
            userid: req.decodedToken.id,
            paymentEmail: req.body.stripeEmail,
            paymentToken: req.body.stripeToken
        }).then(result => {
            res.send('OK');
        }).catch(errorHandler(res));
    });

    router.post('/stripe_7sdjn32', (req, res) => {
        let signature = req.headers["stripe-signature"];
        if (users.payments.handleStripeWebhook(signature, req.body)) {
            res.send('OK');
        } else {
            res.sendStatus(400);
        }
    });

    router.post('/stripe_update_card', ensureValidToken, (req, res) => {
        users.updatePaymentDetails({
            userid: req.decodedToken.id,
            paymentEmail: req.body.stripeEmail,
            paymentToken: req.body.stripeToken
        }).then(result => {
            res.send('OK');
        }).catch(errorHandler(res));
    });

    router.get('/stripe_customer/:customerId/link/:userId', ensureValidToken, (req, res) => {
        if (req.decodedToken.roles.includes('admin')) {
            let customerId = req.params.customerId;
            let userId = req.params.userId;
            users.retrieveAndUpdateUserSubscription( customerId, userId ).then(result => {
                res.send('OK');
            }).catch(errorHandler(res));
        }
    })

    router.get('/user/:username', ensureValidToken, retrieveUser, (req, res) => {
        if (req.user.username == req.params.username) {
            Auth.createTokenForUser(req.user).then(token => {
                let userShallowClone = Object.assign({}, req.user);
                delete userShallowClone.passwordHash;
                sendCookies(res, {authToken: token, username: req.user.username, userid: req.user.id});
                res.json(userShallowClone);
            }).catch(errorHandler(res));
        } else {
            res.status(403).send('Forbidden to retrieve user');
        }
    });

    router.patch('/user/:username', ensureValidToken, retrieveUser, (req, res) => {
        req.user.email = req.body.email.trim();
        req.user.emailNotify = req.body.emailNotify;
        req.user.discordNotify = req.body.discordNotify;
        users.ensureUserValid(req.user).then(valid => {
            req.user.save(users.db);
            res.send('OK');
        }, err => {
            res.status(400).send(err);
        }).catch(errorHandler(res));
    });

    /*
        ---- PREMIUM CODES ----
    */
    router.get('/premium_generate', ensureValidToken, (req, res) => {
        if (req.decodedToken.roles.includes('admin')) {
            let code = crypto.randomBytes(4).toString('hex');
            redis.set('premium:'+code, '1', 'EX', 1000 * 60 * 60 * 42);
            res.send(code);
        } else {
            res.sendStatus(403);
        }
    });
    router.get('/premium/:code', ensureValidToken, (req, res) => {
        users.retrieve(req.decodedToken.id).then(user => {
            if (user) {
                let code = req.params.code;
                return redis.get('premium:'+code).then(result => {
                    if (result) {
                        redis.del('premium:'+code);
                        user.addRole('ninja');
                        user.temporaryNinjaEndDate = moment().add(30, "days").format();
                        user.save(users.db);
                        res.redirect('https://kittenkingdoms.com/lobby?premium=1');
                    } else {
                        throw {status: 403, message: 'Invalid Code'};
                    }   
                });
            } else {
                throw {status: 404, message: 'User not found'};
            }
        }).catch(err => {
            if (err.status) {
                res.status(err.status).send(err.message);
                logger.warn('Premium code', err);
            } else {
                logger.error('Premium code', err);
                res.status(500).send('Internal Server Error');
            }
        });
    });

    this.router = router;
}

module.exports = Routes;