const Discord = require('eris');
const yaml = require('js-yaml');
const fs = require('fs');
const CONFIG = yaml.safeLoad(fs.readFileSync('server/config.yml'));
const moment = require('moment');

var discordClass;
if (CONFIG.DISABLE_DISCORD_BOT) {
    discordClass = class {
        constructor() {
            console.log('Discord bot disabled.');
        }
        sendGameFinishedMessage() { return true; }
        sendNewGameMessage() { return true; }
        generateInviteUrl() {
            return "http://localhost/fake_discord_invite";
        }
    }
} else {
    discordClass = class {
        constructor( botToken, guildId ) {
            this.guildId = guildId;
            let bot = this.bot = new Discord.CommandClient(botToken, {}, {
                description: "Kitten Kingdoms trusty bot.",
                owner: "ljperky",
                prefix: "!"
            });
            this.guild = null;
            bot.on('ready', () => {
                console.log('Discord bot ready!');
                this.guild = bot.guilds.find(guild => guild.id == guildId);
                this.generalChannel = this.guild.channels.find(channel => channel.name == 'general');
                console.log('general channel id', this.generalChannel.id);
            });
            bot.on('guildMemberAdd', this.onNewGuildMember.bind(this));
            bot.registerCommand('games', this.gamesCommand.bind(this), {
                description: 'List all the public games',
                usage: '!games',
                cooldown: 1000 * 60,
                cooldownMessage: 'Must wait a minute before using the /games command again.'
            });
            bot.registerCommand('uptime', this.uptimeCommand.bind(this), {
                description: 'Show how long the bot has been online.',
                usage: '!uptime'
            });
            bot.connect();
            this.startTime = new Date();
        }

        onNewGuildMember( guild, member ) {
            let response = 'Welcome our newest kitten: ${member.mention}!';
            this.bot.createMessage(this.generalChannel.id, response);
        }

        parseGame( game ) {
            let joinUrl = (game.playerCount < game.maxPlayers) ? `https://kittenkingdoms.com/lobby.html?join=${game.guid}` : '';
            return `**${game.name}**: ${game.playerCount}/${game.maxPlayers} players - Turn ${game.turn}/${game.maxTurns} - Fog of war: ${game.fogOfWar}\n${joinUrl}`;
        }

        uptimeCommand( message, args ) {
            let time = moment(this.startTime).fromNow(true);
            return time;
        }

        gamesCommand( message, args ) {
            if (this.getGames) {
                let games = this.getGames();
                let lines = games.reduce((res, game) => {
                    res.push(this.parseGame(game));
                    return res;
                }, []);
                if (lines.length > 0) {
                    return lines.join('\n\n');
                } else {
                    return "There are no public games :(";
                }
            } else {
                return 'Command not implemented';
            }
        }

        sendNewGameMessage( game ) {
            this.bot.createMessage(this.generalChannel.id, `**A new game has started!**\n${this.parseGame(game)}`);
        }

        sendGameFinishedMessage( game ) {
            let players = game.getPlayers();
            let winnerName = players[0].username;
            let winningScore = players[0].score;
            let msg = `**${game.name()}** has finished!\nPlayer ${winnerName} won with a score of ${winningScore} :D\nThere were ${players.length} players in total.`;
            this.bot.createMessage(this.generalChannel.id, msg);
        }

        generateInviteUrl( maxUses = 1 ) {
            return new Promise((resolve, reject) => {
                this.bot.createChannelInvite(this.generalChannel.id, {maxUses}).then(invite => {
                    console.log(invite);
                    resolve("https://discord.gg/"+invite.code);
                }, err => {
                    reject(err);
                });
            });
        }
    }
}

module.exports = discordClass;