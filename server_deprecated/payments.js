const fs = require('fs');
const yaml = require('js-yaml');
const CONFIG = yaml.safeLoad(fs.readFileSync('server/config.yml'));
const StripeAPI = require("stripe");
const stripe = StripeAPI(CONFIG.STRIPE_SECRET);
const FREE_PLAN_ID = "tier1";
const NINJA_PLAN_ID = "tier2";
const WIZARD_PLAN_ID = "tier3";
const END_OF_UNIVERSE_DATE = -1;

function handleWebhook( req, usersDb, mailer ) {
    let sig = req.headers["stripe-signature"];
    let event = stripe.webhooks.constructEvent(req.body, sig, WEBHOOK_SECRET);

    switch(event.id) {
        case 'customer.subscription.deleted':
            onCustomerSubscriptionDeleted(event.data, usersDb, mailer);
            break;
        case 'invoice.payment_succeeded':
            onCustomerInvoicePayed(event.data, usersDb, mailer);
            break;
        case 'invoice.payment_failed':
            onCustomerInvoiceFailed(event.data, usersDb, mailer);
            break;
        case 'charge.succeeded':
            if (event.data.metadata.isTier3Payment) {
                onCustomerTier3Started( event.data.customer, event.data.amount, usersDb, mailer );
            }
    }
}

function onCustomerInvoicePayed( invoice, usersDb, mailer ) {
    let chargePromise = store.customer.retrieve(invoice.charge);
    let customerPromise = store.customer.retrieve(invoice.customer);
    let subscriptionLine = invoice.lines[0];
    Promise.all([chargePromise, customerPromise]).then((charge, customer) => {
        usersDb.findOne({stripeCustomerId: invoice.customer}, (err, user) => {
            if (user) {
                usersDb.update({_id: user._id}, {
                    $set: {"stripePlan.activeUntil": subscriptionLine.period.end}
                });
                mailer.sendMail({
                    to: customer.email,
                    template: 'invoice_payed',
                    params: {
                        username: user.username,
                        charge: `${invoice.amount_due/100}${invoice.currency}`,
                        planName: invoice.lines[0].plan.name,
                        cardEnd: charge.source.last4
                    }
                });
            } else {
                console.warn('Could not find user with customerId', invoice.customer);
            }
        });
    }, err => {
        console.warn('could not find stripe customer and charge for invoice', invoice.id);
    });
}

function onCustomerInvoiceFailed( invoice, usersDb, mailer ) {
    let chargePromise = stripe.charges.retrieve(invoice.charge);
    let customerPromise = stripe.customers.retrieve(invoice.customer);
    Promise.all([chargePromise, customerPromise]).then((charge, customer) => {
        usersDb.findOne({stripeCustomerId: cutomer.id}, (err, user) => {
            if (user) {
                
            } else {
                console.warn('Could not find user with customerId', invoice.customer);
            }
        });
    }, err => {
        console.warn('could not find stripe customer and charge for invoice', invoice.id);
    });
}

function onCustomerTier2Started( customerId, chargeAmount, cardEnd, usersDb, mailer ) {
    stripe.customers.retrieve(customerId).then(customer => {
        mailer.sendMail({
            to: customer.email,
            template: 'payment_plan_started',
            params: {
                planName: 'Kitten Ninja',
                planCost: chargeAmount,
                cardEnd: cardEnd
            }
        })
    }, err => {
        console.warn('Could not find customer', err);
    });
}

function onCustomerTier3Started( customerId, chargeAmount, cardEnd, usersDb, mailer ) {
    stripe.customers.retrieve(customerId).then(customer => {
        mailer.sendMail({
            to: customer.email,
            template: 'eternal_plan_started',
            params: {
                planName: 'Kitten Wizard',
                planCost: chargeAmount,
                cardEnd: cardEnd
            }
        })
    }, err => {
        console.warn('Could not find customer', err);
    });
}

function onCustomerSubscriptionDeleted( subscription, usersDb, mailer ) {
    usersDB.findOne({stripeCustomerId: subscription.customer}, (err, user) => {
        if (user) {
            usersDB.update({stripeCustomerId: subscription.customer}, {
                $pull: {roles: subscription.plan.id},
                $unset: {stripePlan: user.stripePlan.id == subscription.plan.id}
            });
            stripe.customer.retrieve(subscription.customer).then(customer => {
                mailer.sendMail({
                    to: customer.email,
                    template: 'payment_plan_ended',
                    params: {
                        username: user.username,
                        planName: subscription.plan.name,
                        planCost: subscription.plan.amount
                    }
                });
            });
        } else {
            console.warn('Could not find user', err);
        }
    });
}

function subscribeToNinjaPlan( email, token, customerId ) {
    return new Promise((resolve, reject) => {
        let customerPromise = (customerId) ? 
            stripe.customers.retrieve(customerId) : 
            stripe.customers.create({email: email, source: token});
        customerPromise.then(customer => {
            stripe.subscriptions.create({
                customer: customer.id,
                items: [{plan: NINJA_PLAN_ID}]
            }).then(subscription => {
                resolve({
                    customerId: customer.id,
                    plan: {
                        id: NINJA_PLAN_ID,
                        activeUntil: subscription.current_period_end
                    }
                });
            }, err => {
                console.warn(err.type, err.message);
                reject(err.message);
            });
        },
        err => {
            console.warn(err.type, err.message);
            reject(err.message);
        });
    });
}

function subscribeToWizardPlan( email, token ) {
    return new Promise((resolve, reject) => {
        stripe.customers.create({})
        resolve({
            customerId: customer.id,
            plan: {
                id: WIZARD_PLAN_ID,
                activeUntil: END_OF_UNIVERSE_DATE
            }
        })
    });
}

function getCustomerSubscriptionStatus( id ) {
    return new Promise((resolve, reject) => {
        stripe.customers.retrieve(id).then(customer => {
            if (!customer.deleted) {
                let subscription = customer.subscriptions.data[0];
                if (subscription) {
                    resolve({
                        email: customer.email,
                        name: subscription.plan.id,
                        status: subscription.status
                    });
                } else {
                    resolve({
                        email: customer.email,
                        name: FREE_PLAN_ID,
                        status: "active"
                    });
                }
            } else {
                reject({status: 404, message: 'Customer not found.'});
            }
        }, err => {
            switch(err.type) {
                case 'StripeInvalidRequestError':
                    reject({status: 404, message: 'Customer not found.'});
                    break;
                default:
                    reject({status: 500, message: 'Server error.'});
                    break;
            }
        });
    });
}

module.exports = {
    subscribeToNinjaPlan,
    getCustomerSubscriptionStatus
};