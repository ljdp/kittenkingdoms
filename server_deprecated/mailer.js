const AmazonSES = require('node-ses');
const yaml = require('js-yaml');
const fs = require('fs');
const CONFIG = yaml.safeLoad(fs.readFileSync('server/config.yml'));

const emailTemplateFiles = fs.readdirSync(__dirname + '/email_templates/');
const emailTemplates = emailTemplateFiles.reduce((dict, filename) => {
    let filename_no_ext = filename.replace(".js", "");
    dict[filename_no_ext] = require('./email_templates/'+filename);
    return dict;
}, {});

module.exports = class Mailer {
    constructor() {
        this.mailQueue = [];
        if (CONFIG.DISABLE_MAILER) {
            console.log('Mailer disabled.');
        } else {
            this.client = AmazonSES.createClient({
                key: CONFIG.MAILER_KEY_ID,
                secret: CONFIG.MAILER_KEY_SECRET,
                amazon: 'https://email.eu-west-1.amazonaws.com'
            });
            this.updateInterval = setInterval(this.update.bind(this), 100);
        }
    }

    update() {
        if (this.mailQueue.length > 0) {
            let mail = this.mailQueue.shift();
            this._sendMail(mail);
        }
    }

    sendMail( mail ) {
        this.mailQueue.push(mail);
    }

    _sendMail({ to, template, params }) {
        return new Promise((resolve, reject) => {
            if (CONFIG.DISABLE_MAILER) {
                resolve();
            } else {
                let templateFn = emailTemplates[template];
                if (templateFn) {
                    let mailPayload = templateFn(params);
                    mailPayload.to = to;
                    mailPayload.from = 'dontreply@kittenkingdoms.com';
                    this.client.sendEmail(mailPayload, (err, data, res) => {
                        if (err) {
                            reject(err);
                        }
                        if (data) {
                            resolve(data);
                        }
                    });
                } else {
                    reject('Email template ' + template + ' does not exist!');
                }
            }
        });
    }
}