const Datastore = require('nedb');
var inviteCodesDb = new Datastore({filename: 'datastore/inviteCodes'});
inviteCodesDb.loadDatabase();

function generateAndStoreInviteTokens( count ) {
    for (let i = 0; i < count; i++) {
        inviteCodesDb.insert({
            usedBy: null
        }, (err, doc) => {
            console.log(doc._id);
        });
    }
}

let count = process.argv[2] || 1;
generateAndStoreInviteTokens(count);