const Chance = require('chance');
const WebSocket = require('ws');
const https = require('https');
const fs = require('fs');
const url = require('url');
const express = require("express");
const bodyParser = require("body-parser");
const bcrypt = require('bcrypt');
const saltRounds = 10;
const Datastore = require('nedb');
const yaml = require('js-yaml');
const CONFIG = yaml.safeLoad(fs.readFileSync('server/config.yml'));
const Game = require('./game');
const GameHandler = require('./gameHandler');
const Mailer = require('./mailer');
const DiscordBot = require('./discordBot');
const Payments = require('./payments');

const mailer = new Mailer();
const discordBot = new DiscordBot(CONFIG.DISCORD_TOKEN, CONFIG.DISCORD_GUILD_ID);
discordBot.getGames = () => { return gameHandlers.map(mapGamesForLobby); };
const app = express();
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

var shouldResetGame = (process.argv[2] == '--reset');
var chance = new Chance();
var usersDb;
var gamesDb;
var inviteCodesDb;
var migrationsDb;
var gameHandlers = [];

function onGameHandlerUpdate( handler ) {
    storeGame(handler);
}

function loadDB( db ) {
    return new Promise((resolve, reject) => {
        db.loadDatabase(function(err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function migrateDB( db, migrationFile ) {
    if (!fs.existsSync(`${__dirname}/migrations/${migrationFile}`))
        return null;

    return new Promise((resolve, reject) => {
        migrationsDb.findOne({fileName: migrationFile}, (err, migration) => {
            if (!migration) {
                var migrateFunc = require(`./migrations/${migrationFile}`);
                console.log(`Performing migrations with ${migrationFile}`);
                db.find({}, (err, docs) => {
                    if (!err) {
                        docs.forEach(doc => {
                            migrateFunc((set) => { db.update({_id: doc._id}, set); }, doc);
                        });
                        resolve();
                    } else {
                        reject(err);
                    }
                });
                migrationsDb.insert({fileName: migrationFile});
            } else {
                resolve();
            }
        });
    });
}

function createHandlerForGame( game ) {
    let handler = new GameHandler(game, {
        server: wss,
        userDb: {
            findUser: findUserByName,
            getToken: getOrGenUserToken
        },
        mailer: mailer,
        discordBot: discordBot
    });
    gameHandlers.push(handler);
    handler.startUpdate(
        CONFIG.MIN_STARTUP_INTERVAL * 1000, 
        CONFIG.MAX_STARTUP_INTERVAL * 1000, 
        CONFIG.TURN_INTERVAL * 1000,
        onGameHandlerUpdate);
    return handler;
}

const server = https.createServer({
    cert: fs.readFileSync(CONFIG.CERT_PATH, 'utf8'),
    key: fs.readFileSync(CONFIG.PRIVATE_KEY_PATH, 'utf8')
}, app);
const wss = new WebSocket.Server({ server: server, clientTracking: true });
server.listen(CONFIG.PORT, () => {
    console.log('Server listening on port ' + CONFIG.PORT);
    if (CONFIG.IN_MEMORY_DB) {
        usersDb = new Datastore({inMemoryOnly: true});
        gamesDb = new Datastore({inMemoryOnly: true});
        migrationsDb = new Datastore({inMemoryOnly: true});
        inviteCodesDb = new Datastore({inMemoryOnly: true});
    } else {
        usersDb = new Datastore({filename: 'datastore/users'});
        gamesDb = new Datastore({filename: 'datastore/games'});
        migrationsDb = new Datastore({filename: 'datastore/migrations'});
        inviteCodesDb = new Datastore({filename: 'datastore/inviteCodes'});
    }
    Promise.all([loadDB(usersDb), loadDB(gamesDb), loadDB(inviteCodesDb), loadDB(migrationsDb)]).then(() => {
        console.log('Loaded databases.');
        usersDb.ensureIndex({ fieldName: 'username', unique: true });
        usersDb.ensureIndex({ fieldName: 'email', unique: true });
        gamesDb.ensureIndex({ fieldName: 'guid', unique: true });
        migrationsDb.ensureIndex({fieldName: 'fileName', unique: true});
        usersDb.persistence.setAutocompactionInterval(1000 * 60 * 60 * 24);
        gamesDb.persistence.setAutocompactionInterval(1000 * 60 * 60 * 12);
        // Migrate dbs.
        let migrationFiles = fs.readdirSync(__dirname + '/migrations/');
        let migrations = migrationFiles.map(file => {
            if (file.startsWith('user')) {
                return migrateDB(usersDb, file);
            } else if (file.startsWith('game')) {
                return migrateDB(gamesDb, file);
            } else {
                return null;
            }
        });
        Promise.all(migrations).then(() => {
            gamesDb.find({"map.turn": {$lt: 300}}, (err, gameStates) => {
                if (!err) {
                    gameStates.forEach(gameState => {
                        let game = Game.loadFromState(gameState);
                        createHandlerForGame(game);
                    });
                } else {
                    console.error(err);
                }
            });
        });
    });
});

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.post('/login', (req, res) => {
    console.log('/login', req.body.username);
    usersDb.findOne({username: req.body.username}, (err, user) => {
        if (user) {
            bcrypt.compare(req.body.password, user.passwordHash).then(result => {
                if (result) {
                    let token;
                    try {
                        token = loginSuccess(user);
                        res.status(200).send(token);
                    } catch(err) {
                        console.log('login error', err);
                        res.status(400).send('Login failed.');
                    }
                } else {
                    res.status(401).send('Incorrect login credentials.');
                }
            });
        } else if (err) {
            console.log('login error', err);
            res.status(400).send('Login failed.');
        } else {
            res.status(404).send('User not found.');
        }
    });
});

app.post('/signup', (req, res) => {
    console.log('/signup', req.body.username);
    if (req.body.username && req.body.email && req.body.password) {
        inviteCodesDb.findOne({_id: req.body.inviteCode, usedBy: null}, (err, code) => {
            if (err) {
                console.log('invite db findOne error', err);
                res.status(400).send('Invite code was invalid.');
            } else if (code) {
                let user = {
                    username: req.body.username,
                    email: req.body.email,
                    games: []
                };
                bcrypt.hash(req.body.password, saltRounds).then(passwordHash => {
                    user.passwordHash = passwordHash;
                    code.usedBy = user.email;
                    usersDb.insert(user, (err, doc) => {
                        if (!err) {
                            inviteCodesDb.update({_id: code._id}, code);
                            onUserRegistered(user);
                            res.send('OK');
                        } else if(err.errorType == 'uniqueViolated') {
                            res.status(400).send(`${err.key} already exists.`);
                        } else {
                            res.status(400).send('Failed to create user.');
                        }
                    });
                });
            } else {
                res.status(400).send('Invite code was invalid.');
            }
        });
    } else {
        res.status(400).send('Signup request was invalid.');
    }
});

app.post('/logout', (req, res) => {
    let token = req.body.token;
    if (token) {
        findUserAsync(token).then(user => {
            usersDb.update({_id: user._id}, {$unset: {token: true}});
            res.send('OK');
        }, err => {
            res.status(404).send('User not found.');
        });
    } else {
        res.status(404).send('User not found.');
    }
});

app.get('/user', (req, res) => {
    findUserAsync(req.query.token).then(user => {
        let userData = {
            username: user.username,
            email: user.email,
            emailNotify: user.emailNotify || false,
            roles: user.roles || [],
            plan: {name: 'free', status: 'active'}
        }
        if (user.stripeCustomerId) {
            Payments.getCustomerSubscriptionStatus(user.stripeCustomerId).then(plan => {
                if (plan.status != 'canceled') {
                    userData.roles.push(plan.name);
                }
                userData.plan = plan;
                res.send(userData);
            }, err => {
                userData.error = err.message;
                res.send(userData);
            });
        } else {
            res.send(userData);
        }
    }, err => {
        res.status(404).send('User not found');
    });
});

app.post('/user', (req, res) => {
    findUserAsync(req.body.token).then(user => {
        usersDb.update({_id: user._id}, {$set: {email: req.body.email, emailNotify: req.body.emailNotify}});
        res.send('OK');
    }, err => {
        res.status(404).send('User not found');
    });
});

app.get('/list_games', (req, res) => {
    let games = gameHandlers.map(mapGamesForLobby);
    res.send(games);
});

app.post('/my_games', (req, res) => {
    if (req.body.token) {
        findUserAsync(req.body.token).then(user => {
            removeInvalidGamesFromUser(user);
            let games = user.games.map(id => findGameById(id)).map(mapGamesForLobby);
            res.send(games);
        }, err => {
            res.status(404).send('user not found.');
        });
    } else {
        res.status(404).send('user not found.');
    }
});

app.post('/join_game', (req, res) => {
    let token = req.body.token;
    let gameId = req.body.gameId;
    let game = findGameById(gameId);
    if (token && game) {
        findUserAsync(token).then(user => {
            let player = game.getPlayer(user);
            if (player) {
                res.send('Already joined game.');
            } else if (canJoinGame(game, user)) {
                joinGame(game, user);
                res.send('OK');
            } else {
                res.status(400).send('Cannot join game');
            }
        }, err => {
            res.status(404).send('User not found.');    
        })
    } else {
        res.status(404).send('Game not found.');
    }
});

app.post('/leave_game', (req, res) => {
    let token = req.body.token;
    let game = findGameById(req.body.gameId);
    if (token && game) {
        findUserAsync(token).then(user => {
            let player = game.getPlayer(user);
            if (player) {
                leaveGame(game, user);
                res.send('successfully left game');
            } else {
                res.status(400).send('failed to leave game');
            }
        }, err => {
            res.status(404).send('User not found.');
        });
    } else {
        res.status(404).send('Game not found.');
    }
});

app.post('/create_game', (req, res) => {
    findUserAsync(req.body.token).then(user => {
        if (userHasRole(user, 'admin') || userHasRole(user, 'paid')) {
            if (req.body.maxPlayers && req.body.maxPlayers >= 3 && req.body.maxPlayers <= 32
                && req.body.maxCounties && req.body.maxCounties >= 100 && req.body.maxCounties <= 500
                && req.body.mapWidth && req.body.mapWidth >= 100 && req.body.mapWidth <= 800
                && req.body.mapHeight && req.body.mapHeight >= 100 && req.body.mapHeight <= 800
                && req.body.maxTurns && req.body.maxTurns >= 168 && req.body.maxTurns <= 840
                && typeof(req.body.name) == 'string')
                {
                    let newGameConfig = {
                        NAME: req.body.name || 'Public Game',
                        MAX_PLAYERS: req.body.maxPlayers,
                        MAX_COUNTIES: req.body.maxCounties,
                        MAX_TURNS: req.body.maxTurns,
                        MAP_WIDTH: req.body.mapWidth,
                        MAP_HEIGHT: req.body.mapHeight,
                        INCOME_TURN_INTERVAL: req.body.incomeTurnInterval,
                        MAX_TURNS: 336,
                        FOG_OF_WAR: req.body.fogOfWar,
                        CHARACTERS_PER_PLAYER: 2,
                        MAX_CHARACTERS: req.body.maxPlayers,
                        BOTS: true,
                        AUTO_BOT_IDLE_PLAYERS: false
                    };
                    startNewGame(newGameConfig);
                    res.send('OK');
                } else {
                    res.status(400).send('Incorrect game parameters.');
                }
        } else {
            res.status(401).send('User does not have permission to create game');
        }
    }, err => {
        res.status(404).send('User not found.');
    });
});

app.post('/force_quit_game', (req, res) => {
    findUserAsync(req.body.token).then(user => {
        if (user.roles.indexOf('admin') != -1) {
            removeGame(req.body.gameId);
            res.send('OK');
        } else {
            res.status(401).send('User does not have permission to remove game.');
        }
    }, err => {
        res.status(404).send('User not found.');
    });
});

app.get("/payment", (req, res) => {
    fs.readFile(__dirname+'/html_templates/stripe_charge.html', 'utf8', (err, data) => {
        res.send(data);
    });
});

app.post('/subscription', (req, res) => {
    let paymentMethods = {
        'ninja': Payments.subscribeToNinjaPlan,
        'wizard': Payments.subscribeToWizardPlan
    };

    if (!req.body.userToken || !req.body.stripeEmail || !req.body.stripeToken 
        || !req.body.key || !paymentMethods[req.body.key])
    {
        res.status('400').send('Incorrect parameters');
        return;
    }

    findUserAsync(req.body.userToken).then(user => {
        paymentMethods[req.body.key](req.body.stripeEmail, req.body.stripeToken, user.stripeCustomerId)
            .then(result => {
                usersDb.update({_id: user._id}, 
                        {$set: {
                            stripeCustomerId: result.customerId, 
                            stripePlan: result.plan},
                        $push: {roles: result.plan.id}});
                res.send(`User ${user.username} has successfully subscribed to the ${result.subscription.plan.name} plan. Their stripe customer id is: ${result.customerId}`);
            }, fail => {
                res.send('Fail: ' + fail);
            });
    }, err => {
        res.status(404).send('User not found');
    });
});

app.get('/userSubscription', (req, res) => {
    if (req.query.id) {
        Payments.getCustomerSubscriptionStatus(req.query.id)
            .then(result => {
                res.send(`${result.email} is on plan: ${result.name}, which is currently ${result.status}.`);
            }, err => {
                res.status(err.status).send(err.message);
            });
    } else {
        res.status(400).send('Bad request');
    }
});

app.post('/stripe', (req, res) => {
    res.send('OK');
    Payments.handleWebhook(req, usersDb, mailer);
});

function findUserAsync( userToken ) {
    return new Promise((resolve, reject) => {
        usersDb.findOne({token: userToken}, (err, user) => {
            if (user) {
                resolve(user);
            } else {
                reject(err);
            }
        });
    });
}

function findUserByName( username ) {
    return new Promise((resolve, reject) => {
        usersDb.findOne({username: username}, (err, user) => {
            if (user) {
                resolve(user);
            } else {
                reject(err);
            }
        });
    });
}

function onUserRegistered( user ) {
    discordBot.generateInviteUrl().then(discordInviteUrl => {
        mailer.sendMail({
            to: user.email,
            template: 'register_success',
            params: {
                username: user.username,
                discordInviteUrl: discordInviteUrl
            }
        });
    }, err => {
        console.warn(err);
        mailer.sendMail({
            to: user.email,
            template: 'register_success_no_discord',
            params: {
                username: user.username
            }
        });
    });
}

function getOrGenUserToken( user ) {
    let token = user.token;
    if (!user.token) {
        token = bcrypt.hashSync(chance.guid(), 2);
        usersDb.update({_id: user._id}, {$set: {token: token}});
    }
    return token;
}

function loginSuccess( user ) {
    if (user.games.length > 0) {
        // Remove finished games.
        for (let i = user.games.length - 1; i >= 0; i--) {
            let game = findGameById(user.games[i]);
            if (!game) {
                user.games.splice(i, 1);
            }
        }
    }
    return getOrGenUserToken(user);
}

function mapGamesForLobby(game) {
    let state = game.state();
    return {
        guid: game.guid(),
        name: game.name(),
        fogOfWar: state.config.FOG_OF_WAR,
        playerCount: state.players.length,
        maxPlayers: state.config.MAX_PLAYERS,
        maxCounties: state.config.MAX_COUNTIES,
        maxTurns: state.config.MAX_TURNS,
        turn: state.map.turn,
        incomeInterval: state.config.INCOME_TURN_INTERVAL,
        public: true
    }
}

function findGameById( id ) {
    return gameHandlers.find(g => g.guid() == id);
}

function findNonFullGame() {
    return gameHandlers.find(g => {
        return !g.isGameFull();
    });
}

function startNewGame( gameConfig ) {
    let defaultConfig = {
        INCOME_TURN_INTERVAL: 24,
        GRACE: 10,
        MAX_PLAYERS: 10,
        MAX_CHARACTERS: 10,
        MAX_COUNTIES: 200,
        MAX_TURNS: 336,
        CHARACTERS_PER_PLAYER: 2,
        MAP_WIDTH: 550,
        MAP_HEIGHT: 400,
        FOG_OF_WAR: true
    };
    let game = new Game(gameConfig);
    let handler = createHandlerForGame(game);
    gamesDb.insert(game.state);
    discordBot.sendNewGameMessage(mapGamesForLobby(handler));
    return handler;
}

function removeGame( gameId ) {
    let gameIndex = q.findIndex(game => game.guid() == gameId);
    if (gameIndex != -1) {
        let game = gameHandlers[gameIndex];
        let players = game.getPlayers();
        players.forEach(player => {
            findUserByName(player.username).then(user => {
                leaveGame(game, user);
            });
        });
        gamesDb.remove({guid: game.guid()}, {});
        game.stopUpdate();
        gameHandlers.splice(gameIndex, 1);
        return true;
    }
    return false;
}

function storeGame( gameHandler ) {
    gamesDb.update({guid: gameHandler.guid()}, gameHandler.state());
}

function sendMessage(client, type, payload = {}) {
    let msg = Object.assign({type}, payload);
    client.send(JSON.stringify(msg));
}

function userHasRole( user, role ) {
    return user.roles && user.roles.indexOf(role) != -1;
}

function canJoinGame(game, user) {
    return !game.isGameFull() && !game.isGamePrivate() 
        &&  (userHasRole(user, 'admin') 
            || (userHasRole(user, 'Kitten Ninja') && user.games.length < 6)
            || (userHasRole(user, 'Kitten Wizard') && user.games.length < 12 )
               );
}

function canCreateGame( user ) {
    return userHasRole(user, 'admin')
        || (userHasRole(user, 'Kitten Ninja') && countUserCreatedGames(user) < 10)
        || (userHasRole(user, 'Kitten Wizard') && countUserCreatedGames(user) < 10);
}

function joinGame(game, user) {
    if (game.addPlayer(user)) {
        usersDb.update({_id: user._id}, {$push: {games: game.guid()}});
        return true;
    }
    return false;
}
function leaveGame(game, user) {
    if (game.removePlayer(user)) {
        usersDb.update({_id: user._id}, {$pull: {games: game.guid()}});
        return true;
    }
    return false;
}
function connectToGame(game, user, client) {
    return game.connectClient(client, user);
}
function disconnectFromGame(game, user, client) {
    return game.disconnectClient(client);
}

function removeInvalidGamesFromUser(user) {
    for (let i = user.games.length - 1; i >= 0; i--) {
        let game = findGameById(user.games[i]);
        if (!game || game.isGameOver() || !game.getPlayer(user)) {
            user.games.splice(i, 1);
        }
    }
    usersDb.update({_id: user._id}, {$set: {games: user.games}});
}

wss.on('connection', function connection(client, req) {
    client.on('message', function message(data) {
        let msg = JSON.parse(data);
        switch(msg.type) {
            case 'LOGIN_GAME':
                let gameId = msg.gameId;
                let game = findGameById(msg.gameId);
                if (game) {
                    usersDb.findOne({token: msg.token}, (err, user) => {
                        if (user) {
                            if (game && connectToGame(game, user, client)) {
                                sendMessage(client, 'LOGIN_SUCCESS');
                            } else {
                                sendMessage(client, 'LOGIN_FAIL', {msg: 'Cannot connect to game.'});
                            }
                        } else {
                            sendMessage(client, 'LOGIN_FAIL', {msg: 'Cannot find user.'});
                        }
                    });
                } else {
                    sendMessage(client, 'LOGIN_FAIL', {msg: 'Game does not exist'});
                }
                break;
            case 'LOGOUT_GAME':
                client.player = null;
                client.gameHandler = null;
                break;
            default:
                if (client.gameHandler && client.player) {
                    client.gameHandler.handleMessage(msg, client.player, client);
                    storeGame(client.gameHandler);
                } else {
                    console.log('Received message from client but cannot handle it.', msg);
                }
                break;
        }
    });
});