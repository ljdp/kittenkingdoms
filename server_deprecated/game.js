const AICharacter = require('./ai_character');
const d3 = require('d3');
const util = require('./util');
const Chance = require('chance');
const yaml = require('js-yaml');
const fs = require('fs');
const CONFIG = yaml.safeLoad(fs.readFileSync('server/config.yml'));
const DESIRES = yaml.safeLoad(fs.readFileSync('server/data/desires.yml'));
const TECH = yaml.safeLoad(fs.readFileSync('server/data/tech.yml'));
const K = yaml.safeLoad(fs.readFileSync('server/data/constants.yml'));

function ilink( type, guid, text ) {
    return `{${type}|${guid}|${text}}`;
}
function coLink( county ) {
    return county.name;
    // return ilink('county', county.guid, county.name);
}
function chLink( character ) {
    return `${character.name} ${character.house}`;
    // return ilink('character', character.guid, `${character.name} ${character.house}`);
}

class Game {
    constructor( config, loadedState ) {
        let chance = new Chance();
        this.chance = chance;
        if (loadedState) {
            this.state = loadedState;
            this.config = loadedState.config || CONFIG;
            this.state.messages.forEach(msg => {
                if (typeof(msg.to) == 'object') {
                    msg.to = msg.to.guid;
                }
                if (typeof(msg.from) == 'object') {
                    msg.from = msg.from.guid;
                }
            });
        } else {
            this.config = config;
            let generatedData = this.generateCountiesAndCharacters(chance);
            this.state = {
                guid: chance.guid(),
                counties: generatedData.counties,
                characters: generatedData.characters,
                armies: [],
                players: [],
                items: [],
                messages: [],
                news: [],
                map: {
                    turn: 0,
                    width: config.MAP_WIDTH,
                    height: config.MAP_HEIGHT
                },
                config: config,
                grace: 0
            };
        }

        // Create hashmaps.
        this.countyMap = util.createMapFromGuids(this.state.counties);
        this.characterMap = util.createMapFromGuids(this.state.characters);
        this.armyMap = util.createMapFromGuids(this.state.armies);

        // Calculate county sizes.
        let voronoi = d3.voronoi();
        voronoi.extent([[0, 0], [this.config.MAP_WIDTH, this.config.MAP_HEIGHT]]);
        let sites = this.state.counties.map(county => {
            let site = [county.x, county.y];
            site.county_guid = county.guid;
            return site;
        });
        let preDiagram = voronoi(sites);
        let squiggleAmp = 2;
        preDiagram.polygons().forEach((poly, i) => {
            let center = d3.polygonCentroid(poly);
            let area = d3.polygonArea(poly);
            let size = Math.ceil(area * 0.002);
            let cell = preDiagram.cells[i];
            let x = center[0] + Math.sin(chance.floating({min: -Math.PI, max: Math.PI})) * size * squiggleAmp;
            let y = center[1] + Math.cos(chance.floating({min: -Math.PI, max: Math.PI})) * size * squiggleAmp;
            let site = [x, y];
            site.county_guid = cell.site.data.county_guid;
            let county = this.countyMap[cell.site.data.county_guid];
            county.size = size;
            county.impassable = (size <= 1);
            county.sites = [
                [cell.site[0], cell.site[1]],
                [x, y]
            ];
            county.center = center;
            if (!loadedState) {
                county.maxTroops = K.caps.countyTroopsPerCountySize * size;
            }
            sites.push(site);
        });
        this.diagram = voronoi(sites);
        // countyDiagram.polygons().forEach((poly, i) => {
        //     let cell = countyDiagram.cells[i];
        //     let area = d3.polygonArea(poly);
        //     let size = Math.ceil(area * 0.002);
        //     let county = this.getCounty(cell.site.data.county_guid);
        //     county.size = size;
        //     county.impassable = (size <= 1);
        //     if (!loadedState) {
        //         county.maxTroops = K.caps.countyTroopsPerCountySize * size;
        //     }
        // });
        this.diagram.cells.forEach(cell => {
            cell.halfedges.forEach(halfedge => {
                let edge = this.diagram.edges[halfedge];
                if (!edge.left || !edge.right) {
                    let county = this.getCounty(cell.site.data.county_guid);
                    county.impassable = true;
                }
            });
        });

        if (!loadedState) {
            let counties = this.state.counties.filter(county => !county.impassable);
            this.state.characters.forEach(character => {
                // Assign counties to characters.
                let county = counties.pop();
                county.owningCharacter = character.guid;
                character.location = county.guid;
                // Generate desires for characters.
                this.generateDesiresForCharacter(character);
            });
            this.addNews(null, 'Hello world!');
        }
    }

    generateCountiesAndCharacters( chance ) {
        let countyNames = chance.unique(chance.city, this.config.MAX_COUNTIES);
        let countyXCoords = chance.unique(chance.integer, this.config.MAX_COUNTIES, {min: 0, max: this.config.MAP_WIDTH});
        let countyYCoords = chance.unique(chance.integer, this.config.MAX_COUNTIES, {min: 0, max: this.config.MAP_HEIGHT});
        let counties = [];
        for (let i = 0; i < this.config.MAX_COUNTIES; i++) {
            counties.push({
                guid: chance.guid(),
                seed: chance.integer(),
                name: countyNames.pop(),
                x: countyXCoords.pop(),
                y: countyYCoords.pop(),
                troops: chance.integer({min: K.generation.troops.min, max: K.generation.troops.max}),
                money: 0,
                tech: 0,
                armies: [],
                owningCharacter: null
            });
        }
        let houseNames = chance.unique(chance.last, this.config.MAX_CHARACTERS);
        let characters = [];
        for (let i = 0; i < this.config.MAX_CHARACTERS; i++) {
            let character = this.makeCharacter(
                chance.integer({min: K.generation.age.min, max: K.generation.age.max}), 
                houseNames.pop());
            characters.push(character);
        };
        return {counties, characters};
    }

    getAdjacentCells( cell ) {
        let diagram = this.diagram;
        let cellIndex = cell.site.index;
        var leftEdges = diagram.edges.filter(function(edge) {
            return edge.left && edge.left.index == cellIndex;
        });
        var rightEdges = diagram.edges.filter(function(edge) {
            return edge.right && edge.right.index == cellIndex;
        });
        var adjacentCells = [];
        for (var i = 0; i < leftEdges.length; i++) {
            if (leftEdges[i].right) {
                adjacentCells.push(diagram.cells[leftEdges[i].right.index]);
            }
        }
        for (var i = 0; i < rightEdges.length; i++) {
            if (rightEdges[i].left) {
                adjacentCells.push(diagram.cells[rightEdges[i].left.index]);
            }
        }
        return adjacentCells.filter((cell) => {
            if (cell !== undefined) {
                let county = this.getCounty(cell.site.data.county_guid);
                return !county.impassable;
            }
            return false;
        });
    }

    getAdjacentCounties( countyId ) {
        let adjacentIds = [];
        let cells = this.diagram.cells.filter(cell => cell.site.data.county_guid == countyId);
        cells.forEach(cell => {
            this.getAdjacentCells(cell).forEach(adjacent => {
                let adjacentId = adjacent.site.data.county_guid;
                if (adjacentIds.indexOf(adjacentId) == -1) {
                    adjacentIds.push(adjacentId);
                }
            });
        });
        return this.getCounties(adjacentIds);
    }

    getAdjacentCountiesSteps( countyId, maxSteps = 1, step = 1 ) {
        let result = [];
        if (step <= maxSteps) {
            this.getAdjacentCounties(countyId).forEach(adjacent => {
                result.push(adjacent);
                result = result.concat(this.getAdjacentCountiesSteps(adjacent.guid, maxSteps, step + 1));
            });
        }
        return result;
    }

    getCounty( guid ) {
        return this.countyMap[guid];
    }

    getCounties( guids ) {
        let result = [];
        guids.forEach(guid => {
            result.push(this.countyMap[guid]);
        });
        return result;
    }

    getCharacter( guid ) {
        return this.characterMap[guid];
    }

    getCharacters( guids ) {
        let result = [];
        guids.forEach(guid => {
            result.push(this.getCharacter(guid));
        });
        return result;
    }

    fullname( characterOrGuid ) {
        let character = characterOrGuid;
        if (typeof(characterOrGuid) == 'string') {
            character = this.getCharacter(characterOrGuid);
        }
        return `${character.name} ${character.house}`;
    }

    getCharacterCounties( guid ) {
        return this.state.counties
            .filter(county => county.owningCharacter == guid);
    }

    getPlayerCharacters( username ) {
        return this.state.characters.filter(c => c.owningPlayer && c.owningPlayer == username);
    }

    getCharacterArmies( guid ) {
        return this.state.armies
            .filter(army => army.owningCharacter == guid);
    }

    getCharacterChildren( guid, onlyAlive = true ) {
        let character = this.getCharacter(guid);
        let children = this.state.characters.filter(child => {
            if (child.parents && (!onlyAlive || (onlyAlive && child.alive))) {
                return child.parents.indexOf(guid) != -1;
            }
            return false;
        });
        children.sort((a, b) => b.age - a.age);
        return children;
    }

    getCharacterSiblings( guid, onlyAlive = true) {
        let character = this.getCharacter(guid);
        if (!character.parents || character.parents.length == 0) {
            return [];
        }
        let siblings = this.state.characters.filter(sibling => {
            return sibling.guid != guid && (!onlyAlive || (onlyAlive && sibling.alive))
                && sibling.parents && character.parents && sibling.parents.findIndex(parent => character.parents.indexOf(parent)) != -1
        });
        siblings.sort((a, b) => b.age - a.age);
        return siblings;
    }

    getCharacterHeir( guid ) {
        let player = this.getCharacter(guid).owningPlayer;
        let heir = this.state.characters.filter(character => character.heirTo == guid);
        if(heir.length == 0) {
            let children = this.getCharacterChildren(guid, true)
                .filter(child => !child.owningPlayer || child.owningPlayer == player);
            if (children.length > 0) {
                return children[0];
            } else {
                let siblings = this.getCharacterSiblings(guid, true)
                    .filter(sibling => !sibling.owningPlayer || sibling.owningPlayer == player);
                if (siblings.length > 0) {
                    return siblings[0];
                }
            }
        } else if (heir.length == 1) {
            return heir[0];
        } else if (heir.length > 1) {
            console.warn(`Warn: There is more than 1 heir for character ${guid}`);
        }
        return null;
    }

    getCharacterUnderlords( characterId ) {
        return this.state.characters.filter( character => {
            return character.overlord == characterId;
        });
    }

    getCharacterUnderlordHeirarchy( characterId ) {
        let character = this.getCharacter(characterId);
        let tree = {
            character: character,
            underlords: []
        };
        let underlords = this.getCharacterUnderlords(characterId);
        if (underlords && underlords.length > 0) {
            for (let underlord of underlords) {
                tree.underlords.push(this.getCharacterUnderlordHeirarchy(underlord.guid));
            }
        }
        return tree;
    }

    getCharacterPrisoners( characterId ) {
        return this.state.characters.filter(c => c.imprisoner == characterId);
    }

    getCharacterParents( characterId ) {
        let character = this.getCharacter(characterId);
        let result = [];
        character.parents.forEach(parentId => result.push(this.getCharacter(parentId)));
        return result;
    }

    renameCharacter( player, characterId, newName ) {
        let character = this.getCharacter(characterId);
        let parents = this.getCharacterParents(characterId);
        if ((character.owningPlayer == player.username || parents.filter(p => p.owningPlayer == player.username).length > 0)
            && character.age < K.renameCharacterMaxAge 
            && newName != null && newName.length > 0 && newName.length < 16)
        {
            character.name = newName;
        }
    }

    renameCounty( player, countyId, newName ) {
        let county = this.getCounty(countyId);
        let countyNames = this.state.counties.map(c => c.name);
        if (this.doesItemBelongToPlayer(county, player) 
            && (county.lastRenameTurn == null || (this.state.map.turn - county.lastRenameTurn) >= K.renameCountyCooldown)
            && newName != null && newName.length > 0 && newName.length < 16
            && countyNames.indexOf(newName) == -1)
        {
            county.name = newName;
            county.lastRenameTurn = this.state.map.turn;
        }
    }

    getArmy( armyId ) {
        return this.armyMap[armyId];
    }

    getPlayer( username ) {
        return this.state.players.find(player => player.username == username);
    }

    getItem( guid ) {
        return this.state.items.find(item => item.guid == guid);
    }

    getGameSettingFromPath( path ) {
        let splitPath = path.split("/");
        let value = null;
        for (let i = 0, k = K; i < splitPath.length; i++) {
            k = k[splitPath[i]];
            if (i == splitPath.length - 1) {
                value = k;
            }
        }
        return value;
    }

    getGameSettingObject( characterId, path ) {
        let obj = this.getGameSettingFromPath(path);
        let result = {};
        if (obj !== null) {
            for (let key in obj) {
                result[key] = this.getGameSettingValue(characterId, path + '/' + key);
            }
        }
        return result;
    }

    getGameSettingValue( characterId, path ) {
        let value = this.getGameSettingFromPath(path);
        if (value !== null) {
            let activeEffects = this.state.items
                .filter(item => 
                    item.useType == 'global' || 
                    (item.owningCharacter == characterId && (item.active || item.useType == 'passive')))
                .map(item => {
                    if (item.effects) {
                        return item.effects.filter(effect => effect.path == path);
                    }
                    return [];
                })
                .reduce((pre, cur) => pre.concat(cur), []);
            activeEffects.forEach(effect => {
                if (effect.add) {
                    value += effect.add;
                } else if (effect.multiply) {
                    value *= effect.multiply;
                } else if (effect.set) {
                    value = effect.set;
                }
                if (effect.min) {
                    value = Math.min(value, effect.min);
                }
                if (effect.max) {
                    value = Math.max(value, effect.max);
                }
            });
            return value;
        } else {
            console.log('getGameSettingValue, path was retreived null.');
        }
    }

    setCharacterFocus( player, characterId, focus ) {
        let character = this.getCharacter(characterId);
        if (character.owningPlayer == player.username && character.cooldowns.focus == 0) {
            character.cooldowns.focus = (character.focus == focus) ? 0 : K.focusCooldown;
            character.focus = focus;
        }
    }

    setCharacterOverlord( characterId, overlordId ) {
        if (characterId !== overlordId) {
            let character = this.getCharacter(characterId);
            if (character.cooldowns.breakFealty == 0 && character.alive) {
                if (character.overlord) {
                    this.sendMessage(character.overlord, character.overlord, 
                        `${chLink(character)} has broken fealty.`, null);
                }
                if (overlordId) {
                    let overlord = this.getCharacter(overlordId);
                    if (overlord && overlord.alive && overlord.overlord !== character.guid) {
                        character.overlord = overlordId;
                        character.cooldowns.breakFealty = this.getGameSettingValue(characterId, "breakFealtyCooldown");
                    }
                    this.addNews(null, `${chLink(character)} has sworn fealty to ${chLink(overlord)}.`);
                } else {
                    character.overlord = null;
                }
            }
        }
    }

    breakFealty( player, characterId ) {
        let character = this.getCharacter(characterId);
        if ((player == null || character.owningPlayer == player.username) && character.overlord != null) {
            this.setCharacterOverlord(characterId, null);
        }
    }

    makePlayer( username ) {
        let availableCharacters = this.state.characters
            .filter(character => character.owningPlayer == null && character.alive)
            .map(character => character.guid);
        let player = {
            username: username,
            score: 0,
            characters: []
        };
        let charactersToPick = 1;
        if (availableCharacters.length >= charactersToPick) {
            let ids = this.chance.pickset(availableCharacters, charactersToPick);
            ids.forEach(id => {
                let character = this.getCharacter(id);
                if (character) {
                    let id = availableCharacters.pop();
                    player.characters.push(id);
                    character.owningPlayer = username;
                }
            });
        }
        this.state.players.push(player);
        return player;
    }

    removePlayer( username ) {
        let playerIndex = this.state.players.findIndex(p => p.username == username);
        if (playerIndex != -1) {
            let player = this.state.players[playerIndex];
            this.state.characters.filter(ch => ch.owningPlayer == username).forEach(ch => {
                ch.owningPlayer = null;
            });
            this.state.players.splice(playerIndex, 1);
            return true;
        }
        return false;
    }

    makeChild( parentId ) {
        let parent = this.getCharacter(parentId);
        let spouse = this.getCharacter(parent.spouse);
        if (parent && spouse && parent.cooldowns.buildChild == 0 && spouse.cooldowns.buildChild == 0) {
            parent.stats.childrenMade++;
            spouse.stats.childrenMade++;
            parent.cooldowns.buildChild += K.buildChildCooldown;
            spouse.cooldowns.buildChild += K.buildChildCooldown;

            let parents = [parent, spouse];
            let parentIds = [parent.guid, parent.spouse];
            let playerCharacterCount = [
                parent.owningPlayer ? this.getPlayerCharacters(parent.owningPlayer).length : 0,
                spouse.owningPlayer ? this.getPlayerCharacters(spouse.owningPlayer).length : 0
            ];
            let children = [];
            for(let i = 0; i < 2; i++) {
                let child = this.makeCharacter(0, parent.house);
                let parentHeir = this.getCharacterHeir(parentIds[i]);
                if (!parentHeir) {
                    child.heirTo = parentIds[i];
                }
                if (playerCharacterCount[i] < this.config.MAX_CHARACTERS_PER_PLAYER) {
                    child.owningPlayer = parents[i].owningPlayer;
                }
                child.parents = parentIds.slice(0);
                child.location = parent.location;
                child.lifespan = this.chance.integer({
                    min: Math.max(
                        this.getGameSettingValue(parent.guid, "generation/lifespan/min"), 
                        this.getGameSettingValue(parent.spouse, "generation/lifespan/min")),
                    max: Math.max(
                        this.getGameSettingValue(parent.guid, "generation/lifespan/max"), 
                        this.getGameSettingValue(parent.spouse, "generation/lifespan/max"))
                    });
                this.state.characters.push(child);
                this.characterMap[child.guid] = child;
                this.generateDesiresForCharacter(child);
                children.push(child);
            }
            return children;
        }
        return [];
    }

    makeCharacter( age, house, autoAdd = false ) {
        let chance = this.chance;
        let character = {
            guid: chance.guid(),
            name: chance.first(),
            house: house,
            age: age,
            lifespan: chance.integer(K.generation.lifespan),
            alive: true,
            spouse: null,
            overlord: null,
            ports: chance.pickset(K.portNames, K.characterPortCount),
            parents: [],
            research: [Object.assign({}, TECH[0])],
            activeResearchIndex: 0,
            cooldowns: {
                focus: 0,
                buildChild: 0,
                breakFealty: 0,
                movement: 0,
                plotting: 0
            },
            focus: chance.pickone(['money', 'military', 'science']),
            tech: 0,
            maxCounties: K.caps.county,
            maxArmies: K.caps.army,
            money: K.startingMoney,
            location: null,
            owningPlayer: null,
            stats: {
                messagesReceived: 0,
                lastTurnMessageReceived: 0,
                moneyGifted: 0,
                troopsGifted: 0,
                charactersCaptured: [],
                charactersKilled: [],
                armiesDestroyed: 0,
                troopsAttacked: 0,
                lastTurnAttackOrDefended: 0,
                childrenMade: 0,
                technologiesDiscovered: 0,
                experimentsTried: 0
            }
        };
        if (autoAdd) {
            this.state.characters.push(character);
            this.characterMap[character.guid] = character;
        }
        return character;
    }

    setCountyOwner( countyId, characterId ) {
        let county = this.countyMap[countyId];
        county.owningCharacter = characterId;
    }

    moveCharacterToLocation( player, characterId, countyId ) {
        let character = this.getCharacter(characterId);
        if (!character.cooldowns.movement) {
            character.cooldowns.movement = 0;
        }
        if ((player == null || character.owningPlayer == player.username) 
            && character.cooldowns.movement == 0
            && this.isCountyFriendly(countyId, characterId))
        {
            character.location = countyId;
            character.cooldowns.movement = this.getGameSettingValue(characterId, "movementCooldown/character");
        }
    }

    imprisonCharacter( imprisonerId, prisonerId ) {
        let imprisoner = this.getCharacter(imprisonerId);
        let prisoner = this.getCharacter(prisonerId);
        prisoner.imprisoner = imprisonerId;
        this.sendMessage(prisonerId, prisonerId, 
            `I have been imprisoned by ${chLink(imprisoner)}.`);
        this.addNews(imprisonerId, 
            `${chLink(prisoner)} has been imprisoned by ${chLink(imprisoner)}.`);
    }

    releasePrisoner( characterId ) {
        let character = this.getCharacter(characterId);
        if (character.imprisoner) {
            let imprisoner = this.getCharacter(character.imprisoner);
            character.imprisoner = null;
            let counties = this.getCharacterCounties(characterId);
            if (counties.length > 0) {
                character.location = this.chance.pickone(counties).guid;
            }
            this.addNews(imprisoner.guid, 
                `${chLink(character)} has been released from prison by ${chLink(imprisoner)}.`);
        }
    }

    respondToProposal( player, messageId, proposalIndex, response ) {
        let message = this.state.messages.find(msg => msg.guid == messageId);
        let respondingCharacter = this.getCharacter(message.to);
        let proposal = message.proposals[proposalIndex];
        if ((player == null || respondingCharacter.owningPlayer == player.username) && !proposal.receivedResponse) {
            if (message.proposalAcceptType == 'any') {
                if (response && this.canAcceptProposal(proposal, message)) {
                    this.setProposalResponse(message, proposal, true);
                } else if (!response) {
                    this.setProposalResponse(message, proposal, false);
                }
                return message;
            } else if (message.proposalAcceptType == 'all') {
                if (response) {
                    let canAcceptCount = 0;
                    let proposalNames = [];
                    message.proposals.forEach(proposal => {
                        if (this.canAcceptProposal(proposal, message)) {
                            canAcceptCount++;
                        }
                        proposalNames.push(proposal.type);
                    });
                    if (canAcceptCount == message.proposals.length) {
                        message.proposals.forEach(proposal => this.setProposalResponse(message, proposal, true));
                    }
                } else {
                    message.proposals.forEach(proposal => this.setProposalResponse(message, proposal, false));
                }
            }
        }
    }

    setProposalResponse( message, proposal, response ) {
        proposal.response = response;
        proposal.receivedResponse = true;
        if (response && this.canAcceptProposal(proposal, message)) {
            this.acceptProposal(proposal, message);
        } else {
            this.declineProposal(proposal, message);
        }
    }

    canAcceptProposal( proposal, message ) {
        let sender = this.getCharacter(message.from);
        let receiver = this.getCharacter(message.to);
        switch(proposal.type) {
            case 'MARRY':
                return this.canCharactersMarry(sender, receiver);
            case 'GIVE_MONEY':
                return true;
            case 'REQUEST_MONEY':
                return receiver.money >= proposal.intValue;
            case 'GIVE_LAND':
                var county = this.getCounty(proposal.stringValue);
                return sender.location == receiver.location 
                    && !sender.hidden && !receiver.hidden 
                    && county && county.owningCharacter == sender.guid;
            case 'REQUEST_LAND':
                var county = this.getCounty(proposal.stringValue);
                return sender.location == receiver.location
                    && !sender.hidden && !receiver.hidden
                    && county && county.owningCharacter == receiver.guid;
            case 'BUILD_CHILD':
                return sender.location == receiver.location && !sender.hidden && !receiver.hidden
                    && sender.spouse == receiver.guid && sender.cooldowns.buildChild == 0 && receiver.cooldowns.buildChild == 0;
            case 'GIVE_ITEM':
                return sender.location == receiver.location && !sender.hidden && !receiver.hidden;
            case 'REQUEST_ITEM':
                var item = this.state.items.find(item => 
                        item.key == proposal.stringValue 
                        && item.owningCharacter == receiver.guid
                        && !item.active);
                return item != null && sender.location == receiver.location && !sender.hidden && !receiver.hidden;
            case 'SWEAR_FEALTY':
                return !sender.overlord && receiver.overlord !== sender.guid;
            case 'REQUEST_FEALTY':
                return sender.overlord !== receiver.guid;
            case 'GIVE_RELEASE':
                return receiver.imprisoner == sender.guid;
            case 'REQUEST_RELEASE':
                var prisoner = this.getCharacter(proposal.stringValue);
                return prisoner && prisoner.imprisoner == receiver.guid;
            case 'OFFER_VISIT':
            case 'REQUEST_VISIT':
                return sender.location != receiver.location;
            case 'GIVE_PRISONER':
                let givenPrisoner = this.getCharacter(proposal.stringValue);
                return givenPrisoner.imprisoner == sender.guid;
            case 'REQUEST_PRISONER':
                let requestedPrisoner = this.getCharacter(proposal.stringValue);
                return requestedPrisoner.imprisoner == receiver.guid;
            case 'GIVE_BLUEPRINT':
                var tech = TECH.find(t => t.key == proposal.stringValue);
                return tech 
                    && sender.research.find(r => r.key == tech.key) 
                    && receiver.location == sender.location
                    && !receiver.hidden && !sender.hidden;
            case 'REQUEST_BLUEPRINT':
                var tech = TECH.find(t => t.key == proposal.stringValue);
                return tech 
                    && receiver.research.find(r => r.key == tech.key) 
                    && receiver.location == sender.location
                    && !receiver.hidden && !sender.hidden;
            default:
                return true;
        }
    }

    acceptProposal( proposal, message ) {
        let toCharacter = this.getCharacter(message.to);
        let fromCharacter = this.getCharacter(message.from);
        let county = this.getCounty(proposal.stringValue);
        let intValue = proposal.intValue;
        switch(proposal.type) {
            case 'MARRY':
                this.marry(message.from, message.to);
                break;
            case 'GIVE_MONEY':
                toCharacter.money += intValue;
                break;
            case 'REQUEST_MONEY':
                toCharacter.money -= intValue;
                fromCharacter.money += intValue;
                break;
            case 'GIVE_LAND':
                this.setCountyOwner(county.guid, message.to);
                break;
            case 'REQUEST_LAND':
                this.setCountyOwner(county.guid, message.from);
                break;
            case 'BUILD_CHILD':
                this.makeChild(fromCharacter.guid);
                break;
            case 'GIVE_TROOPS':
                if (!this.addTroopsToCharacter(toCharacter.guid, intValue)) {
                    this.addTroopsToCharacter(fromCharacter.guid, intValue);
                }
                break;
            case 'REQUEST_TROOPS':
                this.transferTroopsBetweenCharacters(toCharacter.guid, fromCharacter.guid, intValue);
                break;
            case 'SWEAR_FEALTY':
                this.setCharacterOverlord(fromCharacter.guid, toCharacter.guid);
                break;
            case 'REQUEST_FEALTY':
                this.setCharacterOverlord(toCharacter.guid, fromCharacter.guid);
                break;
            case 'GIVE_ITEM':
                var item = this.state.items.find(item => item.guid == proposal.stringValue && !item.active);
                item.owningCharacter = toCharacter.guid;
                break;
            case 'REQUEST_ITEM':
                var theirItem = this.state.items.find(item => 
                    item.key == proposal.stringValue 
                    && item.owningCharacter == toCharacter.owningCharacter
                    && !item.active);
                theirItem.owningCharacter = fromCharacter.guid;
                break;
            case 'GIVE_RELEASE':
                this.releasePrisoner(toCharacter.guid);
                break;
            case 'REQUEST_RELEASE':
                this.releasePrisoner(fromCharacter.guid);
                break;
            case 'OFFER_VISIT':
                toCharacter.location = fromCharacter.location;
                break;
            case 'REQUEST_VISIT':
                fromCharacter.location = toCharacter.location;
                break;
            case 'GIVE_PRISONER':
                let givenPrisoner = this.getCharacter(proposal.stringValue);
                givenPrisoner.imprisoner = toCharacter.guid;
                if (!toCharacter.hidden) {
                    givenPrisoner.location = toCharacter.location;
                }
                break;
            case 'REQUEST_PRISONER':
                let requestedPrisoner = this.getCharacter(proposal.stringValue);
                requestedPrisoner.imprisoner = fromCharacter.guid;
                if (!fromCharacter.hidden) {
                    requestedPrisoner.location = fromCharacter.location;
                }
                break;
            case 'GIVE_BLUEPRINT':
                var tech = TECH.find(t => t.key == proposal.stringValue);
                toCharacter.research.push(Object.assign({}, tech));
                break;
            case 'REQUEST_BLUEPRINT':
                var tech = TECH.find(t => t.key == proposal.stringValue);
                fromCharacter.research.push(Object.assign({}, tech));
                break;
        }
    }

    declineProposal( proposal, message ) {
        let fromCharacter = this.getCharacter(message.from);
        switch(proposal.type) {
            case 'GIVE_MONEY':
                fromCharacter.money += proposal.intValue;
                break;
            case 'GIVE_TROOPS':
                this.addTroopsToCharacter(fromCharacter.guid, proposal.intValue);
                break;
            case 'GIVE_ITEM':
                var item = this.getItem(proposal.stringValue);
                item.owningCharacter = fromCharacter.guid;
                break;
        }
    }

    sendMessage( fromCharacterId, toCharacterId, message, proposals, proposalAcceptType = 'any', miscData = null ) {
        proposals = proposals || [];
        let messageObj = {
            guid: this.chance.guid(),
            from: fromCharacterId,
            to: toCharacterId,
            message: message,
            proposals: [],
            proposalAcceptType: proposalAcceptType,
            miscData: miscData,
            read: false,
            turn: this.state.map.turn
        };
        for (let i = 0; i < proposals.length; i++) {
            let proposal = proposals[i];
            proposal.receivedResponse = false;
            proposal.response = null;
            proposal.intValue = parseInt(proposal.intValue);
            let fromCharacter = this.getCharacter(fromCharacterId);
            let toCharacter = this.getCharacter(toCharacterId);
            if (this.canAcceptProposal(proposal, messageObj)) {
                switch(proposal.type) {
                    case 'GIVE_MONEY':
                        if (fromCharacter.money >= proposal.intValue) {
                            fromCharacter.money -= proposal.intValue;
                            fromCharacter.stats.moneyGifted += proposal.intValue;
                            messageObj.proposals.push(proposal);
                        }
                        break;
                    case 'GIVE_TROOPS':
                        if (this.removeTroopsFromCharacter(fromCharacterId, proposal.intValue)) {
                            fromCharacter.stats.troopsGifted += proposal.intValue;
                            messageObj.proposals.push(proposal);
                        }
                        break;
                    case 'GIVE_ITEM':
                        var item = this.getItem(proposal.stringValue);
                        if (item && item.owningCharacter == fromCharacterId && !item.active) {
                            item.owningCharacter = null;
                            messageObj.proposals.push(proposal);
                        }
                        break;
                    default:
                        messageObj.proposals.push(proposal);
                        break;
                }
            }
        }
        // Add message to state.
        this.state.messages.push(messageObj);

        let toCharacter = this.getCharacter(toCharacterId);
        let fromCharacter = this.getCharacter(fromCharacterId);

        // Record message stats.
        if (fromCharacterId != toCharacterId && fromCharacter.owningPlayer != toCharacter.owningPlayer) {
            toCharacter.stats.messagesReceived++;
            toCharacter.stats.lastTurnMessageReceived = this.state.map.turn;
        }

        // If sending message to a character controlled by the same player
        // automatically accept all proposals.
        if (fromCharacter.owningCharacter == toCharacter.owningCharacter 
            && fromCharacter.owningPlayer && toCharacter.owningPlayer)
        {
            messageObj.proposals.forEach(proposal => {
                this.setProposalResponse(messageObj, proposal, true);
            });
        } else if (toCharacter.owningPlayer && this.notificationsHandler) {
            // send notification to user.
            this.notificationsHandler.notify(toCharacter.owningPlayer, {
                template: 'received_message',
                params: {
                    catName: chLink(toCharacter),
                    catId: toCharacterId,
                    senderName: chLink(fromCharacter),
                    message: message
                }
            });
        }
        return messageObj;
    }

    markMessageRead( messageId ) {
        let msg = this.state.messages.find(msg => msg.guid == messageId);
        if (msg) {
            msg.read = true;
        }
    }

    addNews( characterId, message ) {
        let character = this.getCharacter(characterId);
        if (character == null || character.money >= K.postAdCost) {
            if (character) {
                character.money -= K.postAdCost;
            }
            if (this.state.newsIndex == null) {
                this.state.newsIndex = 1;
            }
            this.state.news.push({
                index: this.state.newsIndex++,
                turn: this.state.map.turn,
                author: characterId,
                message: message
            });
        }
    }

    denounce( player, characterId, targetId ) {
        let character = this.getCharacter(characterId);
        let target = this.getCharacter(targetId);
        if (character.owningPlayer == player.username 
            && this.state.map.turn >= K.denounce.graceTurns 
            && !character.cooldowns.denounce)
        {
            if (!character.denounces) {
                character.denounces = [];
            }
            character.cooldowns.denounce = this.getGameSettingValue(characterId, 'denounce/cooldown');
            character.denounces.push({
                turn: this.state.map.turn,
                target: targetId
            });
            this.addNews(characterId, `I denounce ${chLink(target)}!`);
        } else {
            return false;
        }
    }

    transferTroopsBetweenCharacters( fromCharacterId, toCharacterId, amount ) {
        let fromCounties = this.getCharacterCounties(fromCharacterId);
        let toCounties = this.getCharacterCounties(toCharacterId);
        if (fromCounties.length > 0 && toCounties.length > 0) {
            let troopsSum = this.getCharacterReservedTroopsCount(fromCharacterId);
            if (troopsSum >= amount) {
                let troopAcc = 0;
                let index = 0;
                while(troopAcc != amount) {
                    fromCounties[index++].troops--;
                    troopAcc++;
                    if (index >= fromCounties.length) index = 0;
                }
                index = 0;
                while(troopAcc != 0) {
                    toCounties[index++].troops++;
                    troopAcc--;
                    if (index >= toCounties.length) index = 0;
                }
            }
            return true;
        }
        return false;
    }

    getCharacterReservedTroopsCount( characterId ) {
        let counties = this.getCharacterCounties(characterId);
        return counties.reduce((a, county) => { return a + county.troops; }, 0);
    }

    removeTroopsFromCharacter( characterId, amount ) {
        let counties = this.getCharacterCounties(characterId);
        if (counties.length > 0) {
            let troopSum = this.getCharacterReservedTroopsCount(characterId);
            if (troopSum >= amount) {
                let troopAcc = 0;
                let index = 0;
                while(troopAcc != amount) {
                    counties[index++].troops--;
                    troopAcc++;
                    if (index >= counties.length) index = 0;
                }
                return true;
            }
        }
        return false;
    }

    addTroopsToCharacter( characterId, amount ) {
        let counties = this.getCharacterCounties(characterId);
        if (counties.length > 0) {
            let troopAcc = amount;
            let index = 0;
            while(troopAcc != 0) {
                counties[index++].troops++;
                troopAcc--;
                if (index >= counties.length) index = 0;
            }
            return true;
        }
        return false;
    }

    canCharactersMarry( character1, character2 ) {
        return character1.spouse == null && character2.spouse == null
            && character1.guid != character2.guid
            && character1.alive && character2.alive
            && !character1.imprisoner && !character2.imprisoner;
    }

    marry( character1_id, character2_id ) {
        let character1 = this.getCharacter(character1_id);
        let character2 = this.getCharacter(character2_id);
        if (this.canCharactersMarry(character1, character2)) {
            character1.spouse = character2_id;
            character2.spouse = character1_id;
            let maidenName = character2.house;
            character2.house = character1.house;
            if (character1.overlord == character2.overlord || character1.overlord == character2_id) {
                // already have same overlord, do nothing.
            } else if (character1.overlord) {
                this.setCharacterOverlord(character2_id, character1.overlord);
            } else if (character2.overlord) {
                this.setCharacterOverlord(character1_id, character2.overlord);
            }
            this.addNews(null, 
                `${chLink(character1)} has married ${chLink(character2)} (formally ${character2.name} ${maidenName}).`);
        }
    }

    changeCharacterResearch( player, characterId, researchIndex ) {
        let character = this.getCharacter(characterId);
        if (character.owningPlayer == player.username && character.research.length > researchIndex) {
            character.activeResearchIndex = researchIndex;
        }
    }

    activateItem( player, itemId ) {
        let item = this.getItem(itemId);
        let specialEffect = null;
        if (this.doesItemBelongToPlayer(item, player) && item.useType == 'once') {
            let activeItemsCap = this.getGameSettingValue(item.owningCharacter, "caps/activeItems");
            let activeItems = this.state.items.filter(i => i.owningCharacter == item.owningCharacter && i.active);
            if (activeItems.length < activeItemsCap) {
                item.active = true;
                item.countdown = item.duration;
                specialEffect = item.specialEffect || 'none';
                switch(specialEffect) {
                    case 'hide':
                        let character = this.getCharacter(item.owningCharacter);
                        character.hidden = true;
                        character.cooldowns.hidden = item.duration;
                        break;
                }
            }
        }
    }

    doesItemBelongToPlayer( item, player ) {
        let character = this.characterMap[item.owningCharacter];
        if (character) {
            return character.owningPlayer == player.username;
        } else { 
            return false;
        }
    }

    removeArmy( armyId ) {
        let army = this.armyMap[armyId];
        this.armyMap[armyId] = null;
        let county = this.countyMap[army.location];
        county.armies.splice(county.armies.indexOf(armyId), 1);
        let mainIndex = this.state.armies.findIndex(a => a.guid == armyId);
        this.state.armies.splice(mainIndex, 1);
    }

    mergeArmies( county ) {
        let armies = county.armies.map(id => this.armyMap[id]);
        let keyArmies = {};
        for (let i = armies.length - 1; i >= 0; i--) {
            let army = armies[i];
            if (keyArmies[army.owningCharacter]) {
                let keyArmy = keyArmies[army.owningCharacter];
                keyArmy.troops += army.troops;
                keyArmy.cooldown = Math.max(keyArmy.cooldown, army.cooldown);
                if (keyArmy.orders && army.orders && army.orders.length > keyArmy.orders.length) {
                    keyArmy.orders = army.orders;
                } else if (!keyArmy.orders && army.orders) {
                    keyArmies.orders = army.orders;
                }
                this.removeArmy(army.guid);
            } else {
                keyArmies[army.owningCharacter] = army;
            }
        }
    }

    getArmyCost( armyId ) {
        let army = this.getArmy(armyId);
        let armyCostPerTroop = this.getGameSettingValue(army.owningCharacter, "armyCostPerTroop");
        let baseArmyCost = this.getGameSettingValue(army.owningCharacter, "baseArmyCost");
        let cost = Math.floor(baseArmyCost + (armyCostPerTroop * army.troops));
        return cost;
    }

    makeArmy( player, countyId, amount ) {
        let county = this.getCounty(countyId);
        let character = this.getCharacter(county.owningCharacter);
        if ((player == null || this.doesItemBelongToPlayer(county, player)) && county.troops >= amount && character && !character.imprisoner) {
            county.troops -= amount;
            let existingArmy = county.armies
                .map(id => this.armyMap[id])
                .find(army => army.owningCharacter == county.owningCharacter);
            if (existingArmy) {
                existingArmy.troops += amount;
            } else {
                let character = this.getCharacter(county.owningCharacter);
                let army = {
                    guid: this.chance.guid(),
                    seed: this.chance.integer(),
                    location: countyId,
                    // cooldown: 0,
                    cooldown: this.getGameSettingValue(character.guid, "movementCooldown/afterRaise"),
                    troops: amount,
                    owningCharacter: county.owningCharacter
                }
                county.armies.push(army.guid);
                this.state.armies.push(army);
                this.armyMap[army.guid] = army;
            }
        } else if (player) {
            console.warn(`Player ${player.username} cannot make this army!`);
        }
    }

    isCountyFriendly( countyId, characterId ) {
        let county = this.getCounty(countyId);
        let predicateCharacter = this.getCharacter(characterId);
        let friendlyCharacters = [characterId];
        if (predicateCharacter.spouse) {
            friendlyCharacters.push(predicateCharacter.spouse);
        }
        if (predicateCharacter.overlord) {
            friendlyCharacters.push(predicateCharacter.overlord);
            let overlord = this.getCharacter(predicateCharacter.overlord);
            if (overlord.spouse) {
                friendlyCharacters.push(overlord.spouse);
            }
        }
        friendlyCharacters.push(
            ...this.getCharacterUnderlords(characterId).map(character => character.guid)
        );
        if (county.owningCharacter) {
            let countyOwner = this.getCharacter(county.owningCharacter);
            return predicateCharacter.house == countyOwner.house || friendlyCharacters.indexOf(county.owningCharacter) != -1;
        } else {
            return false;
        }
    }

    moveArmyWithOrders( player, armyId, orders ) {
        let army = this.getArmy(armyId);
        if (army && orders.length > 0 && this.doesItemBelongToPlayer(army, player)) {
            let location = orders[0];
            if (this.moveArmy(armyId, location)) {
                orders.shift();
            }
            army.orders = orders;
        }
    }

    moveArmy( armyId, countyId ) {
        let army = this.getArmy(armyId);
        if (!army) return;
        let character = this.getCharacter(army.owningCharacter);
        if (!character.imprisoner && army.cooldown == 0) {
            let fromCounty = this.getCounty(army.location);
            let countyCell = this.diagram.find(fromCounty.x, fromCounty.y, 100);
            let adjacentCounties = this.getAdjacentCounties(army.location);
            let canMove = (adjacentCounties.findIndex(adjacent => adjacent.guid == countyId) !== -1);
            if (canMove) {
                let isLocationFriendly = this.isCountyFriendly(countyId, army.owningCharacter);
                army.location = countyId;
                army.cooldown += 
                    isLocationFriendly ? 
                        this.getGameSettingValue(character.guid, "movementCooldown/friendlyTerritory") : 
                        this.getGameSettingValue(character.guid, "movementCooldown/enemyTerritory");
                let toCounty = this.getCounty(army.location);
                let countyArmyIndex = fromCounty.armies.findIndex(a => a == armyId);
                fromCounty.armies.splice(countyArmyIndex, 1);
                toCounty.armies.push(armyId);
                this.mergeArmies(toCounty);
                if (!isLocationFriendly) {
                    this.peformBattle(toCounty, army);
                }
                return true;
            }
        }
        return false;
    }

    standDownArmy( player, armyId ) {
        let army = this.getArmy(armyId);
        let character = this.getCharacter(army.owningCharacter);
        if ((player == null || this.doesItemBelongToPlayer(army, player)) && !character.imprisoner) {
            if (army.cooldown == 0) {
                let county = this.getCounty(army.location);
                county.troops += army.troops;
                if (county.owningCharacter != army.owningCharacter) {
                    this.sendMessage(army.owningCharacter, county.owningCharacter, `
                    I have gifted you ${army.troops} troops in ${coLink(county)}}.
                    `);
                }
                this.removeArmy(armyId);
            } else {
                army.orders = null;
                army.standDownQueued = true;
            }
        }
    }

    getAttackMultiplier( characterId ) {
        let character = this.getCharacter(characterId);
        return (character.focus == 'military') ? 
            this.getGameSettingValue(character.guid, "battleMultipliers/attacking/withFocus") :
            this.getGameSettingValue(character.guid, "battleMultipliers/attacking/withoutFocus");
    }

    getDefendMultiplier( characterId ) {
        let character = this.getCharacter(characterId);
        return (character.focus == 'military') ?
            this.getGameSettingValue(character.guid, "battleMultipliers/defending/withFocus") :
            this.getGameSettingValue(character.guid, "battleMultipliers/defending/withoutFocus");
    }

    peformBattle( county, attackingArmy ) {
        let attackingCharacter = this.getCharacter(attackingArmy.owningCharacter);
        let attackingBonusMultiplier = this.getAttackMultiplier(attackingArmy.owningCharacter);
        // First the attacking army attacks all the defending standing armies.
        let defendingArmies = county.armies.filter(id => id != attackingArmy.guid).map(id => this.armyMap[id]);
        defendingArmies.sort((a, b) => b.troops - a.troops);
        for (let i = 0; i < defendingArmies.length; i++) {
            let defendingArmy = defendingArmies[i];
            let defendingCharacter = this.getCharacter(defendingArmy.owningCharacter);
            let defendingBonusMultiplier = this.getDefendMultiplier(defendingArmy.owningCharacter);
            let defendingTroops = Math.floor(defendingArmy.troops * defendingBonusMultiplier);
            let attackingTroops = Math.floor(attackingArmy.troops * attackingBonusMultiplier);
            let damage = Math.min(defendingTroops, attackingTroops);
            attackingArmy.troops -= damage;
            defendingArmy.troops -= damage;

            defendingCharacter.stats.lastTurnAttackOrDefended = this.state.map.turn;
            attackingCharacter.stats.lastTurnAttackOrDefended = this.state.map.turn;
            attackingCharacter.stats.troopsAttacked += damage;
            if (defendingArmy.troops <= 0) {
                attackingCharacter.stats.armiesDestroyed++;
            }
            if (attackingArmy.troops <= 0) {
                defendingCharacter.stats.armiesDestroyed++;
                break;
            };
        }
        // Then, if the attacking army is still alive, it attacks the county's troops.
        if (attackingArmy.troops > 0) {
            attackingCharacter.stats.lastTurnAttackOrDefended = this.state.map.turn;
            let defendingCharacter = this.getCharacter(county.owningCharacter);
            let defendingBonusMultiplier = 1;
            if (defendingCharacter) {
                defendingCharacter.stats.lastTurnAttackOrDefended = this.state.map.turn;
                defendingBonusMultiplier = this.getDefendMultiplier(county.owningCharacter);
            }
            let countyTroops = Math.floor(county.troops * defendingBonusMultiplier);
            let attackingTroops = Math.floor(attackingArmy.troops * attackingBonusMultiplier);
            let damage = Math.min(countyTroops, attackingTroops);
            county.troops -= damage;
            attackingArmy.troops -= damage;
            attackingCharacter.stats.troopsAttacked += damage;

            // Send messages.
            let msg = `${chLink(attackingCharacter)} attacked ${county.name} with ${attackingTroops}vs${countyTroops} troops.
                Both sides took ${damage} damage.`;
            this.sendMessage(attackingCharacter.guid, attackingCharacter.guid, msg);
            if (county.owningCharacter) {
                this.sendMessage(county.owningCharacter, county.owningCharacter, msg);
            }

            // County was defeated.
            if (county.troops == 0) {
                if (county.owningCharacter) {
                    this.sendMessage(county.owningCharacter, county.owningCharacter,
                        `${chLink(attackingCharacter)} has taken ${county.name}`);
                }
                // Attacking character takes county.
                if (attackingArmy.troops > 0){
                    this.setCountyOwner(county.guid, attackingArmy.owningCharacter);
                    // Imprison character at location.
                    let charactersAtLocation = this.state.characters.filter(c => c.location == county.guid && !c.hidden);
                    if (charactersAtLocation.length > 0) {
                        let characterToImprison = this.chance.pickone(charactersAtLocation);
                        this.imprisonCharacter(attackingArmy.owningCharacter, characterToImprison.guid);
                    }
                    // Release any other prisoners at location.
                    let prisonersAtLocation = this.state.characters.filter(c => 
                        c.location == county.guid && c.imprisoner != null && c.imprisoner != attackingArmy.owningCharacter);
                    prisonersAtLocation.forEach(prisoner => {
                        this.releasePrisoner(prisoner.guid);
                    });
                    // Send message.
                    this.sendMessage(county.owningCharacter, county.owningCharacter,
                        `I have taken ${county.name}`);
                } else {
                    this.setCountyOwner(county.guid, null);
                    // Send message.
                    this.sendMessage(attackingArmy.owningCharacter, attackingArmy.owningCharacter,
                        `I tried to take ${county.name}. The county is now neutral.`);
                }
            } else if(county.owningCharacter) {
                this.sendMessage(county.owningCharacter, county.owningCharacter,
                        `${chLink(attackingCharacter)} has attacked ${coLink(county)}`);
            }
        }

        // Cleanup.
        for (let i = 0; i < county.armies.length; i++) {
            let army = this.armyMap[county.armies[i]];
            if (army.troops <= 0) {
                this.removeArmy(army.guid);
            }
        }
    }

    getFocusBonus( characterId, focus ) {
        let bonusValues = {
            moneyFocus: this.getGameSettingValue(characterId, "focusBonus/money"),
            militaryFocus: this.getGameSettingValue(characterId, "focusBonus/military"),
            scienceFocus: this.getGameSettingValue(characterId, "focusBonus/science"),
        };
        let moneyBonus = ((focus == 'money') ? bonusValues.moneyFocus : 0);
        let militaryBonus = ((focus == 'military') ? bonusValues.militaryFocus : 0);
        let scienceBonus = ((focus == 'science') ? bonusValues.scienceFocus : 0);
        return {moneyBonus, militaryBonus, scienceBonus};
    }

    getTechBonus( characterId ) {
        let bonusValues = {
            moneyBonus: this.getGameSettingValue(characterId, "techBonus/money"),
            militaryBonus: this.getGameSettingValue(characterId, "techBonus/military"),
            scienceBonus: this.getGameSettingValue(characterId, "techBonus/science")
        }
        return bonusValues;
    }

    getFocusBonusStacked( characterId ) {
        let character = this.getCharacter(characterId);
        let bonuses = this.getFocusBonus(characterId, character.focus);
        if (character.spouse) {
            let spouse = this.getCharacter(character.spouse);
            let spouseBonuses = this.getFocusBonus(spouse.guid, spouse.focus);
            for (let bonus in bonuses) {
                bonuses[bonus] += spouseBonuses[bonus];
            }
        }
        if  (character.overlord) {
            let overlord = this.getCharacter(character.overlord);
            let overlordBonuses = this.getFocusBonus(overlord.guid, overlord.focus);
            for (let bonus in bonuses) {
                bonuses[bonus] += overlordBonuses[bonus];
            }
        }
        return bonuses;
    }

    performPlot( player, characterId, targetId, plotType ) {
        let character = this.getCharacter(characterId);
        let target = this.getCharacter(targetId);
        let plotCost = this.getGameSettingValue(characterId, `plotting/${plotType}/cost`);
        if ((player == null || character.owningPlayer == player.username) 
            && character.cooldowns.plotting == 0
            && character.money >= plotCost
            && character.location == target.location
            && character.alive && target.alive
            && !character.imprisoner)
        {
            character.money -= plotCost;
            character.cooldowns.plotting = this.getGameSettingValue(character, 'plotting/cooldown');
            let succeedPublic, succeedSilent, failPublic, failSilent, defense, result;
            defense = this.getGameSettingValue(targetId, `plotting/${plotType}/defense`);
            succeedPublic = this.getGameSettingValue(characterId, `plotting/${plotType}/succeedPublic`);
            succeedSilent = this.getGameSettingValue(characterId, `plotting/${plotType}/succeedSilent`);
            failPublic = this.getGameSettingValue(characterId, `plotting/${plotType}/failPublic`);
            failSilent = this.getGameSettingValue(characterId, `plotting/${plotType}/failSilent`);
            failPublic += defense;
            const SUCCESS_PUBLIC = 1;
            const SUCCESS_SILENT = 2;
            const FAIL_PUBLIC = 3;
            const FAIL_SILENT = 4;
            if (succeedSilent) {
                result = this.chance.weighted(
                        [SUCCESS_PUBLIC, SUCCESS_SILENT, FAIL_PUBLIC, FAIL_SILENT],
                        [succeedPublic, succeedSilent, failPublic, failSilent]);
            } else {
                result = this.chance.weighted(
                        [SUCCESS_PUBLIC, FAIL_PUBLIC, FAIL_SILENT],
                        [succeedPublic, failPublic, failSilent]);
            }

            switch(plotType) {
                case 'imprison':
                    if (result == SUCCESS_PUBLIC) {
                        this.imprisonCharacter(characterId, targetId);
                    } else if (result == FAIL_PUBLIC) {
                        this.sendMessage(targetId, targetId, 
                            `${chLink(character)} was plotting to imprison me!`);
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} caught wind of my plot to imprison.`);
                        this.addNews(null, 
                            `${chLink(character)} was caught plotting to imprison ${chLink(target)}.`);
                        this.imprisonCharacter(targetId, characterId);
                    } else {
                        this.sendMessage(characterId, characterId, 
                            `I have failed to imprison ${chLink(target)}. No-one knows about this.`);
                    }
                    break;
                case 'murder':
                    if (result == SUCCESS_PUBLIC || result == SUCCESS_SILENT) {
                        this.killCharacter(target.guid);
                        if (!character.stats.charactersKilled) {
                            character.stats.charactersKilled = [];
                        }
                        character.stats.charactersKilled.push(target.guid);
                    }
                    if (result == SUCCESS_PUBLIC) {
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} has been murdered in a rather obvious fashion.`);
                        this.addNews(null, `${chLink(target)} has been murdered by ${chLink(character)}.`);
                    } else if (result == SUCCESS_SILENT) {
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} has been murdered. No-one knows who did it.`);
                    } else if (result == FAIL_PUBLIC) {
                        this.sendMessage(targetId, targetId, 
                            `${chLink(character)} was plotting to murder me!`);
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} caught wind of my plot to murder.`);
                        this.addNews(null, 
                            `${chLink(character)} was caught plotting to murder ${chLink(target)}.`);
                            this.imprisonCharacter(targetId, characterId);
                    } else {
                        this.sendMessage(characterId, characterId, 
                            `I have failed to murder ${chLink(target)}. No-one knows about this.`);
                    }
                    break;
                case 'steal':
                    let percentToSteal = this.getGameSettingValue(character.guid, 'plotting/steal/percentToSteal');
                    let moneyStolen = 0;
                    if (result == SUCCESS_PUBLIC || result == SUCCESS_SILENT) {
                        let moneyToSteal = Math.floor(target.money * 0.25);
                        moneyStolen = moneyToSteal;
                        if (moneyToSteal > 0) {
                            target.money -= moneyToSteal;
                            character.money += moneyToSteal;
                        }
                        let possibleItemsToSteal = this.state.items.filter(item => item.owningCharacter == target.guid && !item.active);
                        if (possibleItemsToSteal.length > 0) {
                            let itemToSteal = this.chance.pickone(possibleItemsToSteal);
                            itemToSteal.owningCharacter = character.guid;
                        }
                        if (!character.stats.charactersStolenFrom) {
                            character.stats.charactersStolenFrom = [];
                        }
                        character.stats.charactersStolenFrom.push(target.guid);
                    }
                    if (result == SUCCESS_PUBLIC) {
                        this.addNews(null, `${chLink(character)} has stolen ${moneyStolen}btc from ${chLink(target)}.`);
                        this.sendMessage(targetId, targetId, `${chLink(character)} has stolen ${moneyStolen}btc from me!`);
                    } else if (result == SUCCESS_SILENT) {
                        this.sendMessage(characterId, characterId, 
                            `I have stolen ${moneyStolen}btc from ${chLink(target)}.`);
                    } else if (result == FAIL_PUBLIC) {
                        this.addNews(null, `${chLink(character)} was caught plotting to steal money from ${chLink(target)}.`);
                        this.sendMessage(targetId, targetId, `${chLink(character)} was plotting to steal from me!`);
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} caught wind of my plot to steal.`);
                    } else if (result == FAIL_SILENT) {
                        this.sendMessage(characterId, characterId, 
                            `My plot to steal from ${chLink(target)} has failed. No-one knows about this.`);
                    }
                    break;
                case 'sabotage':
                    let sabotageDuration = this.getGameSettingValue(character.guid, 'plotting/sabotage/duration');
                    let sabotageDelayMinMax = this.getGameSettingValue(character.guid, 'plotting/sabotage/delay');
                    let sabotagePercent = this.getGameSettingValue(character.guid, 'plotting/sabotage/percent');
                    let delay = this.chance.integer({min: sabotageDelayMinMax.min, max: sabotageDelayMinMax.max});
                    if (result == SUCCESS_PUBLIC || result == SUCCESS_SILENT) {
                        if (!target.cooldowns.sabotageDelay || target.cooldowns.sabotageDelay == 0) {
                            target.cooldowns.sabotageDelay = delay;
                        }
                        target.nextSabotageDuration = sabotageDuration;
                        target.sabotagePercent = sabotagePercent;
                        if (!character.stats.charactersSabotaged) {
                            character.stats.charactersSabotaged = [];
                        }
                        character.stats.charactersSabotaged.push(target.guid);
                    }
                    if (result == SUCCESS_PUBLIC) {
                        this.addNews(null, `${chLink(target)} has been sabotaged by ${chLink(character)}!`);
                        this.sendMessage(targetId, targetId, `${chLink(character)} has sabotaged my council!`);
                    } else if (result == SUCCESS_SILENT) {
                        this.sendMessage(characterId, characterId, `I have sabotaged ${chLink(target)}'s council.`);
                    } else if (result == FAIL_PUBLIC) {
                        this.addNews(null, `${chLink(character)} was caught plotting to sabotage ${chLink(target)}.`);
                        this.sendMessage(targetId, targetId, `${chLink(character)} was plotting to sabotage my council!`);
                        this.sendMessage(characterId, characterId, 
                            `${chLink(target)} caught wind of my plot to sabotage.`);
                    } else if (result == FAIL_SILENT) {
                        this.sendMessage(characterId, characterId, 
                            `My plot to sabotage ${chLink(target)} has failed. No-one knows about this.`);
                    }
                    break;
            }
        }
    }

    changeCharacterMoney( character, type, value ) {
        if (!character.moneyRateBreakdown) {
            character.moneyRateBreakdown = {};
        }
        // Loose money to sabotage plot.
        if (character.cooldowns.sabotage && character.cooldowns.sabotage > 0 && value > 0) {
            let preValue = value;
            value = Math.floor(value * character.sabotagePercent);
            let lostValue = value - preValue;
            if (!character.moneyRateBreakdown['sabotage']) {
                character.moneyRateBreakdown['sabotage'] = lostValue;
            } else {
                character.moneyRateBreakdown['sabotage'] += lostValue;
            }
        }
        if (!character.moneyRateBreakdown[type]) {
            character.moneyRateBreakdown[type] = value;
        } else {
            character.moneyRateBreakdown[type] += value;
        }
        character.moneyRate += value;
        character.money += value;
        if (character.money < 0) {
            character.money = 0;
        }
    }

    changeCharacterTech( character, type, value ) {
        if (!character.techRateBreakdown) {
            character.techRateBreakdown = {};
        }
        if (!character.techRateBreakdown[type]) {
            character.techRateBreakdown[type] = value;
        } else {
            character.techRateBreakdown[type] += value;
        }
        character.techRate += value;
        character.tech += value;
        if (character.tech < 0) {
            character.tech = 0;
        }
    }

    killCharacter( characterId ) {
        let character = this.getCharacter(characterId);
        character.alive = false;
        // Check Desires.
        character.desires.forEach(desire => {
            this.checkDesire( character, desire );
        });
        // If this character is a heir, remove the heirTo property and find a new one.
        if (character.heirTo) {
            let newHeir = this.getCharacterHeir(character.heirTo);
            if (newHeir) {
                newHeir.heirTo = character.heirTo;
            }
        }
        // When character dies with an heir, the heir will inherit its titles and resources.
        let heir = this.getCharacterHeir(character.guid);

        // Send mail notification.
        if (character.owningPlayer && this.notificationsHandler) {
            this.notificationsHandler.notify(toCharacter.owningPlayer, {
                template: 'character_death',
                params: {
                    catName: chLink(character),
                    age: character.age,
                    heirName: heir ? `${heir.name} ${heir.house}` : null
                }
            });
        }

        if (heir) {
            heir.heirTo = character.guid;
            // Add news message.
            this.addNews(null, 
                `${chLink(character)} has died. ${chLink(heir)} has inherited their titles.`);
            // Player loses control of dead character.
            if (character.owningPlayer) {
                let playerId = character.owningPlayer;
                let player = this.state.players.find(p => p.username == playerId);
                let playerCharacterIndex = player.characters.indexOf(character.guid);
                player.characters.splice(playerCharacterIndex, 1);
                // Player gains control of heir.
                if (!heir.owningPlayer) {
                    heir.owningPlayer = character.owningPlayer;
                    player.characters.push(heir.guid);
                }
                // Player also gains control of the next heir if under the character limit.
                if (player.characters.length < this.config.MAX_CHARACTERS_PER_PLAYER) {
                    let heirHeir = this.getCharacterHeir(heir);
                    if (heirHeir && !heirHeir.owningPlayer) {
                        heirHeir.owningPlayer = character.owningPlayer;
                        player.characters.push(heirHeir.guid);
                    }
                }
                character.owningPlayer = null;
            }
            character.heir = heir.guid;
            // Heir inherits money.
            heir.money += character.money;
            // Heir inherits items.
            this.state.items
                .filter(item => item.owningCharacter == character.guid)
                .forEach(item => {
                    item.owningCharacter = heir.guid;
                });
            // Heir inherits blueprints.
            character.research.slice(1).forEach(tech => {
                let index = heir.research.findIndex(otherTech => otherTech.key == tech.key);
                if (index == -1) {
                    heir.research.push(tech);
                }
            });
            // Heir inherits parent's titles.
            let counties = this.getCharacterCounties(character.guid);
            counties.forEach(county => {
                county.owningCharacter = heir.guid;
            });
        } else {
            // Add news message.
            this.addNews(null, 
                `${chLink(character)} has died with no heir.`);
            // set counties to no mans land.
            let counties = this.getCharacterCounties(character.guid);
            counties.forEach(county => {
                county.owningCharacter = null;
            });
            // remove items
            if (this.state.items.length > 0) {
                for (let itemIndex = this.state.items.length - 1; itemIndex >= 0; itemIndex--) {
                    if (this.state.items[itemIndex] && this.state.items[itemIndex].owningCharacter == character.guid) {
                        this.state.items.splice(itemIndex, 1);
                    }
                }
            }
        }

        // Clear references to dead character.
        let underlings = this.getCharacterUnderlords(character.guid);
        underlings.forEach(underling => underling.overlord = null);
        if (character.spouse) {
            this.getCharacter(character.spouse).spouse = null;
        }
        let prisoners = this.getCharacterPrisoners(character.guid);
        prisoners.forEach(prisoner => {
            this.sendMessage(prisoner.guid, prisoner.guid, `My imprisoner, ${chLink(character)}, has died. I have been released.`);
            prisoner.imprisoner = null;
        });
        character.overlord = null;
        character.spouse = null;
        character.imprisoner = null;
        character.heirTo = null;
        character.money = 0;
    }

    peformGameTick() {
        this.state.map.turn++;
        let turn = this.state.map.turn;
        let isIncomeTurn = ((turn % this.config.INCOME_TURN_INTERVAL) == 0);
        // Reset production rates and update caps.
        if (isIncomeTurn) {
            for (let i = 0; i < this.state.characters.length; i++) {
                let character = this.state.characters[i];
                character.moneyRate = 0;
                character.techRate = 0;
                if (character.moneyRateBreakdown) {
                    for (let rateKey in character.moneyRateBreakdown) {
                        character.moneyRateBreakdown[rateKey] = 0;
                    }
                } else {
                    character.moneyRateBreakdown = {};    
                }
                if (character.techRateBreakdown) {
                    for (let rateKey in character.techRateBreakdown) {
                        character.techRateBreakdown[rateKey] = 0;
                    }
                } else {
                    character.techRateBreakdown = {};
                }
            }
        }

        // Update counties and score points.
        for (let i = 0; i < this.state.counties.length; i++) {
            let county = this.state.counties[i];
            if (county.owningCharacter) {
                county.maxTroops = this.getGameSettingValue(county.owningCharacter, "caps/countyTroopsPerCountySize") * county.size;
                let focusBonus = this.getFocusBonusStacked(county.owningCharacter);
                let techBonus = this.getTechBonus(county.owningCharacter);
                let character = this.getCharacter(county.owningCharacter);
                let owner = this.getCharacter(county.owningCharacter);
                // Troop recruit.
                county.troopRate = county.size + Math.max(focusBonus.militaryBonus + techBonus.militaryBonus, 0);
                // Loose troop recruitment to sabotage.
                if (owner.cooldowns.sabotage > 0) {
                    county.troopRate = Math.floor(county.troopRate * owner.sabotagePercent);
                }
                county.troops += county.troopRate;
                if (county.troops >= county.maxTroops) {
                    county.troops = county.maxTroops;
                }
                // Money income.
                county.moneyRate = county.size;
                // Loose income due to sabotage.
                if (owner.cooldowns.sabotage > 0) {
                    county.moneyRate = Math.floor(county.moneyRate * owner.sabotagePercent);
                }
                county.money += county.moneyRate;
                if (county.money < 0) {
                    county.money = 0;
                }
                // Science.
                county.techRate = county.size;
                county.tech += county.techRate;

                // If this is an income turn, transfer the money to the owning character.
                if (isIncomeTurn && !character.imprisoner) {
                    this.changeCharacterMoney(character, "Counties", county.money);
                    this.changeCharacterMoney(character, "Focus", Math.max(focusBonus.moneyBonus, 0));
                    this.changeCharacterMoney(character, "Tech", Math.max(techBonus.moneyBonus, 0));
                    county.money = 0;
                    this.changeCharacterTech(character, "Counties", county.tech);
                    this.changeCharacterTech(character, "Focus", Math.max(focusBonus.scienceBonus, 0));
                    this.changeCharacterTech(character, "Tech", Math.max(techBonus.scienceBonus, 0));
                    county.tech = 0;
                }

                if (owner.owningPlayer) {
                    let pointsPerCountySize = this.getGameSettingValue(county.owningCharacter, "pointsPerCountySize");
                    let points = county.size * pointsPerCountySize;
                    let player = this.getPlayer(owner.owningPlayer);
                    player.score += points;
                }
            } else {
                county.troopRate = 1;
                county.moneyRate = 1;
                county.techRate = (county.size >= 4) ? 1 : 0;
                county.maxTroops = K.caps.countyTroopsPerCountySize * county.size;
                county.troops += county.troopRate;
                county.money += county.moneyRate;
                county.tech += county.techRate;
                if (county.troops >= county.maxTroops) {
                    county.troops = county.maxTroops;
                }
            }
        }

        // Update live characters.
        let aliveCharacters = this.state.characters.filter(c => c.alive);
        for (let characterIndex = 0; characterIndex < aliveCharacters.length; characterIndex++) {
            let character = aliveCharacters[characterIndex];

            character.totalTroops = this.state.counties.reduce((sum, county) => {
                if (county.owningCharacter == character.guid) {
                    return sum + county.troops;
                }
                return sum;
            }, 0);
            if (isIncomeTurn) {
                let lastTotalTroops = character.totalTroopsLastIncomeTurn || 0;
                character.troopRate = (character.totalTroops - lastTotalTroops);
                character.totalTroopsLastIncomeTurn = character.totalTroops;
            }

            // AI
            if (!character.owningPlayer && this.config.BOTS) {
                let ai = new AICharacter(this, character);
                ai.tick();
            }

            // Decrease cooldowns.
            for (let key in character.cooldowns) {
                if (character.cooldowns[key] != null && character.cooldowns[key] > 0) {
                    character.cooldowns[key]--;
                }
                if (key == 'hidden' && character.cooldowns[key] == 0) {
                    character.hidden = false;
                    character.cooldowns.hidden = null;
                }
                if (key == 'sabotageDelay' && character.cooldowns[key] == 0) {
                    character.cooldowns.sabotageDelay = null;
                    character.cooldowns.sabotage = character.nextSabotageDuration;
                }
            }

            // Pay tax to overlord.
            if (isIncomeTurn && character.overlord && !character.imprisoner) {
                let overlord = this.getCharacter(character.overlord);
                let overlordUnderlords = this.getCharacterUnderlords(character.overlord);
                let baseTaxRate = (overlord.focus == 'money') ?
                    this.getGameSettingValue(character.guid, "taxes/withFocus/baseTaxRate") :
                    this.getGameSettingValue(character.guid, "taxes/withoutFocus/baseTaxRate");
                let incrementalTaxRate = (overlord.focus == 'money') ?
                    this.getGameSettingValue(character.guid, "taxes/withFocus/incrementalTaxRate") :
                    this.getGameSettingValue(character.guid, "taxes/withoutFocus/incrementalTaxRate");
                let taxRate = baseTaxRate + (overlordUnderlords.length * incrementalTaxRate);
                let tax = Math.floor(Math.max(character.moneyRate, 0) * taxRate);
                overlord.taxRate = taxRate;
                this.changeCharacterMoney(character, "Overlord Tax", -tax);
                this.changeCharacterMoney(overlord, "Vassal Tax Income", tax);
                // Military Taxes.
                baseTaxRate = (overlord.focus == 'military') ?
                    this.getGameSettingValue(character.guid, "taxes/withFocus/baseTaxRate") :
                    this.getGameSettingValue(character.guid, "taxes/withoutFocus/baseTaxRate");
                incrementalTaxRate = (overlord.focus == 'military') ?
                    this.getGameSettingValue(character.guid, "taxes/withFocus/incrementalTaxRate") :
                    this.getGameSettingValue(character.guid, "taxes/withoutFocus/incrementalTaxRate");
                taxRate = baseTaxRate + (overlordUnderlords.length * incrementalTaxRate);
                let troopTax = Math.floor(Math.max(character.troopRate, 0) * taxRate);
                this.transferTroopsBetweenCharacters(character.guid, character.overlord, troopTax);
            }

            // Pay fines for going over cap limits.
            let counties = this.getCharacterCounties(character.guid);
            if (counties.length > character.maxCounties) {
                let finePerCountyOverCap = this.getGameSettingValue(character.guid, "capFines/perCounty");
                let over = counties.length - character.maxCounties;
                let fine = finePerCountyOverCap * over;
                this.changeCharacterMoney(character, "County Limit Fine", -fine);
            }
            let armies = this.getCharacterArmies(character.guid);
            if (armies.length > character.maxArmies) {
                let finePerArmyOverCap = this.getGameSettingValue(character.guid, "capFines/perArmy");
                let over = armies.length - character.maxArmies;
                let fine = finePerArmyOverCap * over;
                this.changeCharacterMoney(character, "Army Limit Fine", -fine);
            }

            // Clear expired denounces.
            if (character.denounces) {
                let denounceDuration = this.getGameSettingValue(character.guid, 'denounce/duration');
                for (let dIndex = character.denounces.length - 1; dIndex >= 0; dIndex--) {
                    let turnStarted = character.denounces[dIndex].turn;
                    let duration = this.state.map.turn - turnStarted;
                    if (duration >= denounceDuration) {
                        character.denounces.slice(dIndex, 1);
                    }
                }
            }
            // Loose money due to denounces.
            if (isIncomeTurn) {
                let denounces = this.state.characters.filter(other => {
                    return other.denounces && other.denounces.findIndex(denounce => denounce.target == character.guid) != -1;
                });
                if (denounces.length > 0) {
                    let denouncePenalty = this.getGameSettingValue(character.guid, "denounce/incomePenalty") * denounces.length;
                    this.changeCharacterMoney(character, "denounces", -denouncePenalty);
                }
            }
            
            // Couples split their money.
            if (isIncomeTurn && character.spouse && !character.imprisoner) {
                let spouseMoneyBonus = this.getGameSettingValue(character.guid, "spouseMoneyBonus");
                let spouse = this.getCharacter(character.spouse);
                if (character.money > spouse.money) {
                    let share = Math.floor((character.money - spouse.money) * 0.5);
                    this.changeCharacterMoney(character, "Marriage Share", -share);
                    this.changeCharacterMoney(spouse, "Marriage Share", share);
                }
                this.changeCharacterMoney(character, "Marriage Bonus", spouseMoneyBonus);
            }

            // Children get pocketmoney, from each parent.
            if (isIncomeTurn && character.parents && !character.imprisoner) {
                for (let parentId of character.parents) {
                    let parent = this.getCharacter(parentId);
                    if (parent.alive) {
                        let pocketmoneyBase = this.getGameSettingValue(character.guid, "pocketmoney/base");
                        let pocketmoneyPercentOfIncome = this.getGameSettingValue(character.guid, "pocketmoney/percentOfIncome");
                        let pocketmoney = pocketmoneyBase + Math.floor(Math.max(parent.moneyRate, 0) * pocketmoneyPercentOfIncome);
                        this.changeCharacterMoney(parent, "Children", -pocketmoney);
                        this.changeCharacterMoney(character, "Pocket Money", pocketmoney);
                    }
                }
            }

            // Research.
            if (character.research.length > 0 && character.activeResearchIndex >= 0) {
                let tech = character.research[character.activeResearchIndex];
                if (!tech.research) tech.research = 0;
                tech.research += character.tech;
                character.tech = 0;
                let researchCompleteCount = Math.floor(tech.research / tech.researchCost);
                tech.research = tech.research % tech.researchCost;
                for (let roll = 0; roll < researchCompleteCount; roll++) {
                    tech.tryCount = tech.tryCount ? (tech.tryCount + 1) : 1;
                    if (tech.yieldChance >= 1 || Math.random() <= tech.yieldChance) {
                        tech.completeCount = tech.completeCount ? (tech.completeCount + 1) : 1;
                        if (tech.key == 'experimentation') {
                            // New tech discovered.
                            let researchKeys = character.research.map(r => r.key);
                            let undiscoveredTech = TECH.filter(t => {
                                return researchKeys.indexOf(t.key) == -1
                            });
                            if (undiscoveredTech.length > 0) {
                                let undiscoveredTechWeights = undiscoveredTech.map(t => t.discoverChance || 1);
                                let newTech = this.chance.weighted(undiscoveredTech, undiscoveredTechWeights);
                                character.research.push(Object.assign({}, newTech));
                                this.sendMessage(character.guid, character.guid, 
                                    `You have discovered '${newTech.name}' to research.`, null);
                            } else {
                                this.sendMessage(character.guid, character.guid, 
                                    `You have discovered all the science!`, null);
                            }
                        } else {
                            // New item created.
                            let item = Object.assign({}, tech);
                            item.guid = this.chance.guid();
                            item.owningCharacter = character.guid;
                            if (item.useType == 'passive' || item.useType == 'global') {
                                item.countdown = item.duration || 0;
                            }
                            this.state.items.push(item);
                        }
                    }
                }
            }

            // Update caps.
            character.maxCounties = this.getGameSettingValue(character.guid, "caps/county");
            character.maxArmies = this.getGameSettingValue(character.guid, "caps/army");
            character.maxCounties += this.getCharacterUnderlords(character.guid).length;
            character.maxActiveItems = this.getGameSettingValue(character.guid, "caps/activeItems");
            if (character.overlord) {
                character.maxCounties += this.getCharacterUnderlords(character.overlord).length;
            }

            // Desires.
            character.desires.forEach(desire => {
                this.checkDesire( character, desire );
            });

            // Increase age.
            character.age++;
            if (character.age >= character.lifespan) {
                this.killCharacter(character.guid);
            }
        }

        // Update army cooldowns and costs.
        for (let i = 0; i < this.state.armies.length; i++) {
            let army = this.state.armies[i]
            if (army.cooldown > 0) {
                army.cooldown--;
            }
            let owner = this.getCharacter(army.owningCharacter);
            let cost = this.getArmyCost(army.guid);
            if (owner.money >= cost) {
                this.changeCharacterMoney(owner, "Armies", -cost);
                army.attrition = null;
            } else {
                this.changeCharacterMoney(owner, "Armies", -cost);
                army.attrition = this.getGameSettingValue(army.owningCharacter, "armyUnpaidAttrition");
                army.troops -= army.attrition;
                if (army.troops <= 0) {
                    this.removeArmy(army.guid);
                }
            }
        }
        // Move Armies.
        for (let i = this.state.armies.length - 1; i >= 0; i--) {
            let army = this.state.armies[i];
            if (army.cooldown == 0 && army.standDownQueued) {
                army.standDownQueued = null;
                this.standDownArmy(null, army.guid);
            } else if (army.cooldown == 0 && army.orders && army.orders.length > 0) {
                let location = army.orders[0];
                if (this.moveArmy(army.guid, location)) {
                    army.orders.shift();
                }
            }
        }

        // Update items.
        for (let i = this.state.items.length - 1; i >= 0; i--) {
            let item = this.state.items[i];
            if (item.countdown && item.countdown > 0) {
                item.countdown--;
                if (item.countdown <= 0) {
                    this.state.items.splice(i, 1);
                }
            }
        }
    }

    fuzzyfyItem( item, field ) {
        item[field] += (new Chance(item.seed)).integer({min: 0, max: item[field] * K.fuzzyfy.multiply});
        item[field] = Math.ceil(item[field] / K.fuzzyfy.roundTo) * K.fuzzyfy.roundTo;
        delete item.seed;
        return item;
    }

    fuzzyfyValue( seed, value ) {
        let chance = new Chance(seed);
        let result = chance.integer({min: 0, max: value * K.fuzzyfy.multiply});
        result = Math.ceil(result / K.fuzzyfy.roundTo) * K.fuzzyfy.roundTo;
        return result;
    }

    getStateForPlayer( player ) {
        let state = {};
        
        // Get relationships.
        let playerCharacters = this.state.characters.filter(c => c.owningPlayer == player.username);
        let playerCharacterIds = playerCharacters.map(c => c.guid);
        let playerHouses = playerCharacters.map(c => c.house);
        let playerOverlordIds = playerCharacters.filter(c => c.overlord).map(c => c.overlord);
        let playerUnderlingIds = this.state.characters.filter(c => {
            return playerCharacterIds.indexOf(c.overlord) !== -1;
        }).map(c => c.guid);
        let friendlyCharacterIds = [...playerOverlordIds, ...playerUnderlingIds];
        let ourCharacterLocations = playerCharacters.map(c => c.location);

        // Sort players by score.
        let playerMap = this.state.players.reduce((pre, cur) => {
            pre[cur.username] = cur;
            return pre;
        }, {});
        let playerNames = this.state.players.map(p => p.username);
        playerNames.sort((a, b) => {
            return playerMap[b].score - playerMap[a].score;
        });
        state.players = playerNames;

        state.player = player;

        // Set the visible state for all counties to null.
        let countyVisibility = {};
        if (this.config.FOG_OF_WAR) {
            // Set visibility on all our counties.
            this.state.counties.forEach(county => {
                let owningCharacter = this.getCharacter(county.owningCharacter);
                if (owningCharacter 
                    && (playerHouses.indexOf(owningCharacter.house) != -1 || friendlyCharacterIds.indexOf(county.owningCharacter) != -1))
                {
                    countyVisibility[county.guid] = true;
                    // Set visibility on counties adjacent to visible counties.
                    let range = this.getGameSettingValue(county.owningCharacter, "fogOfWar/countyRange");
                    this.getAdjacentCountiesSteps(county.guid, range).forEach(adjacentCounty => {
                        countyVisibility[adjacentCounty.guid] = true;
                    });
                }
            });
            // Set visibilty on all counties in range around player characters.
            let visibleCharacters = [...playerCharacters, ...playerCharacters.map(c => this.getCharacter(c.spouse))];
            visibleCharacters.forEach(character => {
                if (character) {
                    let location = this.getCounty(character.location);
                    if (location) {
                        let range = this.getGameSettingValue(character.guid, "fogOfWar/characterRange");
                        this.getAdjacentCountiesSteps(location.guid, range).forEach(adjacentCounty => {
                            countyVisibility[adjacentCounty.guid] = true;
                        });
                    }
                }
            });
        } else {
            this.state.counties.forEach(county => {
                countyVisibility[county.guid] = true;
            });
        }

        // Collate county information.
        state.counties = this.state.counties.map(county => {
            let result = Object.assign({}, county);
            let owningCharacter = this.getCharacter(county.owningCharacter);

            let isOurCharactersAtLocation = (ourCharacterLocations.indexOf(county.guid) != -1);
            let isVisible = countyVisibility[county.guid];
            let isOurHouse = (owningCharacter && playerHouses.indexOf(owningCharacter.house) != -1);
            let isFriendly = (owningCharacter && friendlyCharacterIds.indexOf(county.owningCharacter) != -1);
            let isOwner = (playerCharacterIds.indexOf(county.owningCharacter) != -1);
            result.isVisible = countyVisibility[county.guid];
            result.isOwner = isOwner;

            if (isOwner || isOurHouse || isFriendly) {
                result.infoLevel = 1;
                result.isFriendly = true;
            } else if (isVisible) {
                result.infoLevel = 2;
                result.isFriendly = false;
                result.troops = this.fuzzyfyValue(county.seed, county.troops);
                result.money = this.fuzzyfyValue(county.seed, county.money);
                result.tech = this.fuzzyfyValue(county.seed, county.tech);
                delete result.troopRate;
                delete result.moneyRate;
                delete result.techRate;
            } else {
                result.infoLevel = 3;
                result.isFriendly = false;
                delete result.owningCharacter;
                delete result.troops;
                delete result.troopRate;
                delete result.money;
                delete result.moneyRate;
                delete result.tech;
                delete result.techRate;
            }

            delete result.seed;
            return result;
        });

        state.armies = this.state.armies
            .filter(army => {
                return countyVisibility[army.location];
            })
            .map(army =>
        {
            let result = Object.assign({}, army);
            let owningCharacter = this.getCharacter(army.owningCharacter);
            let isOwner = (playerCharacterIds.indexOf(army.owningCharacter) != -1);
            let isOurHouse = (playerHouses.indexOf(owningCharacter.house) != -1);
            let isFriendly = (friendlyCharacterIds.indexOf(army.owningCharacter) != -1);
            result.isOwner = isOwner;
            result.cost = this.getArmyCost(army.guid);
            if (isOwner || isOurHouse || isFriendly) {
                result.infoLevel = 1;
                result.isFriendly = true;
            } else {
                result.infoLevel = 2;
                result.troops = this.fuzzyfyValue(army.seed, army.troops);
                result.isFriendly = false;
                delete result.orders;
            }

            delete result.seed;
            return result;
        });

        state.items = this.state.items.filter(item => item.owningCharacter == null);
        state.characters = this.state.characters.map(character => {
            let result = Object.assign({}, character);
            delete result.lifespan;

            let isOwner = (character.owningPlayer == player.username);
            let isOurHouse = (playerHouses.indexOf(character.house) != -1);
            let isVisible = countyVisibility[character.location];
            let isFriendly = (friendlyCharacterIds.indexOf(character.guid) != -1);
            let isAtOurLocation = (ourCharacterLocations.indexOf(character.location) != -1);
            let heir = this.getCharacterHeir(character.guid);
            result.isOwner = isOwner;
            result.isVisible = isVisible;
            delete result.owningPlayer;

            if (isAtOurLocation || isOurHouse || isFriendly || isOwner) {
                // High priority information.
                result.infoLevel = 1;
                result.isFriendly = true;
                state.items.push(...this.state.items.filter(item => {
                    return item.owningCharacter == character.guid;
                }));
                result.imprisonPlotting = this.getGameSettingObject(character.guid, 'plotting/imprison');
                result.murderPlotting = this.getGameSettingObject(character.guid, 'plotting/murder');
                result.stealPlotting = this.getGameSettingObject(character.guid, 'plotting/steal');
                result.sabotagePlotting = this.getGameSettingObject(character.guid, 'plotting/sabotage');
                result.blueprintCount = character.research.length - 1;
                result.focusCooldownDuration = this.getGameSettingValue(character, "focusCooldown");
                result.heir = heir ? heir.guid : null;
            } else if(isVisible) {
                // Medium priority information.
                result.infoLevel = 2;
                result.isFriendly = false;
                result.money = this.fuzzyfyValue(character.seed, character.money);
                result.totalTroops = this.fuzzyfyValue(character.seed, character.totalTroops);
                result.blueprintCount = character.research.length - 1;
                result.heir = heir ? heir.guid : null;
                state.items.push(...this.state.items.filter(item => {
                    return item.owningCharacter == character.guid && !item.active;
                }));
                delete result.moneyRate;
                delete result.moneyRateBreakdown;
                delete result.techRate;
                delete result.techRateBreakdown;
                delete result.troopRate;
                delete result.stats;
                delete result.desires;
                delete result.research;
                delete result.activeResearchIndex;
            } else {
                // Low priority information.
                result.infoLevel = 3;
                result.isFriendly = false;
                delete result.focus;
                delete result.money;
                delete result.moneyRate;
                delete result.moneyRateBreakdown;
                delete result.totalTroops;
                delete result.troopRate;
                delete result.tech;
                delete result.techRate;
                delete result.techRateBreakdown;
                delete result.stats;
                delete result.desires;
                delete result.research;
                delete result.activeResearchIndex;
            }

            if (result.infoLevel <= 2) {
                result.attackMultiplier = Math.floor(this.getAttackMultiplier(character.guid) * 100) / 100;
                result.defendMultiplier = Math.floor(this.getDefendMultiplier(character.guid) * 100) / 100;
            }

            if (character.hidden) {
                delete result.location;
            }
            delete result.seed;

            return result;
        });

        state.messages = this.state.messages.filter(m => {
            let toCharacter = this.getCharacter(m.to);
            let fromCharacter = this.getCharacter(m.from);
            return toCharacter != undefined && fromCharacter != undefined &&
                (toCharacter.owningPlayer == player.username) || (fromCharacter.owningPlayer == player.username);
        });

        state.news = this.state.news.filter(news => {
            let newsAge = (this.state.map.turn - news.turn);
            return newsAge <= 42;
        });
        state.map = this.state.map;
        state.config = this.state.config;
        state.glossary = {};
        state.glossary.items = TECH;

        return state;
    }

    generateDesiresForCharacter( character ) {
        character.desires = [];
        character.desires[0] = this.generateDesires(character, 1)[0];
        character.desires[1] = this.generateDesires(character, 1, true)[0];
        this.checkDesire(character, character.desires[0]);
        this.checkDesire(character, character.desires[1]);
    }

    generateDesires( character, count, checkExclusions ) {
        let possibleDesires = this.chance.shuffle(DESIRES.filter(desire => {
            if (checkExclusions) {
                return character.desires.find(otherDesire => {
                    if (!otherDesire) return false;
                    return desire.key == otherDesire.key
                        || (otherDesire.exclude && otherDesire.exclude.indexOf(desire.key) != -1);
                }) == null;
            } else {
                return true;
            }
        }));
        let desires = [];
        for (let i = 0; i < count; i++) {
            let desire = Object.assign({}, possibleDesires[i]);
            if (!desire.generateTarget || this.generateDesireTarget(character, desire)) {
                desires.push(desire);
            }
        }
        return desires;
    }

    generateDesireTarget( character, desire ) {
        var candidates = this.state.characters.filter(c => c.alive && c.guid != character.guid);
        desire.target = null;
        switch(desire.generateTarget) {
            case 'character':
                if (desire.key == 'swear_fealty') {
                    candidates = candidates.filter(c => character.overlord != c.guid);    
                } else if (desire.key == 'marryTarget') {
                    candidates = candidates.filter(c => this.canCharactersMarry(character, c));
                } else if (desire.key == 'captureTarget') {
                    candidates = candidates.filter(c => c.imprisoner != character.guid);
                }
                if (candidates.length > 0) {
                    desire.target = this.chance.pickone(candidates).guid;
                }
                break;
            case 'county':
                candidates = this.state.counties.filter(c => c.owningCharacter != character.guid && !c.impassable);
                if (candidates.length > 0) {
                    desire.target = this.chance.pickone(candidates).guid;
                }
                break;
            case 'item':
                let itemTarget = this.chance.pickone(TECH.slice(1));
                desire.target = {key: itemTarget.key, name: itemTarget.name, description: itemTarget.description};
                break;
        }
        return (desire.target != null);
    }

    acceptDesire( player, messageId, desireIndex ) {
        let message = this.state.messages.find(m => m.guid == messageId);
        let character = this.getCharacter(message.to);
        if (player == null || player.username == character.owningPlayer) {
            character.desires[1] = Object.assign({}, message.miscData.data[desireIndex]);
            message.miscData.acceptedIndex = desireIndex;
        }
    }

    checkDesire( character, desire ) {
        if (!desire) return;
        let satisfiedDesire = false;
        let counties = this.getCharacterCounties(character.guid);
        if (!desire.target && desire.generateTarget) {
            let success = this.generateDesireTarget(character, desire);
            if (!success) {
                console.log("Warning: could not generate target for desire - " + desire.key);
                return;
            }
        }
        // check if we have a character target and that target is now dead.
        if (desire.target && desire.generateTarget == 'character') {
            let targetCharacter = this.getCharacter(desire.target);
            if (!targetCharacter.alive) {
                let success = this.generateDesireTarget(character, desire);
                if (success) {
                    this.sendMessage(character.guid, character.guid, `
                    Your target for the ${desire.name} desire, ${chLink(targetCharacter)}, has died.
                    A new target has been found.
                    `);
                } else {
                    console.log("Warning: could not generate target for desire - " + desire.key);
                    return;
                }
            }
        }
        switch (desire.key) {
            case 'land':
                satisfiedDesire = (counties.length >= desire.target);
                break;
            case 'promiseland':
                satisfiedDesire = (counties.findIndex(c => c.guid == desire.target) !== -1);
                break;
            case 'wealth':
                satisfiedDesire = (character.money >= desire.target);
                break;
            case 'army':
                let armies = this.state.armies.filter(army => {
                    return army.owningCharacter == character.guid && army.troops >= desire.target;
                });
                satisfiedDesire = (armies.length >= desire.secondaryTarget);
                break;
            case 'armyReserve':
                let troops = this.state.counties.reduce((sum, county) => {
                    if (county.owningCharacter == character.guid) {
                        sum += county.troops;
                    }
                }, 0);
                satisfiedDesire = (troops >= desire.target);
                break;
            case 'overlord':
                let underlings = this.getCharacterUnderlords(character.guid);
                satisfiedDesire = (underlings.length >= desire.target);
                break;
            case 'overlordTarget':
                satisfiedDesire = this.getCharacterUnderlords(character.guid)
                    .findIndex(other => other.guid == desire.target) != -1;
                break;
            case 'marryAnyone':
                satisfiedDesire = (character.spouse != null);
                break;
            case 'marryTarget':
                satisfiedDesire = (character.spouse == desire.target);
                break;
            case 'generosity':
                satisfiedDesire = (character.stats.moneyGifted >= desire.target);
                break;
            case 'science':
                satisfiedDesire = character.research.length >= desire.target;
                break;
            case 'social':
                satisfiedDesire =  (character.stats.messagesReceived >= desire.target);
                break;
            case 'hermit':
                // Begin tracking messages.
                if (!desire.state) {
                    desire.state = {
                        messagesReceived: 0,
                        messagesReceivedLastTurn: 0,
                        startTurn: this.state.map.turn                        
                    }
                }
                // How many messages have we received since last turn?
                desire.state.messagesReceived += (character.stats.messagesReceived - desire.state.messagesReceivedLastTurn);
                desire.state.messagesReceivedLastTurn = character.stats.messagesReceived;
                // If the character has gone over the message limit, reset the tracker.
                if (desire.state.messagesReceived >= desire.target) {
                    desire.state.startTurn = this.state.map.turn;
                    desire.state.messagesReceived = 0;
                } else if ((this.state.map.turn - desire.state.startTurn) >= desire.secondaryTarget) {
                    // The target number of turns has elapsed, character satisfied the desire!
                    satisfiedDesire = true;
                    // reset the tracker.
                    desire.state.startTurn = this.state.map.turn;
                    desire.state.messagesReceived = 0;
                }
                break;
            case 'family':
                var children = this.getCharacterChildren(character.guid);
                satisfiedDesire = (children.length >= desire.target);
                break;
            case 'little_family':
                satisfiedDesire = (character.stats.childrenMade == 2 && !character.alive);
                break;
            case 'isolate':
                satisfiedDesire = (character.stats.childrenMade == 0 && !character.alive);
                break;
            case 'warmonger':
                satisfiedDesire = (character.stats.troopsAttacked >= desire.target);
                break;
            case 'pacifist':
                let turnsAtPeace = (this.state.map.turn - character.stats.lastTurnAttackOrDefended);
                satisfiedDesire = (turnsAtPeace >= desire.target);
                break;
            case 'murderTarget':
                if (character.stats.charactersKilled) {
                    satisfiedDesire = (character.stats.charactersKilled.indexOf(desire.target) != -1);
                } else {
                    satisfiedDesire = false;
                }
                break;
            case 'sabotage':
                if (character.stats.charactersSabotaged) {
                    let sabotageIndex = character.stats.charactersSabotaged.indexOf(desire.target);
                    satisfiedDesire = (sabotageIndex != -1);
                    if (sabotageIndex != -1) {
                        character.stats.charactersSabotaged.splice(sabotageIndex, 1);
                    }
                } else {
                    satisfiedDesire = false;
                }
                break;
            case 'steal':
                if (character.stats.charactersStolenFrom) {
                    let stealIndex = character.stats.charactersStolenFrom.indexOf(desire.target);
                    satisfiedDesire = (stealIndex != -1);
                    if (stealIndex != -1) {
                        character.stats.charactersStolenFrom.splice(stealIndex, 1);
                    }
                } else {
                    satisfiedDesire = false;
                }
                break;
            case 'capture':
                satisfiedDesire = (this.state.characters.find(c => c.imprisoner == character.guid) != null);
                break;
            case 'captureTarget':
                satisfiedDesire = (this.getCharacter(desire.target).imprisoner == character.guid);
                break;
            case 'possess':
                satisfiedDesire = desire.target && this.state.items.filter(item => 
                    item.owningCharacter == character.guid && item.key == desire.target.key).length != 0;
                break;
            case 'swear_fealty':
                satisfiedDesire = (character.overlord == desire.target);
                break;
            case 'anarchy':
                let anarchyItems = this.state.items.filter(item => {
                    return item.owningCharacter == character.guid && item.isAnarchy;
                });
                satisfiedDesire = (anarchyItems.length >= desire.target);
                break;
            case 'harmony':
                let harmonyItems = this.state.items.filter(item => {
                    return item.owningCharacter == character.guid && item.isHarmony;
                });
                satisfiedDesire = (harmonyItems >= desire.target);
                break;
        }
        if (satisfiedDesire) {
            let player = this.getPlayer(character.owningPlayer);
            if (player) {
                player.score += desire.score;
            }
            if (desire.upgrades && desire.upgrades.length > 0) {
                let messageText = `I have satisfied my ${desire.name} desire. You gain ${desire.score} points. My desire has upgraded.`;
                this.sendMessage(character.guid, character.guid, messageText);
                let upgrade = desire.upgrades.shift();
                Object.keys(upgrade).forEach(key => {
                    desire[key] = upgrade[key];
                });
            } else {
                let desireIndex = character.desires.findIndex(d => d.key == desire.key);
                if (desireIndex == 0) {
                    character.desires[0] = character.desires[1];
                }
                character.desires.splice(1,1);
                if (player) {
                    let desireOptions = this.generateDesires(character, 2, true);
                    let messageText = `I have satisfied my ${desire.name} desire. You gain ${desire.score} points. Select my next desire:`;
                    this.sendMessage(character.guid, character.guid, messageText, null, null, {
                        type: 'desire_options',
                        data: desireOptions
                    });
                } else {
                    character.desires[1] = this.generateDesires(character, 1, true);
                }
            }
        }
    }

    safePick( arr ) {
        if (arr.length > 0) {
            return this.chance.pickone(arr);
        } else {
            return null;
        }
    }
}

Game.loadFromState = function( state ) {
    return new Game(null, state);
}

module.exports = Game;