module.exports = function( update, doc ) {
    update({$set: {
        "config.NAME": "untitled game", 
        "config.MAX_TURNS": 336,
        "config.BOTS": true,
        "config.AUTO_BOT_IDLE_PLAYERS": false
    }});
}