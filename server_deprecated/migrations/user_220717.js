module.exports = function( update, doc ) {
    if (doc.username == 'perky') {
        update({$set: {
            "roles": ['admin']
        }});
    } else {
        update({$set: {
            "roles": []
        }});
    }
}