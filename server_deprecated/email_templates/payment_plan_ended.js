module.exports = function({username, planName, planCost}) {
    return {
        subject: `Your ${planName} plan has ended.`,
        message:
    
`Dear ${username},
Your ${planName} plan for <a href="https://kittenkingdoms.com">KittenKingdoms.com</a> has ended and we have put you back on the free plan.
With the free plan you can still join up to 2 public or private games simultaneously.
We are sorry to see you cancel, if you have any feedback please email me to <a href="mailto:luke@locogame.co.uk>luke@locogame.co.uk</a>.

Best Regards,
Luke Perkin
Locogame Ltd.
`,

        altText:

`Dear ${username},
Your ${planName} plan for KittenKingdoms.com has ended and we have put you back on the free plan.
With the free plan you can still join up to 2 public or private games simultaneously.
We are sorry to see you cancel, if you have any feedback please email me to luke@locogame.co.uk.

Best Regards,
Luke Perkin
Locogame Ltd.`
    }
}