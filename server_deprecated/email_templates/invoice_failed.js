module.exports = function({username, planName, planCost, cardEnd}) {
    let mail = {
        subject: `Your ${planName} payment has failed.`,
        message:
    
`Dear ${username},
We tried to charge your card ending ${cardEnd} ${charge} and the payment failed.
Please log-in and update your payment details otherwise your ${planName} will be cancelled in a few days time.

Best Regards,
Luke Perkin
Locogame Ltd.
`,
    };
    mail.altText = mail.message;
    return mail;
}