module.exports = function({username, charge, planName, cardEnd}) {
    let mail = {
        subject: `Your ${planName} plan has been renewed.`,
        message:
    
`Dear ${username},
We have charged your card ending ${cardEnd} ${charge}.
You are currently on the ${planName} plan.

Best Regards,
Luke Perkin
Locogame Ltd.
`,
    };
    mail.altText = mail.message;
    return mail;
}