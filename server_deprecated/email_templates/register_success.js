module.exports = function({username, discordInviteUrl}) {
    return {
        subject: 'Welcome to Kitten Kingdoms.',
        message:
    `Thank you for registering to <a href="https://kittenkingdoms.com">Kitten Kingdoms</a>.
    <br>
    Your username is: ${username}<br>
    You can now log in to your account and start your kitten dynasty!
    <br>
    You are also invited to join our official Discord Guild! <br>
    <a href="${discordInviteUrl}">${discordInviteUrl}</a>
    `,
        altText: `Thank you for registering to www.kittenkingdoms.com. Your username is ${username}.
        You can now log in to your account and start your kitten dynasty!
        You are also invited to join our official Discord Guild! ${discordInviteUrl} (invite lasts for 24 hours)`
    }
}