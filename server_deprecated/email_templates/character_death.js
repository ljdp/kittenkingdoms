module.exports = function({userToken, gameId, gameName, turn, catName, age, heirName}) {
    let heirMessage = (heirName) ? `${heirName} has received the inheritance.` : `There was no heir!`;
    return {
        subject: `(Kitten Kingdoms) ${catName} has died`,
        message:
    `Your kitten, ${catName}, has died on turn ${turn} at the age of ${age}. ${heirMessage}
    <br><br>
    <a href="https://kittenkingdoms.com/game.html?userToken=${userToken}&gameId=${gameId}">Go to game</a>.
    `,


        altText:
    `Your kitten, ${catName}, has died on turn ${turn} at the age of ${age}. ${heirMessage}`
    }
}