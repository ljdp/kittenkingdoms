module.exports = function({gameName, turn, players}) {
    let playerScores = "<ol>";
    players.forEach(player => {
        playerScores += `<li>${player.username}: ${player.score}</li>`;
    });
    playerScores += "</ol>";
    return {
        subject: `Game ${gameName} has finished`,
        message:
    `The kitten kingdoms game, ${gameName}, has finished on turn ${turn}. Here are the final scores:<br>
     ${playerScores}
     <br>
     Had fun? <a href="https://kittenkingdoms.com">Play another game!</a>
    `,
        altText:
    `The kitten kingdoms game, ${gameName}, has finished on turn ${turn}. Log-in to view the scores.`
    }
}