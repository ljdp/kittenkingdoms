module.exports = function({username, planName, planCost, cardEnd}) {
    let mail = {
        subject: `Your ${planName} plan has started!`,
        message:
    
`Dear ${username},
We have charged your card ending ${cardEnd} £${planCost/100}.
Your ${planName} plan for <a href="https://kittenkingdoms.com">KittenKingdoms.com</a> has began!
With the ${planName} plan you can rename your kittens and counties (customizing their appearance is coming soon).
You can join up to 6 public or private games simultaneously, create your own public and private games, 
play without ads and earn my eternal thanks!
I am but a lone developer and your support means a lot to me, thank you.

Your plan will auto-renew for £${planCost/100} every month. You can disable this from your <a href="https://kittenkingdoms.com/lobby">profile panel</a>.

If you have any feedback please email me to <a href="mailto:luke@locogame.co.uk>luke@locogame.co.uk</a>.

Best Regards,
Luke Perkin
Locogame Ltd.
`,
    };
    mail.altText = mail.message;
    return mail;
}