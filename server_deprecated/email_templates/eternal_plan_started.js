module.exports = function({username, planName, planCost, cardEnd}) {
    let mail = {
        subject: `Your ${planName} plan has started!`,
        message:
    
`Dear ${username},
We have charged your card ending ${cardEnd} £${planCost/100}.
Your ${planName} plan for <a href="https://kittenkingdoms.com">KittenKingdoms.com</a> has began!
With the ${planName} plan you can rename your kittens and counties (customizing their appearance is coming soon).
You can join up to 12 public or private games simultaneously, create your own public and private games, 
play without ads and earn my eternal thanks!

This plan is a one time cost, the features above, any any future updates, will be available to you forever*.

If you have any feedback please email me to <a href="mailto:luke@locogame.co.uk>luke@locogame.co.uk</a>.

Best Regards,
Luke Perkin
Locogame Ltd.

* If the servers shut down I will open source both the server and client code for Kitten Kingdoms.
`,
    };
    mail.altText = mail.message;
    return mail;
}