module.exports = function({gameId, gameName, turn, userToken}) {
    return {
        subject: `You received income for game: ${gameName}`,
        message:
    `Turn ${turn} has passed in ${gameName} and you have received income.<br>
    <a href="https://kittenkingdoms.com/game.html?userToken=${userToken}&gameId=${gameId}">See what you have earned</a>.
    `,
        altText: `Turn ${turn} has passed in ${gameName} and you have received income.`
    }
}