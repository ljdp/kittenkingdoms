module.exports = function({catName, catId, senderName, message, userToken, gameId}) {
    return {
        subject: 'New message received',
        message:
    `${catName} has received a new message from ${senderName}:
    <br><br>
    <p><em>${message}</em></p>
    <br>
    <a href="https://kittenkingdoms.com/game.html?userToken=${userToken}&gameId=${gameId}&activeCat=${catId}&showMessages=1">View the full message</a>.
    `,
        altText: `${catName} has received a new message from ${senderName}. Log-in to view.`
    }
}