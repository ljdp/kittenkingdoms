module.exports = function({username}) {
    return {
        subject: 'Welcome to Kitten Kingdoms.',
        message:
    `Thank you for registering to <a href="https://kittenkingdoms.com">Kitten Kingdoms</a>.
    <br>
    Your username is: ${username}<br>
    You can now log in to your account and start your kitten dynasty!
    `,
        altText: `Thank you for registering to www.kittenkingdoms.com. Your username is ${username}.
        You can now log in to your account and start your kitten dynasty!`
    }
}