const jsonpatch = require('fast-json-patch');
const Datastore = require('nedb');
const escape = require('escape-html');
const Chance = require('chance');
const chance = new Chance();

module.exports = class GameHandler {
    constructor( game, {server, userDb, mailer, discordBot} ) {
        this.game = game;
        this.server = server;
        this.userDb = userDb;
        this.cachedStateForPlayer = {};
        this.lastTurnTime = Date.now();
        this.mailer = mailer;
        this.discordBot = discordBot;
        game.notificationsHandler = this;
    }

    startUpdate( minPreInterval, maxPreInterval, interval, callback ) {
        setTimeout(() => {
            this.updateInterval = setInterval(() => {
                this.tick();
                this.sendStateUpdateToAllPlayers();
                callback(this);
            }, interval);
        }, chance.integer({min: minPreInterval, max: maxPreInterval}));
    }

    stopUpdate() {
        clearInterval(this.updateInterval);
        this.stopped = true;
        this.updateInterval = null;
    }

    nextIncomeTime() {
        let turnsUntilIncome = this.game.config.INCOME_TURN_INTERVAL - (this.turn() % this.game.config.INCOME_TURN_INTERVAL);
        return this.lastTurnTime + (1000 * 60 * 60 * turnsUntilIncome);
    }

    tick() {
        if (!this.isGameOver()) {
            this.game.peformGameTick();
            this.lastTurnTime = Date.now();
            console.log(`Completed turn ${this.turn()} for game ${this.guid()}.`);
            if (this.isIncomeTurn()) {
                console.log(`Completed income turn ${this.turn()} for game ${this.guid()}.`);
                this.game.state.players.forEach(player => {
                    this.notify(player.username, {
                        template: 'income_turn',
                        params: {}
                    });
                });
            }
            if (this.isGameOver()) {
                console.log(`Finished game ${this.guid()} on turn ${this.turn()}.`);
                this.gameFinishedTime = Date.now();
                this.discordBot.sendGameFinishedMessage(this);
                this.game.state.players.forEach(player => {
                    this.notify(player.username, {
                        template: 'game_end',
                        params: {}
                    });
                });
            }
        }
        if (this.isGameOver() && this.updateInterval) {
            this.stopUpdate();
        }
    }

    guid() {
        return this.game.state.guid;
    }

    turn() {
        return this.game.state.map.turn;
    }

    isIncomeTurn() {
        return (this.turn() % this.game.config.INCOME_TURN_INTERVAL) == 0;
    }

    state() {
        return this.game.state;
    }

    grace() {
        return this.game.state.grace;
    }

    name() {
        return this.game.config.NAME;
    }

    setGrace( value ) {
        this.game.state.grace = value;
    }

    isGameFull() {
        return this.game.state.players.length >= this.game.state.config.MAX_PLAYERS;
    }

    isGameOver() {
        return this.turn() >= this.game.config.MAX_TURNS || this.stopped;
    }

    isGamePrivate() {
        return false;
    }

    getStateForPlayer( player ) {
        return this.game.getStateForPlayer(player);
    }

    sendStateUpdateToAllPlayers() {
        this.server.clients.forEach(client => {
            this.sendStateUpdateToClient(client);
        });
    }

    sendStateUpdateToClient( client ) {
        if (client && client.player && client.gameHandler == this) {
            let state = this.getStateForPlayer(client.player);
            state.nextIncomeTime = new Date(this.nextIncomeTime());
            let cachedState = this.cachedStateForPlayer[client.player.username];
            if (cachedState) {
                let patch = jsonpatch.compare(cachedState, state);
                let message = {
                    type: 'STATE_PATCH',
                    patch: patch
                }
                client.send(JSON.stringify(message));
            } else {
                let message = {
                    type: 'STATE_FULL',
                    state: state
                }
                client.send(JSON.stringify(message));
            }
            this.cachedStateForPlayer[client.player.username] = jsonpatch.deepClone(state);
        }
    }

    sendStateUpdateForCharacter( characterId ) {
        let character = this.game.getCharacter(characterId);
        if (character.owningPlayer) {
            let clients = [...this.server.clients];
            let client = clients.find(client => client.player && client.player.username == character.owningPlayer);
            this.sendStateUpdateToClient(client);
        }
    }

    sendMessage( client, type, data ) {
        let msg = Object.assign({
            type: type
        }, data);
        client.send(JSON.stringify(msg));
    }

    getPlayer( user ) {
        let player = this.game.getPlayer(user.username);
        return player;
    }

    getPlayers() {
        let players = this.game.state.players.slice(0);
        players.sort((a, b) => b.score - a.score);
        return players;
    }

    addPlayer( user ) {
        if (!this.isGameFull()) {
            console.log('adding user ' + user.username);
            return this.game.makePlayer(user.username);
        }
        return false;
    }

    removePlayer( user ) {
        let player = this.getPlayer(user);
        if (player) {
            return this.game.removePlayer(player.username);
        } else {
            return false;
        }
    }

    connectClient( client, user ) {
        let player = this.getPlayer(user);
        if (player) {
            console.log(`Connecting ${user.username} to game ${this.guid()}`);
            client.player = player;
            client.gameHandler = this;
            this.cachedStateForPlayer[player.username] = null;
            this.sendStateUpdateToClient(client);
            return true;
        } else {
            return false;
        }
    }

    disconnectClient( client ) {
        if (client.player && client.gameHandler == this) {
            this.cachedStateForPlayer[client.player.username] = null;
            client.player = null;
            client.gameHandler = null;
        } else {
            return false;
        }
    }

    notify( username, mail ) {
        this.userDb.findUser(username).then(user => {
            if (user.emailNotify) {
                mail.to = user.email;
                mail.params.userToken = this.userDb.getToken(user);
                mail.params.gameId = this.guid();
                mail.params.gameName = this.name();
                mail.params.turn = this.turn();
                this.mailer.sendMail(mail);
            }
        });
    }

    handleMessage( msg, player, client ) {
        if (this.isGameOver()) return;
        let game = this.game;
        switch(msg.type) {
            case 'REFRESH':
                this.sendStateUpdateToClient(client);
                break;
            case 'RAISE_TROOPS':
                game.makeArmy(player, msg.countyId, msg.troops);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'MOVE_ARMY':
                game.moveArmyWithOrders(player, msg.armyId, msg.orders);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'STAND_DOWN_ARMY':
                game.standDownArmy(player, msg.armyId);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'SET_FOCUS':
                game.setCharacterFocus(player, msg.characterId, msg.focus);
                break;
            case 'SEND_MESSAGE':
                var messageText = escape(msg.message);
                game.sendMessage(msg.fromCharacterId, msg.toCharacterId, messageText, msg.proposals, msg.proposalAcceptType);
                this.sendStateUpdateForCharacter(msg.fromCharacterId);
                this.sendStateUpdateForCharacter(msg.toCharacterId);
                break;
            case 'MARK_MESSAGE_READ':
                game.markMessageRead(msg.messageId);
                break;
            case 'SEND_PROPOSAL_RESPONSE':
                let message = game.respondToProposal(player, msg.messageId, msg.proposalIndex, msg.response);
                if (message) {
                    this.sendStateUpdateForCharacter(message.from);
                    this.sendStateUpdateForCharacter(message.to);
                }
                break;
            case 'CHANGE_RESEARCH':
                game.changeCharacterResearch(player, msg.characterId, msg.researchIndex);
                this.sendStateUpdateToClient(client);
                break;
            case 'ACTIVATE_ITEM':
                game.activateItem(player, msg.itemId);
                this.sendStateUpdateToClient(client);
                break;
            case 'MOVE_CHARACTER':
                game.moveCharacterToLocation(player, msg.characterId, msg.countyId);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'BREAK_FEALTY':
                game.breakFealty(player, msg.characterId);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'ACCEPT_DESIRE':
                game.acceptDesire(player, msg.messageId, msg.desireIndex);
                this.sendStateUpdateToClient(client);
                break;
            case 'RENAME_COUNTY':
                game.renameCounty(player, msg.countyId, msg.name);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'RENAME_CHARACTER':
                game.renameCharacter(player, msg.characterId, msg.name);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'START_PLOT':
                game.performPlot(player, msg.characterId, msg.targetId, msg.plotType);
                this.sendStateUpdateToAllPlayers();
                break;
            case 'POST_NEWS':
                var messageText = escape(msg.message);
                game.addNews(msg.characterId, messageText);
                if (msg.denounceTarget) {
                    game.denounce(player, msg.characterId, msg.denounceTarget);
                }
                this.sendStateUpdateToAllPlayers();
                break;
            case 'READ_NEWS':
                player.lastReadNewsIndex = msg.newsIndex;
                break;
        }
    }
}