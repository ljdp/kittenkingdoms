var voronoi = d3.voronoi();
var diagram;
var availablePatterns = [];
var characterPics = {};
var didGenerateMap = false;

view.zoom = 5;
// view.position = new Point(600, 300);

var regionGroups = [];
for (var i = 0; i < 3; i++) {
    regionGroups.push(new Group());
}

var mapBackLayer = new Layer();
var mapFrontLayer = new Layer();
var overlayLayer = new Layer();

function generateMap( gameState ) {
    var counties = gameState.counties;
    // var sites = counties.map(function(county) {
    //     var site = [county.x, county.y];
    //     site.county = county;
    //     return site;
    // });

    var sites = [];
    counties.forEach(function(county) {
        for (var i = 0; i < 4; i++) {
            var x = county.x + (Math.sin(Math.random()) * county.size * 0.1);
            var y = county.y + (Math.cos(Math.random()) * county.size * 0.1);
            var site = [x, y];
            site.county = county;
            sites.push(county);
        }
    });

    voronoi.extent([[0, 0], [gameState.map.width, gameState.map.height]]);
    diagram = voronoi(sites);
    console.log(diagram.cells.length, counties.length);

    mapBackLayer.activate();
    var clipWater = generatePattern(waterPattern);
    for (var i = 0; i < patterns.length; i++) {
        availablePatterns.push(generatePattern(patterns[i]));
    }
    

    var polygons_d3 = diagram.polygons();
    var polygons = polygons_d3.map(function(polygon) {
        return polygon.filter(function(v) { return v !== null; }).map(function(v) {
            return new Point(v[0], v[1]);
        });
    });
    for (var i = 0; i < polygons.length; i++) {
        var polygon = polygons[i];
        if (!polygon) continue;

        var cellPath = new Path();
        cellPath.closed = true;

        var site = diagram.cells[i].site;

        for (var p = 0; p < polygon.length; p++) {
            var point = polygon[p];
            cellPath.add(point);
        }
        var centroid = d3.polygonCentroid(polygons_d3[i]);
        site.data.path = cellPath;
        site.data.centroid = centroid;

        if (site.data.county.impassable) {
            var pathClone = cellPath.clone({deep: true});
            pathClone.fillColor = 'black';
            clipWater.addChild(pathClone);
        }

        if (!site.data.county.impassable) {
            var label = new PointText({
                point: centroid,
                content: site.data.county.name,
                fontSize: 3,
                justification: 'center'
            });
            site.data.label = label;
        } else {
            cellPath.scale(0.2);
        }
    }

    didGenerateMap = true;
}

function updateCells( gameState ) {
    for (var i = 0; i < availablePatterns.length; i++) {
        availablePatterns[i].removeChildren();
    }

    for (var i = 0; i < diagram.cells.length; i++) {
        var cell = diagram.cells[i];
        cell.site.data.county = gameState.counties[i];
        if (cell.site.data.label) {
            cell.site.data.label.content = gameState.counties[i].name;
        }
    }

    var aliveCharacters = gameState.characters.filter(function(c) {
        return c.alive;
    });

    var patternForHouse = {};
    var nextAvailablePatternIndex = 0;

    for (var i = 0; i < aliveCharacters.length; i++) {
        var character = aliveCharacters[i];
        if (!patternForHouse[character.house]) {
            patternForHouse[character.house] = availablePatterns[nextAvailablePatternIndex++];
            if (nextAvailablePatternIndex >= availablePatterns.length) {
                nextAvailablePatternIndex = 0;
            }
        }
        var counties = gameState.counties.filter(function(county) {
            return county.owningCharacter == character.guid;
        });
        for (var c = 0; c < counties.length; c++) {
            var county = counties[c];
            var cell = diagram.cells.find(function(cell) { return cell.site.data.county.guid == county.guid; });
            var path = cell.site.data.path.clone();
            path.fillColor = "black";
            patternForHouse[character.house].addChild(path);
        }
    }

    mapFrontLayer.removeChildren();
    mapFrontLayer.activate();

    var characterMap = {};
    for (var i = 0; i < gameState.characters.length; i++) {
        var character = gameState.characters[i];
        characterMap[character.guid] = character;
    }

    function isFriendly( left, right ) {
        return left.isFriendly && right.isFriendly;
    }

    for (var c = 0; c < gameState.counties.length; c++) {
        var county = gameState.counties[c];
        var cell = diagram.cells.find(function(cell) { return cell.site.data.county.guid == county.guid; });
        for (var e = 0; e < cell.halfedges.length; e++) {
            var edge = diagram.edges[cell.halfedges[e]];
            var strokeColor = new Color(0, 0, 0);
            if (edge.left && edge.right) {
                var leftCounty = edge.left.data.county;
                var rightCounty = edge.right.data.county;
                if (leftCounty.owningCharacter && rightCounty.owningCharacter) {
                    var leftCharacter = characterMap[leftCounty.owningCharacter];
                    var rightCharacter = characterMap[rightCounty.owningCharacter];
                    if (leftCharacter.guid == rightCharacter.guid) {
                        strokeColor = new Color(0, 0, 0, 0.1);
                    } else if (isFriendly(leftCharacter, rightCharacter)) {
                        strokeColor = new Color(0, 1, 0, 1);
                    } else {
                        strokeColor = new Color(1, 0, 0, 1);
                    }
                } else {
                    if (leftCounty.impassable || rightCounty.impassable) {
                        strokeColor = new Color(0, 0, 0, 0);
                    } else if (leftCounty.owningCharacter == null && rightCounty.owningCharacter == null) {
                        strokeColor = new Color(0, 0, 0, 0.2);
                    } else if (leftCounty.owningCharacter == null || rightCounty.owningCharacter == null) {
                        strokeColor = new Color(0, 0, 0, 1);
                    }
                }
            }
            var path = new Path.Line({
                from: edge[0],
                to: edge[1],
                strokeColor: strokeColor
            });
        }
    }

}

function updateOverlay( gameState ) {
    overlayLayer.removeChildren();
    overlayLayer.activate();

    var myCharacters = gameState.characters.filter(function(character) {
        return character.owningPlayer == gameState.player.username;
    });
    var myHouses = myCharacters.map(function(c) {
        return c.house;
    });

    for (var i = 0; i < gameState.armies.length; i++) {
        var army = gameState.armies[i];
        var county = gameState.counties.find(function(c) { return c.guid == army.location; });
        var armyDot = new Shape.Circle(new Point(county.x, county.y), 2);

        var armyOwner = gameState.characters.find(function(c) { return c.guid == army.owningCharacter; });
        var isOwner = (armyOwner && armyOwner.owningPlayer == gameState.player.username);
        var isFriendly = gameState.characters.find(function(c) {
            return myHouses.indexOf(c.house) != -1;
        });
        var isFriendly = myCharacters.findIndex(function(character) { return character.spouse == army.owningCharacter; }) != -1;
        if (isOwner || isFriendly) {
            armyDot.fillColor = (army.cooldown == 0) ? 'green' : 'gray';
        } else {
            armyDot.fillColor = 'red';
        }

        var armySize = new PointText(new Point(county.x, county.y));
        armySize.content = army.troops + '';
        armySize.fontSize = 5;
        armySize.fillColor = 'black';
    }

    var locationCounter = {};
    for (var i = 0; i < gameState.counties.length; i++) {
        locationCounter[gameState.counties[i].guid] = 0;
    }

    var aliveCharacters = gameState.characters.filter(function(c) {
        return c.alive;
    });
    for (var i = 0; i < aliveCharacters.length; i++) {
        var character = aliveCharacters[i];
        if (!character.location) continue;

        var countyGuid = character.location;
        locationCounter[countyGuid]++;

        var counties = gameState.counties.find(function(county) { return county.guid == countyGuid; });
        var cell = diagram.cells.find(function(cell) { return cell.site.data.county.guid == countyGuid; });
        if (cell) {
            if (!characterPics[character.guid]) {
                var profileUrl = "https://robohash.org/" + character.guid;
                var raster = new Raster(profileUrl);
                raster.scale(0.05);
                characterPics[character.guid] = raster;
            } 
            if (characterPics[character.guid]) {
                var raster = characterPics[character.guid];
                var xOffset = (locationCounter[countyGuid] - 1) * 3;
                raster.position = new Point(
                    cell.site.data.centroid[0] + xOffset, 
                    cell.site.data.centroid[1] - 10);
                overlayLayer.addChild(raster);
            }
        }
    }
}

var dragStart = new Point(0, 0);
var mouseDidMove = false;
var selectedCountySite = null;


function onMouseDown( event ) {
    if (!didGenerateMap) return;
    dragStart = event.point;
    mouseDidMove = false;
}

function onMouseUp( event ) {
    if (!didGenerateMap) return;
    var mousePos = event.point;
    if (!mouseDidMove) {
        var foundSite = diagram.find(mousePos.x, mousePos.y, 200);
        if (foundSite !== null && !foundSite.data.county.impassable) {
            var guid = foundSite.data.county.guid;
            selectedCountySite = foundSite;
            foundSite.data.path.fillColor = new Color(0.9, 0.9, 0, 0.5);
            window.app.selectCounty(guid);
            onMouseMove(event);
        }
    }
}

function onMouseDrag(event) {
    if (!didGenerateMap) return;
    mouseDidMove = true;
    var trueDelta = event.point - dragStart;
    view.translate(trueDelta);
    onMouseMove(event);
}

function onMouseMove( event ) {
    if (!didGenerateMap) return;
    if (!diagram) return;
    var mousePos = event.point;

    for (var i = 0; i < diagram.cells.length; i++) {
        var cell = diagram.cells[i];
        var data = cell.site.data;
        if (cell.site !== selectedCountySite) {
            
            if (data.county.impassable) {
                data.path.fillColor = new Color(0, 0, 0);
            } else if (data.county.isVisible) {
                data.path.fillColor = new Color(1, 1, 1, 0.3);
            } else {
                data.path.fillColor = new Color(0, 0, 0, 0.45);
            }
        }
    }

    if (selectedCountySite) {
        var adjacent = GetAdjacentCells(selectedCountySite.index);
        for (var i = 0; i < adjacent.length; i++) {
            adjacent[i].site.data.path.fillColor = new Color(0, 0.3, 0.9, 0.5);
        }
    }

    var foundSite = diagram.find(mousePos.x, mousePos.y, 200);
    if (foundSite !== null && !foundSite.data.county.impassable) {
        foundSite.data.path.fillColor = new Color(0.8, 0.8, 0, 0.5);
    }
}

function GetAdjacentCells( cellIndex ) {
    var leftEdges = diagram.edges.filter(function(edge) {
        return edge.left && edge.left.index == cellIndex;
    });
    var rightEdges = diagram.edges.filter(function(edge) {
        return edge.right && edge.right.index == cellIndex;
    });
    var adjacentCells = [];
    for (var i = 0; i < leftEdges.length; i++) {
        if (leftEdges[i].right) {
            adjacentCells.push(diagram.cells[leftEdges[i].right.index]);
        }
    }
    for (var i = 0; i < rightEdges.length; i++) {
        if (rightEdges[i].left) {
            adjacentCells.push(diagram.cells[rightEdges[i].left.index]);
        }
    }
    return adjacentCells.filter(function(c) { return c !== undefined && !c.site.data.county.impassable; });
}

$('#myCanvas').on('mousewheel', function(event) {
    if (!didGenerateMap) return;
    var delta = event.deltaY * event.deltaFactor * 0.0005;
    view.zoom += delta;
});

function panAndZoomTo( x, y ) {
    view.center = new Point(x, y);
    view.zoom = 5;
}

function generatePattern( source ) {
    var patternRaster = new Raster(source.pattern);
    var clip = new Group();
    var clipGroup = new Group(clip);
    clip.clipMask = true;
    var patternGroup = new Group();
    patternRaster.on('load', function() {
        var pattern = new Symbol(patternRaster);
        var w = Math.floor(600 / patternRaster.width);
        var h = Math.floor(600 / patternRaster.height);
        for (var x = 0; x < w; x++) {
            for (var y = 0; y < h; y++) {
                var platternInst = pattern.place(new Point(x * patternRaster.width, y * patternRaster.height));
                patternGroup.addChild(platternInst);
            }
        }
        var tintRect = new Shape.Rectangle(0, 0, 600, 600);
        tintRect.fillColor = source.tint;
        patternGroup.addChild(tintRect);
        var pgr = patternGroup.rasterize();
        patternGroup.remove();
        clipGroup.addChild(pgr);
    });
    return clip;
}


window.gameMap = {
    generateMap: generateMap,
    updateOverlay: updateOverlay,
    updateCells: updateCells,
    panAndZoomTo: panAndZoomTo
}

var waterPattern = {
    pattern: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAECAYAAACtBE5DAAAAAXNSR0IArs4c6QAAAElJREFUCB1jVHHY/J8BCJ48fcYgIy0FYoIBE4gECZbvCgfTEGEGBiaY4MYLJ1AkGb2P3v7/9NVdhhtlDxk0uuQZpMWUGUykRBkA5owgB/VlTO4AAAAASUVORK5CYII=",
    tint: new Color(1, 1, 1, 0)
}
var _patterns = [
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAEBJREFUGBljsDt1/j8IMxAATATk8Us/uGH3H4SRVVFmIrJJcHbFebv/IAwXwMGgzOpPM+P+gzCy4ZSZiGwSjA0AMx4Wd4lKJLMAAAAASUVORK5CYII=",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAExJREFUGBljYICCpkN+/2Fs48xr/7+/1UPhg+WopwhmFYjGaR1QDm4j9RSBrMTqO2TrgGyQjSC1YIDMgbsJKAMSf/ssCqKQeoqAJgMALCZpes/aA4kAAAAASUVORK5CYII=",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAAXNSR0IArs4c6QAAABlJREFUCB1jTKy85cOAC+CTZMKlCSQ+EJIA7PgEDMxd7PgAAAAASUVORK5CYII=",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAGtJREFUGBmF0AEKgCAMBdCtE4l0pYpO0W4R5Z3SG62+ETTLGgj++RwiRx+Vxk7IVpmJYmgnYKzkU2/9mQ6jzf1ASZcazpCJh+tCDWfoNrf+YUDBtB8s5o1f2MDaZPQf8A2jx/gjbIoSk+cgO2WxPQ3Zha6DAAAAAElFTkSuQmCC",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAICAYAAADA+m62AAAAAXNSR0IArs4c6QAAADFJREFUGBljYMAE/zGFMEVgimA0pgqgCLokOh+sCasgumZcimDWEpKHqcNrI4oi0jgA7L4J95hEPbMAAAAASUVORK5CYII=",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAFFJREFUGBmNjcENwCAMA11WYv8RuhO4StER8gAJkZxPRpL6vDzeK0ZnE7K8xCo4GIHnvOsJ+EYvBaPFWwjXD5vKr9lE+ecV+zIGnI/QwEKW+gDCdwb/LYgRmgAAAABJRU5ErkJggg==",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAAXNSR0IArs4c6QAAABJJREFUCB1jYGBg+A/ExAHsKgFV7gH/ssPq5wAAAABJRU5ErkJggg==",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAGCAYAAAAPDoR2AAAAAXNSR0IArs4c6QAAABdJREFUCB1jiP7O8J8BHyBJAU7F1JEAAMb3EDieN2sMAAAAAElFTkSuQmCC",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAC9JREFUGBlj+MrA8B+EGZAANjEmJPlBy8TmcGyOpcwzn2bG/QdhZJMpMxHZJBgbAGS1EY8ehdtiAAAAAElFTkSuQmCC",
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAAXNSR0IArs4c6QAAAGNJREFUGBljnLhg738GIFio/4Mh/iIHiMnwT2czA9MVXzAbRjAand8KVggTwEWzwEyhnomxf3eDbVvM7MoAY2OzngXuaKBnYGxsnmEBCUKAK9i3MNMQ4hARok1kgplAiCZaIQBATi12OuDryQAAAABJRU5ErkJggg=="
];
var patterns = [];
for (var i = 0; i < _patterns.length; i++) {
    patterns.push({
        pattern: _patterns[i],
        tint: new Color(0.3, 0.2, 1, 0.3)
    });
    patterns.push({
        pattern: _patterns[i],
        tint: new Color(1, 0.3, 0.2, 0.3)
    });
    patterns.push({
        pattern: _patterns[i],
        tint: new Color(0.3, 1, 0.2, 0.3)
    });
}
var shuffler = new window.Chance(837263);
patterns = shuffler.shuffle(patterns);