importScripts('../bower_components/paper/dist/paper-full.js');
importScripts('../node_modules/chance/dist/chance.min.js');
var chance = new Chance(1234);
paper.install(this);
paper.setup([640, 480]);

function makeDotPattern( color, width, height ) {
    let content = new paper.Group();
    for (let x = 0; x < width; x += 15) {
        for (let y = 0; y < height; y += 15) {
            let dot = new paper.Shape.Circle({
                center: [x, y],
                radius: 3,
                fillColor: color
            });
            content.addChild(dot);
        }
    }
    return content;
}

function makeDiamondPattern( color, width, height ) {
    let content = new paper.Group();
    for (let x = 0; x < width; x += 15) {
        for (let y = 0; y < height; y += 15) {
            let dot = new paper.Shape.Rectangle({
                point: [x, y],
                size: [8, 8],
                fillColor: color
            });
            dot.rotate(45);
            content.addChild(dot);
        }
    }
    return content;
}

function makeStripePattern( color, width, height ) {
    let content = new paper.Group();
    for (let x = 0; x < width; x += 20) {
        let stripe = new paper.Path({
            segments: [[x, 0], [x, height]],
            strokeColor: color,
            strokeWidth: 5
        });
        content.addChild(stripe);
    }
    return content;
}

function makeStripeHorizontalPattern( color, width, height ) {
    let content = new paper.Group();
    for (let y = 0; y < height; y += 12) {
        let stripe = new paper.Path({
            segments: [[0, y], [width, y]],
            strokeColor: color,
            strokeWidth: 4
        });
        content.addChild(stripe);
    }
    return content;
}

var patternGenerators = ['none', 
    makeDotPattern, 
    makeStripePattern,
    makeStripeHorizontalPattern,
    makeDiamondPattern];

onmessage = function( event ) {
    let requests = event.data.requests;
    let width = event.data.width;
    let height = event.data.height;
    let result = {};

    requests.forEach(request => {
        let patternGenerator = chance.pickone(patternGenerators);
        if (patternGenerator != 'none') {
            let color = new paper.Color(request.color[0], request.color[1], request.color[2]);
            let pattern = patternGenerator(color, width, height);
            result[request.id] = pattern.exportJSON({ asString: true });
        }
    });

    postMessage(result);
};