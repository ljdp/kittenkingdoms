import Vue from 'vue';
import VueResource from 'vue-resource';
import Buefy from 'buefy';
import 'buefy/lib/buefy.css';
import Lobby from './landing/lobby.vue';

var SERVER_ENDPOINT =  "https://" + process.env.SERVER_ADDRESS;
Vue.use(VueResource);
Vue.use(Buefy);
var app = new Vue({
    el: '#app',
    render: h => h(Lobby)
});
window.app = app;
