import Vue from 'vue';
import Vuex from 'vuex';
import Buefy from 'buefy';
import 'buefy/lib/buefy.css';
import App from './mobile/app.vue';
import jsonpatch from 'fast-json-patch';
// import imagescache from 'images-cache';
import VueResource from 'vue-resource';
import WebSocket from 'reconnecting-websocket';
import Moment from 'moment';
import WorldMap from './world_map.js';
import Util from './util';
import HintView from './mobile/components/hint.vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueResource);
Vue.use(Buefy, {
    defaultIconPack: 'fa'
});

const router = new VueRouter({
    routes: [
        {
            path: '/hint/:name'
        }
    ]
});

var worldMap;

window.characterPhotoProvider = function( guid, format = 'png', background = false, size = '128x128' ) {
    let imgsrc = `https://robohash.org/${guid}.${format}?set=set4&size=${size}`;
    if (background) {
        imgsrc += '&bgset=bg1';
    }
    return Util.fromImageCache(imgsrc, format);
}

function socketSendJson( msg ) {
    msg.gameId = store.state.gameId;
    socket.send(JSON.stringify(msg));
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

Vue.use({
    install(Vue, options) {
        Vue.prototype.$photo = window.characterPhotoProvider;
        Vue.prototype.$urlParams = getParameterByName;
        Vue.prototype.$hint = function(title, body) {
            store.state.hintDialog = this.$dialog.alert({
                title: title,
                message: require("./hints/"+ body + ".html"),
                confirmText: 'Got it.',
                onConfirm: () => {
                    router.push('/');
                }
            });
        }
    }
});

Vue.component('hint', HintView);

const store = new Vuex.Store({
    state: {
        username: '',
        gameId: '',
        hintDialog: null,
        socketCloseEvent: null,
        receivedGameState: false,
        selectedCharacter: null,
        selectedCounty: null,
        selectedArmy: null,
        activeCharacter: null,
        currentView: 'none',
        composeMessageReceiverName: '',
        composeMessageReceiverId: '',
        isComposingMessage: false,
        activeCharacterTab: 0,
        gameInfo: {
            nextTurnTime: '0',
            nextIncomeTurnTime: '0',
            turn: 0
        },
        playerInfo: {
            isPremium: false
        },
        gameState: {
            players: [],
            player: null,
            characters: [],
            counties: [],
            armies: [],
            messages: [],
            items: [],
            map: {}
        }
    },
    mutations: {
        INIT( state, data ) {
            state.username = data.username;
            state.gameId = data.gameId;
        },
        STATE_FULL( state, data ) {
            state.gameState = data.state;
            state.gameInfo = data.gameInfo;
            state.playerInfo = data.playerInfo;
            if (!state.receivedGameState) {
                let useCanvas = (getParameterByName('canvas') == 1);
                window.worldMap = worldMap = new WorldMap("myCanvas", state.gameState, store, useCanvas);
                let mainCharacter = state.gameState.characters.find(c => c.isOwner);
                if (mainCharacter) {
                    let county = state.gameState.counties.find(c => c.guid == mainCharacter.location);
                    if (county) {
                        worldMap.panAndZoomTo(county.x, county.y, 4, 6000);
                    } else {
                        worldMap.panAndZoomTo(0, 0, 4, 6000);    
                    }
                } else {
                    worldMap.panAndZoomTo(0, 0, 4, 6000);
                }
            }
            worldMap.update(state.gameState);
            if (store.getters.myCharacters.length > 0) {
                state.activeCharacter = store.getters.myCharacters[0].guid;
            }
            state.receivedGameState = true;
        },
        STATE_PATCH( state, data ) {
            state.gameState = jsonpatch.applyPatch(state.gameState, data.patch).newDocument;
            state.gameInfo = data.gameInfo;
            state.playerInfo = data.playerInfo;
            worldMap.update(state.gameState);
            if (state.selectedCounty) {
                worldMap.highlightCounties(state.selectedCounty);
            }
            app.$forceUpdate();
        },
        SET_CURRENT_VIEW( state, view ) {
            state.currentView = view;
        },
        COMPOSE_MESSAGE( state, {characterId, characterName} ) {
            state.composeMessageReceiverId = characterId;
            state.composeMessageReceiverName = characterName;
            state.currentView = 'messages';
            state.isComposingMessage = true;
        },
        SET_IS_COMPOSING_MESSAGE( state, value ) {
            state.isComposingMessage = value;
        },
        MARK_MESSAGE_READ( state, messageId ) {
            state.gameState.messages.find(m => m.guid == messageId).read = true;
        },
        SET_PROPOSAL_RESPONSE( state, {messageId, proposalIndex, response}) {
            let message = state.gameState.messages.find(m => m.guid == messageId);
            if (message) {
                message.proposals[proposalIndex].receivedResponse = true;
                message.proposals[proposalIndex].response = response;
            }
        },
        SELECT_COUNTY( state, countyId ) {
            state.selectedCounty = countyId;
            state.currentView = 'county';
        },
        SELECT_CHARACTER( state, characterId ) {
            state.selectedCharacter = characterId;
            state.currentView = characterId ? 'character' : null;
        },
        SELECT_ACTIVE_CHARACTER( state, characterId ) {
            state.activeCharacter = characterId;
        },
        SELECT_ARMY( state, armyId ) {
            state.selectedArmy = armyId;
            state.currentView = 'army';
        },
        RAISE_TROOPS( state, data ) {
            let county = state.gameState.counties.find(county => county.guid == data.countyId);
            county.troops -= data.troops;
            let army = {
                troops: data.troops,
                owner: null
            };
            county.armies.push(army);
        },
        PREPARE_MOVE_ARMY( state, armyId ) {
            let army = state.gameState.armies.find(army => army.guid == armyId);
            army.prepareMove = true;
            state.movingArmyId = armyId;
            state.armyOrders = [];
        },
        CLEAR_MOVE_ARMY( state, armyId ) {
            let army = state.gameState.armies.find(army => army.guid == armyId);
            army.prepareMove = false;
            state.movingArmyId = null;
            state.armyOrders = null;
        },
        COMMIT_MOVE_ARMY( state, data ) {
            let army = state.gameState.armies.find(army => army.guid == data.armyId);
            army.orders = data.orders;
        },
        STAND_DOWN_ARMY( state, armyId ) {
            let army = state.gameState.armies.find(army => army.guid == armyId);
            Vue.set(army, 'standDownQueued', true);
        },
        RAZE_COUNTY( state, countyId ) {
            let county = state.gameState.counties.find(county => county.guid == countyId);
            county.razed = true;
            county.specialists = [];
            county.negatives = [];
        },
        REPAIR_COUNTY( state, countyId ) {
            let county = state.gameState.counties.find(county => county.guid == countyId);
            county.razed = false;
        },
        SET_FOCUS( state, data ) {
            let character = state.gameState.characters.find(c => c.guid == data.characterId);
            character.focus = data.focus;
            character.cooldowns.focus = character.focusCooldownDuration;
        },
        SET_ACTIVE_RESEARCH( state, {characterId, researchType}) {
            let character = state.gameState.characters.find(c => c.guid == characterId);
            let index = character.research.findIndex(research => research.type === researchType);
            character.activeResearchIndex = index;
        },
        ACTIVATE_ITEM( state, itemGuid ) {
            let item = state.gameState.items.find(item => item.guid === itemGuid);
            if (item) {
                item.active = true;
            }
        },
        DEACTIVATE_ITEM( state, itemGuid ) {
            let item = state.gameState.items.find(item => item.guid === itemGuid);
            if (item) {
                item.active = false;
            }
        },
        READ_NEWS( state ) {
            let news = state.gameState.news;
            if (news) {
                let lastNews = news[news.length - 1];
                if (lastNews) {
                    Vue.set(state.gameState.player, 'lastReadNewsIndex', lastNews.index);
                } else {
                    Vue.set(state.gameState.player, 'lastReadNewsIndex', 0);
                }
            }
        }
    },
    actions: {
        NEXT_TURN() {
            socketSendJson({
                type: 'NEXT_TURN'
            });
        },
        RAISE_TROOPS( ctx, data ) {
            if (data.troops == 0) return;
            socketSendJson({
                type: 'RAISE_TROOPS',
                troops: data.troops,
                countyId: data.countyId
            });
            ga('send', 'event', 'gameserver', 'raise_troops', data.troops);
        },
        STAND_DOWN_ARMY( ctx, armyId ) {
            ctx.commit('STAND_DOWN_ARMY', armyId);
            socketSendJson({
                type: 'STAND_DOWN_ARMY',
                armyId: armyId
            });
        },
        PREPARE_MOVE_ARMY( ctx, armyId ) {
            ctx.commit('PREPARE_MOVE_ARMY', armyId);
            let army = store.getters.army(armyId);
            let county = store.getters.county(army.location);
            let adjacent = county.adjacent.filter(border => border.state !== 'CLOSED').map(border => border.countyId);
            worldMap.highlightCounties(...adjacent);
        },
        COMMIT_MOVE_ARMY( ctx, armyId ) {
            if (!ctx.state.movingArmyId) return;
            ctx.commit('COMMIT_MOVE_ARMY', {
                armyId: armyId,
                orders: ctx.state.armyOrders
            });
            socketSendJson({
                type: 'MOVE_ARMY',
                armyId: ctx.state.movingArmyId,
                orders: ctx.state.armyOrders
            });
            ctx.commit('CLEAR_MOVE_ARMY', armyId);
            worldMap.hideCurrentOrder();
            worldMap.highlightAdjacent(null);
        },
        CANCEL_MOVE_ARMY( ctx, armyId ) {
            ctx.commit('CLEAR_MOVE_ARMY', armyId);
            worldMap.hideCurrentOrder();
            worldMap.highlightAdjacent(null);
        },
        RAZE_WITH_ARMY( ctx, armyId ) {
            let army = ctx.getters.army(armyId);
            ctx.commit('RAZE_COUNTY', army.location);
            socketSendJson({
                type: 'RAZE_WITH_ARMY',
                armyId: armyId
            });
        },
        REPAIR_COUNTY( ctx, countyId ) {
            ctx.commit('REPAIR_COUNTY', countyId);
            socketSendJson({
                type: 'REPAIR_COUNTY',
                countyId: countyId
            });
        },
        SELECT_COUNTY( ctx, data ) {
            if (ctx.state.movingArmyId) {
                let armyLocation = store.getters.army(ctx.state.movingArmyId).location;
                let lastLocation;
                if (ctx.state.armyOrders.length == 0) {
                    lastLocation = ctx.state.selectedCounty;
                } else {
                    lastLocation = ctx.state.armyOrders[ctx.state.armyOrders.length - 1];
                }
                let county = store.getters.county(lastLocation);
                if (county.adjacent && lastLocation !== data.countyId) {
                    let border = county.adjacent.find(border => border.countyId === data.countyId);
                    if (border && border.state !== 'CLOSED') {
                        ctx.state.armyOrders.push(data.countyId);
                        worldMap.displayCurrentOrder(ctx.state.movingArmyId, ctx.state.armyOrders);
                        let nextCounty = store.getters.county(data.countyId);
                        let adjacent = nextCounty.adjacent.filter(border => border.state !== 'CLOSED').map(border => border.countyId);
                        worldMap.highlightCounties(...adjacent);
                    }
                }
            } else {
                ctx.commit('SELECT_COUNTY', data.countyId);
                worldMap.highlightCounties(data.countyId);
                if (data.shouldMoveMap) {
                    let county = store.getters.county(data.countyId);
                    worldMap.panAndZoomTo(county.x, county.y - 15);
                }
            }
        },
        SET_FOCUS( ctx, data ) {
            ctx.commit('SET_FOCUS', data);
            socketSendJson({
                type: 'SET_FOCUS',
                characterId: data.characterId,
                focus: data.focus
            });
            ga('send', 'event', 'gameserver', 'set_focus', data.focus);
        },
        SEND_MESSAGE( ctx, data ) {
            socketSendJson({
                type: 'SEND_MESSAGE',
                fromCharacterId: data.fromCharacterId,
                toCharacterId: data.toCharacterId,
                message: data.message,
                proposalAcceptType: data.proposalAcceptType,
                proposals: data.proposals
            });
            ga('send', 'event', 'gameserver', 'send_message');
        },
        MARK_MESSAGE_READ( ctx, data ) {
            ctx.commit("MARK_MESSAGE_READ", data.messageId);
            socketSendJson({
                type: 'MARK_MESSAGE_READ',
                messageId: data.messageId
            });
        },
        SEND_PROPOSAL_RESPONSE( ctx, data ) {
            ctx.commit('SET_PROPOSAL_RESPONSE', data);
            socketSendJson({
                type: 'SEND_PROPOSAL_RESPONSE',
                messageId: data.messageId,
                proposalIndex: data.proposalIndex,
                response: data.response
            });
        },
        CHANGE_RESEARCH( ctx, data ) {
            let character = ctx.getters.character(data.characterId);
            let index = character.research.findIndex(r => r.type == data.researchType);
            ctx.commit('SET_ACTIVE_RESEARCH', data);
            socketSendJson({
                type: 'CHANGE_RESEARCH',
                characterId: data.characterId,
                researchIndex: index
            });
        },
        ACTIVATE_ITEM( ctx, data ) {
            ctx.commit('ACTIVATE_ITEM', data.itemId);
            socketSendJson({
                type: 'ACTIVATE_ITEM',
                itemId: data.itemId
            });
        },
        DEACTIVATE_ITEM( ctx, data ) {
            ctx.commit('DEACTIVATE_ITEM', data.itemId);
            socketSendJson({
                type: 'DEACTIVATE_ITEM',
                itemId: data.itemId
            });
        },
        MIX_POTION( ctx, data ) {
            socketSendJson({
                type: 'MIX_POTION',
                itemId: data.itemId
            });
        },
        MOVE_CHARACTER( ctx, data ) {
            socketSendJson({
                type: 'MOVE_CHARACTER',
                characterId: data.characterId,
                countyId: data.countyId
            });
        },
        BREAK_FEALTY( ctx, data ) {
            socketSendJson({
                type: 'BREAK_FEALTY',
                characterId: data.characterId
            });
        },
        ACCEPT_DESIRE( ctx, data ) {
            socketSendJson({
                type: 'ACCEPT_DESIRE',
                choiceIndex: data.choiceIndex,
                desireIndex: data.desireIndex
            });
        },
        RENAME_CHARACTER( ctx, data ) {
            socketSendJson({
                type: 'RENAME_CHARACTER',
                characterId: data.characterId,
                name: data.name
            });
            ga('send', 'event', 'gameserver', 'rename_character', data.name);
        },
        RENAME_COUNTY( ctx, data ) {
            socketSendJson({
                type: 'RENAME_COUNTY',
                countyId: data.countyId,
                name: data.name
            });
            ga('send', 'event', 'gameserver', 'rename_county', data.name);
        },
        START_PLOT( ctx, data ) {
            socketSendJson({
                type: 'START_PLOT',
                plotType: data.plotType,
                characterId: data.characterId,
                targetId: data.targetId,
                potionId: data.potionId
            });
            ga('send', 'event', 'gameserver', 'start_plot', data.plotType);
        },
        POST_NEWS( ctx, data ) {
            socketSendJson({
                type: 'POST_NEWS',
                characterId: data.characterId,
                message: data.message,
                denounceTarget: data.denounceTarget
            });
            ga('send', 'event', 'gameserver', 'post_news');
        },
        READ_NEWS( ctx, data ) {
            let news = ctx.state.gameState.news;
            if (news) {
                socketSendJson({
                    type: 'READ_NEWS',
                    newsIndex: ctx.state.gameState.player.lastReadNewsIndex
                });
                ctx.commit('READ_NEWS');
            }
        },
        GET_GAMES( ctx, data ) {
            socketSendJson({
                type: 'GET_GAMES'
            });
        },
        JOIN_GAME( ctx, guid ) {
            socketSendJson({
                type: 'JOIN_GAME',
                token: ctx.state.loginToken,
                guid: guid
            });
        },
        CREATE_GAME( ctx ) {
            socketSendJson({
                type: 'CREATE_GAME',
                token: ctx.state.loginToken
            });
        },
        READ_INCOME_TURN_INFO( ctx ) {
            ctx.state.gameState.player.showIncomeTurnInfo = false;
            socketSendJson({
                type: 'READ_INCOME_TURN_INFO'
            });
        }
    },
    getters: {
        turn: state => {
            return state.gameInfo.turn;
        },
        nextTurnTime: state => {
            return Moment().to(state.gameInfo.nextTurnTime);
        },
        nextIncomeTime: state => {
            return Moment().to(state.gameInfo.nextIncomeTurnTime);
        },
        gameStartTime: state => {
            return Moment().to(state.gameInfo.nextTurnTime);
        },
        player: state => {
            return state.gameState.player;
        },
        activeCharacter: (state, getters) => {
            return getters.character(state.activeCharacter);
        },
        mainCharacter: state => {
            if (state.gameState.characters) {
                return state.gameState.characters.find(c => c.isOwner && c.alive);
            }
            return null;
        },
        myCharacters: state => {
            if (state.gameState.characters) {
                return state.gameState.characters.filter(c => c.isOwner && c.alive);
            }
            return null;
        },
        characterPhoto: state => (guid, background, size) => {
            return window.characterPhotoProvider(guid, background, size);
        },
        selectedCounty: state => {
            return state.selectedCounty;
        },
        selectedCharacter: state => {
            if (state.selectedCharacter) {
                return state.gameState.characters.find(c => c.guid == state.selectedCharacter);
            } else if (state.gameState.characters) {
                return state.gameState.characters.find(c => c.isOwner);
            }
            return null;
        },
        character: state => (guid) => {
            return state.gameState.characters.find(character => character.guid == guid);
        },
        county: (state) => (guid) => {
            return state.gameState.counties.find(county => county.guid == guid);
        },
        army: (state) => (guid) => {
            return state.gameState.armies.find(army => army.guid == guid);
        },
        items: (state) => (characterId) => {
            return state.gameState.items.filter(item => item.owningCharacter == characterId);
        },
        prisoners: (state) => (characterId) => {
            return state.gameState.characters.filter(character => character.imprisoner == characterId);
        },
        blueprints: (state) => (characterId) => {
            let character = state.gameState.characters.find(character => character.guid == characterId);
            if (character && character.research && character.research.length > 1) {
                return character.research.slice(1);
            } else {
                return [];
            }
        },
        characterResearch: (state) => (guid) => {
            let character = state.gameState.characters.find(character => character.guid == guid);
            return character.research[character.activeResearchIndex];
        },
        characterSiblings: (state) => (guid) => {
            let character = store.getters.character(guid);
            return state.gameState.characters.filter(other => {
                return other.guid !== guid && other.parents.length > 0 && other.parents[0] == character.parents[0];
            });
        },
        characterCounties: (state, getters) => (guid) => {
            return state.gameState.counties.filter(county => {
                return county.owningCharacter == guid;
            });
        },
        characterChildren: (state, getters) => (guid) => {
            return state.gameState.characters.filter(character => {
                return character.parents.indexOf(guid) != -1;
            });
        },
        characterSiblings: (state, getters) => (guid) => {
            let main = getters.character(guid);
            return state.gameState.characters.filter(character => {
                let matches = character.parents.filter(parent => {
                    return main.parents.indexOf(parent) != -1;
                });
                return character.guid != guid && matches != 0;
            });
        },
        characterArmies: (state, getters) => (guid) => {
            return state.gameState.armies.filter(army => army.owningCharacter == guid).map(army => {
                army.locationName = getters.county(army.location).name;
                return army;
            });
        },
        characterVassals: (state, getters) => (guid) => {
            return state.gameState.characters.filter(character => character.overlord == guid);
        },
        characterItems: (state, getters) => (guid) => {
            return state.gameState.items.filter(item => item.owningCharacter == guid);
        },
        characterBlueprints: (state, getters) => (guid) => {
            let character = getters.character(guid);
            if (character) {
                return character.research;
            }
            return [];
        },
        characterPrisoners: (state, getters) => (guid) => {
            return state.gameState.characters.filter(character => character.imprisoner == guid);
        },
        characterHeir: (state, getters) => (guid) => {
            return state.gameState.characters.find(character => character.heirTo == guid);
        },
        countyCharacters: (state, getters) => (guid) => {
            return state.gameState.characters.filter(character => character.location == guid);
        },
        countyArmies: (state) => (guid) => {
            return state.gameState.armies.filter(army => army.location == guid);
        },
        messages: (state) => (guid) => {
            return state.gameState.messages.filter(msg => msg.to == guid);
        },
        unreadMessages: (state, getters) => (guid) => {
            return getters.messages(guid).filter(msg => !msg.read);
        },
        sentMessages: (state) => (guid) => {
            return state.gameState.messages.filter(msg => msg.from == guid && msg.to != guid);
        },
        trashedMessages: (state) => (guid) => {
            return state.gameState.messages.filter(msg => msg.from == guid);
        },
        unreadMessagesForCharacter: (state) => (guid) => {
            let messages = state.gameState.messages.filter(msg => {
                return msg.to == guid && msg.read == false;
            });
            return messages;
        },
        unreadNewsCount: (state, getters) => {
            if (state.gameState.news) {
                let lastNews = state.gameState.news[state.gameState.news.length - 1];
                if (lastNews) {
                    return lastNews.index - (state.gameState.player.lastReadNewsIndex || 0);
                } else {
                    return 0;
                }
            } else {
                return 0;
            } 
        },
        canMarry: (state, getters) => (id1, id2) => {
            let a = getters.character(id1);
            let b = getters.character(id2);
            if (a && b && (id1 !== id2)) {
                console.log(a.spouse, b.spouse);
                return !a.spouse && !b.spouse;
            }
            return false;
        },
        canGiveRelease: (state, getters) => (imprisonerId, prisonerId) => {
            let prisoner = getters.character(prisonerId);
            return prisoner && prisoner.imprisoner == imprisonerId;
        },
        canHaveChildren: (state, getters) => (a, b) => {
            let cat1 = getters.character(a);
            let cat2 = getters.character(b);
            return (cat1.guid != cat2.guid) && cat1.spouse == cat2.guid
                && cat1.location && cat2.location && cat1.location == cat2.location;
        },
        canSwearFealty: (state, getters) => (a, b) => {
            let cat1 = getters.character(a);
            let cat2 = getters.character(b);
            return cat1 && cat2 && (cat1.guid != cat2.guid) && !cat1.overlord && cat2.overlord != cat1.guid;
        },
        denouncedBy: (state, getters) => (characterId) => {
            return state.gameState.characters.filter(character => {
                if (character.denounces) {
                    return character.denounces.find(d => d.target === characterId);
                }
                return false;
            })
        },
        gameConstant: (state) => (key) => {
            return state.gameState.constants[key];
        }
    }
});

var app = new Vue({
    el: '#app',
    store: store,
    render: h => h(App),
    router,
    methods: {
        selectCounty( countyId ) {
            this.$store.dispatch('SELECT_COUNTY', {countyId});
        } 
    }
});

console.log('Attempting to connect to wss://'+process.env.SERVER_ADDRESS);
const socketOptions = {
    maxReconnectionDelay: 30000,
    minReconnectionDelay: 3000,
    reconnectionDelayGrowFactor: 1.2,
    connectionTimeout: 5000,
    maxRetries: 10,
    debug: false
};
var socket = new WebSocket('wss://'+process.env.SERVER_ADDRESS, [], socketOptions);
window.socket = socket;
socket.onopen = function() {
    console.log('Successfully connected to game server ' + process.env.SERVER_ADDRESS);
    window.hidePreloader();
    store.commit('INIT', {
        username: Cookies.get('username'),
        gameId: getParameterByName('id'),
    });
    socketSendJson({
        type: 'REFRESH'
    });
};
socket.onclose = function(ev) {
    app.$toast.open({
        message: 'Lost connection from game server lost, retrying...',
        type: 'is-danger',
        duration: 2000
    });
    window.showPreloader();
    ga('send', 'event', 'gameserver', 'disconnect');
};
socket.onerror = function(err) {
    switch(err.code) {
        case 'INVALID_AUTH':
            app.$toast.open({
                message: 'Invalid Authorization',
                type: 'is-danger',
                duration: 2000
            });
            setTimeout(function() {
                window.location = 'lobby';
            }, 2000);
            break;
        case 'EHOSTDOWN':
            app.$toast.open({
                message: 'Game server host is down.',
                type: 'is-danger',
                duration: 60000
            });
            ga('send', 'event', 'gameserver', 'hostdown');
            break;
        default:
            console.warn(err);
    }
};
socket.onmessage = function( event ) {
    let msg = JSON.parse( event.data );
    switch(msg.type) {
        case 'STATE_FULL':
            store.commit('STATE_FULL', msg);
            ga('send', 'event', 'gameserver', 'received_full_state');
            break;
        case 'STATE_PATCH':
            store.commit('STATE_PATCH', msg);
            ga('send', 'event', 'gameserver', 'received_state_patch');
            break;
        case 'ERROR':
            console.log(msg);
            app.$toast.open({
                message: msg.message,
                type: 'is-danger',
                duration: 3000
            });
            setTimeout(function() {
                // window.location = '/lobby';
            }, 3500);
            break;
    }
}

window.app = app;