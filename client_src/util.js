var imageCache = {};

function blobToBase64(blob, cb) {
    var reader = new FileReader();
    reader.onload = function() {
        var dataUrl = reader.result;
        var base64 = dataUrl.split(',')[1];
        cb(base64);
    };
    reader.readAsDataURL(blob);
};

function fromImageCache( src, format ) {
    if (imageCache[src]) {
        return imageCache[src];
    } else {
        var oReq = new XMLHttpRequest();
        oReq.open("GET", src, true);
        oReq.responseType = "blob";
        oReq.onloadend = (oEvent) => {
            blobToBase64(oReq.response, (base64) => {
                imageCache[src] = `data:image/${format};base64,${base64}`;
            });
        }
        oReq.send();
        return src;
    }
}

export default {
    fromImageCache
}