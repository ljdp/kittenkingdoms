import {vertex} from './default_vert';
import fragment from './watercolor.frag';
import Shader from './shader';
export default class WatercolorFilter extends Shader {
    constructor(size = 10) {
        super(fragment, {});
    }
}