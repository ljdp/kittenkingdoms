export default class extends PIXI.Filter {
	constructor( filterText ) {
        // Update mainImage to main
        // filterText = filterText.replace('mainImage', 'main');
        // filterText = filterText.replace('out vec4 fragColor, in vec2 fragCoord', '');
        // filterText = filterText.replace('out vec4 fragColor,in vec2 fragCoord', '');
        // filterText = filterText.replace(/texture/g, 'texture2D');

        // // Add uniforms and definitions
        // filterText = 'uniform vec2 iResolution;\nuniform float iGlobalTime;\nuniform float iTimeDelta;\nuniform float iFrame;\nuniform vec2 iMouse;\nuniform sampler2D iChannel0;\nuniform sampler2D iChannel1;\nuniform sampler2D iChannel2;\n\n#define fragColor gl_FragColor\n#define fragCoord gl_FragCoord\n' + filterText;

        // // Make sure that there is a main() function
        // // if (!filterText.includes('void main')) {
        // //     filterText = 'void main() {}';
        // // }

        // console.log(filterText);

        let uniforms = {
            iResolution: {
                type: 'v3',
                value: [1024, 768, 0]
            },
            iGlobalTime: {
                type: 'f',
                value: 0
            },
            iTimeDelta: {
                type: 'f',
                value: 0
            },
            iFrame: {
                type: 'i',
                value: 0
            },
            iMouse: {
                type: 'v4',
                value: [0, 0, 0, 0]
            }
        };

		super('', filterText, uniforms);
	}
};