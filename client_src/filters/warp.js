import warpFrag from './warp.frag';
export default class extends PIXI.Filter {
    constructor( sprite, screenX, screenY ) {
        super(null, warpFrag);
        this.autoFit = false;
        this.sprite = sprite;
        this.myMatrix = new PIXI.Matrix();
        this.apply = function(filterManager, input, output)
        {
            this.uniforms.filterMatrix = filterManager.calculateSpriteMatrix(this.myMatrix, this.sprite);
          this.uniforms.dimensions[0] = input.sourceFrame.width
          this.uniforms.dimensions[1] = input.sourceFrame.height
          this.uniforms.time = performance.now() * 0.0005;
          // draw the filter...
          filterManager.applyFilter(this, input, output);
        }
    }
}