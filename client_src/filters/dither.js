import {vertex} from './default_vert';
import fragment from './dither.frag';
import Shader from './shader';
export default class WatercolorFilter extends Shader {
    constructor() {
        super(fragment);
    }
}