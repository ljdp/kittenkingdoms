import Vue from 'vue';

export default {
    methods: {
        parseMessage( message ) {
            let result = unescape(message);
            this.$store.state.gameState.characters.forEach(character => {
                let fullname = `${character.name} ${character.house}`;
                result = result.replace(fullname, `<a @click="$emit('character', '${character.guid}')">${fullname}</a>`);
            });
            this.$store.state.gameState.counties.forEach(county => {
                result = result.replace(county.name, `<a @click="$emit('county', '${county.guid}')">${county.name}</a>`);
            });
            let urlPattern = /\[(.+)\]\((.+)\)/g;
            let match;
            while ((match = urlPattern.exec(result)) != null) {
                let text = match[1];
                let url = match[2];
                result = result.replace(match[0],
                    `<b-tooltip label="This is an external link that will take you to ${url}" multilined><a href="${url}" target="_blank" class="external-link">${text}</a></b-tooltip>`);
            }
            return Vue.compile(`<p>${result}</p>`);
            // return `<p>${result}</p>`;
        },
        selectCharacter( guid ) {
            this.$store.commit('SELECT_CHARACTER', guid);
        },
        selectCounty( guid ) {
            this.$store.dispatch('SELECT_COUNTY', {countyId: guid, shouldMoveMap: true});
        },
        openURL( url ) {

        }
    }
}