module.exports = {
    methods: {
        parseDesires( desires ) {
            
            return desires.filter(desire => desire !== undefined && desire !== null).map(desire => {
                let title = desire.name;
                let description = desire.description || '';
                let clickable = false;
                description = description.replace("${secondaryTarget}", desire.secondaryTarget);
                switch(desire.generateTarget) {
                    case 'character':
                        let targetCharacter = this.$store.getters.character(desire.target);
                        if (targetCharacter) {
                            description = description.replace("${target}", `${targetCharacter.name} ${targetCharacter.house}`);
                            clickable = true;
                        } else {
                            clickable = false;
                        }
                        break;
                    case 'county':
                        let targetCounty = this.$store.getters.county(desire.target);
                        description = description.replace("${target}", targetCounty.name);
                        clickable = true;
                        break;
                    case 'item':
                        description = description.replace("${target}", desire.target.name);
                        break;
                    default:
                        description = description.replace("${target}", desire.target);
                        break;
                }
                // description += ` Worth ${desire.score} points.`;
                return {
                    title, 
                    description,
                    clickable,
                    score: desire.score, 
                    key: desire.key, 
                    target: desire.target, 
                    targetType: desire.generateTarget,
                    statKey: desire.statKey
                };
            });
        }
    }
}