export default {
    methods: {
        renameFirstName( character ) {
            if (this.$store.state.playerInfo.isPremium) {
                this.$dialog.prompt({
                    message: 'Choose a new first name',
                    inputMaxLength: 16,
                    inputMinLength: 1,
                    inputPlaceholder: 'name',
                    onConfirm: (name) => {
                        this.$store.dispatch('RENAME_CHARACTER', {characterId: character.guid, name: name});
                    }
                });
            } else {
                this.$snackbar.open({
                    duration: 5000,
                    message: 'You can only rename characters with a premium account.',
                    type: 'is-primary',
                    position: 'is-top-left',
                    actionText: 'Get Premium',
                    onAction: () => {
                        window.location = "lobby?premium=1"
                    }
                });
            }
        },
        renameHouseName( character ) {
            if (this.$store.state.playerInfo.isPremium) {
                this.$dialog.prompt({
                    message: 'Choose a new house name',
                    inputMaxLength: 16,
                    inputMinLength: 1,
                    inputPlaceholder: 'name',
                    onConfirm: (name) => {
                        this.$store.dispatch('RENAME_HOUSE', {characterId: character.guid, name: name});
                    }
                });
            } else {
                this.$snackbar.open({
                    duration: 5000,
                    message: 'You can only rename characters with a premium account.',
                    type: 'is-primary',
                    position: 'is-top-left',
                    actionText: 'Get Premium',
                    onAction: () => {
                        window.location = "lobby?premium=1"
                    }
                });
            }
        }
    }
}