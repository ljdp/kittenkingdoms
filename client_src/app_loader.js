window.loadApp = function( next ) {
    let gameId = getParameterByName('id');
    $.get({
        url: 'https://' + process.env.SERVER_ADDRESS + '/game/' + gameId + '/version'
    }).done(version => {
        let v = version.split('.');
        $.ajax({
            url: `dist/v${v[0]}.${v[1]}.x/app.js`,
            dataType: 'script',
            success: next
        });
    })
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}