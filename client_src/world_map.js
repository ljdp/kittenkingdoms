import Pixi from 'pixi.js';
import * as filters from 'pixi-filters';
import Chance from 'chance';
import DitherFilter from './filters/dither';
import WarpFilter from './filters/warp';
import union from '@turf/union';
import turfHelpers from '@turf/helpers';
import Color from 'color';
const d3 = require('d3');
const chance = new Chance(1234);

const TEXTURE_ASSETS = {
    'water_overlay': 'img/water_overlay.png',
    'army_icon': 'img/rally-the-troops.png',
    'hourglass_icon': 'img/sands-of-time.png',
    'science_icon': 'img/bubbling-flask.png',
    'military_icon': 'img/sword-smithing.png',
    'economy_icon': 'img/money-stack.png',
    'hazard_icon': 'img/hazard-sign.png',
    'treasure_icon': 'img/locked-chest.png',
    'razed_icon': 'img/flame.png',
    'bank_icon': 'img/bank.png',
    'barracks_icon': 'img/barracks-tent.png',
    'castle_icon': 'img/castle.png',
    'library_icon': 'img/book-pile.png',
    'university_icon': 'img/graduate-cap.png'
};

function makeLinearRing( polygon ) {
    polygon.push(polygon[0]);
    return turfHelpers.polygon([polygon]);
}

function flattenGeojsonPolygon( polygon ) {
    return polygon.geometry.coordinates[0].reduce((arr, val) => {
        arr.push(val[0], val[1]);
        return arr;
    }, []);
}

export default class WorldMap {
    constructor( containerId, gameState, store, forceCanvas = false, fogOfWarFX = false ) {
        this.gameState = gameState;
        let mapScale = this.mapScale = 2;
        this.mapWidth = gameState.map.width * mapScale;
        this.mapHeight = gameState.map.height * mapScale;
        this.forceCanvas = forceCanvas;
        this.fogOfWarFX = fogOfWarFX;
        let app = this.app = new PIXI.Application({
            antialias: true,
            backgroundColor: 0x7957d5,
            forceCanvas: forceCanvas
        });
        this.containerId = containerId;
        this.container = $('#'+containerId);
        this.store = store;
        document.getElementById(containerId).appendChild(app.view);
        app.view.style.position = "fixed";
        app.view.style.display = "block";
        app.renderer.autoResize = true;
        app.renderer.resize(window.innerWidth, window.innerHeight);
        window.addEventListener("resize", this.resize.bind(this));
        app.stage.interactive = true;
        app.stage.pointerdown = this.onPointerDown.bind(this);
        app.stage.pointerup = this.onPointerUp.bind(this);
        app.view.id = "worldMapCanvas";

        this.zoomSelector = d3.select('#' + containerId);
        this.zoomBehaviour = d3.zoom();
        this.zoomSelector.call(
            this.zoomBehaviour.scaleExtent([1, 8]).on("zoom", this.zoom.bind(this))
        );

        // Setup our scene heirarchy.
        this.backGroup = new PIXI.Container();
        this.mapGroup = new PIXI.Container();
            this.cellGroup = new PIXI.Container();
            this.fowCellGroup = new PIXI.Container();
            this.labelGroup = new PIXI.Container();
            this.fowLabelGroup = new PIXI.Container();
        this.mapEffects = new PIXI.Container();
        this.mapGroup.addChild(this.cellGroup);
        this.mapGroup.addChild(this.fowCellGroup);
        app.stage.addChild(this.backGroup);
        app.stage.addChild(this.mapGroup);
        app.stage.addChild(this.mapEffects);


        if (this.fogOfWarFX) {
            // Create shaders.
            let blurFilter = this.blurFilter = new PIXI.filters.BlurFilter(4, 2);
            var sepiaFilter = this.sepiaFilter =new PIXI.filters.ColorMatrixFilter();
            sepiaFilter.sepia(false);
        
            // Create a render texture to draw the fog of war map to.
            this.fowTextureScale = 4;
            var fowRT = this.fowRT = PIXI.RenderTexture.create(this.mapWidth * this.fowTextureScale, this.mapHeight * this.fowTextureScale);
            var fowRTSprite = this.fowRTSprite = new PIXI.Sprite(fowRT);

            // Create the warp shader (Dependent on the fog of war texture).
            let warpFilter = this.warpFilter = new WarpFilter(fowRTSprite, 1.0, 1.0);

            // Create a render texture to draw the fog of war mask to.
            this.fogOfWarMaskRT = PIXI.RenderTexture.create(this.mapWidth, this.mapHeight);
            this.fogOfWarMask = new PIXI.Sprite(this.fogOfWarMaskRT);
        }
        
        // Create the fog of war graphics we will draw hidden counties to.
        this.fogOfWar = new PIXI.Graphics();
        if (this.fogOfWarFX) {
            this.fogOfWar.filters = [this.blurFilter];
            this.fowLabelGroup.filters = [this.sepiaFilter];
            this.fowRTSprite.filters = [this.sepiaFilter, this.warpFilter];
        }

        if (this.fogOfWarFX) {
            fowRTSprite.scale.x = fowRTSprite.scale.y = 1 / this.fowTextureScale;
            fowRTSprite.mask = this.fogOfWarMask;
            this.mapGroup.addChild(this.fogOfWarMask);
            this.mapGroup.addChild(fowRTSprite);
        }
        this.mapGroup.addChild(this.labelGroup);
        this.mapGroup.addChild(this.fowLabelGroup);

        // Prepare voronoi graph.
        let squiggleAmp = 5;
        let squiggleCount = 1;
        let voronoi = d3.voronoi();
        let sites = [];
        gameState.counties.forEach(county => {
            county.sites.forEach(site => {
                let _site = [site[0] * mapScale, site[1] * mapScale];
                _site.guid = county.guid;
                sites.push(_site);
            });
        });
        voronoi.extent([
            [0, 0],
            [this.mapWidth, this.mapHeight]]);
        let diagram = this.diagram = voronoi(sites);
        this.countyCellMap = {};
        
        // Prepare and union the inner polygons for each county.
        diagram.polygons().forEach((polygon, i) => {
            let guid = diagram.cells[i].site.data.guid;
            let countyCell = this.countyCellMap[guid];
            if (!countyCell) {
                countyCell = {
                    polygon: makeLinearRing(polygon),
                    guid: guid
                }
                this.countyCellMap[guid] = countyCell;
            }
            countyCell.polygon = union(countyCell.polygon, makeLinearRing(polygon));
        });
        gameState.counties.forEach(county => {
            let countyCell = this.countyCellMap[county.guid];
            if (countyCell && !county.impassable) {
                countyCell.graphic = new PIXI.Graphics();
                countyCell.path = flattenGeojsonPolygon(countyCell.polygon);
            }
        });

        // Setup house colors.
        this.houseColors = {};
        gameState.characters.forEach(character => {
            if (!this.houseColors[character.house]) {
                let houseColor = Color('#be7fc1');
                houseColor = houseColor.rotate(chance.integer({min: 0, max: 360}));
                this.houseColors[character.house] = houseColor;
            }
        });

        // Load texture assets.
        for (let id in TEXTURE_ASSETS) {
            app.loader.add(id, TEXTURE_ASSETS[id]);
        }
        // gameState.characters.forEach(character => {
        //     let photoUrl = window.characterPhotoProvider(character.guid, 'png', false, '64x64');
        //     app.loader.add(character.guid, photoUrl);
        // });
        app.loader.load(this.onTexturesLoaded.bind(this));
    }

    onTexturesLoaded( loader, resources ) {
        let app = this.app;
        let stage = this.app.stage;
        this.resources = resources;

        let waterOverlay = this.waterOverlay = new PIXI.extras.TilingSprite(
            resources.water_overlay.texture,
            app.screen.width,
            app.screen.height
        );
        waterOverlay.tileScale.x = waterOverlay.tileScale.y = 1;
        waterOverlay.alpha = 0.2;
        this.backGroup.addChild(waterOverlay);

        app.ticker.add(this.animate.bind(this));
        this.resize();
        this.update(this.gameState);
    }

    update( gameState ) {
        this.gameState = gameState;
        if (!this.resources) {
            return;
        }
        this.cellGroup.removeChildren();
        this.fowCellGroup.removeChildren();
        this.labelGroup.removeChildren();
        this.fowLabelGroup.removeChildren();
        this.fogOfWar.clear();
        this.fogOfWar.beginFill(0x0);
        this.fogOfWar.drawRect(0, 0, this.mapWidth, this.mapHeight);
        this.fogOfWar.endFill();

        // Draw the county polygons.
        gameState.counties.forEach(county => {
            let countyCell = this.countyCellMap[county.guid];
            if (countyCell && !county.impassable && countyCell.graphic) {
                let countyGraphic = countyCell.graphic;
                countyGraphic.clear();

                if (!county.impassable) {
                    if (county.isVisible) {
                        let owner = this.store.getters.character(county.owningCharacter);
                        let cellColor;
                        if (owner) {
                            cellColor = this.houseColors[owner.house];
                        } else {
                            cellColor = Color('gray');
                        }
                        countyGraphic.beginFill(cellColor.rgbNumber());
                        // countyGraphic.lineStyle(3, 0x777777);
                    } else {
                        countyGraphic.beginFill(0x555555);
                        // countyGraphic.lineStyle(3, 0x777777, 0.1);
                    }
                    countyGraphic.drawPolygon(countyCell.path);
                    countyGraphic.endFill();

                    if (county.isVisible) {
                        this.cellGroup.addChild(countyGraphic);
                    } else {
                        this.fowCellGroup.addChild(countyGraphic);
                    }
                }

                if (!county.isVisible) {
                    this.fogOfWar.beginFill(0xFFFFFF, 1);
                    this.fogOfWar.lineStyle(25, 0xFFFFFF);
                    this.fogOfWar.drawPolygon(countyCell.path);
                    this.fogOfWar.endFill();
                } else {
                    let countyIcons = [
                        {
                            texture: this.resources.economy_icon.texture,
                            x: -10, y: 10, scale: 0.12, tint: 0x42cef4,
                            active: county.specialists.find(s => s.type === 'economy')
                        },
                        {
                            texture: this.resources.military_icon.texture,
                            x: -5, y: 10, scale: 0.12, tint: 0x42cef4,
                            active: county.specialists.find(s => s.type === 'military')
                        },
                        {
                            texture: this.resources.science_icon.texture,
                            x: 0, y: 10, scale: 0.12, tint: 0x42cef4,
                            active: county.specialists.find(s => s.type === 'science')
                        },
                        {
                            texture: this.resources.treasure_icon.texture,
                            x: -5, y: -10, scale: 0.15, tint: 0xf4a742,
                            active: county.hasTreasure
                        },
                        {
                            texture: this.resources.hazard_icon.texture,
                            x: 5, y: 10, scale: 0.1, tint: 0xc63b25,
                            active: county.negatives.length > 0
                        },
                        {
                            texture: this.resources.razed_icon.texture,
                            x: 0, y: 0, scale: 0.25, tint: 0xFF0000,
                            active: county.razed
                        },
                        {
                            texture: this.resources.razed_icon.texture,
                            x: -15, y: -20, scale: 0.2, tint: 0xFF0000,
                            active: county.razed
                        },
                        {
                            texture: this.resources.library_icon.texture,
                            x: -20, y: -10, scale: 0.1, tint: 0x5522FF,
                            active: county.buildings.find(b => b.id === 'science_building_1')
                        },
                        {
                            texture: this.resources.university_icon.texture,
                            x: -10, y: -10, scale: 0.1, tint: 0x5522FF,
                            active: county.buildings.find(b => b.id === 'science_building_2')
                        },
                        {
                            texture: this.resources.barracks_icon.texture,
                            x: 0, y: -10, scale: 0.1, tint: 0x5522FF,
                            active: county.buildings.find(b => b.id === 'military_building_1')
                        },
                        {
                            texture: this.resources.castle_icon.texture,
                            x: 10, y: -10, scale: 0.1, tint: 0x5522FF,
                            active: county.buildings.find(b => b.id === 'military_building_2')
                        },
                        {
                            texture: this.resources.bank_icon.texture,
                            x: 20, y: -10, scale: 0.1, tint: 0x5522FF,
                            active: county.buildings.find(b => b.id === 'economy_building_1')
                        },
                    ];
                    countyIcons.forEach(icon => {
                        if (icon.active) {
                            let sprite = new PIXI.Sprite(icon.texture);
                            sprite.x = county.center[0] * this.mapScale + icon.x;
                            sprite.y = county.center[1] * this.mapScale + icon.y;
                            sprite.scale.x = sprite.scale.y = icon.scale;
                            sprite.tint = icon.tint;
                            this.labelGroup.addChild(sprite);
                        }
                    });
                }
                
                if (!county.impassable) {
                    let label = new PIXI.Text(county.name, {
                        fontFamily: 'Segoe UI, Roboto, Oxygen',
                        fontSize: 44 + Math.pow(county.size, 2.25),
                        fill: 0xFFFFFF,
                        align: 'right'
                    });
                    label.scale.x = label.scale.y = 0.15;
                    label.alpha = 0.7;
                    label.rotation = Math.PI * -0.05;
                    label.position.x = county.center[0] * this.mapScale;
                    label.position.y = county.center[1] * this.mapScale;
                    label.position.x = label.position.x - label.getBounds().width * 0.5;
                    
                    if (county.isVisible) {
                        this.labelGroup.addChild(label);
                    } else {
                        this.fowLabelGroup.addChild(label);
                    }
                }
            } else if (!county.impassable) {
                console.warn('Cannot find county cell for ' + county.guid);
            }
        });

        // Borders.
        let borderGraphic = new PIXI.Graphics(false);
        this.cellGroup.addChild(borderGraphic);
        let fowBorderGraphic = new PIXI.Graphics(false);
        this.fowCellGroup.addChild(fowBorderGraphic);

        this.diagram.edges.forEach(edge => {
            if (edge.left && edge.right) {
                let leftCounty = this.store.getters.county(edge.left.data.guid);
                let rightCounty = this.store.getters.county(edge.right.data.guid);
                let graphic = borderGraphic;
                if (leftCounty.impassable ^ rightCounty.impassable) {
                    graphic.lineStyle(2, 0x000000, 1);
                    let start = new PIXI.Point(edge[0][0], edge[0][1]);
                    let end = new PIXI.Point(edge[1][0], edge[1][1]);
                    graphic.moveTo(start.x, start.y);
                    graphic.lineTo(end.x, end.y);
                }

                if (leftCounty.adjacent && leftCounty.guid !== rightCounty.guid) {
                    let border = leftCounty.adjacent.find(border => border.countyId === rightCounty.guid);
                    if (border) {
                        switch(border.state) {
                            case 'INNER':
                                var owner = this.store.getters.character(leftCounty.owningCharacter);
                                if (owner) {
                                    var cellColor = this.houseColors[owner.house];
                                    graphic.lineStyle(1, cellColor.darken(0.2).rgbNumber(), 1);
                                }
                                break;
                            case 'OPEN':
                                graphic.lineStyle(2, 0x00FF00, 1);
                                break;
                            case 'CLOSED':
                                graphic.lineStyle(2, 0xFF0000, 1);
                                break;
                            case 'ENEMY':
                                graphic.lineStyle(2, 0xFFAA00, 1);
                                break;
                            case 'NEUTRAL':
                                graphic.lineStyle(1, 0x000000, 1);
                                break;
                        }
                        let start = new PIXI.Point(edge[0][0], edge[0][1]);
                        let end = new PIXI.Point(edge[1][0], edge[1][1]);
                        graphic.moveTo(start.x, start.y);
                        graphic.lineTo(end.x, end.y);
                    }
                }
            }
        });

        // Update buildings.
        gameState.counties.forEach(county => {
            county.buildings.forEach(building => {
                switch(building.id) {
                    case 'walls_building_1':
                        this.drawCountyWall(county, 0.8, 4, 0x0);
                        this.drawCountyWall(county, 0.8, 2, 0x876052);
                        break;
                }
            });
        });

        // Army icons.
        let chance = new Chance(3456);
        gameState.armies.forEach(army => {
            let armySprite = new PIXI.Sprite(this.resources.army_icon.texture);
            let county = this.store.getters.county(army.location);
            let owner = this.store.getters.character(army.owningCharacter);
            armySprite.anchor.x = armySprite.anchor.y = 0.5;
            armySprite.position.x = (county.center[0] * this.mapScale) + chance.integer({min: -10, max: 10});
            armySprite.position.y = (county.center[1] * this.mapScale) + chance.integer({min: 3, max: 8});
            armySprite.scale.x = armySprite.scale.y = 0.25;
            armySprite.tint = this.houseColors[owner.house].darken(0.4).rgbNumber();
            this.cellGroup.addChild(armySprite);

            // if (army.cooldown > 0) {
            //     let hourglass = new PIXI.Sprite(this.resources.hourglass_icon.texture);
            //     hourglass.position.x = armySprite.position.x + 3;
            //     hourglass.position.y = armySprite.position.y + 3;
            //     hourglass.scale.x = hourglass.scale.y = 0.1;
            //     this.cellGroup.addChild(hourglass);
            // }

            armySprite.interactive = true;
            armySprite.pointerover = () => {
                armySprite.scale.x = armySprite.scale.y = 0.35;
                armySprite.rotation = Math.PI * 0.05;
            };
            armySprite.pointerout = () => {
                armySprite.scale.x = armySprite.scale.y = 0.25;
                armySprite.rotation = 0;
            };
        });

        // Army orders.
        gameState.armies.forEach(army => {
            if (army.orders && army.orders.length > 0) {
                let countyIds = [army.location, ...army.orders];
                for (let i = 0; i < countyIds.length - 1; i++) {
                    let county1 = this.store.getters.county(countyIds[i]);
                    let county2 = this.store.getters.county(countyIds[i + 1]);
                    let pos1 = new PIXI.Point(county1.center[0] * this.mapScale, county1.center[1] * this.mapScale);
                    let pos2 = new PIXI.Point(county2.center[0] * this.mapScale, county2.center[1] * this.mapScale);
                    let arrow = this.createArrow(pos1, pos2);
                    let owner = this.store.getters.character(army.owningCharacter);
                    arrow.tint = this.houseColors[owner.house].darken(0.4).rgbNumber();
                    if (county2.isVisible) {
                        this.cellGroup.addChild(arrow);
                    } else {
                        this.fowCellGroup.addChild(arrow);
                    }
                }
            }
        });

        // Character sprites.
        let charactersAtLocation = {};
        gameState.characters.forEach(character => {
            if (!charactersAtLocation[character.location]) {
                charactersAtLocation[character.location] = [character.guid];
            } else {
                charactersAtLocation[character.location].push(character.guid);
            }
        });
        gameState.characters.forEach(character => {
            let sprite = PIXI.Sprite.fromImage(window.characterPhotoProvider(character.guid, "png", false, "64x64"));
            sprite.anchor.x = sprite.anchor.y = 0.5;
            
            let county = gameState.counties.find(county => county.guid == character.location);
            if (county) {
                sprite.scale.x = sprite.scale.y = 0.5;
                let locationIndex = charactersAtLocation[character.location].indexOf(character.guid);
                let locationCount = charactersAtLocation[character.location].length - 1;
                let spacing = 15;
                sprite.position.x = (county.center[0] * this.mapScale) + (locationIndex - (locationCount/2)) * spacing;
                sprite.position.y = county.center[1] * this.mapScale - 20;
                sprite.visible = true;
            } else {
                sprite.visible = false;
            }

            if (county && county.isVisible) {
                this.labelGroup.addChild(sprite);
            } else {
                this.fowLabelGroup.addChild(sprite);
            }

            if (!character.alive) {
                sprite.alpha = 0.2;
            }

            if (character.age <= 10) {
                sprite.scale.x = sprite.scale.y = 0.2;
            } else if (character.age <= 30) {
                sprite.scale.x = sprite.scale.y = 0.35;
            } else if (character.age <= 50) {
                sprite.scale.x = sprite.scale.y = 0.5;
            } else if (character.age <= 70) {
                sprite.scale.x = sprite.scale.y = 0.4;
            } else {
                sprite.scale.x = sprite.scale.y = 0.3;
            }

            sprite.interactive = true;
            sprite.pointerup = () => {
                if (!this.isDragging) {
                    this.didSelectCharacter = true;
                    this.store.commit('SELECT_CHARACTER', character.guid);
                }
            };
            sprite.pointerover = () => {
                sprite.scale.x += 0.2;
                sprite.scale.y += 0.2;
                sprite.rotation = Math.PI * 0.02;
            };
            sprite.pointerout = () => {
                sprite.scale.x -= 0.2;
                sprite.scale.y -= 0.2;
                sprite.rotation = 0;
            };
        });

        // Render fog of war mask graphic to RenderTexture.
        this.app.renderer.render(this.fogOfWar, this.fogOfWarMaskRT, false, null, false);
    }

    drawCountyWall( county, scale, width, color ) {
        let path = this.countyCellMap[county.guid].path.slice();
        for (let i = 0; i < path.length; i += 2) {
            path[i] = (path[i] - (county.center[0] * this.mapScale)) * 0.75;
            path[i+1] = (path[i + 1] - (county.center[1] * this.mapScale)) * 0.75;
        };
        let wall = new PIXI.Graphics();
        wall.lineStyle(width, color);
        wall.drawPolygon(path);
        wall.x = county.center[0] * this.mapScale;
        wall.y = county.center[1] * this.mapScale;
        this.labelGroup.addChild(wall);
    }

    drawCountyIcon( county, iconName ) {
        let sprite = new PIXI.Sprite(this.resources[iconName].texture);

        this.labelGroup.addChild(sprite);
    }

    resize() {
        this.app.renderer.resize(window.innerWidth, window.innerHeight);
        let width = this.container.offsetWidth;
        let height = this.container.offsetHeight;

        if (this.background) {
            this.background.width = this.app.renderer.width;
            this.background.height = this.app.renderer.height;
        }
    }

    animate( delta ) {
        if (!this.timer) {
            this.timer = 0;
        }
        this.timer += delta;

        if (this.waterOverlay) {
            this.waterOverlay.tilePosition.x = this.timer * 0.1;
            this.waterOverlay.tilePosition.y = this.timer * 0.1;
        }

        if (this.highlightedCells) {
            this.highlightedCells.forEach(cell => {
                cell.highlightGraphic.clear();
                cell.highlightGraphic.lineStyle(5 + Math.sin(this.timer * 0.05) * 3, 0xDDDDDD);
                cell.highlightGraphic.drawPolygon(cell.path);
            });
        }

        // Update fog of war map texture.
        if (this.fogOfWarFX) {
            this.fowRTSprite.visible = false;
            this.fowCellGroup.visible = true;
            this.fowLabelGroup.visible = false;
            let prevScale = this.mapGroup.scale.x;
            let prevPosX = this.mapGroup.position.x;
            let prevPosY = this.mapGroup.position.y;
            this.mapGroup.position.x = this.mapGroup.position.y = 0;
            this.mapGroup.scale.x = this.mapGroup.scale.y = this.fowTextureScale;
            this.app.renderer.render(this.mapGroup, this.fowRT, true, null, false);
            this.mapGroup.position.x = prevPosX;
            this.mapGroup.position.y = prevPosY;
            this.mapGroup.scale.x = this.mapGroup.scale.y = prevScale;
            this.fowRTSprite.visible = true;
            this.fowLabelGroup.visible = true;
            this.fowCellGroup.visible = false;
        }
    }

    isCanvasMode() {
        return this.app.renderer.type == 2;
    }

    onPointerDown( evt ) {
        this.isDragging = false;
        this.didSelectCharacter = false;
    }

    onPointerUp( evt ) {
        if (!this.isDragging && !this.didSelectCharacter) {
            let localPos = this.mapGroup.toLocal(evt.data.global);
            let site = this.diagram.find(localPos.x, localPos.y);
            if (site) {
                let county = this.store.getters.county(site.data.guid);
                if (!county.impassable) {
                    this.store.dispatch('SELECT_COUNTY', {countyId: site.data.guid, shouldMoveMap: true});
                }
            }
        }
    }

    zoom() {
        let itemsToPanZoom = [this.mapGroup];
        itemsToPanZoom.forEach(item => {
            item.position.x = d3.event.transform.x;
            item.position.y = d3.event.transform.y;
            item.scale.x = d3.event.transform.k;
            item.scale.y = d3.event.transform.k;
        });
        this.isDragging = true;
    }

    panAndZoomTo( x, y, zoomScale = 4, animationMS = 2000 ) {
        let zoom = d3.zoomIdentity
            .translate(this.app.screen.width/2, this.app.screen.height/2)
            .scale(zoomScale)
            .translate(-x * this.mapScale, -y * this.mapScale);
        this.zoomSelector
            .transition()
            .duration(animationMS)
            .call(this.zoomBehaviour.transform, zoom);
    }

    highlightCounties( ...countyIds ) {
        this.highlightCells(...countyIds.map(id => this.countyCellMap[id]));
    }

    highlightCells( ...cells ) {
        if (!this.highlightedCells) {
            this.highlightedCells = [];
        }

        this.highlightedCells.forEach(cell => {
            this.fowCellGroup.removeChild(cell.highlightGraphic);
            this.cellGroup.removeChild(cell.highlightGraphic);
            delete cell.highlightGraphic;
        });
        this.highlightedCells = [];

        cells.forEach(cell => {
            if (cell) {
                let highlightGraphic = new PIXI.Graphics();
                let county = this.store.getters.county(cell.guid);
                if (county.isVisible) {
                    this.cellGroup.addChild(highlightGraphic);
                } else {
                    this.fowCellGroup.addChild(highlightGraphic);
                }
                cell.highlightGraphic = highlightGraphic;
                cell.highlightGraphic.alpha = 0.5;
                this.highlightedCells.push(cell);
            }
        });
    }

    highlightAdjacent( countyId ) {
        if (countyId) {
            this.highlightCounties(...this.adjacentCountyIds(countyId));
        } else {
            this.highlightCells(null);
        }
    }

    displayCurrentOrder( armyId, orders ) {
        if (!this.armyOrderGraphics) {
            this.armyOrderGraphics = [];
        }
        this.hideCurrentOrder();
        let army = this.store.getters.army(armyId);
        let countyIds = [army.location, ...orders];
        let lastCounty = this.store.getters.county(orders[orders.length - 1]);
        this.panAndZoomTo(lastCounty.x, lastCounty.y, 2);
        for (let i = 0; i < countyIds.length - 1; i++) {
            let county1 = this.store.getters.county(countyIds[i]);
            let county2 = this.store.getters.county(countyIds[i + 1]);
            let cell1 = this.countyCellMap[countyIds[i]];
            let cell2 = this.countyCellMap[countyIds[i + 1]];
            let pos1 = new PIXI.Point(county1.center[0] * this.mapScale, county1.center[1] * this.mapScale);
            let pos2 = new PIXI.Point(county2.center[0] * this.mapScale, county2.center[1] * this.mapScale);
            let arrow = this.createArrow(pos1, pos2);
            arrow.tint = 0xAA5555;
            if (county2.isVisible) {
                this.cellGroup.addChild(arrow);
            } else {
                this.fowCellGroup.addChild(arrow);
            }
            this.armyOrderGraphics.push(arrow);
        }
    }

    hideCurrentOrder() {
        if (this.armyOrderGraphics) {
            this.armyOrderGraphics.forEach(graphic => {
                if (graphic.parent) {
                    graphic.parent.removeChild(graphic);
                }
            });
            this.armyOrderGraphics = [];
        }
    }

    createArrow( from, to ) {
        let dx = to.x - from.x;
        let dy = to.y - from.y;
        let normal = new PIXI.Point(-dy, dx);
        let arrowPoints = [];
        for (let k = 0; k < 10; k++) {
            let t = k/10;
            let tx = from.x + t * (to.x - from.x);
            let ty = from.y + t * (to.y - from.y);
            tx += Math.sin(t * Math.PI) * normal.x * 0.1;
            ty += Math.sin(t * Math.PI) * normal.y * 0.1;
            arrowPoints.push(new PIXI.Point(tx, ty));
        }
        let arrow = new PIXI.mesh.Rope(PIXI.Texture.fromImage('img/order-arrow.png'), arrowPoints);
        arrow.alpha = 0.75;
        return arrow;
    }

    adjacentCells( cell ) {
        let cellIndex = cell.site.index;
        let leftEdges = this.diagram.edges.filter(function(edge) {
            return edge.left && edge.left.index == cellIndex;
        });
        let rightEdges = this.diagram.edges.filter(function(edge) {
            return edge.right && edge.right.index == cellIndex;
        });
        let adjacentCells = [];
        for (var i = 0; i < leftEdges.length; i++) {
            if (leftEdges[i].right) {
                adjacentCells.push(this.diagram.cells[leftEdges[i].right.index]);
            }
        }
        for (var i = 0; i < rightEdges.length; i++) {
            if (rightEdges[i].left) {
                adjacentCells.push(this.diagram.cells[rightEdges[i].left.index]);
            }
        }
        return adjacentCells.filter((c) => { 
            let county = this.store.getters.county(c.site.data.guid);
            return c !== undefined && !county.impassable; 
        });
    }

    adjacentCountyIds( guid ) {
        let cells = this.diagram.cells.filter(cell => cell.site.data.guid == guid);
        let countyGuids = [];
        cells.forEach(cell => {
            this.adjacentCells(cell).forEach(adjacent => {
                if (countyGuids.indexOf(adjacent.site.data.guid) == -1) {
                    countyGuids.push(adjacent.site.data.guid);
                }
            });
        });
        return countyGuids;
    }

    areCountiesAdjacent( county1Id, county2Id ) {
        return this.adjacentCountyIds(county1Id).indexOf(county2Id) != -1;
    }

    isBorderFriendly( leftOwner, rightOwner ) {
        if (!leftOwner || !rightOwner) return false;
        return leftOwner.spouse == rightOwner.guid || rightOwner.spouse == leftOwner.guid
            || leftOwner.overlord == rightOwner.guid || rightOwner.overlord == leftOwner.guid;
    }
}


function scaleToWindow(canvas, backgroundColor) {
    var scaleX, scaleY, scale, center;

    //1. Scale the canvas to the correct size
    //Figure out the scale amount on each axis
    scaleX = window.innerWidth / canvas.offsetWidth;
    scaleY = window.innerHeight / canvas.offsetHeight;

    //Scale the canvas based on whichever value is less: `scaleX` or `scaleY`
    scale = Math.min(scaleX, scaleY);
    canvas.style.transformOrigin = "0 0";
    canvas.style.transform = "scale(" + scale + ")";

    //2. Center the canvas.
    //Decide whether to center the canvas vertically or horizontally.
    //Wide canvases should be centered vertically, and 
    //square or tall canvases should be centered horizontally
    if (canvas.offsetWidth > canvas.offsetHeight) {
        if (canvas.offsetWidth * scale < window.innerWidth) {
            center = "horizontally";
        } else {
            center = "vertically";
        }
    } else {
        if (canvas.offsetHeight * scale < window.innerHeight) {
            center = "vertically";
        } else {
            center = "horizontally";
        }
    }

    //Center horizontally (for square or tall canvases)
    var margin;
    if (center === "horizontally") {
        margin = (window.innerWidth - canvas.offsetWidth * scale) / 2;
        canvas.style.marginTop = 0 + "px";
        canvas.style.marginBottom = 0 + "px";
        canvas.style.marginLeft = margin + "px";
        canvas.style.marginRight = margin + "px";
    }

    //Center vertically (for wide canvases) 
    if (center === "vertically") {
        margin = (window.innerHeight - canvas.offsetHeight * scale) / 2;
        canvas.style.marginTop = margin + "px";
        canvas.style.marginBottom = margin + "px";
        canvas.style.marginLeft = 0 + "px";
        canvas.style.marginRight = 0 + "px";
    }

    //3. Remove any padding from the canvas  and body and set the canvas
    //display style to "block"
    canvas.style.paddingLeft = 0 + "px";
    canvas.style.paddingRight = 0 + "px";
    canvas.style.paddingTop = 0 + "px";
    canvas.style.paddingBottom = 0 + "px";
    canvas.style.display = "block";

    //4. Set the color of the HTML body background
    document.body.style.backgroundColor = backgroundColor;

    //Fix some quirkiness in scaling for Safari
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("safari") != -1) {
        if (ua.indexOf("chrome") > -1) {
            // Chrome
        } else {
            // Safari
            //canvas.style.maxHeight = "100%";
            //canvas.style.minHeight = "100%";
        }
    }

    //5. Return the `scale` value. This is important, because you'll nee this value 
    //for correct hit testing between the pointer and sprites
    return scale;
}