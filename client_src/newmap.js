import paper from 'paper';
import Chance from 'chance';
const d3 = require('d3');
const chance = new Chance(1234);
var Pt = paper.Point;

export default class Map {
    constructor( canvasId, gameState, eventHandler ) {
        this.gameState = gameState;
        this.eventHandler = eventHandler;
        var canvas = document.getElementById(canvasId);
        paper.setup(canvas);
        
        this.cellsLayer = new paper.Layer();
        this.bordersLayer = new paper.Layer();
        this.overlayLayer = new paper.Layer();
        this.currentOrdersLayer = new paper.Layer();

        let mapScale = this.mapScale = 2;
        let squiggleAmp = 5;
        let squiggleCount = 1;
        let voronoi = d3.voronoi();

        // Collate sites for voronoi diagram.
        let sites = [];
        gameState.counties.forEach(county => {
            county.sites.forEach(site => {
                let _site = [site[0] * mapScale, site[1] * mapScale];
                _site.guid = county.guid;
                sites.push(_site);
            });
        });
        // let sites = gameState.counties.map(county => {
        //     var site = [county.x * mapScale, county.y * mapScale];
        //     site.county = county;
        //     site.guid = county.guid;
        //     return site;
        // });
        voronoi.extent([[0, 0], [gameState.map.width * mapScale, gameState.map.height * mapScale]]);
        
        this.bordersLayer.activate();
        this.labels = [];
        // let preDiagram = voronoi(sites);
        // preDiagram.polygons().forEach((polygon, i) => {
        //     let center = d3.polygonCentroid(polygon);
        //     let cell = preDiagram.cells[i];
        //     sites[i].center = center;
        //     let countySize = cell.site.data.county.size;
        //     for (let k = 0; k < squiggleCount; k++) {
        //         let x = center[0] + Math.sin(chance.floating({min: -Math.PI, max: Math.PI})) * countySize * squiggleAmp;
        //         let y = center[1] + Math.cos(chance.floating({min: -Math.PI, max: Math.PI})) * countySize * squiggleAmp;
        //         let site = [x, y];
        //         site.county = cell.site.data.county;
        //         site.guid = cell.site.data.guid;
        //         site.center = center;
        //         sites.push(site);
        //     }
        //     if (!cell.site.data.county.impassable) {
        //         var label = new paper.PointText({
        //             point: center,
        //             content: cell.site.data.county.name,
        //             fontSize: cell.site.data.county.size + 3,
        //             fillColor: 'white',
        //             justification: 'center'
        //         });
        //         this.labels.push({
        //             label: label,
        //             guid: cell.site.data.guid
        //         });
        //     }
        // });
        let diagram = this.diagram = voronoi(sites);

        // Add county polygons.
        this.cellsLayer.activate();
        this.countyCellMap = {};
        diagram.polygons().forEach((polygon, i) => {
            let path = new paper.Path({
                segments: polygon,
                strokeWidth: 1,
                closed: true
            });
            let guid = diagram.cells[i].site.data.guid;
            if (!this.countyCellMap[guid]) {
                let group = new paper.Group();
                this.countyCellMap[guid] = {
                    polygon: group,
                    data: diagram.cells[i].site.data
                };
            }
            this.countyCellMap[guid].polygon.addChild(path);
        });

        // Add county labels.
        gameState.counties.forEach(county => {
            if (!county.impassable) {
                var label = new paper.PointText({
                    point: [county.center[0] * mapScale, county.center[1] * mapScale],
                    content: county.name,
                    fontSize: county.size + 3,
                    fillColor: 'white',
                    justification: 'center'
                });
                this.labels.push({
                    label: label,
                    guid: county.guid
                });
            }
        });

        // Add border edges.
        this.bordersLayer.activate();
        diagram.edges.forEach(edge => {
            if (edge.left && edge.right) {
                let leftCounty = this.county(edge.left.data.guid);
                let rightCounty = this.county(edge.right.data.guid);
                if (edge.left.data.guid != edge.right.data.guid && !(leftCounty.impassable && rightCounty.impassable))
                {
                    let path = new paper.Path();
                    path.strokeWidth = 3;
                    path.strokeColor = 'black';
                    path.strokeCap = 'round';
                    let start = new Pt(edge[0][0], edge[0][1]);
                    let end = new Pt(edge[1][0], edge[1][1]);
                    path.moveTo(start);
                    path.lineTo(end);
                    edge.path = path;
                }
            }
        });

        // Generate the house colors.
        this.houseColors = {};
        this.housePatterns = {};
        let patternRequests = [];
        gameState.characters.forEach(character => {
            if (!this.houseColors[character.house]) {
                this.houseColors[character.house] = new paper.Color({
                    hue: chance.floating({min: 0, max: 360}),
                    saturation: chance.floating({min: 0.3, max: 0.6}),
                    brightness: chance.floating({min: 0.7, max: 1})
                });
                let patternColor = this.houseColors[character.house].clone();
                patternColor.hue += 180;
                patternRequests.push({
                    id: character.house,
                    color: [patternColor.red, patternColor.green, patternColor.blue]
                });
            }
        });

        // Start a worker to generate the house patterns.
        // let patternWorker = new Worker('src/map_patterns.js');
        // patternWorker.postMessage({
        //     requests: patternRequests, 
        //     width: gameState.map.width * mapScale,
        //     height: gameState.map.height * mapScale
        // });
        // patternWorker.onmessage = (event) => {
        //     this.housePatterns = {};
        //     this.patternsLayer.activate();
        //     console.log('Patterns generated');
        //     for (let id in event.data) {
        //         let content = new paper.Group();
        //         content.importJSON(event.data[id]);
        //         let raster = content.rasterize();
        //         content.remove();
        //         let mask = new paper.Group();
        //         mask.clipMask = true;
        //         let pattern = new paper.Group([mask, raster]);
        //         this.housePatterns[id] = pattern;
        //     }
        //     this.update(this.gameState);
        // };

        this.characterIcons = {};
        this.houseArmyIcons = {};
        this.overlayLayer.activate();
        paper.project.importSVG('img/sands-of-time.svg', (item, svg) => {
            item.scale(0.02);
            this.cooldownIcon = new paper.SymbolDefinition(item);
        });
        paper.project.importSVG('img/rally-the-troops.svg', (item, svg) => {
            item.scale(0.05);
            item.style.strokeWidth = 1;
            item.style.strokeColor = 'black';
            gameState.characters.forEach(character => {
                let armyIcon = item.clone();
                armyIcon.style.fillColor = this.houseColors[character.house];
                this.houseArmyIcons[character.house] = new paper.SymbolDefinition(armyIcon);
            });
            this.update(this.gameState);
        });

        let mainProject = paper.project;
        this.overlayProject = new paper.Project("myCanvasOverlay");

        mainProject.activate();
        paper.view.onMouseMove = this.onMouseMove.bind(this);
        paper.view.onMouseDrag = this.onMouseDrag.bind(this);
        paper.view.onMouseDown = this.onMouseDown.bind(this);
        paper.view.onMouseUp = this.onMouseUp.bind(this);
        // paper.view.onFrame = this.onFrame.bind(this);
        this.canvasId = canvasId;
        $('#'+canvasId).on('mousewheel', this.onMouseWheel.bind(this));

        this.overlayProject.view.center = paper.view.center;
        this.overlayProject.view.zoom = paper.view.zoom;
        
        // Draw the view now:
        paper.view.draw();
    }

    clear() {
        $('#'+this.canvasId).off('mousewheel', this.onMouseWheel.bind(this));
        paper.project.clear();
        this.overlayProject.clear();
        paper.project.remove();
        this.overlayProject.remove();
        paper.view.remove();
    }

    county( guid ) {
        return this.gameState.counties.find(c => c.guid == guid);
    }

    character( guid ) {
        return this.gameState.characters.find(c => c.guid == guid);
    }

    isBorderFriendly( leftCounty, rightCounty ) {
        let leftOwner = this.character(leftCounty.owningCharacter);
        let rightOwner = this.character(rightCounty.owningCharacter);
        if (!leftOwner || !rightOwner) return false;

        return leftOwner.spouse == rightOwner.guid || rightOwner.spouse == leftOwner.guid
            || leftOwner.overlord == rightOwner.guid || rightOwner.overlord == leftOwner.guid;
    }

    update( gameState ) {
        this.gameState = gameState;
        // Update patterns
        // for (let house in this.housePatterns) {
        //     this.housePatterns[house].firstChild.removeChildren();
        // }
        // this.diagram.cells.forEach(cell => {
        //     let county = this.county(cell.site.data.guid);
        //     if (county.owningCharacter) {
        //         let owner = this.character(county.owningCharacter);
        //         if (this.housePatterns[owner.house]) {
        //             let pattern = this.housePatterns[owner.house];
        //             pattern.firstChild.addChild(cell.site.data.polygon.clone());
        //         }
        //     }
        // });

        // Update county colors.
        this.diagram.cells.forEach(cell => {
            let county = this.county(cell.site.data.guid);
            if (county.impassable) {
                this.setCountyColor(cell.site.data.guid, 'lightblue');
            } else if (!county.isVisible && !county.impassable) {
                this.setCountyColor(county.guid, new paper.Color(0.2, 0.2, 0.2));
            } else {
                this.setCountyColor(cell.site.data.guid, this.getCountyHouseColor(county));
            }
        });

        // Update borders.
        this.diagram.edges.forEach(edge => {
            if (edge.path) {
                let leftCounty = this.county(edge.left.data.guid);
                let rightCounty = this.county(edge.right.data.guid);
                let leftOwner = this.character(leftCounty.owningCharacter);
                let rightOwner = this.character(rightCounty.owningCharacter);
                if (leftOwner && rightOwner && leftCounty.owningCharacter == rightCounty.owningCharacter) {
                    // edge.path.strokeColor = new paper.Color(0, 0, 0, 0.1);
                    // let color = this.getCountyHouseColor(leftCounty).clone();
                    // color.brightness *= 0.9;
                    edge.path.strokeColor = new paper.Color(0,0,0,0); 
                } else if (this.isBorderFriendly(leftCounty, rightCounty)) {
                    edge.path.strokeColor = 'green';
                } else if(!leftCounty.isVisible && !rightCounty.isVisible) {
                    edge.path.strokeColor = new paper.Color(0.15, 0.15, 0.15);
                } else if (!leftOwner || !rightOwner) {
                    edge.path.strokeColor = 'black';
                } else {
                    edge.path.strokeColor = 'red';
                }
            }
        });

        // Update labels.
        this.labels.forEach(label => {
            let county = this.county(label.guid);
            label.label.content = county.name;
            label.label.fillColor = county.isVisible ? 'black' : 'white';
        });
        // Update overlay layer.
        this.overlayLayer.activate();
        this.overlayLayer.removeChildren();

        // Update character icons.
        let charactersAtLocation = {};
        gameState.characters.forEach(character => {
            if (character.alive && character.location) {
                if (!charactersAtLocation[character.location]) {
                    charactersAtLocation[character.location] = [];
                }
                charactersAtLocation[character.location].push(character);
            }
        });
        for (let location in charactersAtLocation) {
            charactersAtLocation[location].forEach((character, i) => {
                let county = this.county(location);
                let cell = this.countyCellMap[county.guid];
                let center = [county.center[0] * this.mapScale, county.center[1] * this.mapScale];
                if (!this.characterIcons[character.guid]) {
                    let raster = new paper.Raster(window.characterPhotoProvider(character.guid, 'png', false, '64x64'));
                    raster.scale(0.5);
                    this.characterIcons[character.guid] = raster;
                    raster.on('mouseup', (event) => {
                        this.eventHandler.commit('SELECT_CHARACTER', character.guid);
                        return false;
                    });
                    raster.on('mouseenter', () => {
                        raster.scale(1.25);
                        raster.rotate(10);
                    });
                    raster.on('mouseleave', () => {
                        raster.scale(0.8);
                        raster.rotate(-10);
                    });
                }
                let locationCount = charactersAtLocation[location].length;
                let icon = this.characterIcons[character.guid];
                let spacing = 8;
                icon.position = [
                    center[0] - (locationCount * spacing * 0.5) + (i * spacing),
                    center[1] - 20
                ];
                this.overlayLayer.addChild(icon);
            });
        }
        // Army icons
        gameState.armies.forEach(army => {
            let county = this.county(army.location);
            let owner = this.character(army.owningCharacter);
            let icon = this.houseArmyIcons[owner.house];
            if (icon) {
                let iconInstance = icon.place();
                iconInstance.position = new Pt(county.x * this.mapScale, county.y * this.mapScale);
                if (army.cooldown > 0) {
                    let cooldownIcon = this.cooldownIcon.place();
                    cooldownIcon.position = new Pt(county.x * this.mapScale, county.y * this.mapScale).add(new Pt(-5, -15));
                }
            }
        });
        // Update orders layer.
        gameState.armies.forEach(army => {
            if (army.orders) {
                this.createOrdersPath(army, army.orders, 'orangered');
            }
        });
    }

    getCountyHouseColor( county ) {
        let owner = this.gameState.characters.find(ch => ch.guid == county.owningCharacter);
        if (owner) {
            return this.houseColors[owner.house];
        }
        return new paper.Color(0.8, 0.8, 0.8);
    }

    getCharacterHouseColor( character ) {
        if (character) {
            return this.houseColors[character.house];
        }
        return new paper.Color(0.8, 0.8, 0.8);
    }

    setCountyColor( guid, color ) {
        let polygon = this.countyCellMap[guid].polygon;
        polygon.style.fillColor = color;
        polygon.style.strokeColor = color;
    }

    panAndZoomTo( x, y ) {
        paper.view.center = new Pt(x * this.mapScale, y * this.mapScale);
        paper.view.zoom = 2;
        this.overlayProject.view.center = paper.view.center;
        this.overlayProject.view.zoom = paper.view.zoom;
    }

    adjacentCells( cell ) {
        let cellIndex = cell.site.index;
        let leftEdges = this.diagram.edges.filter(function(edge) {
            return edge.left && edge.left.index == cellIndex;
        });
        let rightEdges = this.diagram.edges.filter(function(edge) {
            return edge.right && edge.right.index == cellIndex;
        });
        let adjacentCells = [];
        for (var i = 0; i < leftEdges.length; i++) {
            if (leftEdges[i].right) {
                adjacentCells.push(this.diagram.cells[leftEdges[i].right.index]);
            }
        }
        for (var i = 0; i < rightEdges.length; i++) {
            if (rightEdges[i].left) {
                adjacentCells.push(this.diagram.cells[rightEdges[i].left.index]);
            }
        }
        return adjacentCells.filter((c) => { 
            let county = this.county(c.site.data.guid);
            return c !== undefined && !county.impassable; 
        });
    }

    adjacentCountyIds( guid ) {
        let cells = this.diagram.cells.filter(cell => cell.site.data.guid == guid);
        let countyGuids = [];
        cells.forEach(cell => {
            this.adjacentCells(cell).forEach(adjacent => {
                if (countyGuids.indexOf(adjacent.site.data.guid) == -1) {
                    countyGuids.push(adjacent.site.data.guid);
                }
            });
        });
        return countyGuids;
    }

    areCountiesAdjacent( county1Id, county2Id ) {
        return this.adjacentCountyIds(county1Id).indexOf(county2Id) != -1;
    }

    createOrdersPath( army, orders, color ) {
        let county = this.county(army.location);
        let orderCounties = orders.map(order => this.county(order));
        let path = new paper.Path();
        path.moveTo(county.x * this.mapScale, county.y * this.mapScale);
        let previousCounty = county;
        orderCounties.forEach(county => {
            path.lineTo(county.x * this.mapScale, county.y * this.mapScale);
            let start = new Pt(previousCounty.x * this.mapScale, previousCounty.y * this.mapScale);
            let end = new Pt(county.x * this.mapScale, county.y * this.mapScale);
            let dir = start.subtract(end).normalize(12);
            let arrowhead = new paper.Path({
                segments: [end.add(dir.rotate(30)), end, end.add(dir.rotate(-30))],
                strokeColor: color,
                strokeWidth: 2
            });
            previousCounty = county;
        });
        path.strokeColor = color;
        path.strokeWidth = 2;
    }

    displayCurrentOrder( currentArmyId, currentOrders ) {
        if (currentArmyId) {
            this.currentOrdersLayer.activate();
            this.currentOrdersLayer.removeChildren();
            let army = this.gameState.armies.find(army => army.guid == currentArmyId);
            if (army) {
                this.createOrdersPath(army, currentOrders, 'orange');
                if (currentOrders.length > 0) {
                    this.adjacentCountiesToHighlight = this.adjacentCountyIds(currentOrders[currentOrders.length - 1]);
                }
            }
        }
    }

    hideCurrentOrder() {
        this.currentOrdersLayer.removeChildren();
    }

    onFrame( event ) {
        let selectedCounty = this.eventHandler.getters.selectedCounty;
        if (selectedCounty) {
            if (!this.selectedCounty || this.selectedCounty.guid != selectedCounty) {
                this.selectedCounty = this.county(selectedCounty);
                this.selectedOwner = this.character(this.selectedCounty.owningCharacter);
            }
            // Find adjacent counties.
            if (!this.adjacentCountiesToHighlight && this.shouldHighlightAdjacent) {
                let cells = this.diagram.cells.filter(cell => cell.site.data.guid == selectedCounty);
                this.adjacentCountiesToHighlight = this.adjacentCountyIds(selectedCounty);
            }
            // Highlight adjacent counties.
            if (this.adjacentCountiesToHighlight && this.shouldHighlightAdjacent) {
                this.adjacentCountiesToHighlight.forEach(guid => {
                    let adjacentCounty = this.county(guid);
                    let adjacentCountyColor = this.getCharacterHouseColor(this.selectedOwner).clone();
                    if (adjacentCounty.isVisible) {
                        if (adjacentCounty.owningCharacter) {
                            adjacentCountyColor.saturation = (Math.sin(event.time * 4) * 0.4) + 0.6;
                        } else {
                            adjacentCountyColor.brightness = (Math.sin(event.time * 4) * 0.2) + 0.6;
                        }
                    } else {
                        adjacentCountyColor.brightness = Math.min(Math.abs(Math.sin(event.time * 4)), 0.3);
                    }
                    this.setCountyColor(guid, adjacentCountyColor);
                });
            }
            this.selectedCountyId = selectedCounty;
            // Highlight selected county.
            let countyColor = this.getCountyHouseColor(this.selectedCounty).clone();
            if (this.selectedCounty.isVisible) {
                countyColor.brightness = (Math.sin(event.time * 2) * 0.2) + 0.75;
            } else {
                countyColor.brightness = (Math.sin(event.time * 2) * 0.2) + 0.3;
            }
            // if (this.cellsByCountyGuid[selectedCounty]) {
            //     for (let i = 0; i < this.cellsByCountyGuid[selectedCounty].length; i++) {
            //         this.cellsByCountyGuid[selectedCounty][i].site.data.polygonGroup.style.fillColor.brightness = Math.sin(event.time);
            //     }
            // }
            // this.cellsByCountyGuid[guid].forEach(cell => {
                // cell.site.data.polygon.fillColor = countyColor;
                //cell.site.data.polygon.strokeColor = color;
            // });
            this.setCountyColor(selectedCounty, countyColor);
        } else {
            this.selectedCountyId = null;
            this.selectedCounty = null;
            this.selectedOwner = null;
        }
    }

    highlightAdjacent( countyId ) {
        if (!countyId) {
            if (this.adjacentHighlights) {
                this.adjacentHighlights.remove();
                this.adjacentHighlights = null;
                this.overlayProject.view.onFrame = null;
            }
            return;
        }

        let adjacent = this.adjacentCountyIds(countyId);
        let polys = adjacent.map(a => this.countyCellMap[a].polygon.clone());
        if (this.adjacentHighlights) {
            this.adjacentHighlights.remove();
        }
        this.adjacentHighlights = new paper.Group();
        this.adjacentHighlights.addChildren(polys);
        this.adjacentHighlights.style.fillColor = 'blue';
        this.adjacentHighlights.style.strokeColor = 'blue';
        this.adjacentHighlights.opacity = 0.5;
        this.overlayProject.activeLayer.addChild(this.adjacentHighlights);
        this.overlayProject.view.onFrame = ( event ) => {
            this.adjacentHighlights.opacity = (Math.sin(event.time * 3) * 0.3) + 0.65;
        };
    }

    onMouseMove( event ) {
        // Change the color of the mouseovered cell.
        let site = this.diagram.find(event.point.x, event.point.y);
        if (site && site.data.guid != this.selectedCountyId) {
            let county = this.county(site.data.guid);
            if (!county.impassable) {
                let poly = this.countyCellMap[site.data.guid].polygon.clone();
                poly.style.fillColor = 'lightgreen';
                poly.style.strokeColor = 'lightgreen';
                poly.opacity = 0.4;
                if (this.hoverCounty) {
                    this.hoverCounty.remove();
                }
                this.hoverCounty = poly;
                this.overlayProject.activeLayer.addChild(poly);
            }
        } else if (this.hoverCounty) {
            this.hoverCounty.remove();
            this.hoverCounty = null;
        }
    }

    onMouseDrag( event ) {
        var trueDelta = event.point.subtract(this.dragStart);
        paper.view.translate(trueDelta);
        this.overlayProject.view.center = paper.view.center;
        this.mouseDidMove = true;
        this.onMouseMove(event);
    }

    onMouseDown( event ) {
        this.dragStart = event.point;
        this.mouseDidMove = false;
    }

    onMouseUp( event ) {
        if (!this.mouseDidMove) {
            let site = this.diagram.find(event.point.x, event.point.y, 100 * this.mapScale);
            if (site) {
                let county = this.county(site.data.guid);
                if (!county.impassable) {
                    this.selectedCountyId = site.data.guid;
                    this.eventHandler.dispatch('SELECT_COUNTY', {countyId: site.data.guid});

                    if (!this.adjacentHighlights) {
                        this.overlayProject.activeLayer.removeChildren();
                        this.overlayProject.activeLayer.opacity = 0.5;
                        this.overlayProject.activeLayer.blendMode = 'overlay';
                        let poly = this.countyCellMap[site.data.guid].polygon.clone();
                        poly.style.fillColor = 'lightgreen';
                        poly.style.strokeColor = 'lightgreen';
                        this.overlayProject.activeLayer.addChild(poly);
                        this.overlayProject.view.onFrame = (event) => {
                            poly.opacity = (Math.sin(event.time * 2) * 0.3) + 0.5;
                        };
                    }
                }
            }
        }
        this.onMouseMove(event);
    }

    onMouseWheel( event ) {
        var delta = event.deltaY * event.deltaFactor * -0.00005;
        let min = 0.75;
        let max = 3;
        paper.view.zoom = Math.min(Math.max(paper.view.zoom + delta, min), max);
        this.overlayProject.view.zoom = paper.view.zoom;
    }
}

